import random

# Function to solve the cut-rod problem using dynamic programming
def cut_rod(p, n):
    r = [0] * (n + 1)
    r[0] = 0
    for i in range(1, n + 1):
        max_val = float('-inf')
        for j in range(1, i + 1):
            max_val = max(max_val, p[j-1] + r[i - j])
        r[i] = max_val
    return r[n]

# Generate a random price array p of length 10
random.seed(42)  # for reproducibility
n = 5  # length of the rod
p_random = [random.randint(1, 30) for _ in range(n)]

# Calculate the maximum obtainable value using the random price array
max_value = cut_rod(p_random, n)

print(p_random)
print(max_value)
