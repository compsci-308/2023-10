#!/usr/bin/env bash

find . -name "lecture-*.pdf" -exec cp {} ~/now/compsci308/slides \;
