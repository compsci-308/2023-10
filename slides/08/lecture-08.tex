\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Dynamic Programming (Part 2)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ita} 15.2 Matrix-chain multiplication}

\begin{frame}
    \frametitle{Review: Matrix Multiplication}

    Let $A$ and $B$ be two matrices. How to compute $A B$?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{matrix-multiplication.png}
        \caption{Matrix Multiplication}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Row and Column rules}

    Given matrices \( A \) of size \( m \times n \) and \( B \) of size \( n \times p \),
    \[
    A = \begin{bmatrix}
        a_{11} & a_{12} & \cdots & a_{1n} \\
        a_{21} & a_{22} & \cdots & a_{2n} \\
        \vdots & \vdots & \ddots & \vdots \\
        a_{m1} & a_{m2} & \cdots & a_{mn}
    \end{bmatrix}
    ,
    \qquad
    B = \begin{bmatrix}
        b_{11} & b_{12} & \cdots & b_{1p} \\
        b_{21} & b_{22} & \cdots & b_{2p} \\
        \vdots & \vdots & \ddots & \vdots \\
        b_{n1} & b_{n2} & \cdots & b_{np}
    \end{bmatrix}
    \]
    The product \( C = A \times B \) is a matrix of size \( m \times p \) 
    \begin{equation*}
        C = \begin{bmatrix}
            c_{11} & c_{12} & \cdots & c_{1p} \\
            c_{21} & c_{22} & \cdots & c_{2p} \\
            \vdots & \vdots & \ddots & \vdots \\
            c_{m1} & c_{m2} & \cdots & c_{mp}
        \end{bmatrix}
        \end{equation*}
    where
    \begin{equation*}
    c_{ij} = \sum_{k=1}^{n} a_{ik} \times b_{kj}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Quick Test}
    
    \cake{} What is
    \begin{equation*}
        \begin{bmatrix}
            1 & 2 \\
            3 & 4 \\
        \end{bmatrix}
        \begin{bmatrix}
            0 & 2 & 0 \\
            1 & 0 & 3 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The Pseudo Code}

    \cake{} If $A$ is of size $m \times n$ and $B$ is of size $n \times p$,
    how many scalar \emoji{multiply} do we need to compute $A B$?
    
    \begin{algorithmic}
        \Procedure{Matrix-Multiply}{$A, B$}
        \State let $C$ be a new $A.\text{rows} \times B.\text{columns}$ matrix
        \For{$i = 1$ to $A.\text{rows}$}
        \For{$j = 1$ to $B.\text{columns}$}
        \State $C_{ij} = 0$
        \For{$k = 1$ to $A.\text{columns}$}
        \State $C_{ij} = C_{ij} + A_{ik} \times B_{kj}$
        \EndFor
        \EndFor
        \EndFor
        \State \Return $C$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Associative Law of Multiplication}

    \cake{} Let
    \begin{equation*}
        A
        =
        \left[
            \begin{array}{cc}
                3 & 0 \\
                5 & 4 \\
            \end{array}
        \right]
        ,
        \qquad
        B
        =
        \left[
            \begin{array}{c}
                0 \\
                2 \\
            \end{array}
        \right]
        ,
        \qquad
        C
        =
        \left[
            \begin{array}{cc}
                2 & 4 \\
            \end{array}
        \right]
    \end{equation*}
    
    \only<1>
    {
        What are $A B$ and $(A B) C$?
    }

    \only<2>
    {
        What are $B C$ and $A (B C)$?
    }

    \only<3>{
        Given any matrices $A, B, C$, we have
        \begin{equation*}
            A (B C) = (A B) C
        \end{equation*}
        This is called the \alert{associative law of matrix multiplication}.
    }
\end{frame}

\begin{frame}
    \frametitle{Fully Parenthesized Product}

    When computing a product like $A_1 A_2 \dots A_n$,
    we can use parentheses to indicate the order of \temoji{multiply}.

    For example         
    \only<1>{
        \begin{equation*}
            \begin{aligned}
                A_1 A_2 A_3
                =
                &(A_1(A_2 A_3)) \\
                &((A_1A_2)A_3).
            \end{aligned}
        \end{equation*}
    }
    \only<2>{
        \begin{equation*}
            \begin{aligned}
                A_1 A_2 A_3 A_4 =
                &(A_1(A_2(A_3A_4))) \\
                &(A_1((A_2A_3)A_4)) \\
                &((A_1A_2)(A_3A_4)) \\
                &((A_1(A_2A_3))A_4) \\
                &(((A_1A_2)A_3)A_4).
            \end{aligned}
        \end{equation*}
    }
\end{frame}

\begin{frame}
    \frametitle{Why Does Parentheses Matter?}

    Consider three matrices $A_1, A_2, A_3$ of sizes
    $10 \times 100, 100 \times 5, 5 \times 50$.

    \cake{} How many scalar \emoji{multiply} do we need to compute
    \begin{equation*}
        (A_1(A_2 A_3))
    \end{equation*}

    \pause{}

    \cake{} What about
    \begin{equation*}
        ((A_1A_2)A_3)
    \end{equation*}

    \pause{}

    \cake{} Can you think of a case when any parenthesization of $A_1, A_2, \dots A_n$
    uses the same amount of \temoji{multiply}?
\end{frame}

\begin{frame}
    \frametitle{Exhaustive Search}
    
    Let $P(n)$ be the number of ways to fully parenthesize $A_1 A_2 \dots A_n$.
    Then
    \begin{equation*}
        P(n) = 
        \begin{cases}
            1 & \text{if } n = 1, \\
            \sum_{k=1}^{n-1} P(k)P(n-k) & \text{if } n > 1.
        \end{cases}
    \end{equation*}

    \pause{}

    In discrete mathematics, we have seen that
    \begin{equation}
        \label{eq:catalan}
        P(n) 
        = 
        \frac{1}{n} \binom{2n-2}{n-1}
        =
        \Theta\left(\frac{4^n}{n^{3/2}}\right)
    \end{equation}
    There simply too many ways to parenthesize to check exhaustively.

    \cake{} What is the number $P(n)$ called in discrete math?

    \puzzle{} Try to prove \eqref{eq:catalan}.
\end{frame}

\begin{frame}
    \frametitle{The Structure of an Optimal Parenthesization}
    
    \begin{block}{\emoji{monocle-face} A key observation}
        If the optimal way to parenthesize $A_{i} A_{i+1} \dots
        A_{j}$ is to ``break'' the chain into two parts between $A_k$ and $A_{k+1}$, i.e.,
        \begin{equation*}
            ((A_{i} \dots A_k) (A_{k+1} \dots A_j))
        \end{equation*}
        then the parenthesizations of both
        $A_{i} \dots A_{k}$ 
        and
        $A_{k+1} \dots A_{j}$ 
        are both optimal.
    \end{block}

    \cake{} Why is this true?
\end{frame}

\begin{frame}
    \frametitle{A Recursive Solution}
    
    Let $m[i, j]$ be the minimum number of scalar \emoji{multiply} needed to
    compute $A_{i} A_{i+1} \dots A_{j}$.

    Denote the size of $A_{i}$ by $p_{i-1}p_{i}$.

    Then by the previous observation
    \begin{equation*}
        m[i, j]
        =
        \begin{cases}
            0 & \text{if } i = j, \\
            \min_{i \le k < j} \{ m[i, k] + m[k+1, j] + p_{i-1} p_{k} p_{j} \} & \text{if } i < j.
        \end{cases}
    \end{equation*}

    \cake{} Why is $m[i, j] = 0$ when $i = j$?
\end{frame}

\begin{frame}
    \frametitle{Dynamic Programming To Help}
    
    To compute $m[i,j]$, we can fill up the
    optimal cost table and split table bottom as shown below.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.7\textwidth]{matrix-chain-cost-table.png}
        \includegraphics<2>[width=0.7\textwidth]{matrix-chain-split-table.png}
        \caption{The matrix chain \only<1>{cost}\only<2>{split} table for
        matrices with $\langle p_0, \dots p_6 \rangle = \langle 30, 35, 15, 5, 10, 20, 25\rangle$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Pseudo Code}
    
    \begin{algorithmic}
    \Procedure{MATRIX-CHAIN-ORDER}{$p$}
        \State Initialize $m$ and $s$ \Comment{\cake{} What should the initial
        values be?}
        \For{$l = 2$ to $n$} \Comment{$l$ is the chain length}
            \For{$i = 1$ to $n - l + 1$}
                \State $j \gets i + l - 1$
                \State $m[i, j] \gets \infty$
                \For{$k = i$ to $j - 1$}
                    \State $q \gets m[i, k] + m[k + 1, j] + p[i-1] \times p[k] \times p[j]$
                    \If{$q < m[i, j]$}
                        \State $m[i, j] \gets q$
                        \State $s[i, j] \gets k$
                    \EndIf
                \EndFor
            \EndFor
        \EndFor
        \State \Return $m$ and $s$
    \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Construct the Optimal Parenthesization}
    
    \begin{algorithmic}
    \Procedure{Print-Optimal-Parens}{$s, i, j$}
    \If{$i = j$}
        \State \textbf{print} “$A_i$”
    \Else
        \State \textbf{print} “(”
        \State \Call{Print-Optimal-Parens}{$s, i, s[i, j]$}
        \State \Call{Print-Optimal-Parens}{$s, s[i, j] + 1, j$}
        \State \textbf{print} “)”
    \EndIf
    \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Consider matrices $A_1, A_2, A_3$ whose sizes are given by
    \begin{equation*}
        \langle 4, 5, 7, 3 \rangle
    \end{equation*}

    What is $m(1, 3)$ and what is the optimal parenthesization?
\end{frame}

\section{\acs{ita} 15.3 Elements of dynamic programming}

\begin{frame}{Optimal Structure}

    A pattern in problems suitable for dynamic programming is:
    \begin{enumerate}[<+->]
        \item A solution entails making a choice, e.g., an initial cut in a rod or a matrix split index. This choice results in subproblems.
        \item Assume that you already have the choice leading to an optimal solution.
        \item Based on this choice, identify the subproblems and characterize
            the resulting space.
        \item Prove subproblem solutions within an optimal solution must be
            optimal by contradiction ---
            If a subproblem's solution isn't optimal, by replacing it with
            the optimal one, a better solution to the original problem is
            obtained.
    \end{enumerate}
\end{frame}

\begin{frame}{Subproblem Space Characterization}
        Aim for a simple space of subproblems, then expand if needed.

        \only<1>{Rod-cutting: The optimal cutting of a rod with length \(i\).}

        \only<2>{Matrix-chain multiplication: Optimal parenthesization in \(A_i \dots A_k\) 
        and \(A_{k+1} \dots A_j\) for $i \le k \le j-1$.}
\end{frame}

\begin{frame}
    \frametitle{Look Out for Pitfalls}
    In the directed graph below, the longest simple path from \(q\) to \(t\) is
    $q \to r \to t$.

    \cake{} What is the longest simple path from \(q\) to \(r\)
    and from \(r\) to \(t\)?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=1.5cm,
            thick,main node/.style={circle,draw,fill=LightGray}]

            \node[main node] (q) {q};
            \node[main node] (r) [right of=q] {r};
            \node[main node] (s) [below of=q] {s};
            \node[main node] (t) [right of=s] {t};

            \path
                (q) edge[bend right] node {} (r)
                edge[bend left] node {} (s)
                (r) edge[bend right] node {} (q)
                edge[bend right] node {} (t)
                (s) edge[bend left] node {} (q)
                edge[bend right] node {} (t)
                (t) edge[bend right] node {} (r)
                edge[bend right] node {} (s);
        \end{tikzpicture}
        \caption{A directed graph}
    \end{figure}

    \bomb{} Some times the optimal solution does not contain optimal solution
    for sub-problems.

    \cake{} Can you think another problem like this?
\end{frame}

\begin{frame}
    \frametitle{Overlapping Subproblems}

    Another pattern in problems suitable for dynamic programming is there are
    overlapping subproblems.

    For example, in the matrix-chain multiplication problem, there are
    overlapping subproblems as shown in the recursion tree below.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{matrix-multiplication-tree.png}
        \caption{The recursion tree for the matrix chain multiplication problem}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Compute the Optimal Chain Naively}

    \begin{algorithmic}[1]
        \Procedure{RECURSIVE-MATRIX-CHAIN}{$p, i, j$}
        \If{$i = j$}
        \State \Return 0
        \EndIf
        \State $min \gets \infty$
        \For{$k = i$ to $j-1$}
        \State $\begin{aligned}c \gets & \Call{RECURSIVE-MATRIX-CHAIN}{p, i, k} \\
            & + \Call{RECURSIVE-MATRIX-CHAIN}{p, k+1, j} \\
            & + p[i-1] \times p[k] \times p[j]
        \end{aligned}$
        \If{$c < min$}
        \State $min \gets c$
        \EndIf
        \EndFor
        \State \Return $min$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{The Cost of Being Naive}
    
    Let $T(n)$ be the running time of \texttt{RECURSIVE-MATRIX-CHAIN}. 
    Then we have
    \begin{equation*}
        T(n) \geq 1 + \sum_{k=1}^{n-1} \left( T(k) + T(n-k) + 1 \right) \quad \text{for } n > 1.
    \end{equation*}

    We can show by induction that 
    \begin{equation*}
        T(n) \ge 2^{n-1}.
    \end{equation*}
    \hint{} This is why bottom-up or memoization is necessary.
\end{frame}

\section{A Few More Examples in Dynamic Programming}

\subsection{The Change-making Problem}

\begin{frame}[c]
    \frametitle{Motivating Example: Buying a Magic Mushroom}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Consider wanting to purchase a magic \emoji{mushroom} priced at
            $99¢$.

            \bigskip{}

            However, the vendor only accepts \coin{} in denominations of \(5¢, 10¢, 25¢, 50¢\).

            \bigskip{}

            Determine the fewest number of \coin{} required to make the purchase.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-mushroom.jpg}
                \caption{A magic \emoji{mushroom} purchase}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{The Change-making Problem}
    For a given set of \coin{} denominations \( D = \{d_1, d_2, \ldots, d_n\} \) and a specified amount \( A \):

    Determine the minimum count of coins, from the set, that combine to reach the amount \( A \).

    Let \( c_i \) represent the count of coins of denomination \( d_i \).

    Our objective is to minimize:
    \begin{equation*}
    C(A) = \sum_{i=1}^{n} c_i
    \end{equation*}
    Subject to the constraint:
    \begin{equation*}
    \sum_{i=1}^{n} c_i \cdot d_i = A
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Dynamic Programming Solution}
    
    \begin{algorithmic}
    \Procedure{ChangeMaking}{$D, A$}
        \State Initialize array \( M[0 \ldots A] \) with values set to \(\infty\)
        \State \( M[0] \) = 0

        \For{\(i = 1\) to \(A\)}
            \For{each \(d\) in \(D\)}
                \If{\(i - d \geq 0\) and \(M[i - d] + 1 < M[i]\)}
                    \State \(M[i] = M[i - d] + 1\)
                \EndIf
            \EndFor
        \EndFor

        \State \Return \(M[A]\)
    \EndProcedure
    \end{algorithmic}

    \cake{} What is the output when $D = \{5, 10, 25, 50\}$ and $A = 99$?
\end{frame}

\subsection{Treasure Digging}

\begin{frame}{Treasure Hunt Problem}
    \only<1>{
        \begin{figure}[htpb]
            \centering
            \colorbox{white}{
                \begin{tikzpicture}
                    % Draw the grid with thin gray lines
                    \draw[step=1, gray, thin] (0,0) grid (6,5);

                    % Draw a red path from (0,0) to (6,5)
                    \draw[red, ultra thick] (0,0) -- (0,2) -- (3,2) -- (3,4) -- (6,4) -- (6,5);

                    % Loop to place a random emoji at each other grid point
                    \foreach \i in {0,...,6} {
                        \foreach \j in {0,...,5} {
                            % Skip the points (0,0) and (6,5)
                            % This does not work for texlive 2022
                            %\ifnum\i=0
                            %    \ifnum\j=0
                            %        \continueforeach
                            %    \fi
                            %\fi
                            %\ifnum\i=6
                            %    \ifnum\j=5
                            %        \continueforeach
                            %    \fi
                            %\fi

                            % Generate a random number between 1 and 6
                            \pgfmathtruncatemacro{\myRandom}{random(1,6)}

                            % Select an emoji based on the random number
                            \ifnum\myRandom=1
                                \def\myEmoji{car}
                            \fi
                            \ifnum\myRandom=2
                                \def\myEmoji{tv}
                            \fi
                            \ifnum\myRandom=3
                                \def\myEmoji{coin}
                            \fi
                            \ifnum\myRandom=4
                                \def\myEmoji{moneybag}
                            \fi
                            \ifnum\myRandom=5
                                \def\myEmoji{toilet}
                            \fi
                            \ifnum\myRandom=6
                                \def\myEmoji{carrot}
                            \fi

                            % Place the selected emoji
                            \node at (\i,\j) {\emoji{\myEmoji}};
                        }
                    }

                    % Place emojis at specified coordinates
                    \node at (-0.5,-0.5) {\emoji{robot}};
                    \node at (6.5,5.5) {\emoji{fuelpump}};

                \end{tikzpicture}
            }
            \caption{A treasure hunt on a grid}
        \end{figure}
    }
    \only<2>{
        A \emoji{robot} starts at coordinate $(0,0)$ on a $m \times n$ grid.

        Each grid cell $(i, j)$ holds a treasure with value $t_{ij}$.

        The \emoji{robot} can move either up or right one unit at a time.

        \emoji{dart} Maximize the total treasure collected.

        Let $c_{ij}$ be the maximum treasure collected by the robot starting
        from coordinate $(i,j)$.

        \cake{} Can you find a recursion for $c_{ij}$?
    }
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 15.2.
                \item
                    Section 15.3.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 15.2: 1--6.
                    Exercises 15.3: 1--5.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
