\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../language.tex}

\title{Lecture \lecturenum{} --- NP-Completeness (Part 2)}

\begin{document}

\maketitle

\begin{frame}[standout,c]
    \vspace{1em}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-evalution.jpg}
        \caption*{\emoji{pray}\textcolor{white}{Please use the next 15 minutes
        to write course evaluation.}}
    \end{figure}
\end{frame}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 34.2
                \item
                    Section 34.3 except pp.~1073-1077.
                \item
                    Section 34.4.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 34.2: 1-6, 8-10.
                \item
                    Exercises 34.3: 1-3, 6-7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 34.2 Polynomial-Time Verification}

\subsection{Verification Algorithms}

\begin{frame}
    \frametitle{Verification Algorithms}

    A \alert{verification algorithm} $A$ takes two binary strings $x$ and $y$ as
    input.

    The algorithm $A$ \alert{verifies} $x$ if there exists an $y$ such that
    $A(x,y) = 1$.

    The \alert{language verified} by a verification algorithm $A$ is
    \begin{equation*}
        L = \{x : \exists y \in \{0,1\}^{*}, A(x,y) = 1\}.
    \end{equation*}
    Example: Hamiltonian graphs.
\end{frame}


\subsection{The Complexity Class NP}

\begin{frame}
    \frametitle{Complexity Class NP}

    A language $L$ belongs to NP if and only if
    there exists a polynomial-time algorithm $A$ and a constant $c$ such that
    \begin{equation*}
        \begin{aligned}
            L =
            \{
                x \in \{0,1\}^{*}: 
                &
                \exists y \in \{0,1\}^{*},  \\
                &
                A(x,y) = 1 \wedge \abs{y} = O(\abs{x}^c).
            \}
        \end{aligned}
    \end{equation*}
    In this case, we say $L$ is \alert{verified in polynomial time} by $A$. 

    \pause{}

    Example: Hamiltonian graphs.

    \cake{} If $x$ is a binary string representing a graph and $y$ is a
    certificate for a hamiltonian cycle, then why $\abs{y} = O(\abs{x}^c)$ for
    some constant $c$?
\end{frame}

\begin{frame}
    \frametitle{P is a subset of NP}

    A polynomial time algorithm that solves $L$ can be easily modified to
    verify $L$ in polynomial time.

    Thus $\text{L} \in \text{P}$ implies $\text{L} \in \text{NP}$, i.e.,
    $\text{P} \subseteq \text{NP}$.

    \puzzle{} Can you make the ``easily modified'' part rigorous?
\end{frame}

\begin{frame}
    \frametitle{Complex Class co-NP}

    Let $\text{co-NP} = \{L: \overline{L} \in \text{NP}\}$.

    We know that $\text{P} = \overline{\text{P}}$, 
    which implies $\text{P} \subseteq \text{co-NP}$ (Exercise).

    \emoji{cry} But we do know which picture below is true.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{co-np.png}
        \caption{Four possible pictures.}
    \end{figure}
\end{frame}

\section{\acs{ita} 34.3 NP-completeness and reducibility}

\begin{frame}[c]
    \frametitle{Reducibility}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            A problem $Q$ can be \alert{reduced} to a problem $Q'$
            if an instance of $Q$ can be easily rephrased to an instance $Q'$.

            \bigskip{}

            For example, the
            \emph{good \emoji{student} problem}
            can be reduced to \emph{GPA = 4.0 problem},
            in a hypothetical \emoji{earth-asia}
            where 
            \begin{equation*}
                \text{getting \emoji{100}} = \text{being \emoji{+1}}
            \end{equation*}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-grades.jpg}
                \caption{\emoji{turtle} -- I am fastest runner in class because I got a
                \emoji{100} in calculus.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Polynomial-Time Reducibility}

    We define \alert{polynomial-time reducibility} between two languages \( L_1
    \) and \( L_2 \) as follows:

    \begin{itemize}
        \item \( L_1 \leq_p L_2 \) if a \alert{polynomial-time computable function} \( f \) exists with the property \( f: \{0,1\}^* \rightarrow \{0,1\}^* \).
        \item \( x \in L_1 \) if and only if \( f(x) \in L_2 \).
    \end{itemize}

    The function \( f \) is known as the \alert{reduction function}.

    An algorithm \( F \) that computes \( f \) efficiently is termed a \alert{reduction algorithm}.
\end{frame}

\begin{frame}
    \frametitle{Lemma 34.3}
    If $L_{1} \leq_p L_2$, then $L_{2} \in \text{P}$ implies $L_{1} \in \text{P}$.

    \hint{} So to solve the decision problem of \emph{is $x \in L_{1}$}, we can instead solve
    \emph{is $f(x) \in L_{2}$}.

    Proof: If $A_2$ solves $L_{2}$ in polynomial time, 
    then the algorithm $A_{1}$ in the picture below solves $L_{1}$ in polynomial time.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\textwidth]{reduction-1.png}
        \caption{Algorithm $A_{1}$}
    \end{figure}
\end{frame}

\subsection{NP-Completeness}

\begin{frame}
    \frametitle{Understanding NP-Completeness}

    A language \( L \subseteq \{0, 1\}^* \) is \alert{NP-complete} if:

    \begin{enumerate}
        \item \( L \) is in NP, and
        \item For every \( L' \) in NP, \( L' \leq_p L \).
    \end{enumerate}

    If a language \( L \) satisfies the second condition but not necessarily the
    first, we call \( L \) as \alert{NP-hard}. 

    The class of NP-complete languages is denoted by NPC.
\end{frame}

\begin{frame}
    \frametitle{Theorem 34.4}

    If any problem in NPC is polynomial-time solvable, then \emph{all} problems
    in NP are polynomial-time solvable, i.e., $\text{P} = \text{NP}$.

    If any problem in NP is \emph{not} polynomial-time solvable, then \emph{no}
    problem in NPC is polynomial-time solvable.

    \medskip{}

    \weary{} It sounds easy, but no one has found such problems in the past 50
    years!
\end{frame}

\subsection{Circuit satisfiability}

\begin{frame}
    \frametitle{Boolean Combinational Circuits}

    \alert{Boolean combinational circuits} consist of interconnected
    \alert{boolean combinational elements}. 

    These are circuits with fixed numbers of boolean
    inputs and outputs, executing well-defined functions. 

    A \alert{Boolean input} is either 0 (\texttt{FALSE}) or 1 (\texttt{TRUE}).

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{circuit-adder.png}
        \caption{A circuit for adding two boolean inputs.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Logic Gates}
    These circuits consists of using \alert{logic gates}:
    \begin{itemize}
        \item \alert{NOT gate}: Flips the binary input \( x \).
        \item \alert{AND gate}: Outputs 1 if both binary inputs \( x \) and \( y \) are 1.
        \item \alert{OR gate}: Outputs 1 if either binary input \( x \) or \( y \) is 1.
    \end{itemize}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\textwidth]{circuit-gates.png}
        \caption{Logic Gates}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Circuit Satisfiability}

    The \alert{Circuit Satisfiability Problem} is a decision problem which asks
    if there exist inputs which make a boolean combinatorial circuit \( C \)
    output $1$.

    We define the formal language:
    \begin{equation*}
        \begin{aligned}
            &
            \text{CIRCUIT-SAT}
            =
            \{\langle C \rangle :  \\
            &
            \qquad
            C \text{ is a satisfiable boolean combinatorial circuit}\}
        \end{aligned}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{circuit-assign.png}
        \caption{A satisfiable circuit.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 34.7}

    It follows from the following two lemma that CIRCUIT-SAT is NP-complete:

    \begin{block}{Lemma 34.5}
        The circuit-satisfiability problem is in NP.
    \end{block}

    \begin{block}{Lemma 34.6}
        The circuit-satisfiability problem is NP-Hard.
    \end{block}

    \smiling{} The proofs of the two lemma is not required.

    \emoji{seedling} This is the \emph{first} NP-complete problem.
\end{frame}

\section{\acs{ita} 34.4 NP-Completeness Proofs}

\subsection{A Recipe for Proving NP-Completeness}

\begin{frame}
    \frametitle{Lemma 34.8}

    If $L$ is language in NP such that there exists $L' \in \text{NPC}$
    such that $L' \leq_p L$, then $L$ is NP-complete.

    \pause{}

    \hint{} This allows us to prove many many many problems are NP-complete with
    the following recipe:
    \begin{enumerate}
        \item Prove that $L \in \text{NP}$.
        \item Select problem $L' \in \text{NPC}$.
        \item Find $f: L' \mapsto L$.
        \item Show that $x \in L'$ if and only if $f(x) \in L$.
        \item Prove that $f$ is polynomial-time computable.
    \end{enumerate}

\end{frame}

\begin{frame}[c]{Karp's 21 NP-complete problems}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            In 1972, Richard Karp used this method 
            to show that 21 problems combinatorial and graph theory
            are NP-complete.

            \medskip{}

            These are known as \alert{Karp's 21 NP-complete problems}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Richard-Karp.jpg}
                \caption{Richard Karp (1935-). \emoji{us} computer scientist.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Formula Satisfiability}


\begin{frame}
    \frametitle{Boolean Formula}

    A boolean formula $\phi$, is described by:

    \begin{itemize}
        \item Variables: A set of boolean variables, $\{x_1, x_2, \ldots, x_n\}$.
        \item Connectives: Boolean functions, such as $\land$ (AND), $\lor$
            (OR), $\neg$ (NOT), $\rightarrow$ (implies), $\iff$ (iff).
        \item Parentheses: Used to group expressions, with no redundant pairs.
    \end{itemize}

    Examples include:
    \begin{itemize}
        \item $x_1 \land x_2$
        \item $(x_1 \imp x_2) \vee \neg (\neg x_1 \iff x_3)$
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Boolean Formulas and SAT Problem}

A \alert{truth assignment} is a set of values for the variables of $\phi$. 

A \alert{satisfying assignment} is a truth assignment that makes the
formula evaluate to 1.

\alert{Satisfiability (SAT)} is the decision problem to determine 
if a boolean formula has a \emph{satisfying assignment}. 

In formal terms, 
\begin{equation*} 
    \text{SAT} = \{ \phi : \phi \text{ is a satisfiable boolean formula} \}. 
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Example}
For instance, consider the formula:
\begin{equation*} 
    \phi = ((x_1 \lor \neg x_2) \lor (\neg (x_1 \land x_3) \lor x_4)) \land \neg x_2 
\end{equation*}

The satisfying assignment for this formula is:
\begin{equation*} 
    x_1 = 0, x_2 = 0, x_3 = 1, x_4 = 1 
\end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Theorem 34.9}

    Satisfiability of boolean formulas is NP-complete.

    Proof:
    \only<1>{%
         Step 1 --- Prove that $\text{SAT} \in \text{NP}$.
    }%
    \only<2>{%
        Step 2 --- Prove that $\text{SAT}$ is NP-hard by showing that
        $\text{CIRCUIT-SAT} \leq_p \text{SAT}$.

        It can be done by giving each wire a variable and
        express each gate with
        an $\iff$ formula, which we call a \alert{clause}.

        \cake{} What clauses do the following circuit reduce have?
    }%
    \only<3-4>{%
        A formula can be produced by taking conjunction (AND) 
        of the output variable and the clauses.

        For the following circuit, this is
        \begin{equation*}
            \phi = 
                x_{10} 
                \land 
                (x_{10} \iff x_{8} \wedge x_{9} \wedge x_{7})
                \land 
                (x_{8} \iff x_{5} \vee x_{6}) \land \dots
        \end{equation*}
    }%
    \only<2-3>{%
        \vspace{-1em}

        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.6\textwidth]{sat-reduction.png}
        \end{figure}
    }%
    \only<4>{%
        Constructing $\phi$ takes polynomial time.

        The circuit is satisfiable if and only if $\phi$ is satisfiable.

        Thus we have $\text{CIRCUIT-SAT} \leq_p \text{SAT}$.
        \emoji{tada}
    }
\end{frame}

\begin{frame}
    \frametitle{\zany{} Applications of SAT}

    Many real-world problems can be efficiently formulated as SAT (Boolean
    Satisfiability) problems:
    \begin{enumerate}
        \item Sudoku puzzles solving
        \item Formal verification in hardware and software design
        \item Optimising scheduling tasks
        \item Addressing certain combinatorial challenges in mathematics
    \end{enumerate}
    
    Modern SAT solvers (software) are remarkably efficient in tackling these
    challenges.

    \emoji{trophy} There is an annual SAT solver competition:
    \url{http://satcompetition.org/}
\end{frame}

\subsection{3-CNF Satisfiability}

\begin{frame}
    \frametitle{3-CNF}

    A \alert{literal} is either a variable or its negation.

    A formula in \alert{conjunctive normal form (CNF)} is an AND of
    \alert{clauses}, each being the OR of one or more literals.

    A formula is in \alert{3-conjunctive normal form}, or \alert{3-CNF}, if each
    clause has \textit{exactly three distinct literals}.

    \pause{}

    For example, the formula
    \begin{equation*}
        (x_1 \lor \neg x_1 \lor \neg x_2) 
        \land 
        (x_3 \lor x_2 \lor x_4) 
        \land 
        (\neg x_1 \lor \neg x_3 \lor \neg x_4)
    \end{equation*}
    is in 3-CNF.
\end{frame}

\begin{frame}
    \frametitle{3-CNF Satisfiability Problem}

    The 3-CNF-SAT is a decision problem which asks if a formula in 3-CNF
    is satisfiable.

    Consider
    \begin{equation*}
        (x_1 \lor x_2 \lor x_3) \land (\neg x_1 \lor \neg x_2 \lor x_4) \land (\neg x_3 \lor \neg x_4 \lor x_1)
    \end{equation*}
    and
    \begin{equation*}
        (x_1 \lor \neg x_2 \lor x_3) \land (x_2 \lor \neg x_3 \lor x_4) \land (\neg x_1 \lor x_3 \lor \neg x_4)
    \end{equation*}
    \cake{} Which one is satisfiable and which one is not?

\end{frame}

\begin{frame}
    \frametitle{Theorem 34.10}

    Satisfiability of boolean formulas in 3-conjunctive normal form is NP-complete.

    Proof: 
    \only<1>{%
        We have seen that $\text{SAT} \in \text{NP}$, so $\text{3-CNF-SAT} \in \text{NP}$.

        So we only need to show that
        \begin{equation*}
            \text{SAT} \leq_p \text{3-CNF-SAT}
        \end{equation*}
        Given a formula like
        \begin{equation*}
            \phi = \left( (x_1 \rightarrow x_2) \vee \left( \neg(x_1 \leftrightarrow x_3) \vee x_4 \right) \right) \wedge \neg x_2.
        \end{equation*}
        we need to convert it into an equivalent 3-CNF formula.
    }%
    \only<2>{%
        We start by convert 
        \begin{equation*}
            \phi = \left( (x_1 \rightarrow x_2) \vee \left( \neg(x_1 \leftrightarrow x_3) \vee x_4 \right) \right) \wedge \neg x_2.
        \end{equation*}
        into a tree as follows:
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.4\textwidth]{sat-tree.png}
        \end{figure}
    }%
    \only<3>{%
        The next step is to convert the tree into a CNF formula:
        \vspace{-1em}
        \begin{columns}
            \begin{column}{0.5\textwidth}
                \begin{equation*}
                    \begin{aligned}
                        \phi' 
                        & = y_1 \\
                        & \land (y_1 \leftrightarrow (y_2 \land \neg x_2)) \\
                        & \land (y_2 \leftrightarrow (y_3 \lor y_4)) \\
                        & \land (y_3 \leftrightarrow (x_1 \rightarrow x_2)) \\
                        & \land (y_4 \leftrightarrow \neg y_5) \\
                        & \land (y_5 \leftrightarrow (y_6 \lor x_4)) \\
                        & \land (y_6 \leftrightarrow (\neg x_1 \leftrightarrow x_3))
                    \end{aligned}
                \end{equation*}       
                This is already a conjunction of clauses.
            \end{column}
            \begin{column}{0.5\textwidth}
                \begin{figure}[htpb]
                    \centering
                    \includegraphics[width=\textwidth]{sat-tree.png}
                \end{figure}
            \end{column}
        \end{columns}
    }%
    \only<4>{%
        The final step is to convert clauses to a 3-CNF formula.

        For example
        \begin{equation*}
            y_1 \leftrightarrow (y_2 \land \neg x_2)
        \end{equation*}
        can be converted
        \begin{align*}
            &(\neg y_1 \lor \neg y_2 \lor \neg x_2) \land \\
            &(\neg y_1 \lor y_2 \lor \neg x_2) \land \\
            &(\neg y_1 \lor y_2 \lor x_2) \land \\
            &(y_1 \lor \neg y_2 \lor x_2).
        \end{align*}
        See the textbook for more details.
    }%
\end{frame}

\end{document}
