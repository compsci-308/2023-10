\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Shortest Paths (Part 2)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 24.3.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 24.3: 1--7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 24.3 Dijkstra's algorithm}

\begin{frame}{Dijkstra's Algorithm Overview}
    Dijkstra's algorithm solves the single-source shortest-paths problem on a
    weighted, directed graph \( G = (V, E) \) with \alert{nonnegative}  edge
    weights. 

    \pause{}

    Dijkstra's algorithm can be more
    efficient than the Bellman-Ford algorithm with good implementation.

    \pause{}

    The algorithm constructs a set \( S \) of vertices, where each vertex's
    shortest path from the source \( s \) is known. 

    \pause{}

    It repeatedly selects the vertex \( u \in V - S \) with the smallest distance
    estimate, adds \( u \) to \( S \), and relaxes all edges leaving \( u \). 
\end{frame}

\begin{frame}
    \frametitle{Review: Min-Priority Queue}

    A \alert{min-priority}  queue is a data structure that
    provides the following operations: 
    \begin{itemize}
        \item $\textsc{Build-Queue}(V)$: Build a queue. Complexity: \( O(\abs{V}) \).
        \item $\textsc{Extract-Min}(Q)$: Removes and returns the element with
            the lowest \texttt{key}. 
            Complexity: \( O(\log \abs{V}) \).
        \item $\textsc{Decrease-Key}(Q, u, \texttt{newKey})$: Reduce the
            \texttt{key} of $u$ to \texttt{newKey}.  
            Complexity: \( O(\log \abs{V}) \).
    \end{itemize}

    The above time complexity are for a binary heap implementation
    (section 6.5).

    A more efficient implementation is by \alert{Fibonacci heap} (chapter 19).
\end{frame}

\begin{frame}
    \frametitle{Pseudocode}
    \cake{} What is the \emoji{stopwatch} complexity of Dijkstra's algorithm?

    \begin{algorithmic}
    \Procedure{Dijkstra}{$G, w, s$}
        \State \Call{Initialize-Single-Source}{$G, s$}
        \State $S \gets \emptyset$
        \State $Q \gets G.V$
        \While{$Q \neq \emptyset$}
            \State $u \gets \Call{Extract-Min}{Q}$
            \State $S \gets S \cup \{u\}$
            \For{\textbf{each} vertex $v \in G.Adj[u]$}
                \State \Call{Relax}{$u, v, w$}
            \EndFor
        \EndWhile
    \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Example Run}
    In the example, if $(u,v)$ is shaded, then $v.\pi = u$.

    \begin{figure}[htpb]
        \centering
        \begin{overlayarea}{0.7\textwidth}{4cm} % Adjust the height to fit your images
            \foreach \n in {1,...,6}{%
                \only<\n>{%
                    \pgfmathtruncatemacro{\imagenumber}{\n - 1} % subtract 1 for the actual image number
                    \includegraphics[width=\textwidth]{dijkastra-tile-0\imagenumber.png}
                }
            }
        \end{overlayarea}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 24.6 --- Correctness of Dijkstra's algorithm}

    Dijkstra's algorithm, run on a weighted, 
    directed graph $G = (V,E)$ with
    non-negative weight function $w$ and source $s$, 
    terminates with $v.d = \delta(s, v)$ for all $v \in V$.

    \hint{} Sometimes being greedy is a good idea.
\end{frame}

\begin{frame}
    \frametitle{Proof of Theorem 24.6}

    We prove by induction that ---

    \begin{tcolorbox}
        At the start of each iteration of the while loop in lines 5--11,
        $v.d = \delta(s, v)$ for all $v \in S$.
    \end{tcolorbox}

    \cake{} Base Case: Why is this true for the first iteration?

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{algorithmic}[1]
                \footnotesize
                \Procedure{Dijkstra}{$G, w, s$}
                    \State \Call{Initialize-Single-Source}{$G, s$}
                    \State $S \gets \emptyset$
                    \State $Q \gets G.V$
                    \algstore{bkmark}
            \end{algorithmic}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{algorithmic}[2]
                \footnotesize
                \algrestore{bkmark}
                    \While{$Q \neq \emptyset$}
                        \State $u \gets \Call{Extract-Min}{Q}$
                        \State $S \gets S \cup \{u\}$
                        \For{each $v \in G.Adj[u]$}
                            \State \Call{Relax}{$u, v, w$}
                        \EndFor
                    \EndWhile
                \EndProcedure
            \end{algorithmic}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Proof of Theorem 24.6: Induction Step}

    \only<1>{
        Now assume the statement holds until a vertex
        $u$ becomes the first vertex with $u.d \ne \delta(s, u)$ when
        it is added to $S$.

        Let $y$ be the first vertex outside $S$ on the shortest path from $s$ to $u$.
    }
    \only<1-3>{
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.4\textwidth]{dijkastra-proof.png}
        \end{figure}
    }
    \only<2>{
        We will show that
        \begin{equation*}
            y.d = \delta(s, y) \le \delta(s, u) \le u.d
        \end{equation*}
        Since $u$ comes out of $Q$ before $y$, we have $y.d \ge u.d$.

        \cake{} Why is this a contradiction?

        \cake{} Where do we use that the weights are non-negative?
    }
    \only<3>{
        It remains to show that $y.d = \delta(s,y)$.
        For this we use
        \begin{tcolorbox}[title={Convergence property (Lemma 24.14)}]
            If \( s \rightsquigarrow u \rightarrow v \) is a shortest path in \(
            G \) for some \( u, v \in V \), and if \( u.d = \delta(s, u) \) at
            any time prior to relaxing edge \( (u, v) \), then \( v.d =
            \delta(s, v) \) at all times afterwards.
        \end{tcolorbox}
    }
\end{frame}

\begin{frame}
    \frametitle{\tps{} Exercise 24.3-3}

    Suppose we modify Dijkstra's as below.
    Is it still correct?

    \begin{algorithmic}
    \Procedure{Dijkstra}{$G, w, s$}
        \State \Call{Initialize-Single-Source}{$G, s$}
        \State $S \gets \emptyset$
        \State $Q \gets G.V$
        \While{$\abs{Q} > 1$} \Comment{Was $Q \neq \emptyset$}
            \State $u \gets \Call{Extract-Min}{Q}$
            \State $S \gets S \cup \{u\}$
            \For{\textbf{each} vertex $v \in G.Adj[u]$}
                \State \Call{Relax}{$u, v, w$}
            \EndFor
        \EndWhile
    \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}[c]
    \frametitle{Edsger Wybe Dijkstra}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Dijkstra was \emoji{netherlands} \emoji{computer} scientist.

            \bigskip{}
            
            In 1968, Dijkstra published a letter titled \emph{\texttt{GOTO} Statement
            Considered Harmful}. 
            
            \bigskip{}

            This letter sparked a major debate and led to a shift towards
            structured programming practices.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Edsger-Wybe-Dijkstra.jpg}
                \caption{Edsger Wybe Dijkstra (1930--2002)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 24.2 Single-source Shortest Paths in Directed Acyclic Graphs}

\begin{frame}
    \frametitle{Shortest Paths in a \ac{dag}}
    Shortest paths in a \ac{dag} are computed efficiently by
    leveraging topological sorting:

    \begin{itemize}
        \item Topological sorting of the vertices imposes a linear ordering.
            (Section 22.4)
        \item For any two vertices \( u \) and \( v \), if \( u \rightarrow v
            \), \( u \) precedes \( v \) in this order.
        \item A single pass over the sorted vertices is made, relaxing outgoing
            edges from each vertex.
    \end{itemize}

    \cake{} Why is the shortest path from $u$ to $v$ always well-defined in
    \ac{dag} if $u$ can reach $v$?
\end{frame}

\begin{frame}
    \frametitle{Pseudocode}

    \cake{} What is \emoji{stopwatch} complexity?
    
    \begin{algorithmic}[1]
    \Procedure{Dag-Shortest-Paths}{$G, w, s$}
        \State topologically sort the vertices of $G$
        \State \Call{Initialize-Single-Source}{$G, s$}
        \For{each vertex $u$, taken in topologically sorted order}
            \For{each vertex $v \in G.\text{Adj}[u]$}
                \State \Call{Relax}{$u, v, w$}
            \EndFor
        \EndFor
    \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Example Run}
    In the example, if $(u,v)$ is shaded, then $v.\pi = u$.

    \begin{figure}[htpb]
        \centering
        \begin{overlayarea}{0.8\textwidth}{4cm} % Adjust the height to fit your images
            \foreach \n in {1,...,6}{%
                \only<\n>{%
                    \pgfmathtruncatemacro{\imagenumber}{\n - 1} % subtract 1 for the actual image number
                    \includegraphics[width=\textwidth]{dag-shortest-path-0\imagenumber.png}
                }
            }
        \end{overlayarea}
    \end{figure}
\end{frame}


\begin{frame}
    \frametitle{Theorem 24.5}

    Given a \ac{dag} \( G = (V, E) \) and a source vertex \( s \), 
    then at the termination of \textsc{Dag-Shortest-Paths}, 
    \( v.d = \delta(s, v) \) for all vertices \( v \in V \).
\end{frame}

\begin{frame}
    \frametitle{Theorem 24.5 --- Proof}

    The proof uses the following property:

    \begin{tcolorbox}[title={Path-relaxation property (Lemma 24.15)}]
    If
    \begin{equation*}
        p = \langle v_0, v_1, \ldots, v_k \rangle
    \end{equation*}
        and we relax the edges of \( p \) in the order
    \begin{equation*}
        (v_0, v_1), (v_1, v_2), \ldots, (v_{k-1}, v_k)
    \end{equation*}
    then \( v_k.d = \delta(s, v_k) \). 

    This property holds regardless of any other relaxation
    steps that occur.
    \end{tcolorbox}
\end{frame}

\begin{frame}
    \frametitle{Critical Paths}

    Edges in a \ac{dag} can represent jobs, with weights indicating the time
    required.

    A \alert{critical path} is identified as the \textit{longest} path through the
    \ac{dag}, signifying the minimum project duration.

    \begin{figure}[htpb]
        \begin{center}
        \begin{tikzpicture}[node distance=2cm, auto]
            % Vertices
            \node (A) {A};
            \node[right of=A] (B) {B};
            \node[right of=B] (C) {C};
            \node[right of=C] (D) {D};
            \node[right of=D] (E) {E};
            \node[right of=E] (F) {F};

            % Edges
            \draw[->] (A) to[bend left=20] node {\emoji{coffee} 15} (B);
            \draw[->] (A) to[bend right=20] node[swap] {\emoji{drum} 30} (C);
            \draw[->] (B) to[bend left=30] node {\emoji{tv} 45} (D);
            \draw[->] (C) to node[swap] {\emoji{sandwich} 20} (D);
            \draw[->] (C) to[bend right=40] node[swap] {\emoji{soccer} 90} (E);
            \draw[->] (D) to node {\emoji{sleepy} 60} (E);
            \draw[->] (E) to node {\emoji{pencil} 90} (F);
        \end{tikzpicture}
        \end{center}
        \caption{What is the minimum time need for \emoji{pencil} homework?}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Computing the Critical Path}

    To find a critical path, we can manipulate edge weights and utilize
    shortest-path algorithms.
    \begin{itemize}
        \item Approach 1: \emph{Negate} edge weights and apply
            \textsc{DAG-Shortest-Paths}.
        \item Approach 2: Modify \textsc{Dag-Shortest-Paths} by initializing
            distances with $-\infty$ and adjusting the \textit{Relax} procedure
            comparison.
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{\tps{} Exercise 24.2-2}

    Is the algorithm still correct? Why?

    \begin{algorithmic}[1]
    \Procedure{Dag-Shortest-Paths}{$G, w, s$}
        \State topologically sort the vertices of $G$
        \State \Call{Initialize-Single-Source}{$G, s$}
        \For{each $u$ \textcolor{red}{of the first $\abs{V}-1$} vertex in topologically sorted order}
            \For{each vertex $v \in G.\text{Adj}[u]$}
                \State \Call{Relax}{$u, v, w$}
            \EndFor
        \EndFor
    \EndProcedure
    \end{algorithmic}
\end{frame}
\end{document}
