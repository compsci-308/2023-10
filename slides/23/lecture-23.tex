\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Hash Tables (Part 1)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 11.1-11.2.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 11.1: 1-4.
                \item
                    Exercises 11.2: 2-6.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 11 Hash Table}

\begin{frame}[c]
    \frametitle{What is Hash?}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            hash:
            \begin{itemize}
                \item transitive verb
                    \begin{itemize}
                        \item a: to chop (food) into small pieces
                        \item b: CONFUSE, MUDDLE
                    \end{itemize}
                \item noun
                    \begin{itemize}
                        \item chopped food
                        \item \dots
                    \end{itemize}
            \end{itemize}

            \flushright{-- Merriam Webster Dictionary}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{hash-dish.jpg}
                \caption{Beef Hash. Source:
                \href{https://en.wikipedia.org/wiki/Hash\_(food)}{Wikipedia}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{Hash Table and Dictionary}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            In everyday life, we use dictionary to look up the \emph{meaning} of a
            \emph{word}.

            \bigskip{}

            In programming, we can use a \alert{hash table}  to look up the \emph{data}
            associated with a \emph{key (string)}.

            \bigskip{}

            It is one of many possible implementations of an analogue of
            dictionaries in programming.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-hash-table.jpg}
                \caption{A Hash Table}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{Overview of Hash Table}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            A hash table provides three \alert{dictionary operations}:
            \begin{itemize}
                \item \textsc{Insert}(key, value),
                \item \textsc{Search}(key),
                \item \textsc{Delete}(key).
            \end{itemize}

            \medskip{}

            \cool{} All three $O(1)$ average time complexity under reasonable assumptions.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{hash-table.png}
                \caption{Hash table works by mapping keys to indices in an array.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}


\begin{frame}
    \frametitle{Hash Table Implementation}

    \alert{Direct addressing}  allows $O(1)$ time complexity by having one array
    position per possible key. (11.1)

    Collision of keys is addressed through:
    \begin{itemize}
        \item \alert{Chaining}: storing multiple keys at the same index using
            linked lists. (11.2)
        \item \alert{Open addressing}: finding the next empty slot in the array
            for collision resolution. (11.4)
    \end{itemize}

    \alert{Hash functions} map keys to indices (11.3).
\end{frame}

\section{\acs{ita} 11.1 Direct-address Tables}

\begin{frame}
    \frametitle{The Idea}

    Imaging that in a class: 
    \begin{itemize}
        \item there are at most $10$ \temoji{student},
        \item each \temoji{student} has a unique ID from $1$ to $10$.
    \end{itemize}

    To quickly access each student's data,
    we can use $10$ \temoji{file-folder} named $1$ to $10$.

    \cake{} What are some benefit and draw backs of this method?
\end{frame}

\begin{frame}
    \frametitle{Direct-address Tables}

    Assume that the set of all possible keys, also known as the \alert{universe}
    of keys, is $U = \{0, 1, \dots, m-1\}$, where $m$ is not too large.

    A \alert{directed-address table} $T[0 \dots m-1]$ is an array in which each
    position, or \alert{slot}, corresponds to one key in $U$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{hash-table-directed-addresses.png}
        \caption{Direct-address Table}
    \end{figure}
\end{frame}

\begin{frame}{Dictionary Operations}
    When the elements to be stored all have \emph{distinct} keys,
    the dictionary operations are trivial to implement:
    \begin{algorithmic}[1]
        \Procedure{Direct-Address-Search}{T, \text{key}}
            \State \Return $T[\text{key}]$
        \EndProcedure
    \end{algorithmic}
    \pause{}
    \begin{algorithmic}[1]
        \Procedure{Direct-Address-Insert}{T, x}
            \State $T[x.\text{key}] = x$
        \EndProcedure
    \end{algorithmic}
    \pause{}
    \begin{algorithmic}[1]
        \Procedure{Direct-Address-Delete}{T, x}
            \State $T[x.\text{key}] = \textsc{NIL}$
        \EndProcedure
    \end{algorithmic}
    Each of these operations takes only \(O(1)\) time.
\end{frame}

\begin{frame}
    \frametitle{Exercise 11.1-4}

    We wish to implement a dictionary by using direct addressing on a huge
    array. At the start, the array entries may contain garbage, and initializing
    the entire array is impractical because of its size. Describe a scheme for
    implementing a direct-address dictionary on a huge array.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    How to implement a direct-address table in which the keys of stored elements
    do \emph{not} need to be distinct?

    All three dictionary operations should run in $O(1)$.

    \hint{} Use a basic data structure that you have learned before this course.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{hash-table-directed-addresses.png}
        \caption{Direct-address Table}
    \end{figure}
\end{frame}

\section{\acs{ita} 11.2 Hash Tables}

\begin{frame}
    \frametitle{A Comparison}

    Let $K$ be the set of keys of the elements to be stored.

    A hash table uses $\Theta(\abs{K})$ space, whereas a direct-address table
    uses $\Theta(\abs{U})$ space.

    Operations on a hash table still require $O(1)$ time, \emph{on
    average}, whereas operations on a direct-address table require $O(1)$ time
    in the \alert{worst case}.

    \cake{} When should we use a hash table and when should we use direct-address table?
\end{frame}

\begin{frame}
    \frametitle{Hash Function}

    A hash table uses an array $T[0 \dots m-1]$ to store the data.

    It needs a \alert{hash function} 
    \begin{equation*}
        h: U \mapsto \{0, 1, \dots, m-1\}
    \end{equation*}
    to map keys in $U$ to \alert{slots} (indices) in $T$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{hash-table-with-hash-function.png}
        \caption{Hash Function}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{\emoji{boom} Collision}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Pigeon-hole Principle: Given more than $m$ \emoji{bird}
            and $m$ holes to host them,
            at least one hole must host more than one \emoji{bird}.

            \bigskip{}

            Thus whatever hash function we choose, 
            if $\abs{U} > m$,
            multiple keys will be mapped into the same slot.

            \bigskip{}

            This is called a \alert{collision} \emoji{boom}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{pigeon-hole.jpg}
                \caption{When there are more \emoji{bird} than holes.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Chaining}

    One way to solve collision is to put all elements mapped into the same slot
    in a \emph{doubly} linked list.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{hash-table-chain.png}
        \caption{Chaining}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Dictionary Operations}
    In a hash-table with chaining, the dictionary operations are implemented as follows:

    \begin{algorithmic}[1]
        \Procedure{Chained-Hash-Insert}{T, x}
        \State $\textsc{List-Insert}(T[h(x.\text{key})], x)$
        \EndProcedure
    \end{algorithmic}
    \pause{}
    \begin{algorithmic}[1]
        \Procedure{Chained-Hash-Search}{T, \text{key}}
            \State \Return $\textsc{List-Search}(T[h(\text{key})], \text{key})$
        \EndProcedure
    \end{algorithmic}
    \pause{}
    \begin{algorithmic}[1]
        \Procedure{Chained-Hash-Delete}{T, x}
            \State $\textsc{List-Delete}(T[h(x.\text{key})], x)$
        \EndProcedure
    \end{algorithmic}

    \cake{} Which of these two operations take $O(1)$ time in the worst case?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Demonstrate what happens when we insert the keys 
    \begin{equation*}
        5, 28, 19, 15, 20, 33
    \end{equation*}
    into a hash table with collisions resolved by chaining. 

    Let the table have 7 slots, and let the hash function be $h(k) = k \mod{7}$.
\end{frame}

\begin{frame}
    \frametitle{Simple Uniform Hashing}

    \weary{} If all keys are mapped into one slot, then running time of \textsc{Search}
    and is $\Theta(n)$.

    The average case depends on how well the hash function distributes the keys.

    For now we assume that each key is equally likely to be mapped into any slot.

    We call this assumption \alert{simple uniform hashing}.
\end{frame}

\begin{frame}
    \frametitle{Load Factor}

    Consider a hash table $T$ with $m$ slots and $n$ elements.

    Let $n_{j}$ be the number of keys mapped to slot $j$.

    Then under \emph{simple uniform hashing} assumption,
    \begin{equation*}
        \begin{aligned}
            \E{n_{j}} 
            = 
            &
            \sum_{i=1}^{n}
            I\{\text{element $i$ is mapped into slot $j$}\}
            \\
            &
            =
            \sum_{i=1}^{n}
            \p{\text{element $i$ is mapped into slot $j$}}
            =
            \frac{n}{m}
        \end{aligned}
    \end{equation*}
    where the number $n/m$ is called the \alert{load factor}.

    \cake{} How do we get the last equality above?
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.1}
    In a hash table using chaining, an \emph{unsuccessful} \textsc{Search} takes
    $\Theta(1 + \alpha)$ time under the simple uniform hashing assumption.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Professor C decide to teach \emoji{student} a lesson in probability by
    playing a game.

    He will choose a number from $\{1, 2, \dots, m\}$ uniformly at random.

    If a \emoji{student} can get this number correctly, they will get a
    \emoji{bubble-tea}.

    \pause{}

    Consider two strategies:
    \begin{itemize}
        \item Choose a number from $\{1, 2, \dots, m\}$ uniformly at random.
        \item Choose $1$.
    \end{itemize}
    Which strategy is better? Why?
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.2}
    In a hash table using chaining, a successful \textsc{Search} takes
    $\Theta(1 + \alpha)$ time under the simple uniform hashing assumption.

    \think{} Note that longer list are more likely to be searched.
\end{frame}

\end{document}
