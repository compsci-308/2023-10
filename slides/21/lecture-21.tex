\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Probabilistic Analysis and Randomize Algorithms}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 5.1-5.3
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 5.1: 1-3.
                \item
                    Exercises 5.2: 1-5.
                \item
                    Exercises 5.3: 1-4, 6-7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 5.1 The Hiring Problem}

\begin{frame}
    \frametitle{Hiring Detectives}
    
    You are a \emoji{cop} chief and you need to hire
    some \emoji{detective}.

    Your hiring algorithm is the following:

    \begin{algorithmic}
        \Procedure{Hire-Detective}{}
            \State $\text{best} = 0$ \Comment{A dummy candidate}
            \For{$i \gets 1$ to $n$}
                \State interview candidate $i$
                \If{$i$ is better than $\text{best}$}
                    \State $\text{best} = i$
                    \State hire candidate $i$ to be a \emoji{detective}
                \EndIf
            \EndFor
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Analysis of the Cost}

    Let $c_{h}$ be the costs of \coin{}
    for hiring a candidate.

    Let $n$ be the number of candidates and $m$ be the number of \emoji{detective}
    which are hired.

    Then the total cost of hiring caused by \textsc{Hire-Detective} is
    \begin{equation*}
        O(c_{h} m)
    \end{equation*}

    \cake{} What is $c_{h} m$ in the worst case?
\end{frame}

\begin{frame}
    \frametitle{Ranks}

    Assume that $\rank(i)$ indicates how good candidate $i$ is
    compared to all other candidates,
    with $\rank(i) = 1$ being the worst and $\rank(i) = n$ being the best.

    \cake{} How many \emoji{detective} would be hired if
    \begin{equation*}
        \langle \rank(1), \rank(2), \dots, \rank(n) \rangle
        =
        \langle 1, 2, 3, \dots, n \rangle
    \end{equation*}

    \pause{}

    \cake{} What if 
    \begin{equation*}
        \langle \rank(1), \rank(2), \dots, \rank(n) \rangle
        =
        \langle n, n-1, n-2, \dots, 1 \rangle
    \end{equation*}

    \pause{}

    There are $n!$ possible $\langle \rank(1), \dots , \rank(n) \rangle$.

    \emoji{bullseye} Assuming that each one is \alert{equally likely}, i.e.,
    \alert{uniformly at random} , what is the average of $c_{h} m$?  
\end{frame}

\begin{frame}
    \frametitle{Probabilistic Analysis}
    Using probability theory to answer this type of questions is called
    \alert{probabilistic analysis}. 

    Examples of probabilistic analysis are:
    \begin{itemize}
        \item What is the final grade I will likely get in this course?
        \item If I choose to become a \emoji{cop}, how much \emoji{coin} can I expect to have when I retire?
        \item If I keep \emoji{smoking}, how long can I hope to live?
    \end{itemize}
    
    \cake{} Can you think of some examples where 
    probabilistic analysis may help you?
\end{frame}

\section{\acs{ita} 5.2 Indicator random variables}

\begin{frame}
    \frametitle{Sample Space}

    In probability theory, the \alert{sample space} is the set of all
    possible outcomes of a random experiment.

    Examples:
    \begin{itemize}
        \item The \alert{sample space} of \coin{} flip is $\{\text{Head},
            \text{Tail}\}$
        \item The \alert{sample space} of \emoji{game-die} rolling is 
            $\{1, 2, \dots, 6\}$
        \item The \alert{sample space} of the grade of a course is 
            $\{\text{A+}, \text{A}, \text{A-}, \dots, \text{F}\}$.
    \end{itemize}

    \cake{} What is the sample space of 
    $\langle \rank(1), \rank(2), \dots, \rank(n) \rangle$?
\end{frame}

\begin{frame}
    \frametitle{Events}

    Let $S$ be a sample space.

    We call a set $A$ an \alert{event} if it is a subset of $S$.

    Examples:
    \begin{itemize}
        \item An event in \coin{} flip is $\{\text{Head}\}$.
        \item An event in \emoji{game-die} is $\{2, 4, 6\}$.
        \item An event in the grade of a course is can be $\{\text{A+}, \text{F}\}$.
    \end{itemize}

    \cake{} If the sample space of the weather is $\{\temoji{sun}, 
    \temoji{cloud}, \temoji{cloud-with-rain}\}$, what are all the possible events?
\end{frame}

\begin{frame}
    \frametitle{Probability}

    Given a sample space $S$,
    a function $\Pr$ from all possible events to $[0,1]$ is called
    a \alert{probability distribution} if
    \begin{itemize}
        \item $\Pr(S) = 1$ and
        \item $\Pr(A \cup B) = \Pr(A) + \Pr(B)$ if $A$ and $B$ are disjoint.
    \end{itemize}

    \only<1>{%
        \begin{tcolorbox}[title={\zany{} Philosophical Note}]
        From pure mathematical point of view, 
        $\Pr$ is simply a function.

        It does not really answer the philosophical question of ``What is
        \alert{probability}?'' or ``Is the world a random world?''
        \end{tcolorbox}
    }%
    \only<2>{%
        For example, the \alert{uniform probability distribution}  for \coin{} flip
        is defined by
        \begin{equation*}
            \begin{aligned}
                \Pr(\{\text{Head}\}) &= 1/2 \\
                \Pr(\{\text{Tail}\}) &= 1/2
            \end{aligned}
        \end{equation*}

        \cake{} What is $\p{\{\text{Head}\} \cup \{\text{Tail}\}}$?
    }%
    \only<3>{%
        For another example, 
        the \alert{uniform probability distribution}  for \emoji{game-die} rolling
        is defined by
        \begin{equation*}
            \Pr(\{k\}) = 1/6  \qquad \text{for } k \in \{1, 2, 3, 4, 5, 6\}.
        \end{equation*}

        \cake{} What is $\p{\{\text{roll an even number}\}}$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Random Variable}

    A \alert{random variable} is a function from a sample space $S$ to a real
    number.

    For example, let $S$ be the sample space of grades of a course.

    Then we can define a random variable $X$ can be defined as the
    \emoji{dollar} reward you will receive from your parents, such as
    \begin{equation*}
        \begin{aligned}
            X(\text{A+}) & = 1000, \\
            X(\text{A}) & = 900, \\
            X(\text{A-}) & = \dots \\
            X(\text{D-}) & = 0, \\
            X(\text{F}) & = -1000.
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Indicator Random Variable}
    In this section, we mostly care about \alert{indicator random variables}
    $I\{A\}$ defined by
    \begin{equation*}
        I\{A\} =
        \begin{cases}
            1 & \text{if $A$ occurs} \\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}

    \pause{}

    For example, if $S = \{\temoji{sun}, \temoji{cloud}, \temoji{cloud-with-rain}\}$
    and $A = \{\temoji{cloud}, \temoji{cloud-with-rain}\}$,
    then $I\{A\}$ is defined by
    \begin{equation*}
        I\{A\} =
        \begin{cases}
            1 & \text{if it is \temoji{cloud} or \temoji{cloud-with-rain}} \\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Expect Value}

    The \alert{expected value} or \alert{expectation}  of a random variable $X$ is defined
    by
    \begin{equation*}
        \E{X} = \sum_{\omega \in S} \p{ \{\omega\} } X(\omega)
    \end{equation*}

    \pause{}

    In the \emoji{dollar} for grade example, you expected reward would be
    \begin{equation*}
        \E{X} 
        = 
        \p{ \{\text{A+}\} } 1000
        +
        \p{ \{\text{A}\} } 900
        \dots
        +
        \p{ \{\text{D-}\} } 0
        +
        \p{ \{\text{F}\} } -1000
        .
    \end{equation*}

    \pause{}

    In the uniform random \coin{} flip example,
    \begin{equation*}
        \E{I\{ \{\text{Head}\} \}} 
        = 
        \p{ \{\text{Head}\} } 1
        +
        \p{ \{\text{Tail}\} } 0
        =
        \frac{1}{2} \cdot 1
        +
        \frac{1}{2} \cdot 0
        =
        \frac{1}{2}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Lemma 5.1}

    Let $S$ be a sample space and $A$ be an event of $S$.
    Let $X_A = I\{A\}$.
    Then
    \begin{equation*}
        \E{X_A} = \p{A}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Applications of Indicator Random Variables}

    By combining indicator random variables, we can create more complex random variables.

    \cake{} We repeat \coin{} flip $n$ times.
    Let $X_{i}$ be the indicator random variable for a head in the $i$th flip.
    What does
    \begin{equation*}
        X = \sum_{i = 1}^{n} X_{i}
    \end{equation*}
    represent?
\end{frame}

\begin{frame}
    \frametitle{Linearity of Expectation}

    A very convenient property of expectation is its \alert{linearity}.

    In other words, we have
    \begin{equation*}
        \E{X + Y} = \E{X} + \E{Y},
    \end{equation*}
    regardless of what random variables $X$ and $Y$ are.

    \cake{} How does this help us to compute
    \begin{equation*}
        \E{X} = \E{\sum_{i = 1}^{n} X_{i}}
    \end{equation*}
    in the $n$ \coin{} flips example?
\end{frame}

\begin{frame}
    \frametitle{Back to Hiring Detectives}

    Let $X_{i}$ be the indicator variable for hiring the $i$-th candidate
    as \temoji{detective}.

    Then the total number of \emoji{detective} hired is
    \begin{equation*}
        X = \sum_{i = 1}^{n} X_{i}
    \end{equation*}

    \pause{}

    Let $A_{i}$ be the event of hiring the $i$-th candidate.

    As each of the first $i$ candidates are equally likely to be the best among
    them,
    \begin{equation*}
        \p{A_{i}} = \frac{1}{i}.
    \end{equation*}

    \pause{}

    Thus
    \begin{equation*}
        \E{X}
        =
        \sum_{i=1}^{n} \E{X_{i}}
        =
        \sum_{i=1}^{n} \p{A_{i}}
        =
        \sum_{i=1}^{n} \frac{1}{i}
        =
        H_{n}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Harmonic Numbers}

    The number
    \begin{equation*}
        H_{n} 
        = \sum_{i=1}^{n} \frac{1}{i}
        = \frac{1}{1} + \frac{1}{2} + \dots + \frac{1}{n},
    \end{equation*}
    is called the $n$-th \alert{harmonic number}.

    It is well-known that
    \begin{equation*}
        H_{n} = \log(n) + \gamma + O(n^{-1}),
    \end{equation*}
    where
    \begin{equation*}
        \gamma \approx 0.5772156649
    \end{equation*}
    is the \alert{Euler constant}.
\end{frame}

\begin{frame}
    \frametitle{Lemma 5.2}

    Assume that 
    \begin{equation*}
        \langle \rank(1), \dots, \rank(n) \rangle
    \end{equation*}
    for the candidates for \emoji{detective}
    is uniformly random.

    Then the \textsc{Hiring-Detective} has an average (expected) hiring cost $\Theta(c_{h}
    \log(n))$.
\end{frame}

\section{\acs{ita} 5.3 Randomized Algorithms}

\begin{frame}
    \frametitle{Randomized algorithms}

    We cannot be sure if the ranks of the candidates are truly uniformly
    random.

    \weary{} This makes probabilistic analysis unreliable.

    \pause{}

    But we can permute the order of the candidates uniformly at random, 
    using a \alert{random number generator}.

    \smiling{} This makes sure that the ranks of the candidates are uniformly distributed.

    An algorithm is called \alert{randomized} if its output depends on not only
    the input, but also the random number generator.
\end{frame}

\begin{frame}[c]
    \frametitle{Random Number Generator}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            We assume that $\textsc{Random}(a,b)$ returns
            a random integer between $a$ and $b$.

            \medskip{}

            This can be done by:
            \begin{itemize}
                \item rolling a $(b-a+1)$ sided \emoji{game-die},
                \item flipping a \coin{},
                \item detecting radioactive decay,
                \item radio noise in the atmosphere, etc.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{smoke-detctor.jpg}
                \caption{A convenient way to get random number is by using
                Americium 241 in household smoke detectors.}
            \end{figure}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{Pseudorandom Number Generator}

    Most random number generators are actually \alert{pseudorandom number
    generators} which deterministically produce a sequence of numbers which look
    random.

    \begin{tcolorbox}[title={\zany{} Example}]
        \small
        The \alert{linear congruential generator}
        produces a sequence of integers $X_n$ with the formula
        \begin{equation*}
            X_{n+1} = (a X_n + c) \mod{m}
        \end{equation*}
        where $a, c, m$ are integers.

        Many implementations of C language use this algorithm with
        \begin{equation*}
            a = 1103515245, \qquad c = 12345, \qquad m = 2^{31}.
        \end{equation*}
    \end{tcolorbox}
\end{frame}

\begin{frame}
    \frametitle{Randomized version of \textsc{Hiring-Detective}}

    To make sure our analyze is correct, 
    we will use randomized version of \textsc{Hiring-Detective}:

    \begin{algorithmic}
        \Procedure{Randomized-Hire-Detective}{}
            \State \textcolor{red}{permute the list of candidates uniformly at
            random}
            \State $\text{best} = 0$
            \For{$i \gets 1$ to $n$}
                \State interview candidate $i$
                \If{$i$ is better than $\text{best}$}
                    \State $\text{best} = i$
                    \State hire candidate $i$
                \EndIf
            \EndFor
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Lemma 5.3}

    The \textsc{Randomized-Hiring-Detective} has an average hiring cost $\Theta(c_{h}
    \log(n))$.

    \hint{} Note that we have dropped the assumption about the input.

    \think{} But how can we permute the list of candidates uniformly at random?
\end{frame}

\begin{frame}
    \frametitle{Permute by Sorting}

    One way to permute the array is to assign each element a random key
    and then sort the array according to the keys.

    \begin{algorithmic}
        \Procedure{Permute-By-Sorting}{A}
        \State $n = A.\text{length}$
        \State let $P[1 \dots n]$ be a new array
        \For{$i = 1$ to $n$}
            \State $P[i] = \textsc{Random}(1, n^3)$
        \EndFor
        \State sort $A$ according to $P$ in ascending order
        \EndProcedure
    \end{algorithmic}

    We choose $n^3$ to make it more likely that the keys are unique.
\end{frame}

\begin{frame}
    \frametitle{Lemma 5.4}

    The \textsc{Permute-By-Sorting} produces a uniformly random permutation of
    the array $A$.

    \begin{tcolorbox}[title={Proof Sketch}]
        The probability that the permutation does change $A$ is
        \begin{equation*}
            \p{P[1] < P[2] < \dots < P[n]}
            =
            \frac{1}{n} \cdot \frac{1}{n-1} \dots \frac{1}{1}
            =
            \frac{1}{n!}
        \end{equation*}

        \pause{}

        The same holds true for all other possible permutations.

        Thus no permutation of $A$ is more likely than any others.
    \end{tcolorbox}
\end{frame}

\begin{frame}
    \frametitle{In-place Permutations}

    A more efficient way to permute the array is the following:

    \begin{algorithmic}
        \Procedure{Permute-In-Place}{A}
        \State $n = A.\text{length}$
        \For{$i = 1$ to $n$}
            \State swap $A[i]$ with $A[\textsc{Random}(i, n)]$
        \EndFor
        \EndProcedure
    \end{algorithmic}

    \laughing{} Let's try it out on $A[1 \dots 4]$
    with you as the random number generator.
\end{frame}

\begin{frame}
    \frametitle{Lemma 5.5}

    The procedure \textsc{Permute-In-Place} produces a uniformly random permutation of

    \begin{tcolorbox}[title={Proof Sketch}]
        The \alert{$k$-permutation} of $A$ is a permutation of $k$ elements of $A$.

        We can show by induction that before the $i$-th iteration of the
        \textbf{for} loop, $A[1 \dots i - 1]$ is equally likely to be any
        $(i-1)$-permutation of $A$.

        \pause{}

        Then after the $n$-th iteration,
        $A[1 \dots n]$ is equally likely to be any permutation of $A$.
    \end{tcolorbox}
\end{frame}

\end{document}
