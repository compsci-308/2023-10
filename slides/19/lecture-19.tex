\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Linear Programming (Part 3)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 29.3.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 29.3: 1, 8.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 29.3 The Simplex Algorithm}

\begin{frame}
    \frametitle{Review: Tuple Notation}
    
    Recall that a slack form \ac{lp} problem 
    \begin{equation*}
        \begin{aligned}
            z & = v + \sum_{j \in N} c_j x_j \\
            x_{i} & = b_{i} - \sum_{j \in N} a_{ij} x_j \qquad \text{for all } i \in B
        \end{aligned}
    \end{equation*}
    can be written as $(N, B, A, b, c, v)$.
\end{frame}

\begin{frame}
    \frametitle{Example of Tuple Notations}
    For example, the following slack form
    \begin{align*}
        z &= 0 + 3x_1 + x_2 + 2x_3 \\
        x_4 &= 30 - x_1 - x_2 - 3x_3 \\
        x_5 &= 24 - 2x_1 - 2x_2 - 5x_3 \\
        x_6 &= 36 - 4x_1 - x_2 - 2x_3
    \end{align*}
    can be written as as a tuple $(N, B, A, b, c, v)$ with
    \only<1>{%
        \begin{equation*}
            N = \{1, 2, 3\},
            \qquad
            B = \{4, 5, 6\},
            \qquad
            A
            =
            \begin{bmatrix}
                a_{41} & a_{42} & a_{43} \\
                a_{51} & a_{52} & a_{53} \\
                a_{61} & a_{62} & a_{63} \\
            \end{bmatrix}
            =
            \begin{bmatrix}
                1 & 1 & 3 \\
                2 & 2 & 5 \\
                4 & 1 & 2
            \end{bmatrix}
        \end{equation*}
    }
    \only<2>{
        \begin{equation*}
            b = 
            \begin{bmatrix} 
                30 \\ 24 \\ 36
            \end{bmatrix} 
            ,
            \qquad
            c = 
            \begin{bmatrix}
                3 \\ 1 \\ 2
            \end{bmatrix}
            \qquad
            v = 0.
        \end{equation*}
        We will use $n = \abs{N}$ and $m = \abs{B}$ to denote the number of
        non-basic and basic variables, respectively.
    }
\end{frame}

\begin{frame}
    \frametitle{Quick Exercise}
    \cake{} What are $(N, B, A, b, c, v)$, $m$ and $n$ for
    \begin{align*}
        z &= 7 - 3x_1 - x_2      \\
        x_3 &= 3 - x_1 - x_2    \\
        x_4 &= 4 - 2x_1 - 2x_2  \\
        x_5 &= 6 - 4x_1 - x_2  
    \end{align*}
\end{frame}

\subsection{More Examples of Pivoting}

\begin{frame}
    \frametitle{Exercise 29.3-5}

    \only<1>{%
        Solve the following \ac{lp} problem with simplex method:
        \begin{align*}
            z & = 18 x_1 + 12.5 x_2 \\
            x_3 & = 20 - x_1 - x_2 \\
            x_4 & = 12 - x_1 \\
            x_5 & = 16 - x_2
        \end{align*}
    }
    \only<2>{%
        After one pivot with $x_{e} = x_1$ and $x_{l} = x_4$, we get
        \begin{align*}
            z & = 216 - 18x_4 + 12.5 x_2 \\
            x_3 & = 8 + x_4 - x_2 \\
            x_1 & = 12 - x_4 \\
            x_5 & = 16 - x_2
        \end{align*}
        \cake{} What should the $x_{e}$ and $x_{l}$ be?
    }
    \only<3>{%
        After the second pivot, we have
        \begin{align*}
            z & = 316 - 5.5x_4 - 12.5x_3 \\
            x_2 & = 8 + x_4 - x_3 \\
            x_1 & = 12 - x_4 \\
            x_5 & = 8 - x_4 + x_3
        \end{align*}

        \cake{} What is the optimal objective value? Which solution gives this
        value?

        \zany{} To verify that you get the correct answer, you can use
        \href{https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.linprog.html}{SciPy}
    }
\end{frame}

\begin{frame}
    \frametitle{Unbounded Case}
    
    Now consider a slightly different \ac{lp} problem:
    \begin{align*}
        z & = 18 x_1 + 12.5 x_2 \\
        x_3 & = 20 + x_1 - x_2 \\
        x_4 & = 12 + x_1 - 2 x_2
    \end{align*}
    Note that $a_{31} = -1$, $a_{42} = -1$.

    \cake{} Do you see why the problem is unbounded?

    \pause{}

    \hint{} If $c_{e} > 0$ and $a_{je} < 0$ for all $j \in B$, then the problem
    is unbounded.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Solve the following \ac{lp} problem with simplex method:
    \begin{align*}
        \text{maximize} \quad & 4x_1 - 2x_2 \\
        \text{subject to} \quad & x_1 - x_2 \leq 1 \\
                                & 2x_1 + x_2 \leq 2 \\
                                & x_1, x_2 \geq 0.
    \end{align*}
\end{frame}

\subsection{Formalize Pivoting}

\begin{frame}[t]
    \frametitle{The Pseudocode of Pivot}

    \begin{columns}[t]
        \begin{column}{0.5\textwidth}
            \small{}%
            \begin{algorithmic}
                \Procedure{Pivot}{$N, B, A, b, c, v, l, e$}
                    \onslide<1>{%
                    \State Let $\hat{A}$ be a new $m \times n$ matrix
                    }
                    \onslide<1, 2>{%
                    \State $\hat{b}_e \gets \frac{b_l}{a_{le}}$
                    \For{each $j \in N - \{e\}$}
                        \State $\hat{a}_{ej} \gets \frac{a_{lj}}{a_{le}}$
                    \EndFor
                    \State $\hat{a}_{el} \gets \frac{1}{a_{le}}$
                    }
                    \onslide<1, 3>{%
                    \For{each $i \in B - \{l\}$}
                        \State $\hat{b}_i \gets b_i - a_{ie}\hat{b}_e$
                        \For{each $j \in N - \{e\}$}
                            \State $\hat{a}_{ij} \gets a_{ij} - a_{ie}\hat{a}_{ej}$
                        \EndFor
                    \State $\hat{a}_{il} \gets -a_{ie}\hat{a}_{el}$
                    \EndFor
                    }
                    \algstore{bkmark}
            \end{algorithmic}
        \end{column}
        \begin{column}{0.5\textwidth}
            \small{}%
            \begin{algorithmic}
                \algrestore{bkmark}
                \onslide<1, 4>{%
                \State $\hat{v} \gets v + c_e\hat{b}_e$
                \For{each $j \in N - \{e\}$}
                \State $\hat{c}_j \gets c_j - c_e\hat{a}_{ej}$
                \EndFor
                \State $\hat{c}_l \gets -c_e\hat{a}_{el}$
                \State $\hat{N} \gets N - \{e\} \cup \{l\}$
                \State $\hat{B} \gets B - \{l\} \cup \{e\}$
                \State \Return $(\hat{N}, \hat{B}, \hat{A}, \hat{b}, \hat{c}, \hat{v})$
                }
                \EndProcedure
            \end{algorithmic}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Lemma 29.1}
    Assume that \textsc{Pivot}$(N, B, A, b, c, v, l, e)$ with $a_{le}
    \neq 0$ returns $(\hat{N}, \hat{B}, \hat{A}, \hat{b}, \hat{c}, \hat{v})$. 
    Then basic solution for the new \ac{lp} is
    \begin{enumerate}
        \item $x_j = 0$ for each $j \in \hat{N}$.
        \item $x_e = \hat{b}_e = b_l/a_{le}$.
        \item $x_i = \hat{b}_i = b_i - a_{ie}x_e$ for each $i \in
            \hat{B}-\{e\}$.
    \end{enumerate}

    \pause{}
    
    \begin{tcolorbox}[title={Proof}]
        \small{}
        The \textsc{Pivot} procedure gives us the new \ac{lp} problem
        \begin{equation*}
            \begin{aligned}
                z & = \hat{v} + \sum_{j \in \hat{N}} \hat{c}_j x_j \\
                x_i & = \hat{b}_i - \sum_{j \in \hat{N}} \hat{a}_{ij} x_j 
                \qquad \text{for all } i \in \hat{B}
            \end{aligned}
        \end{equation*}
        What is the basic solution is obvious.
    \end{tcolorbox}
\end{frame}

\subsection{Formalize the Simplex Method}

\begin{frame}
    \frametitle{Potential Challenges in Simplex Method}
    The simplex method can encounter various challenges:

    \begin{itemize}
        \item Determining the feasibility of a linear program.
        \item Handling an initial infeasible basic solution in a feasible linear
            program.
    \end{itemize}

    \pause{}

    To address these two problems,
    Section~29.5 will introduce the \textsc{Initialize-Simplex}
    procedure with:
    \begin{itemize}
        \item Input: standard form linear program as input.
        \item Output: The feasibility and a slack form with \emph{feasible basic
            solution} if the problem is feasible.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The Pseudocode of Simplex Method}
    \begin{algorithmic}[1]
        \Procedure{Simplex}{$A, b, c$}
            \State $\text{slack-form} \gets \textsc{Initialize-Simplex}(A, b, c)$
            \If{$\text{slack-form} = \text{``Infeasible''}$}
                \State \Return \text{``Infeasible''}
            \EndIf
            \State $\text{final-slack-form} \gets 
                \textsc{Pivot-Repeatedly}(\text{slack-form})$
            \If{$\text{final-slack-form} = \text{``Unbounded''}$}
                \State \Return \text{``Unbounded''}
            \Else
                \State \Return $\textsc{Basic-Solution}(\text{final-slack-form})$
            \EndIf
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{The Pseudocode of Repeated Pivoting}
    \vspace{-0.3cm}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \footnotesize{}
            \begin{algorithmic}
                \Procedure{Pivot-Repeatedly}{$N, B, A, b, c, v$}
                    \State let $\Delta$ be a new array of length $n$
                    \While{some $j \in N$ has $c_j > 0$}
                        \State choose $e \in N$ with $c_e > 0$
                        \For{each index $i \in B$}
                            \If{$a_{ie} > 0$}
                                \State $\Delta_i \gets b_i / a_{ie}$
                            \Else
                                \State $\Delta_i \gets \infty$
                            \EndIf
                        \EndFor
                        \algstore{bkmark}
            \end{algorithmic}
        \end{column}
        \begin{column}{0.6\textwidth}
            \footnotesize{}
            \begin{algorithmic}
                        \algrestore{bkmark}
                        \State choose $l \in B$ with minimal $\Delta_i$
                        \If{$\Delta_l = \infty$}
                            \State \Return ``unbounded''
                        \Else
                            \State $(N, B, A, \dots) \gets \textsc{Pivot}(N,
                            B, A, \dots)$
                        \EndIf
                    \EndWhile
                    \State \Return $(N, B, A, b, c, v)$
                \EndProcedure
            \end{algorithmic}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{The Pseudocode of Finding Basic Solution}

    The \textsc{Basic-Solution} procedure just return the basic solution
    of a slack form.

    \begin{algorithmic}[1]
        \Procedure{Basic-Solution}{$N, B, A, b, c, v$}
        \For{$i = 1$ to $n$}
        \If{$i \in B$}
        \State $x_i \gets b_i$
        \Else
        \State $x_i \gets 0$
        \EndIf
        \EndFor
        \State \Return $(x_1, x_2, \ldots, x_n)$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Lemma 29.2}
    
    \emoji{unamused} If \textsc{Simplex} returns returns ``Infeasible'', then
    the problem is infeasible.

    \emoji{slightly-smiling-face} If it returns a solution, then it is a
    feasible solution.

    \emoji{laughing} If it return ``Unbounded'', then the problem is unbounded.

    \pause{}

    \begin{tcolorbox}[title=Proof-Sketch]
        \only<2>{%
            If the problem is infeasible, then \textsc{Initialize-Simplex} will return ``Infeasible'' and \textsc{Simple} will also return ``Infeasible''.
        }%
        \only<3>{%
            If the problem is feasible, then at the start of the \textbf{while} loop 
            in \textsc{Pivot-Repeatedly}, we have

            \begin{enumerate}
                \item the slack form is equivalent to the one returned by \textsc{Initialize-Simplex},
                \item for each $i \in B$, $b_i \ge 0$, and
                \item the basic solution for the slack form is feasible.
            \end{enumerate}
        }%
    \end{tcolorbox}
\end{frame}

\subsection{Termination of the Simplex Method}

\begin{frame}
    \frametitle{While it End?}
    
    \emoji{slightly-smiling-face} Each pivot in the simplex method will not \emph{decrease} the objective
    value.

    \emoji{unamused} Unfortunately, the objective value may stay the same.
    This is called \alert{degeneracy}.

    \pause{}

    \begin{tcolorbox}[title={Example}]
        \only<2>{
            \cake{} What is the objective value for the basic solution?
            \begin{equation*}
                \begin{aligned}
                    z &= 8 + x_3 - x_4 \\
                    x_1 &= 8 - x_2 - x_4 \\
                    x_5 &= x_2 - x_3
                \end{aligned}
            \end{equation*}

            \cake{} What happens if we pivot with $x_e = x_3$?
        }
        \only<3>{
            After one pivot, we get
            \begin{equation*}
                \begin{aligned}
                    z &= 8 + x_2 - x_4 - x_5 \\
                    x_1 &= 8 - x_2 - x_4 \\
                    x_3 &= x_2 - x_5
                \end{aligned}
            \end{equation*}

            \cake{} What is the objective value?
        }
        \only<4>{
            Luckily for us, one pivot with $x_e = x_2$ is enough
            \begin{equation*}
                \begin{aligned}
                    z &= 16 - x_1 -2 x_4  - x_5 \\
                    x_2 &= 8 - x_1 - x_4 \\
                    x_3 &= x_2 - x_5
                \end{aligned}
            \end{equation*}

            \cake{} What is the optimal objective value?
        }
    \end{tcolorbox}
\end{frame}

\begin{frame}[c]
    \frametitle{Cycling}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Degeneracy in the Simplex algorithm may lead to \alert{cycling}, where
            two different iterations have identical slack forms. 

            \medskip{}

            Since Simplex is deterministic, cycling can result in infinite iteration
            without termination.

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-cycling.jpg}
                \caption{A cycling simplex method goes nowhere}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}


\begin{frame}
    \frametitle{Lemma 29.5: When Does Simplex Method Fail?}

    The simplex method either terminates in
    $\binom{m+n}{n}$ steps, or it cycles.

\end{frame}

\begin{frame}
    \frametitle{A Little Experiment}
    
    \only<1>{
        Let $\alpha, \beta,$ and $\gamma$ be some real numbers.
        Assume that for any $x \in \mathbb{R}$, the following holds true:
        \begin{equation*}
            \alpha x = \gamma + \beta x
        \end{equation*}
        \cake{} What can we infer about $\alpha, \beta,$ and $\gamma$?
    }
    \only<2>{
        Consider $\alpha_1, \alpha_2, \beta_1, \beta_2,$ 
        and $\gamma$ as real numbers.
        Assume that for any $x_1, x_2 \in \mathbb{R}$, 
        the following equations are satisfied:
        \begin{equation*}
            \begin{aligned}
                \alpha_1 x_1 +\alpha_2 x_2 & 
                = 
                \gamma + \beta_1 x_1 + \beta_2 x_2, \\
            \end{aligned}
        \end{equation*}
        \cake{} What conclusions can be drawn about $\alpha_1, \alpha_2, \beta_1, \beta_2,$ and $\gamma$?
    }
\end{frame}

\begin{frame}
    \frametitle{Lemma 29.3}

    Let $I$ be a set of indices. 
    Let $\alpha_{j}, \beta_{j}$ be real numbers for all $j \in I$.
    Let $\gamma$ be a real number.

    If for \emph{any} choice of $x_{j}$'s
    \begin{equation*}
        \sum_{j \in I} \alpha_{j} x_{j} = \gamma + \sum_{j \in I} \beta_{j}
        x_{j}
    \end{equation*}
    then
    \begin{equation*}
        \begin{aligned}
            \alpha_{j} & = \beta_{j} \qquad \text{for } j \in I \\
            \gamma & = 0.
        \end{aligned}
    \end{equation*}

    \puzzle{} Try to prove this!
\end{frame}

\begin{frame}
    \frametitle{Lemma 29.4}

    Let $(A,b,c)$ be a linear program in standard form. 

    Given a set $B$ of basic variables,
    the associated slack form is uniquely determined.

\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    How many different slack forms are there for the following problem?
    \begin{align*}
        \text{maximize} \quad & 4x_1 - 2x_2 \\
        \text{subject to} \quad & x_1 \leq 1 \\
                                & x_2 \leq 2 \\
                                & x_1, x_2 \geq 0.
    \end{align*}
\end{frame}
\begin{frame}
    \frametitle{Avoiding Cycling}

    \astonished{} Degeneracy is common. But cycling is
    \href{https://doi.org/10.1016/S0305-0548(02)00226-5}{rare}.

    It can also be avoided by:

    \begin{tcolorbox}[title={Bland's rule}]
        When there are more than one option for $x_e$ or $x_l$, choose the one
        with smaller index.
    \end{tcolorbox}

    \pause{}

    \begin{tcolorbox}[title={Lemma 29.7}]
        \textsc{Simplex} with Bland's rule always terminates.
    \end{tcolorbox}

    \zany{} See
    \href{https://personal.math.ubc.ca/\~anstee/math340/340blandsrule.pdf}{here}
    for a proof.
\end{frame}

\begin{frame}
    \frametitle{Lemma 29.7: The Correctness of Simplex Method}
    
    Assume that \textsc{Simplex} is given a feasible \ac{lp} problem.

    The \textsc{Simplex} with Bland's rule either report ``unbounded'' or a
    feasible solution within at most $\binom{m+n}{m}$ iterations.
\end{frame}

%\begin{frame}
%    \frametitle{The Problem of Video Game}
%
%    \only<1-2>{
%        Let's try the simplex method on the video game problem:
%        {\footnotesize
%        \begin{equation*}
%            \begin{aligned}
%                \text{maximize} \quad & x_1 + 10 x_2 \\
%                \text{subject to} \quad & x_1 \ge 5 \\
%                                        & x_2 \ge 3 \\
%                                        & x_1 + x_2 = 20 \\
%                                        & x_1, x_2 \geq 0.
%            \end{aligned}
%        \end{equation*}
%        }
%    }
%    \only<2-4>{
%        Turning this into standard form, we have
%        {\footnotesize
%        \begin{equation*}
%            \begin{aligned}
%                \text{maximize} \quad & x_1 + 10 x_2 \\
%                \text{subject to} \quad & -x_1 \le -5 \\
%                                        & -x_2 \le -3 \\
%                                        & x_1 + x_2 \le 20 \\
%                                        & -x_1 - x_2 \le -20 \\
%                                        & x_1, x_2 \geq 0.
%            \end{aligned}
%        \end{equation*}
%        }
%    }
%    \only<3>{
%        %The procedure \textsc{Initialize-Simplex} will convert this to
%        Converting this to slack form naively, we have
%        {\footnotesize
%            \begin{equation*}
%                \begin{aligned}
%                    & z = x_1 + 10 x_2 \\
%                    & x_3 = -5 + x_1 \\
%                    & x_4 = -3 + x_2 \\
%                    & x_5 = 20 - x_1 - x_2 \\
%                    & x_6 = -20 +x_1 + x_2  \\
%                \end{aligned}
%            \end{equation*}
%        }
%        \cake{} What is wrong with this slack form?
%    }
%\end{frame}

\end{document}
