\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Greedy Algorithms (Part 2)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ita} 16.3 Huffman codes}

\begin{frame}
    \frametitle{How to Travel in Space}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            There is vast distance between the solar system and the stars.

            \medskip{}

            It is extremely difficult to built \emoji{rocket} to take humans to
            traverse the interstellar space.

            \medskip{}

            One option is to send digital copies (uploads) of humans instead.

            \medskip{}

            \think{} How can we compress the data so we can send as many digital
            humans as possible?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{digital-humans.jpg}
                \caption{Digital humans travelling through space}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Encoding Digital Data}

    Consider the text: \emph{a deaf add bade a bee feed a babe}

    To represent the text as a binary string, we can use the following
    \alert{code}:
    \begin{table}[h]
        \centering
        \input{../tables/code-fix-length.tex}
    \end{table}
    
    \only<1>{
        We call the binary string representation of a letter its \alert{codeword}.

        \cake{} How can we encode the word \emph{bad} with the above codeword?
    }

    \only<2>{
        The example text (with white space removed) is encoded as:

        \begin{center}
            \begin{tabular}{ccccccc}
                000 & 011 & 100 & 000 & 101 & 000 & 011 \\
                011 & 001 & 000 & 011 & 100 & 000 & 001 \\
                100 & 100 & 101 & 100 & 100 & 011 & 000 \\
                001 & 000 & 001 & 100 \\
            \end{tabular}
        \end{center}

        The result is a binary string of length 75.
    }
\end{frame}

\begin{frame}
    \frametitle{Variable-length Codeword}

    We also can take the frequency of each letter into account when choosing
    codewords. For example:

    \begin{table}[h]
        \centering
        \input{../tables/code-variable-length.tex}
    \end{table}
    \only<1>{
        We call the binary string representation of a letter its \alert{codeword}.

        \cake{} How can we encode the word \emph{bad} with the above codewords?
    }

    \only<2>{
        Encoding the text with variable-length codewords, we get:

        \begin{center}
            \begin{tabular}{cccccccc}
                10 & 00 & 11 & 10 & 010 & 10 & 00 & 11 \\
                10 & 011 & 10 & 00 & 11 & 10 & 011 & 10 \\
                00 & 11 & 10 & 011 & 10 & 00 & 11 & 010 \\
                11 & 10 & 00 & 11 & 010 & 11 & 10 & 011 \\
                10 & 00 & 11 & 10 & \\
            \end{tabular}
        \end{center}

        The result is a binary string of length 56.
    }
\end{frame}

\begin{frame}
    \frametitle{Possible Ambiguity}

    Consider the following codewords:

    \begin{table}[h]
        \centering
        \begin{tabular}{|l|c|c|c|c|c|c|}
            \hline
            Character & a & e & d & c & b & f \\
            \hline
            Codewords & 10 & 11 & 01 & 001 & 011 & 010 \\
            \hline
        \end{tabular}
    \end{table}

    \cake{} Can you see the two say to interpret
    \begin{center}
        \Large{}
        0111001
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Prefix Code}

    To avoid ambiguity, we can will only consider \alert{prefix codes} in which
    no codeword is prefix of another.

    \cake{} Which of the following two codes are prefix codes?

    \begin{table}[h]
        \centering
        \begin{tabular}{|l|c|c|c|c|c|c|}
            \hline
            Character & a & e & d & c & b & f \\
            \hline
            Codewords & 10 & 11 & 00 & 001 & 011 & 010 \\
            \hline
        \end{tabular}
    \end{table}

    \begin{table}[h]
        \centering
        \begin{tabular}{|l|c|c|c|c|c|c|}
            \hline
            Character & a & e & d & c & b & f \\
            \hline
            Codewords & 10 & 11 & 01 & 001 & 011 & 010 \\
            \hline
        \end{tabular}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{Binary Tree Representation}

    We can represent the following code
    \begin{table}[h]
        \centering
        \input{../tables/code-variable-length.tex}
    \end{table}
    with a binary tree.
    \vspace{-1em}
    \begin{figure}[htpb]
        \centering
        \input{../graphs/binary-tree-01.tex}
        \caption{A Binary Tree Representation of the above codewords}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Binary Tree Representation of Prefix Codes}

    Let's require that in the binary tree presentation, each codeword is
    represented by a leaf.

    \cake{} Why does this guarantee the code is a prefix code?
    
    \begin{figure}[htpb]
        \centering
        \input{../graphs/binary-tree-01.tex}
        \caption{A binary tree representation of a code}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Optimal Code}
    
    Let $C$ be the set of letters in a text.

    For $c \in C$, let $c.freq$ be its frequency.

    Given a code represented by a binary $T$, let $d_{T}(c)$ be the depth of the
    \emoji{leaves} in $T$ for the codeword of $c$.

    Our aim is to find $T$ which minimize
    \begin{equation*}
        \sum_{c \in C} d_{T}(c) \cdot c.freq,
    \end{equation*}
    which we call the \alert{cost}.

    This is on average how much bits it take to encode one letter in the text.
\end{frame}

\begin{frame}
    \frametitle{Example of the Cost}
    
    \cake{} What is the cost
    \begin{equation*}
        \sum_{c \in C} d_{T}(c) \cdot c.freq,
    \end{equation*}
    for the following tree $T$.

    \begin{figure}[htpb]
        \centering
        \only<1>{\input{../graphs/binary-tree-01.tex}}
        \only<2>{\input{../graphs/binary-tree-02.tex}}
        \caption{A Tree $T$ representing a code}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Full Binary Tree}

    A \alert{full binary tree} is a binary tree where every node has either two
    children or no children.

    \cake{} Why a code being optimal implies that its binary tree representation is
    full?
    
    \begin{figure}[htpb]
        \centering
        \only{\input{../graphs/binary-tree-02.tex}}
        \caption{Not a full binary tree, not an optimal code}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Number of Leaves}
    
    A full binary tree with $n$ leaves has $n-1$ internal nodes.

    \puzzle{} Try to prove this by induction.

    \begin{figure}[htpb]
        \centering
        \input{../graphs/binary-tree-01.tex}
        \caption{A full binary tree with $5$ leaves and $4$ internal nodes}
    \end{figure}

    Thus the binary tree for an optimal code for $n$ letters contains $n-1$
    internal nodes.
\end{frame}

\begin{frame}
    \frametitle{Huffman Code}
    
    Huffman invented an algorithm to create an optimal code for a text with $n$ letters.

    It works as follows:

    \begin{itemize}
        \item Represent each letter with a leaf node.
        \item Select two nodes with the least frequencies and connect them with a
            parent node whose frequency is the sum of the two.
        \item Repeat this $n-1$ times is only one node left.
    \end{itemize}

    The \hint{} is that nodes with lower frequencies are merged earlier so that
    they are further away from the root.
\end{frame}

\begin{frame}
    \frametitle{Example of Huffman Code}
    
    The following code is the Huffman code for the given frequency
    table.
    \begin{table}[h]
        \centering
        \input{../tables/code-variable-length.tex}
    \end{table}

    \cake{} How can construct the Huffman code from the frequency table?
\end{frame}

\begin{frame}
    \frametitle{Min-priority Queue}
    
    To implement Huffman's algorithm, we need a
    data structure called \alert{min-priority queue},

    Let $Q$ denote such a queue.
    Then 
    \begin{itemize}
        \item $\textsc{Extract-Min}(Q)$ returns an object in the queue with the minimal 
        $freq$.
        \item $\textsc{Insert}(Q, z)$ stores the object of $z$ in the queue.
    \end{itemize}

    We assume the \emoji{stopwatch} complexity of each operation is $O(\log n)$.
\end{frame}

\begin{frame}
    \frametitle{Pseudocode}
    
    \begin{algorithmic}[1]
        \Procedure{Huffman}{$C$}
        \State $n \gets |C|$
        \State $Q \gets C$
        \For{$i \gets 1$ \textbf{to} $n-1$}
        \State $z \gets$ \textbf{new} Node
        \State $z.\text{left} \gets x \gets \Call{Extract-Min}{Q}$
        \State $z.\text{right} \gets y \gets \Call{Extract-Min}{Q}$
        \State $z.\text{freq} \gets x.\text{freq} + y.\text{freq}$
        \State \Call{Insert}{$Q, z$}
        \EndFor
        \State \Return \Call{Extract-Min}{$Q$} \Comment{Return the root of the tree}
        \EndProcedure
    \end{algorithmic}

    \cake{} What is the \emoji{stopwatch} complexity?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    The frequency table of
    \emph{aced cafe faced a bed}
    is the following
    \begin{center}
        \begin{tabular}{|c|c|c|c|c|c|}
            \hline
            b & f & c & d & a & e \\
            \hline
            0.0588 & 0.118 & 0.176 & 0.176 & 0.235 & 0.235 \\
            \hline
        \end{tabular}
    \end{center}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \medskip{}
            \cake{} What is the Huffman code?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{aced-cafe.jpg}
                \caption{Aced cafe faced a bed}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Lemma 16.2}
    
    Given an alphabet $C$ where each character $c \in C$ has frequency $c.freq$, 
    for the least frequent characters $x, y \in C$, there exists an optimal
    prefix code where codewords for $x$ and $y$ have equal length and differ
    only in the last bit.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{huffman-correctness-01.png}
        \caption{A proof of Lemma 16.2}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lemma 16.3}
    
    Given an alphabet $C$ with frequency $c.freq$ for each $c \in C$, let $x, y
    \in C$ be the characters with minimum frequencies. Construct $C' = C - \{x,
    y\} \cup \{z\}$ where $z.freq = x.freq + y.freq$. 

    For a tree $T'$
    representing an optimal prefix code for $C'$, the tree $T$ formed by
    replacing $T'$'s leaf node for $z$ with an internal node having $x$ and $y$
    as children, yields an optimal prefix code for $C$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 16.4}
    
    Procedure \textsc{Huffman} produces an optimal prefix code.
\end{frame}

 
\section{\acs{ita} 16.4 Matroids and greedy methods}

\begin{frame}{Definition of a Matroid}

A \alert{matroid} is an ordered pair \( M = (S, I) \) satisfying the
following conditions:

\begin{enumerate}[<+->]
    \item \( S \) is a finite set.
    \item \( I \) is a nonempty family of subsets of \( S \), called
        the \alert{independent subsets} of \( S \), such that if \( B \in
        I \) and \( A \subseteq B \), then \( A \in I \). We
        say that \( I \) is \alert{hereditary} if it satisfies this
        property. Note that the empty set \( \emptyset \) is necessarily a
        member of \( I \).
    \item If \( A \in I \), \( B \in I \), and \( |A| < |B|
        \), then there exists some element \( x \in B - A \) such that \( A \cup
        \{x\} \in I \). We say that \( M \) satisfies the
        \alert{exchange property}.
\end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Free Matroid}
    
    Let $I$ be the set containing all subsets of $S$. Then $M = (S, I)$ is a
    matroid called \alert{free matroid} over $S$.
\end{frame}

\begin{frame}{Graphic Matroid}
    As another example of matroids, consider the \alert{graphic matroid} \( M_G =
    (S_G, I_G) \) defined in terms of a given undirected graph \( G = (V,
    E) \) as follows:
    \begin{itemize}[<+->]
        \item The set \( S_G \) is defined to be \( E \), the set of edges of \( G \).
        \item If \( A \) is a subset of \( E \), then \( A \in I_G \) if and
            only if \( A \) is acyclic. That is, a set of edges \( A \) is
            independent if and only if the subgraph \( G_A = (V, A) \) forms a
            forest.
    \end{itemize}
    \only<+->{
        \begin{block}{Theorem 16.5}
            Let \( M_G = (S_G, I_G) \) be a graphic matroid defined
            above. Then \(M_G\) is a matroid.
        \end{block}
    }
\end{frame}

\begin{frame}
    \frametitle{Example of an Graphic Matroid}
    
    Let $G = C_{3}$, the cycle graph with three vertices.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{c3.pdf}
        \caption{$G = C_{3}$}
    \end{figure}

    \cake{} What is the graphic matroid $M_{G} = (S_G, I_G)$?
\end{frame}

\begin{frame}
\frametitle{Extensions in Matroids}

Given a matroid \( M = (S, I) \), we call an element \( x \notin A \)
an \alert{extension} of \( A \subseteq I \) if \( A \cup \{x\} \notin I \). 

\pause{}

As an example, consider a graphic
matroid \( M_G \). If \( A \) is an independent set of edges, then edge \( e \)
is an extension of \( A \) if and only if \( e \) is not in \( A \) and the
addition of \( e \) to \( A \) does not create a cycle.

\pause{}

An independent subset is called \alert{maximal} if it has no extensions.
\end{frame}

\begin{frame}
    \frametitle{Example of Extension}
    
    Consider the graphic matroid $M_G$ for $G = C_{3}$.

    \cake{} What is are the maximal independent subsets?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{c3.pdf}
        \caption{$G = C_{3}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Spanning Trees}
    
    A \alert{spanning tree} of a graph is a subgraph that includes all the
    vertices of the graph and is a single connected tree.
    
    \cake{}  Is this subgraph highlighted with green a spanning tree?

    \begin{figure}
        \centering
        \includegraphics<1>[width=0.4\linewidth]{subgraph-spanning.pdf}
        \includegraphics<2>[width=0.4\linewidth]{subgraph-spanning-tree.pdf}
    \end{figure}

    \hint{} A maximal independent subset in a graphic matroid $M_G$ is a spanning
    tree of $G$.
\end{frame}


%\begin{frame}
%    \frametitle{Theorem 16.6}
%    
%    All maximal independent subsets in a matroid have the same size.
%\end{frame}

\begin{frame}
    \frametitle{Weighted Matroid}

    A matroid \( M = (S, I) \) is \alert{weighted} if it
    is associated with a function \( w: S \mapsto (0, \infty)\). 

    \pause{}

    For any \( A \subseteq S \) we define
    \begin{equation*}
        w(A) = \sum_{x \in A} w(x)
    \end{equation*}

    \pause{}

    For example, if we let \( w(e) \) denote the weight of an edge \( e \) in a
    graphic matroid $M_G$, then $w(A)$ is the total weight of the edges in edge
    set $A$.
\end{frame}

\begin{frame}
    \frametitle{Maximum-weight Independent Subset in a Matroid}

    Given a weighted
    matroid \( M = (S, I) \), the goal is to discover an independent
    set \( A \in I \) that maximizes \( w(A) \).

    \begin{itemize}[<+->]
        \item An \alert{optimal subset} is independent and possesses the greatest possible weight within the matroid.
        \item Since \( w(x) > 0 \) for any \( x \in S \), an optimal subset is invariably a maximal independent subset.
        \item Maximizing \( A \) is always beneficial due to the positive weights.
    \end{itemize}
\end{frame}

\begin{frame}{Greedy Algorithm Pseudocode}
    \begin{algorithmic}[1]
        \Procedure{Greedy}{$M, w$}
        \State $A \gets \emptyset$
        \State sort $M.S$ into decreasing order by weight $w$
        \For{each $x \in M.S$}
        \If{$A \cup \{x\} \in M.I$}
        \State $A \gets A \cup \{x\}$
        \EndIf
        \EndFor
        \State \Return $A$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Minimal spanning trees}
    
    A \alert{minimal spanning tree} is a spanning tree of a weighted graph that
    minimizes the sum of the weights of its edges.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{Minimum-spanning-tree.png}
        \caption{A minimal spanning tree. Source: \href{https://en.wikipedia.org/wiki/Minimum_spanning_tree\#/media/File:Minimum_spanning_tree.svg}{Wikipedia}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Maximum-spanning Tree Problem}

    A \alert{maximum spanning tree} is a spanning tree of a weighted graph that
    minimizes the sum of the weights of its edges.

    This problem can be efficiently solved by the
    \emph{matroid greedy algorithm}:
    \begin{itemize}[<+->]
        \item Let $A = \emptyset$
        \item Sort the edge set $S$ in decreasing order of weight.
        \item For each edge $x \in S$, if $A \cup \{x\}$ does not contain an
            cycle, add $x$ to $A$.
        \item Return $A$.
    \end{itemize}

    \only<+->{\cake{} How can we convert the minimum spanning tree into a maximum spanning
    tree problem?}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 16.3.
                \item
                    Section 16.4.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 16.3: 1--8.
                \item
                    Exercises 16.4: 1, 5.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
