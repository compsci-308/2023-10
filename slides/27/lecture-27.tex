\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

\title{Lecture \lecturenum{} --- NP-Completeness (Part 3)}

\begin{document}

\maketitle

\begin{frame}[standout]
    \vspace{1em}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-evalution.jpg}
        \caption*{\emoji{pray}\textcolor{white}{Please use the next 15 minutes
        to write course evaluation.}}
    \end{figure}
\end{frame}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 34.4: 3-CNF Satisfiability.
                \item
                    Section 34.5 except the proof of Theorem 34.13 and
                    34.15.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 34.4: 2, 5-7.
                \item
                    Exercises 34.5: 1-3, 5-8.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{Sudoku and Satisfiability Problem}

\begin{frame}[c]
    \frametitle{Sudoku}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            In Sudoku, the objective is to fill a $9 \times 9$ 
            grid with digits so that 
            \begin{itemize}
                \item each column, 
                \item each row, 
                \item and each of the nine $3 \times 3$ subgrids 
            \end{itemize}
            contains all of the digits in $\{1,\dots, 9\}$.

            \medskip{}

            \smiling{} We can formulate a sudoku puzzle as an SAT problem!
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{sudoku.png}
                \caption{A Sudoku puzzle.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Convert Sudoku to SAT}
    
    Let $x_{r,c}^{v}$ be the boolean variable indicate that the digit $v$ is in
    the row $r$ and column $c$.

    Then one of the rules of sudoku can be expressed as:

    \begin{equation*}
        \left(\bigvee_{c=1}^{9} x_{2,c}^{4}\right)
        \wedge
        \left(\neg \bigvee_{1 \le c_1 < c_2 < 9}(x_{2,c_1}^{4} \vee x_{2,c_2}^{4})\right)
    \end{equation*}

    \cake{} Do you see which one?

    \emoji{monocle-face} See \href{%
        https://codingnest.com/modern-sat-solvers-fast-neat-underused-part-1-of-n/}%
        {Modern SAT solvers: fast, neat and underused}.
\end{frame}

\section{\acs{ita} 34.4 NP-Completeness Proofs}

\subsection{3-CNF Satisfiability}

\begin{frame}
    \frametitle{Aliens and Teddy-Bears}

    \only<1>{%
        Four \emoji{teddy-bear} can be coloured red on one side and blue on the other.

        Each of three of 3-armed \emoji{alien} grabs 3 teddy bear hands as shown
        below.

        \cake{} Can you colour the \emoji{teddy-bear} such that every \emoji{alien} is holding at least one blue hand?
    }%
    \only<2>{%
        How can this be converted to a boolean satisfiability
        problem?
    }%

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{alien-bear.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{3-CNF}

    A \alert{literal} is either a variable or its negation.

    A formula in \alert{conjunctive normal form (CNF)} is a \alert{conjunction}
    (AND) of \alert{clauses}, each being a \alert{disjunction} (OR) of one or more literals.

    A formula is in \alert{3-conjunctive normal form}, or \alert{3-CNF}, if 
    it is a CNF and each of its clause has \textit{exactly three distinct literals}.

    For example, the formula for the teddy bear problem is in 3-CNF
    \begin{equation*}
        \left( x_1 \lor x_2 \lor \neg x_1 \right) 
        \land 
        \left( \neg x_2 \lor x_3 \lor x_4 \right) 
        \land 
        \left( x_2 \lor \neg x_3 \lor \neg x_4 \right) 
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{3-CNF Satisfiability Problem}

    The 3-CNF-SAT is a decision problem which asks if a formula in 3-CNF
    is satisfiable.

    For example, the formula for the \emoji{teddy-bear} and \emoji{alien} problem
    \begin{equation*}
        \left( x_1 \lor x_2 \lor \neg x_1 \right) 
        \land 
        \left( \neg x_2 \lor x_3 \lor x_4 \right) 
        \land 
        \left( x_2 \lor \neg x_3 \lor \neg x_4 \right) 
    \end{equation*}
    is satisfiable.

    \cake{} What is a \emph{truth assignment} which satisfies this formula?
\end{frame}

\begin{frame}
    \frametitle{Theorem 34.10}

    \only<1>{%
        Satisfiability of boolean formulas in 3-CNF is NP-complete.
    }%

    Proof: 
    \only<1>{%
        We have seen that $\text{SAT} \in \text{NP}$, so $\text{3-CNF-SAT} \in \text{NP}$.

        So we only need to show that
        \begin{equation*}
            \text{SAT} \leq_p \text{3-CNF-SAT}
        \end{equation*}
        Given a formula like
        \begin{equation*}
            \phi = \left( (x_1 \rightarrow x_2) \vee \left( \neg(x_1 \leftrightarrow x_3) \vee x_4 \right) \right) \wedge \neg x_2.
        \end{equation*}
        we need to convert it into an equivalent 3-CNF formula (in
        polynomial-time).
    }%
    \only<2>{%
        We start by convert 
        \begin{equation*}
            \phi = \left( (x_1 \rightarrow x_2) \vee \left( \neg(x_1 \leftrightarrow x_3) \vee x_4 \right) \right) \wedge \neg x_2.
        \end{equation*}
        into a tree as follows:
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.4\textwidth]{sat-tree.png}
        \end{figure}
    }%
    \only<3>{%
        The next step is to convert the tree into a conjunction of formulas:
        \vspace{-1em}
        \begin{columns}
            \begin{column}{0.5\textwidth}
                \begin{equation*}
                    \begin{aligned}
                        \phi' 
                        & = y_1 \\
                        & \land (y_1 \leftrightarrow (y_2 \land \neg x_2)) \\
                        & \land (y_2 \leftrightarrow (y_3 \lor y_4)) \\
                        & \land (y_3 \leftrightarrow (x_1 \rightarrow x_2)) \\
                        & \land (y_4 \leftrightarrow \neg y_5) \\
                        & \land (y_5 \leftrightarrow (y_6 \lor x_4)) \\
                        & \land (y_6 \leftrightarrow (\neg x_1 \leftrightarrow x_3))
                    \end{aligned}
                \end{equation*}       
                Note that each clause has at most three literals.
            \end{column}
            \begin{column}{0.5\textwidth}
                \begin{figure}[htpb]
                    \centering
                    \includegraphics[width=\textwidth]{sat-tree.png}
                \end{figure}
            \end{column}
        \end{columns}
    }%
    \only<4>{%
        The final step is to convert clauses to a 3-CNF formula.

        For example the clause $c_{1} = y_1 \leftrightarrow (y_2 \land \neg x_2)$ has truth table
        \vspace{-0.5em}
        \begin{center}
            \small{}
            \begin{tabular}{ccc|c}
                $y_1$ & $y_2$ & $x_2$ & $c_{1}$ \\
                \hline
                1 & 1 & 1 & 0 \\
                1 & 1 & 0 & 1 \\
                1 & 0 & 1 & 0 \\
                1 & 0 & 0 & 0 \\
                0 & 1 & 1 & 1 \\
                0 & 1 & 0 & 0 \\
                0 & 0 & 1 & 1 \\
                0 & 0 & 0 & 1 \\
            \end{tabular}
        \end{center}
    }%
    \only<4->{%
        This implies that
        \begin{equation*}
            \neg c_{1}
            =
            (y_1 \land y_2 \land x_2) \lor
            (y_1 \land \neg y_2 \land x_2) \lor
            (y_1 \land \neg y_2 \land \neg x_2) \lor
            (\neg y_1 \land y_2 \land \neg x_2).
        \end{equation*}
    }%
    \only<5->{%
        Thus we can apply De-Morgan's laws to get
    }%
    \only<6>{%
        \begin{equation*}
            c_{1}
            =
            (\neg y_1 \lor \neg y_2 \lor \neg x_2) \land
            (\neg y_1 \lor y_2 \lor \neg x_2) \land 
            (\neg y_1 \lor y_2 \lor x_2) \land
            (y_1 \lor \neg y_2 \lor x_2).
        \end{equation*}
        And this is a 3-CNF formula.

        Other clauses with three literals can be converted to 3-CNF in similar ways.

        \cake{} What about clauses such as $y_4
        \leftrightarrow \neg y_5$ and $y_{1}$?
    }%
\end{frame}

\section{\acs{ita} 34.5 NP-complete Problems}

\subsection{The Clique Problem}

\begin{frame}
    \frametitle{NP-complete Proofs}

    In this section, we will prove a few more problems are NP-complete.
    
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            node distance=1cm, 
            font={\footnotesize},
            scale=0.8,
            every node/.style={
                scale=0.8, 
                rounded corners=0.1cm,
                fill=lightgray},
            auto]
            % Nodes
            \node[fill=MintGreen] (circuit) {CIRCUIT-SAT};
            \node[fill=MintGreen] (sat) [below of=circuit] {SAT};
            \node[fill=MintGreen] (3cnf) [below of=sat] {3-CNF-SAT};
            \node (clique) [below left of=3cnf, xshift=-1cm] {CLIQUE};
            \node (vertex) [below of=clique] {VERTEX-COVER};
            \node (ham) [below of=vertex] {HAM-CYCLE};
            \node (tsp) [below of=ham] {TSP};
            \node (subset) [below right of=3cnf, xshift=1cm] {SUBSET-SUM};

            % Paths
            \draw[->] (circuit) -- (sat);
            \draw[->] (sat) -- (3cnf);
            \draw[->] (3cnf) -- (clique);
            \draw[->] (3cnf) -- (subset);
            \draw[->] (clique) -- (vertex);
            \draw[->] (vertex) -- (ham);
            \draw[->] (ham) -- (tsp);
        \end{tikzpicture}
        \caption{The structure of NP-completeness proofs}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Cliques}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{Clique} ---
            a narrow exclusive circle or group of persons.

            \begin{flushright}
                --- \itshape{Merriam-Webster Dictionary}
            \end{flushright}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{clique-tv.jpg}
                \caption*{Clique is also a \emoji{uk} \emoji{tv} show}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Cliques in graphs}

    Given a graph $G = (V,E)$, 
    a subset of vertices $S$ is a \alert{clique} if there is an edge between any
    two vertices in $S$.

    \cake{} What is largest clique in $G$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.35\textwidth]{clique-number-01.pdf}
        \caption{A graph $G$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Clique Problem}
    
    The formal definition of the clique problem is a language
    \begin{equation*}
        \text{CLIQUE} = 
        \{\langle G, k \rangle: \text{$G$ has a clique of size $k$}\}
    \end{equation*}

    \cake{} What is decision problem corresponding to this language?

    \cake{} Can you think of a brute-force algorithm for the clique problem?
    What is the time complexity?
\end{frame}

\begin{frame}
    \frametitle{Theorem 34.11}
    
    \only<1>{%
        The clique problem is NP-complete.

        Proof: Given a subset $V' \subseteq V$, it takes only O($|V'|^2$) time
        to check if $V'$ is a clique.

        Thus the clique problem is in NP.

        \cake{} Where does $O(|V'|^2)$ come from?
    }%
    \only<2>{%
        The next step is to show $\text{3-CNF-SAT} \leq_p \text{CLIQUE}$.
        As an example, given 3-CNF $\phi = C_1 \land C_2 \land C_3$,
        we create a graph $G$ as follows:
    }%
    \only<3>{%
        Then $\phi$ is satisfiable if and only if $G$ has a clique of size $3$.

        The lightly coloured vertices is such a clique. Thus $\phi$ is satisfiable
        with $x_2 = 0$ and $x_{3} = 1$.
    }%
    \only<2-3>{%
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.9\textwidth]{3-cnf-to-clique.png}
        \end{figure}
    }%
    \only<2>{%
        \cake{} Can you see how the edges are added?
    }%
    \only<3>{%
        \cake{} Can this clique of size $3$ contain two vertices for the same
        clause?
    }%
\end{frame}

\subsection{The vertex-cover problem}

\begin{frame}
    \frametitle{Our Progress So Far}

    Only four to go!

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            node distance=1cm, 
            font={\footnotesize},
            scale=0.8,
            every node/.style={
                scale=0.8, 
                rounded corners=0.1cm,
                fill=lightgray},
            auto]
            % Nodes
            \node[fill=MintGreen] (circuit) {CIRCUIT-SAT};
            \node[fill=MintGreen] (sat) [below of=circuit] {SAT};
            \node[fill=MintGreen] (3cnf) [below of=sat] {3-CNF-SAT};
            \node[fill=MintGreen] (clique) [below left of=3cnf, xshift=-1cm] {CLIQUE};
            \node (vertex) [below of=clique] {VERTEX-COVER};
            \node (ham) [below of=vertex] {HAM-CYCLE};
            \node (tsp) [below of=ham] {TSP};
            \node (subset) [below right of=3cnf, xshift=1cm] {SUBSET-SUM};

            % Paths
            \draw[->] (circuit) -- (sat);
            \draw[->] (sat) -- (3cnf);
            \draw[->] (3cnf) -- (clique);
            \draw[->] (3cnf) -- (subset);
            \draw[->] (clique) -- (vertex);
            \draw[->] (vertex) -- (ham);
            \draw[->] (ham) -- (tsp);
        \end{tikzpicture}
        \caption{The structure of NP-completeness proofs}
    \end{figure}
\end{frame}
\begin{frame}
    \frametitle{Lights and trails}
    
    The follow graph depicts hiking trails in a forest.

    \begin{figure}[htpb]
        \centering

        \begin{tikzpicture}[
            every node/.style={graphnode},
            every path/.style={graphedge},
            scale=1.2
            ]
            \node (u) at (1,1) {u};
            \node (v) at (2,1) {v};
            \node (w) at (3,0) {w};
            \node (x) at (2,-1) {x};
            \node (y) at (1,-1) {y};
            \node (z) at (0,0) {z};

            \draw (v) -- (w);
            \draw (w) -- (x);
            \draw (z) -- (v);
            \draw (u) -- (w);
            \draw (z) -- (y);
        \end{tikzpicture}
    \end{figure}

    The city wants to build some \emoji{hut} in the forest at the conjunction of
    the trails so that each trail has at least one \emoji{hut}.

    \cake{} Can you think of a way to put the \emoji{hut}?

    \hint{} In graph theory, this is asking for a vertex cover.
\end{frame}

\begin{frame}
    \frametitle{Vertex Cover}

    Given a graph $G = (V,E)$, a subset of vertices $V'$ is a \alert{vertex cover}
    if for every edge $(u,v) \in E$, either $u \in V'$ or $v \in V'$.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            every node/.style={graphnode},
            every path/.style={graphedge},
            scale=1.2
            ]
            \node[fill=MintGreen] (u) at (1,1) {u};
            \node[fill=MintGreen] (v) at (2,1) {v};
            \node (w) at (3,0) {w};
            \node[fill=MintGreen] (x) at (2,-1) {x};
            \node[fill=MintGreen] (y) at (1,-1) {y};
            \node (z) at (0,0) {z};

            \draw (v) -- (w);
            \draw (w) -- (x);
            \draw (z) -- (v);
            \draw (u) -- (w);
            \draw (z) -- (y);
        \end{tikzpicture}
        \caption{The set of vertices $\{u,v,x,y\}$ forms a vertex cover}
    \end{figure}


    The vertex cover problem is formally defined as the language
    \begin{equation*}
        \text{VERTEX-COVER} = 
        \{\langle G, k \rangle: \text{$G$ has a vertex cover of size $k$}\}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 34.12}
    \only<1>{%
        The vertex-cover problem is NP-complete.

        Proof: Given a subset of vertices $V' \subseteq V$ with $\abs{V'} = k$,
        it takes only polynomial time to check for every edge $(u,v) \in E$ if
        $u \in V'$ or $v \in V'$.

        Thus the vertex-cover problem is in NP.
    }%
    \only<2>{%
        Give a graph $G = (V,E)$ its \alert{complement graph} 
        is defined by $\overline{G} = (V, \overline{E})$
        where
        \begin{equation*}
            \overline{E} = \{ (u,v) : (u,v) \notin E\}
        \end{equation*}

        We will show that $G$ has a clique of size $k$ if and only if $G$ has a vertex cover of
        size $\abs{V} - k$.

        \cake{} Why does this imply $\text{CLIQUE} \leq_p \text{VERTEX-COVER}$?
        \begin{figure}[htpb]
            \centering
            \hfill
            \begin{tikzpicture}[
                every node/.style={graphnode, scale=0.8},
                every path/.style={graphedge},
                scale=1
                ]
                \node (u) at (1,1) {u};
                \node (v) at (2,1) {v};
                \node[fill=MintGreen] (w) at (3,0) {w};
                \node (x) at (2,-1) {x};
                \node (y) at (1,-1) {y};
                \node[fill=MintGreen] (z) at (0,0) {z};

                \draw (u) -- (v);
                \draw (z) -- (u);
                \draw (z) -- (w);
                \draw (x) -- (y);
                \draw (v) -- (y);
                \draw (x) -- (u);
                \draw (z) -- (x);
                \draw (w) -- (y);
                \draw (u) -- (y);
                \draw (v) -- (x);
            \end{tikzpicture}
            \hfill
            \begin{tikzpicture}[
                every node/.style={graphnode, scale=0.8},
                every path/.style={graphedge},
                scale=1.2
                ]
                \node[fill=MintGreen] (u) at (1,1) {u};
                \node[fill=MintGreen] (v) at (2,1) {v};
                \node (w) at (3,0) {w};
                \node[fill=MintGreen] (x) at (2,-1) {x};
                \node[fill=MintGreen] (y) at (1,-1) {y};
                \node (z) at (0,0) {z};

                \draw (v) -- (w);
                \draw (w) -- (x);
                \draw (z) -- (v);
                \draw (u) -- (w);
                \draw (z) -- (y);
            \end{tikzpicture}
            \hfill
            \caption{A graph $G$ and its complement $\overline{G}$}
        \end{figure}
    }%
    \only<3-4>{%
        Let's prove this by showing that 
        \begin{itemize}
            \item<3-> if $G$ has clique $V'$, 
                then $V-V'$ is a vertex cover of $\overline{G}$.
            \item<4> if $\overline{G}$ has vertex cover $V'$, 
                then $V-V'$ is a clique of $G$.
        \end{itemize}
    }%
\end{frame}

\subsection{The Hamiltonian-Cycle Problem}

\begin{frame}
    \frametitle{Our Progress So Far}

    Just three left!

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            node distance=1cm, 
            font={\footnotesize},
            scale=0.8,
            every node/.style={
                scale=0.8, 
                rounded corners=0.1cm,
                fill=lightgray},
            auto]
            % Nodes
            \node[fill=MintGreen] (circuit) {CIRCUIT-SAT};
            \node[fill=MintGreen] (sat) [below of=circuit] {SAT};
            \node[fill=MintGreen] (3cnf) [below of=sat] {3-CNF-SAT};
            \node[fill=MintGreen] (clique) [below left of=3cnf, xshift=-1cm] {CLIQUE};
            \node[fill=MintGreen] (vertex) [below of=clique] {VERTEX-COVER};
            \node (ham) [below of=vertex] {HAM-CYCLE};
            \node (tsp) [below of=ham] {TSP};
            \node (subset) [below right of=3cnf, xshift=1cm] {SUBSET-SUM};

            % Paths
            \draw[->] (circuit) -- (sat);
            \draw[->] (sat) -- (3cnf);
            \draw[->] (3cnf) -- (clique);
            \draw[->] (3cnf) -- (subset);
            \draw[->] (clique) -- (vertex);
            \draw[->] (vertex) -- (ham);
            \draw[->] (ham) -- (tsp);
        \end{tikzpicture}
        \caption{The structure of NP-completeness proofs}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 34.13}

    The hamiltonian cycle problem is NP-complete.

    Proof Strategy: It is obvious (\emoji{unamused}) that $\text{HAM-CYCLE}$ is in NP.

    Thus we only need to prove that $\text{VERTEX-COVER} \leq_{p} \text{HAM-CYCLE}$.

    Given a graph $G = (V,E)$ and an integer $k$,
    we construct a graph $G'$ such that $G$ has a vertex cover of size $k$ if
    and only if $G'$ has a hamiltonian cycle.

    \zany{} The details of this proof is not required.
\end{frame}

\subsection{The Travelling Salesman Problem}

\begin{frame}
    \frametitle{Our Progress So Far}

    Only two! 11:15 AM is not far!

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            node distance=1cm, 
            font={\footnotesize},
            scale=0.8,
            every node/.style={
                scale=0.8, 
                rounded corners=0.1cm,
                fill=lightgray},
            auto]
            % Nodes
            \node[fill=MintGreen] (circuit) {CIRCUIT-SAT};
            \node[fill=MintGreen] (sat) [below of=circuit] {SAT};
            \node[fill=MintGreen] (3cnf) [below of=sat] {3-CNF-SAT};
            \node[fill=MintGreen] (clique) [below left of=3cnf, xshift=-1cm] {CLIQUE};
            \node[fill=MintGreen] (vertex) [below of=clique] {VERTEX-COVER};
            \node[fill=MintGreen] (ham) [below of=vertex] {HAM-CYCLE};
            \node (tsp) [below of=ham] {TSP};
            \node (subset) [below right of=3cnf, xshift=1cm] {SUBSET-SUM};

            % Paths
            \draw[->] (circuit) -- (sat);
            \draw[->] (sat) -- (3cnf);
            \draw[->] (3cnf) -- (clique);
            \draw[->] (3cnf) -- (subset);
            \draw[->] (clique) -- (vertex);
            \draw[->] (vertex) -- (ham);
            \draw[->] (ham) -- (tsp);
        \end{tikzpicture}
        \caption{The structure of NP-completeness proofs}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Travelling Salesman Problem}
    Formally, the travelling salesman problem is defined by
    \begin{equation*}
        \begin{aligned}
            \text{TSP} = \{&(G, c, k) : \\
                    &G = (V, E) \text{ is a complete graph,} \\
                    &c \text{ is a function from } V \times V \rightarrow \mathbb{Z}, \\
                    &k \in \mathbb{Z}, \text{ and} \\
                    &G \text{ has a traveling-salesman tour with cost at most } k \}.
            \end{aligned}
        \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 34.14}

    The travelling-salesman problem is NP-complete.

    Proof: It is obvious (\emoji{unamused}) that $\text{TSP}$ is in NP.

    Thus we only need to prove that $\text{HAM-CYCLE} \leq_{p} \text{TSP}$.

    Given a graph $G = (V,E)$, we create a graph $G'$ and a cost function $c$
    such that $G$ has
    a hamiltonian cycle if and only if $G'$ has a travelling-salesman tour of
    cost at most $\abs{V}$.
\end{frame}

\begin{frame}
    \frametitle{Reduction}

    We choose $G'$ to be the complete graph with vertex set $V$.

    \cake{} How should we assign the cost of each
    edge?
    \begin{figure}[htpb]
        \centering
        \hfill{}
        \includegraphics[width=0.4\textwidth]{hamiltonian-graph-small.pdf}
        \hfill{}
        \includegraphics[width=0.4\textwidth]{hamiltonian-to-tsp.pdf}
        \hfill{}
        \caption{Example of $G$ and $G'$}
    \end{figure}
\end{frame}

\subsection{Subset Sum Problem}

\begin{frame}
    \frametitle{Our Progress So Far}

    The last one!

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            node distance=1cm, 
            font={\footnotesize},
            scale=0.8,
            every node/.style={
                scale=0.8, 
                rounded corners=0.1cm,
                fill=lightgray},
            auto]
            % Nodes
            \node[fill=MintGreen] (circuit) {CIRCUIT-SAT};
            \node[fill=MintGreen] (sat) [below of=circuit] {SAT};
            \node[fill=MintGreen] (3cnf) [below of=sat] {3-CNF-SAT};
            \node[fill=MintGreen] (clique) [below left of=3cnf, xshift=-1cm] {CLIQUE};
            \node[fill=MintGreen] (vertex) [below of=clique] {VERTEX-COVER};
            \node[fill=MintGreen] (ham) [below of=vertex] {HAM-CYCLE};
            \node[fill=MintGreen] (tsp) [below of=ham] {TSP};
            \node (subset) [below right of=3cnf, xshift=1cm] {SUBSET-SUM};

            % Paths
            \draw[->] (circuit) -- (sat);
            \draw[->] (sat) -- (3cnf);
            \draw[->] (3cnf) -- (clique);
            \draw[->] (3cnf) -- (subset);
            \draw[->] (clique) -- (vertex);
            \draw[->] (vertex) -- (ham);
            \draw[->] (ham) -- (tsp);
        \end{tikzpicture}
        \caption{The structure of NP-completeness proofs}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{A Party Puzzle}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            You are organizing a party for your 15th birthday.

            \medskip{}

            You want to invite some of your
            friends, whose names are $2, 3, 4, 7, 9$.

            \medskip{}

            You are very superstitious and want the name of guests names to sum
            up to $15$.

            \medskip{}

            \cake{} Is it possible?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-party.jpg}
                \caption{Do you get a $15$ party?}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Theorem 34.15}
    The subset-sum problem defined by
    \begin{equation*}
        \text{SUBSET-SUM} = \left\{\langle S, t \rangle : 
            \text{there exists $S' \subseteq S$ such that $\sum_{x \in S'} x = t$}\right\}
    \end{equation*}
    is NP-complete.

    Proof: Obviously $\text{SUBSET-SUM}$ (\emoji{unamused}) is in NP.

    Thus we only need to prove that $\text{3-CNF-SAT} \leq_{p} \text{SUBSET-SUM}$.

    \zany{} The details of this proof is not required.
\end{frame}

\end{document}
