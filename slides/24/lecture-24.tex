\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Hash Tables (Part 2)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 11.1-11.2.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 11.1: 1-4.
                \item
                    Exercises 11.2: 2-6.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 11.3 Hash Functions}

\subsection{What Is a Good Hash Function?}

\begin{frame}
    \frametitle{Good Hash Function}
    
    A good hash function approximates \alert{simple uniform hashing} , where each key
    is \emph{equally likely} to be hashed into any slot.

    \weary{}
    However, this is challenging to verify due to unknown key distributions.

    \cake{} Are Duke Unique ID uniformly distributed?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{duke-card.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{When Distribution of Keys is Known}

    Occasionally, we do know the distribution of keys.

    \cake{} If the keys are uniform random on $[0,1)$, 
    such as
    \begin{equation*}
        \begin{aligned}
            0.7987709139743563 \\
            0.5162029990085875 \\
            0.33280261737525896
        \end{aligned}
    \end{equation*}
    what would be a good choice of $h$ for a hash table with slots $0, 1, \dots,
    m-1$?
\end{frame}

\begin{frame}[c]
    \frametitle{When Distribution of Keys is Unknown}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Heuristics are often used to design efficient hash functions.

            \bigskip{}

            A rule-of-\emoji{+1} is to choose a hash function that is independent of 
            (not affected by) any patterns in the key distribution.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-hash-function.jpg}
                \caption{Using heuristics means making the best guess and \emoji{pray}
                the hash function is good.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}


\begin{frame}
    \frametitle{Interpreting Keys as Natural Numbers}

    In the rest of this lecture, we will use natural numbers $\dsN$ as \emoji{key}.

    Thus, keys of strings need to be converted to natural numbers.

    One way to do so is using ASCII code:
    \begin{equation*}
        \text{Shouzhifan} = \texttt{0x53686f757a686966616e}
    \end{equation*}

    \begin{tcolorbox}[title={Hexadecimal Numbers}]
        Hexadecimal numbers use base (radix) $16$ to represent numbers.
        The letters \texttt{a}, \texttt{b}, \dots,  \texttt{f} represent $10, 11
        \dots, 16$. For example $\texttt{0xc} = 12$.

        \cake{} What is \texttt{0x1a} in decimal?
    \end{tcolorbox}
\end{frame}

\subsection{The Division Method}

\begin{frame}
    \frametitle{The Division Method}
    The \alert{division method} chooses a hash function
    \begin{equation*}
        h(k) = k \bmod m
    \end{equation*}
    where $m$ is the number of slots.

    \only<1>{%
        Some $m$ should be avoided, such as $m = 2^p$ with $p \in \dsN$.

        For example, if $m = 2^4 = 16$,
        then
        \begin{equation*}
            \begin{aligned}
                &
                h(\texttt{0x616\textcolor{red}{e}}) =
                \texttt{0x\textcolor{red}{e}} \\
                &
                h(\texttt{0x313\textcolor{red}{a}}) =
                \texttt{0x\textcolor{red}{a}}
            \end{aligned}
        \end{equation*}

        \cake{} Why is this a \emoji{-1} choice?
    }%
    \only<2>{%
        \begin{tcolorbox}[title={\tps{}}]
            When the keys are hexadecimal numbers, is the hash function
            \begin{equation*}
                h(k) = k \bmod 2^{4} - 1
            \end{equation*}
            a good choice?

            \hint{} Check what are $h(\texttt{0xa6})$ and $h(\texttt{0x6a})$.
        \end{tcolorbox}
    }%
    \only<3->{%
        A prime that is not to close to $2^{p}$ is often a \emoji{+1} choice in
        practice.

        For example, taking $m = 23 = \texttt{0x17}$, then
    }%
    \only<3>{%
        \begin{equation*}
            \begin{aligned}
                h( \texttt{0xa56} ) & = \texttt{0x01} \\
                h( \texttt{0x6a5} ) & = \texttt{0x16} \\
                h( \texttt{0x65a} ) & = \texttt{0x10}
            \end{aligned}
        \end{equation*}
        In other words, keys containing the same digits will not always be mapped
        to the same slot.
    }%
    \only<4>{%
        \begin{equation*}
            \begin{aligned}
                h( \texttt{0xa6} ) & = \texttt{0x05} \\
                h( \texttt{0xb6} ) & = \texttt{0x15} \\
                h( \texttt{0xc6} ) & = \texttt{0x0e}
            \end{aligned}
        \end{equation*}
        In other words, keys containing the same last few digits will not always 
        be mapped to the same slot.
    }%
\end{frame}

\subsection{The Multiplication Method}

\begin{frame}
    \frametitle{The Multiplication Method}

    The multiplication method chooses a real number $A \in (0, 1)$ and uses the
    hash function
    \begin{equation*}
        h(k) = \floor{m \times (A k \bmod 1)}
    \end{equation*}
    where $A k \bmod 1$ is just the fractional part of $Ak$.

    Let's try computing $h(15)$ if $A = 0.5$ and $m = 2^{4} = 16$.
\end{frame}

\begin{frame}
    \frametitle{Another Way to Compute}

    Assume that
    \begin{itemize}
        \item $k \in [0, 2^{w}-1]$, i.e., it has at most $w$ bits,
        \item and that $A = s/2^{w}$ for some $s \in \{1, 2, \dots, 2^{w}-1\}$,
        \item $m = 2^{p}$.
    \end{itemize}
    Then the multiplication method can be as done as follows:
    \vspace{-0.5em}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{hash-table-multiplication-method.png}
    \end{figure}
    \vspace{-0.5em}
    \smiling{} Doing so is more efficient on a \emoji{computer} with word size $w$.
\end{frame}

\begin{frame}
    \frametitle{Example of Multiplication Method}
    What is $h(15)$ if $A = 0.5$, $w = 8$, $p = 4$ and $m = 2^{p} = 16$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{hash-table-multiplication-method.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Advantage of Multiplication Method}

    \smiling{} Using the multiplication method, the choice of $m$ is not critical anymore.

    Although any $A$ works, Knuth suggested that
    \begin{equation*}
        A \approx \frac{\sqrt{5}-1}{2}
    \end{equation*}
    is likely work better than other choices.

    \emoji{wink} Ask Knuth why this is true next time you see him.
\end{frame}

\section{\acs{ita} 11.4 Open Addressing}

\begin{frame}[c]
    \frametitle{Open Addressing}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            In \alert{open addressing}, elements occupies the hash table (array) itself.

            \medskip{}

            No elements are store outside the table in lists.

            \medskip{}

            \smiling{} If the hash table is full, then no further insertion is possible.

            \medskip{}

            \weary{} It saves memory by reducing unused slots in the table.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-hash-table-full.jpg}
                \caption{What may happen in open addressing.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Probe}

    In open addressing, the hash function is of the form
    \begin{equation*}
        h : U \times \{ 0, 1, \dots, m-1 \} \to \{ 0, 1, \dots, m-1 \}
    \end{equation*}

    \pause{}

    When we try to insert a key $k$ to the table, we \emph{probe} a sequence of
    slots
    \begin{equation*}
        \langle h(k, 0), h(k, 1), \dots, h(k, m-1) \rangle
    \end{equation*}
    until we find an empty slot.

    \bomb{} To make sure every slot is probed eventually, we require this sequence to be a
    permutation of $\{0, 1, \dots, m-1\}$.
\end{frame}

\begin{frame}
    \frametitle{Pseudocode}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \small{}
            \begin{algorithmic}[1]
                \Procedure{Hash-Insert}{T, k}
                \State $i = 0$
                \Repeat
                \State $j = h(k, i)$
                \If{$T[j] = \textsc{NIL}$}
                \State $T[j] = k$
                \State \Return $j$
                \Else
                    \State $i=i+1$
                \EndIf
                \Until $i == m$
                \EndProcedure
                \State \textbf{error} ``Table if full \emoji{cry}''
            \end{algorithmic}
        \end{column}
        \begin{column}{0.5\textwidth}
            \only<2>{%
                \small{}

                \begin{algorithmic}[1]
                    \Procedure{Hash-Search}{$T, k$}
                    \State $i = 0$
                    \Repeat
                    \State $j = h(k, i)$
                    \If{$T[j] == k$}
                    \State \Return $j$ \Comment{Found it}
                    \Else
                        \State $i=i+1$
                    \EndIf
                    \Until $i == m$ \bf{or} $T[j] == \textsc{NIL}$
                    \EndProcedure
                    \State \Return{\textsc{NIL}}
                \end{algorithmic}

                \cake{} Why do we end the search when $T[j] == \textsc{NIL}$?
            }
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{The Problem of Delete}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \small{}

            \begin{algorithmic}[1]
                \Procedure{Hash-Delete}{$T, i$}
                \State $T[i] = \textsc{NIL}$ \Comment{Delete slot $i$}
                \EndProcedure
            \end{algorithmic}

            \medskip{}

            \dizzy{} Can you see what is wrong with the following code?

            \medskip{}

            \hint{} Look at \textsc{Hash-Search} again.

            \medskip{}

            \cake{} What is your solution?
        \end{column}
        \begin{column}{0.5\textwidth}
            \small{}
            \begin{algorithmic}[1]
                \Procedure{Hash-Search}{$T, k$}
                \State $i = 0$
                \Repeat
                \State $j = h(k, i)$
                \If{$T[j] == k$}
                \State \Return $j$ \Comment{Found it}
                \EndIf
                \Until $i == m$ \bf{or} $T[j] == \textsc{NIL}$
                \EndProcedure
                \State \Return{\textsc{NIL}}
            \end{algorithmic}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Uniform Hashing}

    In our analysis, we will assume that for any $k$,
    the sequence
    \begin{equation*}
        \langle h(k, 0), h(k, 1), \dots, h(k, m-1) \rangle
    \end{equation*}
    is equally likely to be any permutation of $\{0, 1, \dots, m-1\}$.

    This is called \alert{uniform hashing}.

    \weary{} It is difficult to achieve uniform hashing in practice.

    \smiling{} But there are good approximation implementations.
\end{frame}

\begin{frame}
    \frametitle{Linear Probing}
    One way to implement open addressing is to use
    \begin{equation*}
        h(k, i) = (h'(k) + i) \bmod m,
    \end{equation*}
    where $h':U \mapsto \{ 0, 1, \dots, m-1 \}$ is a normal hashing function.

    This is called \alert{linear probing} and its probing sequence is
    \begin{equation*}
        \langle h'(k), h'(k)+1, h'(k)+2, \dots, m, 0, 1, \dots, h'(k)-1 \rangle
    \end{equation*}
    \cake{} Is this a uniform hashing?
\end{frame}

\begin{frame}
    \frametitle{The Problem of Linear Probing}
    \smiling{} Linear probing is essay to implement.

    \emoji{weary} But it often has long runs of occupied slots called \alert{primary clustering}.

    \cake{} If there are $l$ occupied slots before an empty slot $j$,
    what is the probability that insertion of $k$ will occupy slot $j$,
    assuming that each slot is equally likely to be $h'(k)$?
\end{frame}

\begin{frame}
    \frametitle{Quadratic Probing}

    An improvement over linear probing is
    \begin{equation*}
        h(k, i) = (h'(k) + c_1 i + c_2 i^{2}) \bmod m,
    \end{equation*}
    where $h':U \mapsto \{ 0, 1, \dots, m-1 \}$ is a normal hashing function.
    This is called \alert{quadratic probing}.

    \smiling{} Quadratic probing works better than linear probing.

    \weary{} But it has two problems:
    \begin{itemize}
        \item the choices of $c_1, c_2, m$ are limited,
        \item there are still some mild clustering.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Double Probing}

    The best probing method for open addressing is double probing which uses
    \begin{equation*}
        h(k, i) = (h_1'(k) + i \times h_2'(k)) \bmod m,
    \end{equation*}
    where $h_1'$ and $h_2'$ are normal hashing functions.

    For example if $h_{1}(k) = 80$, $h_{2}(k) = 257$, $m = 701$, then the probing
    sequence would be
    \begin{equation*}
        \langle 80, 337, 594, 150, 407, \dots \rangle
    \end{equation*}

    \smiling{} Double probing with $m$ be a prime or a power of $2$ is a good
    approximation of uniform hashing.
\end{frame}

\begin{frame}
    \frametitle{\emoji{game-die} Game of Hash Tables}

    Professor C introduces setups a hash table with $11$ slots, some of which
    are already occupied and gives the following hashing function
    \begin{equation*}
        h(k, i) = ((k \bmod 7) + i (k \bmod 5)) \bmod 11,
    \end{equation*}

    Then, Professor C selects a random key $k$.

    Subsequently, each \emoji{student} takes turns to compute the subsequent slot in the
    probing sequence using the double hashing technique. 

    The \emoji{student} who successfully does this is
    rewarded with a \emoji{bubble-tea}.
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.6}
    Given an open-address hash table with load factor $\alpha = n/m < 1$, the expected
    number of probes in an unsuccessful search is at most $1/(1-\alpha)$, 
    assuming uniform hashing.

    \only<2>{
        \begin{tcolorbox}[title={Corollary 11.7}]
        Insertion into an open-address hash table with load factor $\alpha = n/m
        < 1$ requires at most $1/(1-\alpha)$ probes on average,
        assuming uniform hashing.
        \end{tcolorbox}
    }
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.8}
    Given an open-address hash table with load factor $\alpha = n/m < 1$, the expected
    number of probes in an successful search is at most
    \begin{equation*}
        \frac{1}{\alpha} \log(\frac{1}{1-\alpha})
    \end{equation*}
    assuming uniform hashing and that each key is equally likely to be searched.
\end{frame}

\end{document}
