\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Quicksort}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 7.1-7.4.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 7.1: 1-3.
                \item
                    Exercises 7.2: 1-5.
                \item
                    Exercises 7.3: 1-2.
                \item
                    Exercises 7.4: 1-5.
                \item
                    Problems 7-1.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 7.1 Description of Quicksort}

\begin{frame}
    \frametitle{Overview of Quicksort}
    
    Quicksort is a divide-and-conquer sorting algorithm.

    Given an array $A[p \dots r]$, the algorithm works as follows:
    \begin{itemize}
        \item Divide: partition $A[p \dots r]$ into $A[p \dots q-1]$ 
            and $A[q+1 \dots r]$ such that 
            \begin{equation*}
                \begin{aligned}
                    A[j] & \leq A[q] 
                    \qquad \text{for all } j \in \{p, \dots, q-1\} \\
                    A[q] & \geq A[j] 
                    \qquad \text{for all } j \in \{q+1, \dots, r\}.
                \end{aligned}
            \end{equation*}
            The number $A[q]$ is call the \alert{pivot} .
        \item Conquer: Sort $A[p \dots q-1]$ and $A[q+1 \dots r]$.
        \item Combine: \laughing{} No work needed!
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Pseudocode}

    The pseudocode for Quicksort is as follows:
    \vspace{-1em}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \only<1-2>{%
                \begin{algorithmic}
                    \Procedure{Quicksort}{$A, p, r$}
                    \If{$p < r$}
                    \State $q = \textsc{Partition}(A, p, r)$
                    \State $\textsc{Quicksort}(A, p, q-1)$
                    \State $\textsc{Quicksort}(A, q+1, r)$
                    \EndIf
                    \EndProcedure
                \end{algorithmic}
            }%
            \cake{} What does \textsc{Quicksort} check if $p<r$?
        \end{column}
        \begin{column}{0.5\textwidth}
            \only<2>{%
                \begin{algorithmic}
                    \Procedure{Partition}{$A, p, r$}
                    \State $x = A[r]$ \Comment{The pivot}
                    \State $i = p - 1$
                    \For{$j = p \dots r-1$}
                    \If{$A[j] \leq x$}
                    \State $i = i + 1$
                    \State swap $A[i]$ with $A[j]$
                    \EndIf
                    \EndFor
                    \State swap $A[i+1]$ with $A[r]$
                    \State \Return $i+1$
                    \EndProcedure
                \end{algorithmic}
            }%
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example Run}
    
    \begin{figure}[htpb]
        \centering
        \begin{overlayarea}{0.8\textwidth}{4cm} % Adjust the height to fit your images
            \foreach \n in {1,...,8}{% start from 1, go up to 8
                \only<\n>{%
                    \pgfmathtruncatemacro{\imagenumber}{\n - 1} % subtract 1 for the actual image number
                    \includegraphics[width=\textwidth]{quicksort-partition-tile-0\imagenumber.png}
                    \includegraphics[width=\textwidth]{quicksort-partition-tile-0\n.png}
                }%
            }%
        \end{overlayarea}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The correctness of \textsc{Partition}}

    At the beginning of each iteration in \textsc{Partition},
    the situation in the following picture holds true.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{quicksort-partition.png}
        \caption*{Situation before each iteration}
    \end{figure}
\end{frame}

\section{\acs{ita} 7.2 Performance of Quicksort}

\begin{frame}
    \frametitle{Worst Case}

    \weary{} In the worst case, quicksort partitions the array $A[1\dots n]$
    into an empty array and an array of size $n-1$.

    This happens when $A[n]$ is the largest or the smallest element.

    In this case, the running time satisfies
    \begin{equation*}
        T(n) 
        = T(n-1) + T(0) + \Theta(n)
        = T(n-1) + \Theta(n).
    \end{equation*}

    \cake{} If the same holds for $T(n-1), T(n-2), \dots $, what is the
    time complexity?
\end{frame}

\begin{frame}
    \frametitle{Best Case}

    \smiling{} In the best case, quicksort partitions the array $A[1\dots n]$
    into two arrays of size $\ceil{n/2}-1$ and $\floor{n/2}$.

    In this case, the running time satisfies
    \begin{equation*}
        T(n) 
        = 2 T\left(\frac{n}{2}\right) + \Theta(n)
    \end{equation*}

    \cake{} If the same holds for $T(n/2), T(n/4), \dots$, what is the
    time complexity?
\end{frame}

\begin{frame}
    \frametitle{Average Case}

    \cool{} On average, the running time of quicksort is close to the best case.

        To see this, assume that for any $n$
        \begin{equation*}
            T(n) = T\left(\frac{n}{10}\right) + T\left(\frac{9 n}{10}\right) + c n
        \end{equation*}
        In other words, partition is quite unbalanced each time, but not as
        bad as in the worst case.

        Checking the recursion tree, we see that
        \begin{equation*}
            T(n) = O(n \log n)
        \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Recursion Tree}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{quicksort-recursion-tree.png}
        \caption*{The recursion tree for $1/10$ to $9/10$ split}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Intuition for the average case}

    Assume that the ranks of the elements of $A[1\dots n]$ are uniformly
    random.

    Then
    \begin{equation*}
        \p{[\rank(n) < \alpha n] \cup [\rank(n) > (1-\alpha) n]} \le 2 \alpha,
    \end{equation*}
    which is small if $\alpha$ is small.

    \cool{} In other words, most of the time, the partition is not too unbalanced.

    If the splits at every level are within $[\alpha, 1-\alpha]$
    for any $\alpha \in (0, 1/2)$, 
    then the time complexity is $O(n \log n)$.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \emoji{nerd-face} Assume that you have 1000 \emoji{closed-book} titled
    \begin{equation*}
        \langle 1, 2, 3, \dots, 997, 998, 999, 1000 \rangle
    \end{equation*}
    and that you have ordered them perfectly in the increasing order of their
    names on your bookshelf.

    \weary{} You careless roommate turn this into
    \begin{equation*}
        \langle 1, 2, 3, \dots, 997, 1000, 998, 999 \rangle
    \end{equation*}

    \pause{}

    If you want to put the books back into the right order,
    which sorting algorithms should you choose:
    \begin{itemize}
        \item Quicksort
        \item Merge Sort
        \item Insertion Sort
        \item Radix Sort
    \end{itemize}
\end{frame}

\section{\acs{ita} 7.3 Randomized Quicksort}

\begin{frame}
    \frametitle{Randomized Quicksort}

    We have assumed that the ranks of the elements of $A$ are uniformly
    random.

    \weary{} This usually is not true in the real-word.

    To achieve average performance derived from the last section, we can
    randomize partition.

    \begin{algorithmic}
        \Procedure{Randomized-Partition}{$A, p, r$}
            \State $i = \textsc{Random}(p, r)$
            \State swap $A[i]$ with $A[r]$ \Comment{Make the pivot random}
            \State \Return $\textsc{Partition}(A, p, r)$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\section{\acs{ita} 7.4 Analysis of Quicksort}

\begin{frame}
    \frametitle{Worst-Case Analysis}

    Let $T(n)$ be the worst-case running time of quicksort.
    Then $T(n)$ satisfies
    \begin{equation*}
        T(n) \le \max_{q: 0\le q \le n-1} (T(q) + T(n-1-q)) + \Theta(n)
    \end{equation*}

    We can prove by induction that there exists $c > 0$ such that
    \begin{equation*}
        T(n) \le c n^{2} \qquad \text{for all } n \ge 0.
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{Lemma 7.1}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{tcolorbox}
                Let $X$ be the number of times the comparison $A[j] \le x$ is
                executed in the entire run of quicksort.

                \bigskip{}

                Then the running time of quicksort is $O(X + n)$.
            \end{tcolorbox}

            \cake{} How many times will \textsc{Partition} be called in the
            worst case?
        \end{column}
        \begin{column}{0.5\textwidth}
            \small{}
            \begin{algorithmic}
                \Procedure{Partition}{$A, p, r$}
                \State $x = A[r]$ \Comment{The pivot}
                \State $i = p - 1$
                \For{$j = p \dots r-1$}
                \If{$A[j] \leq x$}
                \State $i = i + 1$
                \State swap $A[i]$ with $A[j]$
                \EndIf
                \EndFor
                \State swap $A[i+1]$ with $A[r]$
                \State \Return $i+1$
                \EndProcedure
            \end{algorithmic}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Compute $X$}
    Let the elements of $A$ be $z_1, z_2, \dots, z_n$ where $z_i$ is the $i$th
    smallest element.

    We will assume that $z_{1}, \dots, z_{n}$ are distinct.

    \pause{}

    Let $X_{ij} = I\{\text{$z_{i}$ and $z_{j}$ are compared in \textsc{Partition}}\}.$
    Then 
    \begin{equation*}
        X = \sum_{i=1}^{n-1} \sum_{j=i+1}^{n} X_{ij}.
    \end{equation*}
    \pause{}%
    Thus
    \begin{equation*}
        \begin{aligned}
            \E{X} 
            &
            = 
            \sum_{i=1}^{n-1} \sum_{j=i+1}^{n} \E{X_{ij}}
            \\
            &
            =
            \sum_{i=1}^{n-1} \sum_{j=i+1}^{n} \Pr{\text{$z_{i}$ and $z_{j}$ are compared in \textsc{Partition}}}.
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{When Do Comparisons Happen?}
    Assume that $A = \langle 1,2,\dots, 10 \rangle$ 
    and that the first pivot is $7$.

    \cake{} Which numbers in $A$ are compared with $7$?

    \pause{}

    The first pivot will split $A$ into
    \begin{equation*}
        \langle 1,2,3,4,5,6 \rangle 
        \quad \text{and} \quad \langle 8,9,10 \rangle.
    \end{equation*}

    \cake{} Will the number $2$ and $9$ be compared after this split?
\end{frame}

\begin{frame}
    \frametitle{Three Cases}
    Let the elements of $A$ be $z_1, z_2, \dots, z_n$ where $z_{i}$ is the
    $i$-th smallest element of $A$.

    Let $Z_{ij} = \{z_{i}, z_{i+1}, \dots, z_{j}\}$.

    Then consider the first element in $Z_{ij}$ chosen as the pivot:
    \begin{itemize}[<+->]
        \item 
            If it is $x$ with $z_{i} < x < z_{j}$, then $z_{i}$
            and $z_{j}$ will no be compared.
        \item
            If it $z_{i}$,
            then $z_{i}$ will be compared with $z_{i+1}, \dots, z_{j}$.
        \item
            If $z_{j}$,
            then $z_{j}$ will be compared with $z_{i}, \dots, z_{j-1}$.
    \end{itemize}
    \only<+->{%
        \cake{}
        In which two cases will $z_{i}$ and $z_{j}$ be compared?
    }
\end{frame}

\begin{frame}
    \frametitle{The Probability of a Comparison}

    \cake{} Then, what is
    \begin{equation*}
        \p{\text{$z_{i}$ is compared with $z_{j}$}} 
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Expected Running Time}
    Thus we have shown that
    \begin{equation*}
        \E{X} 
        = \sum_{i=1}^{n-1} \sum_{j=i+1}^{n} \Pr{\text{$z_{i}$ and $z_{j}$ are compared}}
        = \sum_{i=1}^{n-1} \sum_{j=i+1}^{n} \frac{2}{j-i+1}
    \end{equation*}

    \cake{} What is this in big O notation?

\end{frame}

\section{\acs{ita} 7 Problems}

\begin{frame}
    \frametitle{Problem 7-1}
    In the original quicksort algorithm by Hoare, the partition is done with the
    following algorithm:

    \vspace{-1em}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{algorithmic}[1]
                \small{}
                \Procedure{Hoare-Partition}{$A, p, r$}
                \State $x \gets A[p]$
                \State $i \gets p - 1$
                \State $j \gets r + 1$
                \While{true}
                \Repeat 
                \State $j \gets j - 1$
                \Until{$A[j] \leq x$}
                \algstore{bkmark}
            \end{algorithmic}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{algorithmic}[1]
                \small{}
                \algrestore{bkmark}
                \Repeat 
                \State $i \gets i + 1$
                \Until{$A[i] \geq x$}
                \If{$i < j$}
                \State swap $A[i]$ and $A[j]$
                \Else 
                \State \Return $j$
                \EndIf
                \EndWhile
                \EndProcedure
            \end{algorithmic}
        \end{column}
    \end{columns}

    \cake{} Can we replace \textsc{Partition} directly with \textsc{Hoare-Partition}?

    \laughing{} Let's find out!
\end{frame}

\end{document}
