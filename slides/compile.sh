#!/usr/bin/env bash

find . -name "lecture-*.tex" -execdir latexmk -pdflatex='lualatex' -pdf {} \;
