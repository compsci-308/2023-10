\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Asymptotic Notations}

\begin{document}

\maketitle

\lectureoutline{}

\section{Asymptotic Analysis of Insertion Sort}

\begin{frame}
    \frametitle{Insertion Sort}
    Let $c_\ell$ be the cost (running time) of line $\ell$ when the algorithm is running on
    \ac{ram}.

    Let $t_i$ be the number line $4$ is executed.

    What is $T(n)$ the total running time of the algorithm?

    \begin{algorithmic}[1]
        \For{$i = 2$ to $A.\text{length}$}
        \State $key = A[i]$
        \State $j = i - 1$
        \While{$j > 0$ \textbf{and} $A[j] > key$}
        \State $A[j + 1] = A[j]$
        \State $j = j - 1$
        \EndWhile
        \State $A[j + 1] = key$
        \EndFor
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Best-case Analysis}
    
    Given $T(n)$, how small can it be?
\end{frame}

\begin{frame}
    \frametitle{Worst-case Analysis}
    
    Given $T(n)$, how large can it be?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let $T(n)$ be the running time of insertion sort on \ac{ram}.

    \cake{} Which of the following are correct?
    \begin{enumerate}
        \item $T(n) = O(n^{3})$
        \item $T(n) = O(2 n^{2})$
        \item $T(n) = O(n^2)$
        \item $T(n) = \Theta(n^2)$
        \item $T(n) = \Omega(n^2)$
        \item $T(n) = \Omega(n)$
        \item $T(n) = \Omega(2 n)$
        \item $T(n) = \Omega(\sqrt{n})$
    \end{enumerate}
\end{frame}

\section{Asymptotic Notations in Equations and Inequalities}

\begin{frame}
    \frametitle{Asymptotic Notations in Equations}

    Given equations like
    \begin{equation}
        \label{eq:anonymous}
        2 n^2 + 3n + 1 = 2 n^2 + \Theta(n)
    \end{equation}
    we interpret $\Theta(n)$ as an \alert{anonymous function} $f(n)$ which
    satisfies
    \begin{equation*}
        f(n) = \Theta(n)
    \end{equation*}

    \only<1>{
        How can we show that \eqref{eq:anonymous} is correct?
    }
    \only<2>{
        \cake{} Is the following equation correct?
        \begin{equation*}
            2 n^2 + 3n + 1 = n^2 + \Omega(n)
        \end{equation*}
    }
\end{frame}

\begin{frame}
    \frametitle{Anonymous Functions}
    Each appearance of an asymptotic notation in an equation interpreted as
    an \alert{anonymous function}.

    \dizzy{} Thus, the sum
    \begin{equation*}
        \sum_{i=1}^n O(i)
    \end{equation*}
    should \emph{not} be interpreted as
    \begin{equation*}
        O(1) + O(2) + \dots + O(n)
    \end{equation*}
    but
    \begin{equation*}
        \sum_{i=1}^n f(i)
    \end{equation*}
    where $f(i) = O(i)$ for all $i \in \{1, \dots, n\}$ is an anonymous function.

    \dizzy{} However, this does not really have a clean interpretation.
\end{frame}

\begin{frame}
    \frametitle{Merge Sort}
    
    The worst-case running time of merge sort is
    \begin{equation*}
        T(n) = 2 T(n/2) + \Theta(n)
    \end{equation*}

    Later we will see that this equation is enough to find the asymptotic
    behaviour of $T(n)$.

    \laughing{} So there is no need to figure out what the $\Theta(n)$ part is exactly.
\end{frame}

\begin{frame}
    \frametitle{Asymptotic Notations on Both Sides of Equations}
    
    We interpret the equation
    \begin{equation}
        \label{eq:two:anonymous}
        2 n^2 + \Theta(n) = \Theta(n^2)
    \end{equation}
    as the following:

    For any $f(n) = \Theta(n)$, there exists a $g(n) = \Theta(n^2)$ such that
    \begin{equation*}
        2 n^2 + f(n) = g(n)
    \end{equation*}

    \cake{} Is \eqref{eq:two:anonymous} correct?
\end{frame}

\begin{frame}
    \frametitle{Chaining Asymptotic Equations}
    
    We interpret
    \begin{equation}
        \label{eq:chain}
        2 n^2 + 3n + 1 
        = 2 n^2 + \Theta(n)
        = \Theta(n^2)
    \end{equation}
    as two separate equations
    \begin{equation*}
        2 n^2 + 3n + 1 
        = 2 n^2 + \Theta(n)
    \end{equation*}
    and
    \begin{equation*}
        2 n^2 + \Theta(n)
        = \Theta(n^2)
    \end{equation*}

    \cake{} Do \eqref{eq:chain} hold?
\end{frame}

\section{Small $o$ and Small $ω$ Notations}

\begin{frame}
    \frametitle{Tight Asymptotic Upper Bound}
    
    The upper bound provided $O$ may or may not be tight.

    For example, $2 n^2 = O(n^2)$ is tight in the sense that
    \begin{equation*}
        \lim_{n \to \infty} \frac{2n^2}{n^2} = 2.
    \end{equation*}

    On the other hand, $n = O(n^2)$ is \emph{not} tight in the sense that
    \begin{equation*}
        \lim_{n \to \infty} \frac{n}{n^2} = 0.
    \end{equation*}

    In the latter case, we can replace the $O$ by $o$ and write
    \begin{equation*}
        n = o(n^2).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{$o$-Notations}
    
    Formally, we write $f(n) = o(g(n))$ if for any $c > 0$, there exists $n_0$
    such that
    \begin{equation*}
        0 \le f(n) < c g(n), \qquad \text{for all} \; n \geq n_0.
    \end{equation*}

    \cake{} Can you see why $f(n) = o(g(n))$ if and only if
    \begin{equation*}
        \lim_{n \to \infty} \frac{f(n)}{g(n)} = 0.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{$\omega$-Notations}
    
    Formally, we write $f(n) = \omega(g(n))$ if for any $c > 0$, there exists $n_0$
    such that
    \begin{equation*}
        0 \le c g(n) < f(n), \qquad \text{for all} \; n \geq n_0.
    \end{equation*}

    In other words, $f(n) = \omega(g(n))$ if and only if
    \begin{equation*}
        \lim_{n \to \infty} \frac{f(n)}{g(n)} = \infty.
    \end{equation*}
\end{frame}

\section{Comparing Functions}

\begin{frame}
    \frametitle{Transitivity in Asymptotic Notations}

    \alert{Transitivity} means that if \( f(n) \) is related to \( g(n) \) and
    \( g(n) \) is related to \( h(n) \) by the same notation, then \( f(n) \) is
    also related to \( h(n) \) by that notation:
    \begin{align*}
        f(n) &= \Theta(g(n)) \text{ and } g(n) = \Theta(h(n)) \implies f(n) = \Theta(h(n)), \\
        f(n) &= O(g(n)) \text{ and } g(n) = O(h(n)) \implies f(n) = O(h(n)), \\
        f(n) &= \Omega(g(n)) \text{ and } g(n) = \Omega(h(n)) \implies f(n) = \Omega(h(n)), \\
        f(n) &= o(g(n)) \text{ and } g(n) = o(h(n)) \implies f(n) = o(h(n)), \\
        f(n) &= \omega(g(n)) \text{ and } g(n) = \omega(h(n)) \implies f(n) = \omega(h(n)).
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Reflectivity in Asymptotic Notations}

    \alert{Reflectivity} means that any function \( f(n) \) is related to
    itself by some asymptotic notations:
    \begin{align*}
        f(n) &= \Theta(f(n)), \\
        f(n) &= O(f(n)), \\
        f(n) &= \Omega(f(n)) \\
    \end{align*}

    \cake{}
    Do we have
    \begin{align*}
        f(n) &= o(f(n)), \\
        f(n) &= \omega(f(n)).
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Symmetry in Asymptotic Notations}

    \alert{Symmetry} means that if \( f(n) \) is related to \( g(n) \) by an
    asymptotic notation, then \( g(n) \) should be related to \( f(n) \) by its
    the same notation:
    \begin{equation*}
        f(n) = \Theta(g(n)) \implies g(n) = \Theta(f(n)).
    \end{equation*}

    \cake{}
    Do we have
    \begin{align*}
        f(n) = O(g(n)) &\implies g(n) = \Omega(f(n)), \\
        f(n) = \Omega(g(n)) &\implies g(n) = O(f(n)) \\
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Transpose Symmetry in Asymptotic Notations}

    \alert{Transpose Symmetry} means that if \( f(n) \) is related to \( g(n) \)
    by one asymptotic notation, then \( g(n) \) should be related to \( f(n) \) by
    another notation:
    \begin{align*}
        f(n) = O(g(n)) & \iff g(n) = \blankshort{}(f(n)), \\
        f(n) = o(g(n)) & \iff g(n) = \blankshort{}(f(n)).
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Trichotomy in Asymptotic Notations}

    \begin{block}{Trichotomy \emph{noun}}
        \begin{enumerate}
            \item The division into three mutually exclusive or contrasting
                groups, categories, or parts.
            \item A situation or system characterized by having three distinct
                options or pathways.
        \end{enumerate}
    \end{block}


    Real numbers have a \alert{Trichotomy} --- For any $a, b \in \dsR$, 
    exactly one of the following must hold: 
    \begin{equation*}
        a < b, \qquad a = b, \qquad a > b.
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Trichotomy in Asymptotic Notations}

    We say that 
    \begin{itemize}
        \item $f(n)$ is \alert{asymptotically smaller} than $g(n)$ if $f(n) =
            o(g(n))$, denoted by $f(n) \prec g(n)$;
        \item $f(n)$ is \alert{asymptotically larger} than $g(n)$ if $f(n) =
            \omega(g(n))$, denoted by $f(n) \succ g(n)$.
    \end{itemize}

    \bomb{} Not all functions are asymptotically comparable, e.g.: 
    \begin{equation*}
        f(n) = n, \qquad g(n) = n^{1+(-1)^n}.
    \end{equation*}

    \pause{}

    \puzzle{} Are monotonly increasing functions always asymptotically comparable?
\end{frame}

\section{Common Time Complexity}

\begin{frame}
    \frametitle{Common Time Complexity}

    \alert{Time complexity}  is a way to express how the running time of an algorithm grows
    as the size of the input to the algorithm increases.

    Some common time complexities are:
    \begin{itemize}
        \only<1>{
            \item constant: $O(1)$
            \item log-logarithmic: $O(\log \log n)$
            \item logarithmic: $O(\log n)$
            \item polylogarithmic: $O(\mathrm{poly}(\log n))$
        }
        \only<2>{
            \item sub-linear: $o(n)$
            \item linear: $O(n)$
            \item linearithmic: $O(n \log n)$
            \item quadratic: $O(n^2)$
            \item cubic: $O(n^3)$
            \item exponential: $2^O(n)$
        }
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Constant Time Complexity}

    A constant time algorithm has running time \( O(1) \).

    In other words, its running time is bounded by a constant regardless of the
    input size.

    Examples include:
    \begin{itemize}
      \item Conditional branch
      \item Arithmetic/logic operation
      \item Declare/initialize a variable
      \item Follow a link in a linked list
      \item Access element \(i\) in an array
      \item Compare/exchange two elements in an array
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Linear Time Complexity}

    A linear time algorithm has running time \( O(n) \).

    In other words, its running time is bounded by a constant factor times the input size.

    A common reason for this is the algorithm processes the input in a single
    pass.

    Examples include finding the maximum element in an array.

    \cake{} Can you think of another example?

    \puzzle{} Can we find the median of an unsorted array in linear time?
\end{frame}

\begin{frame}
    \frametitle{Logarithmic Time Complexity}

    A logarithmic time algorithm has running time \( O(\log n) \).

    Examples include searching for an element in a sorted array.

    \only<1>{
    \begin{small}
        \begin{algorithmic}[1]
            \Procedure{BinarySearch}{$A[1..n]$, $key$}
            \State $low \gets 1$; $high \gets n$
            \While{$low \leq high$}
            \State $mid \gets (low + high) // 2$
            \If{$A[mid] = key$} \Return $mid$
            \ElsIf{$A[mid] < key$} \State $low \gets mid + 1$
            \Else \State $high \gets mid - 1$
            \EndIf
            \EndWhile
            \State \Return $NULL$
            \EndProcedure
        \end{algorithmic}
    \end{small}
    }
    \only<2>{
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\textwidth]{binary-search.png}
            \caption{An example of binary search}
        \end{figure}
    }
\end{frame}

\begin{frame}
    \frametitle{Worst Case Analysis of Binary Search}

    How can we show that the running time of a binary search algorithm is
    $O(\log n)$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Given the array of length $15$,
    \begin{equation*}
        A = \langle 
        12, 25, 32, 37, 41, 48, 58, 60, 66, 73, 74, 79, 83, 91, 95
        \rangle
    \end{equation*}

    How many comparisons does it take to find the position of 74?

    What if we do a linear search from left to right?
\end{frame}

\begin{frame}
    \frametitle{Quadratic Time Complexity}
    An algorithm with $O(n^2)$ time complexity is the following which finds the
    closest pair of points on a plane given a set of points $(x_1, y_1), \ldots,
    x_n, y_n)$.

    \begin{small}
        \begin{algorithmic}[1]
            \Procedure{ClosestPair}{$ x[1..n] $, $ y[1..n] $}
            \State $d \gets \infty$
            \State $cp \gets (NULL, NULL)$
            \For{$i \gets 1$ to $n-1$}
            \For{$j \gets i+1$ to $n$}
            \State $dt \gets \sqrt{(x[i]-x[j])^2 + (y[i]-y[j])^2}$
            \If{$dt < d$}
            \State $d \gets dt$
            \State $cp \gets ((x[i], y[i]), (x[j], y[j]))$
            \EndIf
            \EndFor
            \EndFor
            \Return $cp$
            \EndProcedure
        \end{algorithmic}
    \end{small}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{\courseassignment{}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \acf{ita}
            \begin{itemize}
                \item[\emoji{pencil}] 
                    Exercises 3.1: 7--8.
                \item[\emoji{pencil}] 
                    Exercises 3.2: 1--8.
                \item[\emoji{pencil}] 
                    Problems 3-1, 3-2, 3-3, 3-4.
            \end{itemize}
        \end{column}
    \end{columns}

\end{frame}

\end{document}
