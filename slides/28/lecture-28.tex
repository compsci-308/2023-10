\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Approximation Algorithm}

\begin{document}

\maketitle

\begin{frame}[standout]
    \vspace{1em}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-evalution.jpg}
        \caption*{\emoji{pray}\textcolor{white}{Please use the next 15 minutes
        to write course evaluation.}}
    \end{figure}
\end{frame}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Chapter 35 introduction.
                \item
                    Section 35.1.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 35.1: 1-2, 4-5.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 35 Approximation Algorithms}

\begin{frame}
    \frametitle{Approximation Algorithms}
    
    An algorithm that returns a near-optimal solution is called an \alert{approximation algorithm}.

    We say an algorithm has an \alert{approximation ratio} of $\rho(n)$ if for
    any input size $n$
    \begin{equation*}
        \max \left(\frac{C}{C^*}, \frac{C^*}{C}\right) \le \rho(n)
    \end{equation*}
    where 
    \begin{itemize}
        \item $C$ is the cost of the solution produced by the algorithm,
        \item and $C^*$ is the cost of the optimal solution.
    \end{itemize}
\end{frame}

\section{\acs{ita} 35.1 The Vertex Cover Problem}

\begin{frame}
    \frametitle{Lights and trails}
    
    The follow graph depicts hiking trails in a forest.

    \begin{figure}[htpb]
        \centering

        \begin{tikzpicture}[
            every node/.style={graphnode},
            every path/.style={graphedge},
            scale=1.2
            ]
            \node (u) at (1,1) {$u$};
            \node (v) at (2,1) {$v$};
            \node (w) at (3,0) {$w$};
            \node (x) at (2,-1) {$x$};
            \node (y) at (1,-1) {$y$};
            \node (z) at (0,0) {$z$};

            \draw (v) -- (w);
            \draw (w) -- (x);
            \draw (z) -- (v);
            \draw (u) -- (w);
            \draw (z) -- (y);
        \end{tikzpicture}
    \end{figure}

    The city wants to build some \emoji{hut} in the forest at the conjunction of
    the trails so that each trail has at least one \emoji{hut}.

    \cake{} Can you think of a way to put the \emoji{hut}?

    \hint{} In graph theory, this is asking for a vertex cover.
\end{frame}

\begin{frame}
    \frametitle{Vertex Cover}

    Given a graph $G = (V,E)$, a subset of vertices $V'$ is a \alert{vertex cover}
    if for every edge $(u,v) \in E$, either $u \in V'$ or $v \in V'$.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            every node/.style={graphnode},
            every path/.style={graphedge},
            scale=1.2
            ]
            \node[fill=MintGreen] (u) at (1,1) {$u$};
            \node[fill=MintGreen] (v) at (2,1) {$v$};
            \node (w) at (3,0) {$w$};
            \node[fill=MintGreen] (x) at (2,-1) {$x$};
            \node[fill=MintGreen] (y) at (1,-1) {$y$};
            \node (z) at (0,0) {$z$};

            \draw (v) -- (w);
            \draw (w) -- (x);
            \draw (z) -- (v);
            \draw (u) -- (w);
            \draw (z) -- (y);
        \end{tikzpicture}
        \caption{The set of vertices $\{u,v,x,y\}$ forms a vertex cover}
    \end{figure}

    The vertex cover problem is formally defined as the language
    \begin{equation*}
        \text{VERTEX-COVER} = 
        \{\langle G, k \rangle: \text{$G$ has a vertex cover of size $k$}\}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Approximation Algorithm for Vertex Cover}

    According the following theorem, it is unlikely to find a polynomial-time algorithm
    for the vertex-cover problem.

    \begin{block}{Theorem 34.12}
        The vertex-cover problem is NP-complete.
    \end{block}

    But we do have a good approximation algorithm!

    \begin{algorithmic}[1]
        \small{}
        \Procedure{Approx-Vertex-Cover}{$G$}
            \State $C = \emptyset$
            \State $E' = G.E$
            \While{$E'$ is not empty}
                \State let $(u,v)$ be an arbitrary edge in $E'$
                \State $C = C \cup \{u,v\}$
                \State remove edge incident to $u$ or $v$ from $E'$
            \EndWhile
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Example Run}
    
    Let's try the algorithm on the following graph

    \begin{figure}[htpb]
        \begin{tikzpicture}[
            node distance=2cm, 
            auto, 
            every node/.style={graphnode, minimum size=0.6cm, inner sep=0pt, font=\footnotesize},
            every path/.style={graphedge},
            ]
            % Nodes
            \node (a) {$a$};
            \node (b) [above of=a] {$b$};
            \node (c) [right of=b] {$c$};
            \node (e) [below of=c] {$e$};
            \node (d) [right of=c] {$d$};
            \node (f) [below of=d] {$f$};
            \node (g) [right of=f] {$g$};

            % Edges
            \draw 
                (a) -- (b)
                (b) -- (c)
                (c) -- (d)
                (c) -- (e)
                (d) -- (e)
                (e) -- (f)
                (d) -- (f)
                (d) -- (g)
                ;
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 35.1}
    
    APPROX-VERTEX-COVER is a polynomial-time 2-approximation algorithm.
\end{frame}

\begin{frame}[c,standout]
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-maze.jpg}
        \caption{\textcolor{white}{\emoji{raising-hand} Q \& A for the remaining time!}}
    \end{figure}
\end{frame}

\end{document}
