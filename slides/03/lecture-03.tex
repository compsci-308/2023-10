\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Asymptotic Notations}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acf{ram}}

\begin{frame}
    \frametitle{\acf{ram}}
    
    We will design and analyze algorithms on an abstract computation model
    \acf{ram} with the following properties:

    \begin{itemize}
        \item It is an abstract \emoji{desktop-computer}.
        \item Each instruction is executed sequentially on \ac{ram}.
        \item Each simple operation $(+, \times, -, =, \texttt{if})$ takes exactly one time unit.
        \item Loops and calls of subroutines are \emph{not} simple operations.
        \item Each memory access takes exactly one time unit.
        \item It has infinitely many memory.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{What \ac{ram} Can Do}
    
    The \ac{ram} model contains instructions commonly found in real
    \emoji{desktop-computer}, each instruction takes a constant amount of time
    \begin{itemize}
        \item Arithmetic: \emoji{plus}, \emoji{minus}, \emoji{multiply}, \emoji{divide},
            remainder, floor, ceiling.
        \item Data movement: load, store, copy
        \item Control: conditional and unconditional branch, loop, subroutine call and
            return
    \end{itemize}

    But we do \emph{not} unrealistic instructions such as \texttt{sort}.
\end{frame}

\begin{frame}
    \frametitle{Data Types in \ac{ram}}
    
    In the \ac{ram} model, two primary data types are used: integers and
    floating-point numbers.

    While the focus is generally \emph{not} on precision in floating-point arithmetic,
    there are scenarios where precision is critical, e.g.:
    \begin{itemize}
        \item Scientific Computing
        \item Financial Systems
        \item Computer Graphics
        \item Machine Learning Algorithms
        \item Signal Processing
    \end{itemize}

    \emoji{wink} We will \emph{not} talk about it as it belongs to Numeric Analysis.
\end{frame}

\begin{frame}[c]
    \frametitle{What is a Word?}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            In computing, a \alert{word} refers to a fixed-sized piece of data that is processed as a unit by the CPU.

            \begin{itemize}
                \item The size of a word varies by architecture.
                \item In a 32-bit system, a word is usually 32 bits  long.
                \item In a 64-bit system, a word is typically 64 bits  long.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{cpu-32-bit.jpg}
                \caption{A 32-bit CPU}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Assumptions on Word Size in \ac{ram}}

    In the \ac{ram} model, it is often assumed that there is a limit on the size
    of each word of data. Specifically:

    \begin{itemize}[<+->]
        \item When the input has size \( n \), integers are typically
            represented by \( c \ceil{\lg n} \) bits for some constant \( c \geq 1 \).
        \item The constant \( c \geq 1 \) ensures that each
            word can hold any integer in \(\{0, 1, \dots n \}\).
        \item \( c \) is restricted to be a constant to prevent the word size
            from growing arbitrarily.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Gray Areas in the \ac{ram} Model}

    Operations like exponentiation \( x^y \) may require multiple instructions
    in real \temoji{desktop-computer}. 

    Thus they are \emph{not} defined as instructions in the \ac{ram} model.

    However, specific cases such as computing \( 2^k \) with a small \( k \) can
    be executed as a constant-time operation through a \alert{shift left}
    instruction.

    \only<1>{
        \cake{} How can we compute \(5^{2}\) using \emph{shift left}?
    }
    \only<2>{
        \hint{} We will adhere to the set of instructions
        defined in the \ac{ram} model whenever possible and treat special cases,
        like \( 2^k \), as constant-time operations.
    }
\end{frame}

\begin{frame}
    \frametitle{The Unrealistic \ac{ram} Model}

    The \ac{ram} model is unrealistic in many ways. 

    For example, in \emoji{desktop-computer}:
    \begin{itemize}
        \item \emoji{multiply} takes much longer than \emoji{plus}.
        \item Compiler may unroll loops.
        \item Memory access is not at all constant time.
    \end{itemize}
    
    \cake{} Why do we still use \ac{ram}?
\end{frame}

\begin{frame}
    \frametitle{Running Time in \ac{ram}}

    In \ac{ram}, we count the algorithm's instruction executions as
    running time, a valid estimate since most operations take constant time on
    real \emoji{desktop-computer}.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.7\textwidth]{java-operation-cost-1.png}
        \includegraphics<2>[width=0.8\textwidth]{java-operation-cost-2.png}
        \caption{The running time of basic operations in Java on \emoji{laptop}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Aphorism --- All Models Are Wrong, Some Are Useful}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{quote}
                Since all models are wrong the scientist cannot obtain a ``correct'' one
                by excessive elaboration. 

                \dots{} the ability to devise simple but evocative models is the signature of the
                great scientist \dots{}

                \flushright{--- George Box}
            \end{quote}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{flat-earth.jpg}
                \caption{The flat earth model is good enough for building a
                house}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{Order of Growth of Running Time}

\begin{frame}
    \frametitle{Order of Growth}

    Let $T(n)$ be the worst-case/average running time of an algorithm
    with input size $n$ on \ac{ram}.
    
    The \alert{order/rate of growth} of $T(n)$ describes how fast $T(n)$ grows
    as $n$ increases.

    For example, if
    \begin{equation*}
        T(n) = a n^2 + b n + c,
    \end{equation*}
    we say its \emph{order of growth}
    is 
    \begin{equation*}
        \Theta(n^{2})
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Typical Order of Growth}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{growth-rates.png}
        \caption{Some Common Order of Growth}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Why Order of Growth?}

    We care about the order of growth because
    \begin{itemize}
        \item it is a simple characterization of the algorithm's efficiency,
        \item and there are enormous differences between different rates.
    \end{itemize}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{comparison-of-growth-rate.png}
        \caption{Running Time of Algorithms with Different Order of Growth on A
        Computer with $10^6$ Instructions Per Second}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Concerns with Precise Running Time}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            Why not aim for a precise running time like:
            \begin{equation*}
                1.63 n^2 + 3.5 n + 8 ?
            \end{equation*}

            \begin{itemize}
                \item Achieving precise bounds can be exhausting.
                \item Coarser granularity reveals clearer algorithmic patterns.
                \item Overly detailed time measuring can be misleading.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{too-much-details.jpg}
                \caption{Too much details can be misleading}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{Asymptotic Analysis}

\begin{frame}
    \frametitle{Asymptotic Analysis}
    In mathematics, \alert{Asymptotic analysis} or \alert{asymptotics}, 
    is a method of describing limiting behaviour of a function.

    \only<1>{
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\textwidth]{asymptotic-01.pdf}
            \caption{$(n^2+3)/n^2$}
        \end{figure}
    }
    \only<2>{
        For example, when $n$ is large, 
        \begin{equation*}
            n^2 + 3n \approx n^2
        \end{equation*}
        In other words, the term $3n$ becomes insignificant.

        Asymptotic notations makes $\approx$ more precise by precisely defining equations
        equations like
        \begin{equation*}
            n^2 + 3n = \Theta(n^2).
        \end{equation*}
    }
\end{frame}

\begin{frame}
    \frametitle{The $\Theta$ Notation}

    We say that $f(n) = \Theta(g(n))$ if there exist positive constants $n_0$, $c_1$ and
    $c_2$ such that
    \begin{equation*}
        c_1 g(n) \le f(n) \le c_2 g(n), \qquad \text{for all} \; n \geq n_0.
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{big-Theta.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example of $\Theta$ Notation}

    \cake{} How can we show that
    \begin{equation*}
        \frac{1}{2} n^2 - 3n = \Theta(n^2).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Quadratic Functions}

    To show that
    \begin{equation*}
        a n^2 + b n +c = \Theta(n^2),
    \end{equation*}
    when $a > 0$,
    we can take
    \begin{equation*}
        c_1 = \frac{a}{4},
        \qquad
        c_2 = \frac{7 a}{4}
        \qquad
        n_0 = 2 \max\left(\frac{\abs{b}}{a},\sqrt{\frac{\abs{c}}{a}}\right)
    \end{equation*}

    \puzzle{} Try to prove this.
\end{frame}

\begin{frame}
    \frametitle{Polynomial Functions}
    Given that
    \begin{equation*}
        p(n) = \sum_{i=0}^{d} a_i n^{i},
    \end{equation*}
    where $a_{d} > 0$,
    we have
    \begin{equation*}
        p(n) = \Theta(n^d).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The $O$ Notation}

    We say that $f(n) = O(g(n))$ if there exist positive constants $n_0$, $c$ such that
    \begin{equation*}
        0 \le f(n) \le c g(n), \qquad \text{for all} \; n \geq n_0.
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{big-O.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example of $O$ Notation}

    \cake{} How can we show that
    \begin{equation*}
        a n^2 + b n +c = O(n^2),
    \end{equation*}
    if $a > 0$?
\end{frame}

\begin{frame}
    \frametitle{The $\Omega$ Notation}

    We say that $f(n) = \Omega(g(n))$ if there exist positive constants $n_0$, $c$ such that
    \begin{equation*}
        f(n) \ge c g(n), \qquad \text{for all} \; n \geq n_0.
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{big-Omega.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example of $\Omega$ Notation}

    \cake{} How can we show that
    \begin{equation*}
        a n^2 + b n +c = O(n^2),
    \end{equation*}
    if $a > 0$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 3.1}

    For any two functions $f(n)$ and $g(n)$, we have
    \begin{equation*}
        f(n) = \Theta(g(n))
    \end{equation*}
    if and only if 
    \begin{equation*}
        f(n) = O(g(n)) \qquad \text{and} \qquad f(n) = \Omega(g(n)).
    \end{equation*}

    \puzzle{} Try prove this.
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{\courseassignment{}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \acf{ita}
            \begin{itemize}
                \item[\emoji{pencil}] 
                    Exercises 3.1: 1--6.
            \end{itemize}
        \end{column}
    \end{columns}

\end{frame}

\end{document}
