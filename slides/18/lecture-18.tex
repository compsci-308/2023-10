\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Linear Programming (Part 2)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 29.2.
                \item
                    Section 29.3.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 29.2: 1--7.
                \item
                    Exercises 29.3: 2--7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 29.2 Formulating problems as linear programs}

\begin{frame}
    \frametitle{The Single-Pair Shortest-Path Problem}

    Given a directed graph \( G = (V, E) \) with weight function \( w\), 
    we aim to compute the weight of the shortest path from \( s \) to \( t \).


    This can be modelled as an \ac{lp} problem:
    \begin{equation*}
        \begin{aligned}
            \text{maximize} \qquad 
                & d_{t} \\
                \text{subject to} \qquad 
                & d_v \leq d_u + w(u, v) & \text{for each } (u, v) \in E, \\
                & d_s = 0, \\
                & d_v \ge 0 & \text{for each } v \in V.
        \end{aligned}
    \end{equation*}

    \pause{}

    Taking \( d_v = \delta(s,v)\) is obviously a feasible solution.

    The maximization of $d_t$ makes sense because given the constraints,
    $d_t \leq \delta(s,t)$.
\end{frame}

\begin{frame}
    \frametitle{Maximum-Flow Problem}
    
    Assume on the \emoji{canada} highway network: 
    \begin{itemize}
        \item there is limit of traffic between every two cities, 
        \item the traffic coming into a city must go out of it.
    \end{itemize}

    What is the maximum traffic possible from Vancouver to Winnipeg?

    This is called a \alert{maximum-flow problem}.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{max-flow.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Maximum-Flow Problem Formulation}

    Consider a directed graph \( G = (V, E) \) where each edge \( (u, v) \) has
    a nonnegative capacity \( c(u, v) \). 

    Our goal is to find the maximum flow from a source \( s \) to a sink \( t \).


    This can be modelled as an \ac{lp} problem:
    \begin{equation*}
        \begin{aligned}
            \text{maximize} \qquad 
                & \sum_{v:(s,v)\in E} f_{sv} - \sum_{v:(v,s) \in E} f_{vs} \\
            \text{subject to} \qquad 
                & f_{uv} \leq c(u, v) & \text{for each } (u, v) \in E, \\
                & 0 = \sum_{v:(v,u) \in E} f_{vu} - \sum_{v:(u,v) \in E} f_{uv} & \text{for each } u \in V - \{s, t\}, \\
                & f_{uv} \ge 0 & \text{for each } (u, v) \in V.
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Minimum-Cost Flow}

    Assume that transfer one unit flow through $(u,v)$ cost $\alpha(u,v)$.

    We want to send $d$ unit from $s$ to $t$ while minimizing the cost.
    
    This can be modelled as an \ac{lp} problem:
    \begin{equation*}
        \begin{aligned}
            \text{minimize}  
                \qquad 
                & \sum_{(u,v) \in E} \alpha(u,v) f_{uv} \\
            \text{subject to} \qquad 
                & f_{uv} \leq c(u, v) & \text{for each } (u, v) \in E, \\
                & 0 = \sum_{v:(v,u) \in E} f_{vu} - \sum_{v:(u,v) \in E} f_{uv} & \text{for each } u \in V - \{s, t\}, \\
                & d = \sum_{v:(s,v)\in E} f_{sv} - \sum_{v:(v,s) \in E} f_{vs} \\
                & f_{uv} \ge 0 & \text{for each } (u, v) \in V.
        \end{aligned}
    \end{equation*}

    \hint{} \ac{lp} can often be used to solve problems without specialized algorithm.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{} Please write down the \ac{lp} formulation for the following
    minimum-cost flow problem.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{min-cost-flow.png}
    \end{figure}
\end{frame}

\section{\acs{ita} 29.3 The Simplex Algorithm}

\begin{frame}
    \frametitle{Overview}

    The simplex algorithm is the classical method for solving linear programs. 

    \weary{} Its running time is not polynomial in the worst case. 

    It does yield insight into linear programs and is often remarkably fast in
    practice.

    We can view the simplex method as Gaussian elimination for
    inequalities.

    \zany{} A simplex in geometry is a generalization of $\triangle{}$.
    Simplex method does not really use simplex.
\end{frame}

\begin{frame}
    \frametitle{Gaussian elimination}
    
    Gaussian elimination solves a system of linear equalities (linear system)
    by iteratively rewriting the system in an equivalent form. 

    Eventually, the system is in a form for which the solution is
    simple to obtain. 

    How can we solve the following system?
    \begin{equation*}
        \begin{aligned}
            x_1 + 2 x_2 & = 8 \\
            2 x_1 + x_2 & = 10
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Standard Form to Slack Form}
    
    Before applying simplex method, we need to convert an \ac{lp} problem in
    \emph{standard form}:
    \begin{align*}
        \text{maximize} \quad & 3x_1 + x_2 + 2x_3 \\
        \text{subject to} \quad & x_1 + x_2 + 3x_3 \leq 30 \\
                                & 2x_1 + 2x_2 + 5x_3 \leq 24 \\
                                & 4x_1 + x_2 + 2x_3 \leq 36 \\
                                & x_1, x_2, x_3 \geq 0
    \end{align*}
    to \ac{lp} problem in \emph{slack form}:
    \begin{align*}
        z &= 0 + 3x_1 + x_2 + 2x_3 \\
        x_4 &= 30 - x_1 - x_2 - 3x_3 \\
        x_5 &= 24 - 2x_1 - 2x_2 - 5x_3 \\
        x_6 &= 36 - 4x_1 - x_2 - 2x_3
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{The Basic Solution}
    
    The basic solution to an \ac{lp} problem in \emph{slack form} is
    the one which sets all nonbasic variables to $0$.

    \only<1>{%
        For example, the \emph{basic solution} of
        \begin{align*}
            z &= 0 + 3x_1 + x_2 + 2x_3 \\
            x_4 &= 30 - x_1 - x_2 - 3x_3 \\
            x_5 &= 24 - 2x_1 - 2x_2 - 5x_3 \\
            x_6 &= 36 - 4x_1 - x_2 - 2x_3
        \end{align*}
        is $x = (0, 0, 0, 30, 24, 36)$, which has an objective value
        $z = 0$.
    }
    \only<2>{%
        \bomb{} A \emph{basic solution} is \emph{not} necessarily a feasible solution.

        For example, the \emph{basic solution} of
        \begin{align*}
            z &= 0 + 3x_1 + x_2 + 2x_3 \\
            x_4 &= -30 + x_1 + x_2 + 3x_3 \\
            x_5 &= 24 - 2x_1 - 2x_2 - 5x_3 \\
            x_6 &= 36 - 4x_1 - x_2 - 2x_3
        \end{align*}
        is $x = (0, 0, 0, -30, 24, 36)$, which is not feasible because
        $x_4 < 0$.
    }
\end{frame}

\begin{frame}
    \frametitle{A Pivot}

    Given an \ac{lp} problem in \emph{slack form},
    we select a non-basic variable $x_{e}$ in the \emph{objective function} and
    increase its value as much as possible.

    This is done by turning $x_{e}$ into a \emph{basic variable} and
    select another variable $x_{l}$ to become a \emph{nonbasic variable}.

    We call $x_e$ the \alert{entering variable} and $x_l$ the \alert{leaving
    variable}.

    The new \ac{lp} problem will have a different basic solution which has a
    larger objective value.
\end{frame}

\begin{frame}
    \frametitle{Example of a Pivot}

    \only<1-2>{
        Given the following \ac{lp} problem in \emph{slack form}:
        \begin{align*}
            z &= 0 + 3x_1 + x_2 + 2x_3 \\
            x_4 &= 30 - x_1 - x_2 - 3x_3 \\
            x_5 &= 24 - 2x_1 - 2x_2 - 5x_3 \\
            x_6 &= 36 - 4x_1 - x_2 - 2x_3
        \end{align*}
        we can select $x_1$ as the entering variable.

    }
    \only<1>{
        \cake{} How much can we increase $x_{1}$ from $0$ in the basic solution
        $x = (0, 0, 0, 30, 24, 36)$
        without violating any nonnegativity constraints?
    }
    \only<2>{
        Since the constraint with $x_6$ as basic variable is the most
        ``strict'',
        we choose $x_{6}$ as the leaving variable by rewrite it as
        \begin{equation*}
            x_{1} = 9 - \frac{1}{4} x_2 - \frac{1}{2} x_3 - \frac{1}{4} x_6.
        \end{equation*}
        and replace $x_{1}$ by the \ac{rhs} everywhere else.
    }
    \only<3>{
        This gives an equivalent \ac{lp} problem in \emph{slack form}:
        \begin{align*}
            z &= 27 + \frac{x_2}{4} + \frac{x_3}{2} - \frac{3x_6}{4} \\
            x_1 &= 9 - \frac{x_2}{4} - \frac{x_3}{2} - \frac{x_6}{4} \\
            x_4 &= 21 - \frac{3x_2}{4} - \frac{5x_3}{2} + \frac{x_6}{4} \\
            x_5 &= 6 - \frac{3x_2}{2} - \frac{4x_3}{2} + \frac{x_6}{2}
        \end{align*}

        \cake{} What is the basic solution for this \ac{lp} problem and the
        corresponding objective value?
    }
\end{frame}

\begin{frame}
    \frametitle{Another Pivot}

    Suppose we want to pivot again.

    \cake{} Can we select $x_{6}$ as the entering variable $x_e$?

    \cake{} If instead we select $x_3$ as $x_e$, how much can we increase it?
    What should $x_l$ be?
    \begin{align*}
        z &= 27 + \frac{x_2}{4} + \frac{x_3}{2} - \frac{3x_6}{4} \\
        x_1 &= 9 - \frac{x_2}{4} - \frac{x_3}{2} - \frac{x_6}{4} \\
        x_4 &= 21 - \frac{3x_2}{4} - \frac{5x_3}{2} + \frac{x_6}{4} \\
        x_5 &= 6 - \frac{3x_2}{2} - \frac{4x_3}{2} + \frac{x_6}{2}
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{A Third Pivot}

    After $x_3$ becomes a basic variable, we are left with
    \begin{align*}
        z &= \frac{111}{4} + \frac{x_2}{16} - \frac{x_5}{8} - \frac{11x_6}{16} \\
        x_1 &= \frac{33}{4} - \frac{x_2}{16} + \frac{x_5}{8} - \frac{5x_6}{16} \\
        x_3 &= \frac{3}{2} - \frac{3x_2}{8} - \frac{x_5}{4} + \frac{x_6}{8} \\
        x_4 &= \frac{69}{4} + \frac{3x_2}{16} + \frac{5x_5}{8} - \frac{x_6}{16}
    \end{align*}
    \cake{} What should the next $x_e$ and $x_l$ be?
\end{frame}

\begin{frame}
    \frametitle{Optimal Solution}

    After three pivots, we reach the follow \ac{lp} problem
    \begin{align*}
        z  &= 28 - \frac{x_3}{6} - \frac{x_5}{6} - \frac{2x_6}{3} \\
        x_1 &= 8 + \frac{x_3}{6} + \frac{x_5}{6} - \frac{x_6}{3} \\
        x_2 &= 4 - \frac{8x_3}{3} - \frac{2x_5}{3} + \frac{x_6}{3} \\
        x_4 &= 18 - \frac{x_3}{2} + \frac{x_5}{2} \cdot
    \end{align*}
    which is equivalent to the original \ac{lp} problem.

    \cake{} Why does this imply that $z \le 28$.

    \cake{} Which solution makes $z=28$?

    \emoji{tada} We have found the optimal solution!
\end{frame}

\end{document}
