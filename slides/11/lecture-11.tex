\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Elementary Graph Algorithms (Part 1)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ita} 22.1 Representation of Graphs}

\begin{frame}[c]
    \frametitle{Friendships}

    We can model friendships as a graph in which each vertex (node) represents a
    friend and each edge (line) represents a friendship.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{graph-1.pdf}
        \caption{Friendships in COMPSCI 308}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Metro Network}

    A metro network can be modelled as a graph. In this representation, each vertex corresponds to a station, and each edge signifies a metro line.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{metro.png}
        \caption{Mini Metro game screenshot. Source: 
        \href{https://commons.wikimedia.org/wiki/File:Mini\_Metro\_screenshot\_0.png}{Wikipedia}.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Definition of Graph}

    \only<1>{
        A \alert{graph} \( G \) is defined as a pair \( (V, E) \),
        where \(V\) is the \alert{vertex set}, and \( E \) is the \alert{edge set}.

        An element in \(V\) is called a \alert{vertex} or \alert{node}.

        An element in \(E\) is called an \alert{edge}.

        \cake{} What is the vertex set $V$ and edge set $E$ of the following graph?
    }
    \only<2>{
        If \( (x, y) \in E \), then \( x \) and \( y \) are \alert{adjacent}, and the
        edge \( (x, y) \) is \alert{incident to} both \( x \) and \( y \).

        If the edges are undirected, i.e., \( (x,y) \) and \( (y,x) \) are the same edge,
        the graph is said to be \alert{undirected}.

        \cake{} What is the maximum number of edges a graph with $\abs{V}$
        vertices can have?
    }
    \only<3>{
        If the edges are directed, i.e., \( (x,y) \) and \( (y,x) \) are different edges,
        the graph is said to be \alert{directed} or a \alert{digraph}.
    }

    \begin{figure}[htpb]
        \centering
        \only<1-2>{
            \input{../graphs/fig-22-1.tex}
            \caption{An undirected graph $G$}
        }
        \only<3>{
            \input{../graphs/fig-22-2.tex}
            \caption{A directed graph $G$}
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Adjacent-List Representation of Graph}

    If $G$ is \alert{sparse}, i.e., $|E| = o(|V|^2)$, then we often represent $G$
    with in \alert{adjacent-list} form.
    
    \begin{figure}[htpb]
        \centering
        \hfill{}
        \input{../graphs/fig-22-1.tex}
        \hfill{}
        \includegraphics[width=0.6\linewidth]{graph-adjacent-list.png}
        \hfill{}
        \caption{A graph and its adjacent-list representation}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Adjacent-Matrix Representation of Graph}

    If $G$ is \alert{dense}, i.e., $|E| = \Theta(|V|^2)$, then we often represent $G$
    with in \alert{adjacent-matrix} form.

    \begin{figure}[htpb]
        \centering
        \hfill{}
        \input{../graphs/fig-22-1.tex}
        \hfill{}
        \includegraphics[width=0.3\linewidth]{graph-adjacent-matrix.png}
        \hfill{}
        \caption{A graph and its adjacent-matrix representation}
    \end{figure}

    \cake{} Why is the adjacent-matrix for an undirected graph symmetric over
    the diagonal?
\end{frame}

\begin{frame}
    \frametitle{Representation of Digraph}

    We can also represent a \alert{digraph} in both adjacent-list and adjacent-matrix form.

    \begin{figure}[htpb]
        \centering
        \hfill{}
        \input{../graphs/fig-22-2.tex}
        \hfill{}
        \includegraphics<1>[width=0.4\linewidth]{digraph-adjacent-list.png}
        \includegraphics<2>[width=0.3\linewidth]{digraph-adjacent-matrix.png}
        \hfill{}
        \caption{A digraph and its representations}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Attributes}

    We use the notations $u.f$ and $(u,v).f$ to indicate the attributes of a vertex or an edge.

    These attributes, such as edge weight, can be stored in adjacent-list or
    adjacent-matrix form too.

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \input{../graphs/fig-22-1-weight.tex}
                \caption{A weighted graph}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \[
                \begin{bmatrix}
                    0 & 5 & 0 & 9 & 15 \\
                    5 & 0 & 12 & 0 & 4 \\
                    0 & 12 & 0 & 7 & 0 \\
                    9 & 0 & 7 & 0 & 3 \\
                    15 & 4 & 0 & 3 & 0 \\
                \end{bmatrix}
            \]
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 22.2 Breadth-first search}

\begin{frame}
    \frametitle{$8$-Puzzle}

    \cake{} Can you design an algorithm which moves the tiles to their correct
    positions using the empty space?

    \begin{figure}[htpb]
        \centering
        \hfill{}
        \includegraphics[width=0.45\textwidth]{dog-shuffle.jpg}
        \hfill{}
        \includegraphics[width=0.45\textwidth]{dog.jpg}
        \hfill{}
        \caption{An $8$-puzzle}
    \end{figure}

    Try it at \url{https://murhafsousli.github.io/8puzzle/}.
\end{frame}

\begin{frame}
    \frametitle{A Pandemic on a Graph}

    Assume that a \emoji{sneezing-face} is spreading on a graph \( G = (V, E) \).

    Vertex $1$ is infected first on day $0$.

    Each day, an infected vertex \(v\) infects all its neighbours.

    How many days will it take for all vertices to become \emoji{sneezing-face}?

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \tikzstyle{graphnode}=[
                    circle,
                    draw=black,
                    fill=white, 
                    ultra thick,
                    minimum height = 0.8cm,
                    minimum width = 0.8cm,
                ]
                \input{../graphs/fig-22-1.tex}
                \caption{A graph $G$}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{\ac{bfs}}
    \ac{bfs} is a cornerstone graph traversal algorithm which works like the
    spread of \emoji{mask}.

    \ac{bfs} operates on a graph \( G = (V, E) \) with a source vertex \( s \).

    The algorithm explores edges of \( G \) to find all vertices reachable from \( s \).

    \ac{bfs} computes the distance from \( s \) to each vertex, constructing a breadth-first tree.

    Paths in the tree represent the shortest paths in \( G \).
\end{frame}

\begin{frame}
    \frametitle{Colours in \ac{bfs}}

    Vertices are initially \colorbox{white}{\color{black} white}, becoming \colorbox{TrolleyGrey}{\color{white} grey} upon discovery, and black once fully explored.
    \pause{}

    Discovery occurs upon first encounter in the search.
    \pause{}

    A \colorbox{TrolleyGrey}{\color{white} grey} vertex has been discovered but not fully explored.
    \pause{}

    A \colorbox{black}{\color{white} black} vertex, and all its adjacent vertices, have been fully explored.
    \pause{}

    The frontier between discovered and undiscovered vertices is marked by grey vertices with adjacent \colorbox{white}{\color{black} white} vertices.
    \pause{}

    The colouring ensures that the algorithm progresses level by level in true breadth-first fashion.
\end{frame}

\begin{frame}
    \frametitle{The Queue Data Structure}

    Definition: A queue is a linear data structure that follows the First In,
    First Out (FIFO) principle.

    Operations:
    \begin{itemize}
        \item Enqueue: Add an element to the end of the queue.
        \item Dequeue: Remove an element from the front of the queue.
    \end{itemize}

    Real-world Analogy: A line of customers waiting their turn.
\end{frame}

\begin{frame}[fragile]{\ac{bfs} Pseudocode}
    \begin{algorithmic}[1]
        \only<1>{
        \Procedure{BFS}{$G, s$}
            \For{each vertex $u \in G.V - \{s\}$}
                \State $u.color = \text{WHITE}$
                \State $u.d = \infty$
                \State $u.\pi = \text{NIL}$
            \EndFor
            \State $s.color = \text{GRAY}$
            \State $s.d = 0$
            \State $s.\pi = \text{NIL}$
            \State $Q = \emptyset$
            \State \textsc{Enqueue}$(Q, s)$
            \algstore{bkmark}
        }
        \only<2>{
            \algrestore{bkmark}
            \While{$Q \neq \emptyset$}
                \State $u = \textsc{Dequeue}(Q)$
                \For{each $v \in G.Adj[u]$}
                    \If{$v.color == \text{WHITE}$}
                        \State $v.color = \text{GRAY}$
                        \State $v.d = u.d + 1$
                        \State $v.\pi = u$
                        \State \textsc{Enqueue}$(Q, v)$
                    \EndIf
                \EndFor
                \State $u.color = \text{BLACK}$
            \EndWhile
        \EndProcedure
        }
    \end{algorithmic}

    \only<2>{\cake{} What's the \emoji{stopwatch} complexity?}
\end{frame}

\begin{frame}
    \frametitle{Example of Running \ac{bfs}}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{fig-22-3.png}
        \caption{Run \ac{bfs} on this graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{} Run \ac{bfs} on the following graph with $s=1$ and stops after three
    vertices become black. 

    What are the colours and $v.d$ for all vertices $v$ at this moment?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[scale=1,auto=left,every
            node/.style={circle,fill=white,draw=black}]
            \node[label=1] (n1) at (90:1.5) {$\infty$};
            \node[label=2] (n2) at (18:1.5) {$\infty$};
            \node[label=3] (n3) at (306:1.5) {$\infty$};
            \node[label=4] (n4) at (234:1.5) {$\infty$};
            \node[label=5] (n5) at (162:1.5) {$\infty$};

            \foreach \from/\to in {n1/n2,n2/n3,n3/n4,n4/n5,n5/n1}
            \draw (\from) -- (\to);
            \draw (n1) -- (n3);
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Distance in Graph}

    For two vertices $u, v$ in \(G\), the \alert{distance}  from \(u\) to \(v\) is the
    length of the shortest path from \(u\) to \(v\).

    We denote this distance by $\delta(u, v)$.

    If there is no path from \(u\) to \(v\), then $\delta(u, v) = \infty$.

    \cake{} What are $\delta(1, 2), \delta(1, 3), \delta(5,3)$?

    \begin{figure}[htpb]
        \centering
        \input{../graphs/fig-22-1.tex}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lemma 22.1}

    Let $G=(V, E)$ be a graph. Let $s \in V$ be an arbitrary vertex.

    Then for every edge $(u, v) \in E$,
    \begin{equation*}
        \delta(s, u) \le \delta(s, v) + 1.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Lemma 22.2}
    Upon the termination, for each vertex \( v \in V \), the value \( v.d \) computed by
    \ac{bfs} satisfies \( v.d \geq \delta(s, v) \).
\end{frame}

\begin{frame}
    \frametitle{Lemma 22.3}

    Suppose that during the execution of \ac{bfs} the queue \( Q \)
    contains the vertices
    \begin{equation*}
        (v_1, v_2, \ldots, v_r),
    \end{equation*}
    where \( v_1 \) is the head of \( Q \) and \( v_r \) is the tail.
    Then,
    \begin{equation*}
        v_r.d \leq v_1.d + 1
    \end{equation*}
    and
    \begin{equation*}
        v_i.d \leq v_{i+1}.d \qquad \text{for } i \in\{1, \ldots, r - 1\}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Corollary 22.4}
    Suppose that vertices \( v_i \) and \( v_j \) are enqueued during the
    execution of ac{bfs}, and that \( v_i \) is enqueued before \( v_j \). Then \(
    v_i.d \leq v_j.d \) at the time that \( v_j \) is enqueued.
\end{frame}


\begin{frame}{Theorem 22.5 --- Correctness of \ac{bfs}}
    During its execution, ac{bfs} discovers every vertex \( v \in V \) that is reachable from
    the source \( s \), and upon termination, \( v.d = \delta(s, v) \) for all \( v
    \in V \). 

    Moreover, for any vertex \( v \neq s \) that is reachable from \( s
    \), one of the shortest paths from \( s \) to \( v \) is a shortest path from \(
    s \) to \( v.\pi \) followed by the edge \( (v.\pi, v) \).
\end{frame}

\begin{frame}
    \frametitle{Predecessor Subgraph}

    For a graph $G = (V, E)$ with source $s$, we define the
    \alert{predecessor subgraph} of a \ac{bfs} as $G_{\pi} = (V_{\pi}, E_{\pi})$,
    where
    \begin{equation*}
        V_{\pi} = \{ v \in V : v.\pi \neq \text{NIL}\} \cup \{s\}
    \end{equation*}
    and
    \begin{equation*}
        E_{\pi} = \{(v.\pi, v) : v \in V_{\pi} - \{s\}\}.
    \end{equation*}

    \cake{} What is $G_{\pi}$ for the following graph if $s=1$?

    \begin{figure}[htpb]
        \centering
        \input{../graphs/fig-22-1.tex}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Breadth-first Tree}

    A subgraph of $G$ is called a \alert{breadth-first tree} if
    \begin{itemize}
        \item it contains all vertices reachable from $s$,
        \item every path in the it is also the shortest path between the two
            endpoints,
        \item it is a tree.
    \end{itemize}

    \cake{} \only<1>{What is a breath-first tree for the following graph if
    $s=1$?}\only<2>{Can you think of another one?}

    \begin{figure}[htpb]
        \centering
        \input{../graphs/fig-22-1.tex}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lemma 22.6}

    The predecessor subgraph of a graph $G_{\pi}$ produced by \ac{bfs} is a
    breath-first tree.
\end{frame}

\begin{frame}
    \frametitle{Find the Shortest Path}

    Given a graph $G$ whose breath-first tree has computed by \ac{bfs},
    we can print the shortest path from $s$ to $v$ using the following algorithm:


    \begin{algorithmic}
        \Procedure{Print-Path}{$G, s, v$}
        \If{$v == s$}
        \State \text{print } s
        \ElsIf{$v.\pi == \text{NIL}$}
        \State \text{print ``no path from'' } s \text{ ``to'' } v \text{ ``exists''}
        \Else
        \State \Call{Print-Path}{$G, s, v.\pi$}
        \State \text{print } v
        \EndIf
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Solve Maze}

    \cake{} How can we solve this maze using \ac{bfs}?

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{
            \begin{tikzpicture}[scale=0.5]
                % Grid
                \draw[gray] (0,0) grid (8,8);

                \foreach \x in {0,1,2,3,4,5,6,7,8}
                {
                    \foreach \y in {0,1,2,3,4,5,6,7,8}
                    {
                        \node[circle, minimum width=0.1cm, inner sep=0pt, fill=orange] at (\x,\y) {};
                    }
                }

                % Maze code
                \draw[line width=5pt,
                    cap=round,
                    rounded corners=1pt,
                    draw=black!75] (-0.5,-0.5) -| (8.5,8.5)
                    (7.5,8.5)-|(-0.5,0.5) -- (0.5,0.5) (-0.5,2.5)-|(0.5,4.5)
                    (0.5,5.5)|-(4.5,7.5)|-(3.5,3.5)|-(2.5,2.5)|-(3.5,1.5)
                    (8.5,0.5)--(6.5,0.5)
                    (5.5,0.5)-|(3.5,-0.5)
                    (3.5,0.5)-|(1.5,3.5)-|(2.5,5.5)
                    (2.5,4.5)-|(3.5,6.5)-|(1.5,4.5)
                    (7.5,8.5)|-(5.5,6.5)--(5.5,7.5)
                    (6.5,7.5)--(6.5,8.5)
                    (8.5,3.5)-|(6.5,4.5)-|(7.5,5.5)
                    (6.5,5.5)-|(5.5,2.5)--(4.5,2.5)
                    (5.5,2.5)-|(7.5,1.5)--(4.5,1.5)
                    (5.5,4.5)--(4.5,4.5)
                    (1.5,1.5)--(0.5,1.5)
                    (1.5,7.5)--(1.5,8.5);

                % Start and End Points
                \draw[-latex,line width=3pt,green] (-1,0)--(0,0);
                \draw[-latex,line width=3pt,green] (8,8) -- (8,9);
            \end{tikzpicture}
        }
        \caption{A maze}
    \end{figure}

    Try it out at \url{https://brkwok.github.io/Maze-Solver/}.
\end{frame}

\begin{frame}
    \frametitle{Solve $8$-Puzzle}

    \cake{} How can we solve this puzzle using \ac{bfs}?

    \begin{figure}[htpb]
        \centering
        \hfill{}
        \includegraphics[width=0.45\textwidth]{dog-shuffle.jpg}
        \hfill{}
        \includegraphics[width=0.45\textwidth]{dog.jpg}
        \hfill{}
        \caption{A $8$-puzzle}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Water Pouring Puzzle}

    We have a jug filled with 8 units of water, and two empty jugs of sizes 5 and 3. 

    How can we make the first and second jugs both contain 4 units, and the
    third is empty, without using any extra tools?

    \puzzle{} How to solve this puzzle using \ac{bfs}?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{water-pouring-puzzle.jpg}
    \end{figure}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 22.1.
                \item
                    Section 22.2.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 22.1: 1--6.
                \item
                    Exercises 22.2: 1--7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
