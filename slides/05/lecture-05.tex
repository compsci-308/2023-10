\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Divide and Conquer (Part 1)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ita} 2.3 Merge Sort}

\begin{frame}[c]
    \frametitle{Divide and Conquer}
    
    \begin{columns}[c]
        % Left Column
        \begin{column}{0.5\textwidth}
            \alert{Divide and Conquer} is a common technique in designing
            algorithms:
            \begin{itemize}
                \item \alert{Divide}: Break the problem into smaller sub-problems.
                \item \alert{Conquer}: Solve the smaller sub-problems.
                \item \alert{Combine}: Merge the solutions to create a solution for the original problem.
            \end{itemize}
        \end{column}
        % Right Column
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-divide-and-conquer.jpg}
                \caption{How to divide and conquer a big pizza}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Merge Sort}

    \alert{Merge Sort} is a classic example of a divide and conquer algorithm.

    \begin{itemize}
        \item \textit{Divide}: Split the unsorted array into two subarrays, each
            containing half of the elements.
        \item \textit{Conquer}: Sort each subarray.
        \item \textit{Combine}: Merge the two sorted subarrays.
    \end{itemize}

    \pause{}

    The running time \( T(n) \) satisfies
    \begin{equation*}
        T(n) = 2T\left(\frac{n}{2}\right) + \Theta(n)
    \end{equation*}
    In the next lecture, we will derive that \( T(n) = \Theta(n \log n) \).
\end{frame}

\begin{frame}
    \frametitle{Pseudo Code of Merge Sort}
    
    \begin{algorithmic}[1]
        \Procedure{Merge-Sort}{$A, p, r$}
            \If{$p < r$}
                \State $q \gets \lfloor (p + r) / 2 \rfloor$
                \State \Call{Merge-Sort}{$A, p, q$}
                \State \Call{Merge-Sort}{$A, q+1, r$}
                \State \Call{Merge}{$A, p, q, r$} \Comment{\dizzy{} Most complicated!}
            \EndIf
        \EndProcedure
    \end{algorithmic}

    \cake{} Let $D(n)$ be the running time of line 3. What is the order of growth of $D(n)$?
\end{frame}

\begin{frame}
    \frametitle{Example of Merge Sort}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.9\textwidth]{merge-sort-divide.png}
        \includegraphics<2>[width=0.9\textwidth]{merge-sort-combine.png}
        \caption{\only<1>{Divide}\only<2>{Combine}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Pseudo Code of Merge Function}
    
    \begin{algorithmic}[1]
        \only<1>{
            \Procedure{Merge}{$A, p, q, r$}
                \State $n_1 \gets q - p + 1$
                \State $n_2 \gets r - q$
                \State Let $L[1 \ldots n_1 + 1]$ and $R[1 \ldots n_2 + 1]$ be new arrays
                \For{$i \gets 1$ to $n_1$}
                    \State $L[i] \gets A[p + i - 1]$
                \EndFor
                \For{$j \gets 1$ to $n_2$}
                    \State $R[j] \gets A[q + j]$
                \EndFor
                \State $L[n_1 + 1] \gets \infty$ \Comment{A sentinel}
                \State $R[n_2 + 1] \gets \infty$
            \algstore{bkmark}
        }
        
        \only<2>{
            \algrestore{bkmark}
            \State $i \gets 1$
            \State $j \gets 1$
            \For{$k \gets p$ to $r$}
                \If{$L[i] \leq R[j]$}
                    \State $A[k] \gets L[i]$
                    \State $i \gets i + 1$
                \Else
                    \State $A[k] \gets R[j]$
                    \State $j \gets j + 1$
                \EndIf
            \EndFor
        \EndProcedure
        }
    \end{algorithmic}
    \only<2>{\cake{} Let $C(n)$ be the running time of the above procedure. 
    What is the order of growth of $C(n)$?}
\end{frame}

\begin{frame}
    \frametitle{Example of Merge}
    
    Let's run $\texttt{Merge}(A, 9, 12, 16)$ given the following array:

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{merge-sort-merge-01.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Time Complexity of Merge Sort}

    Let \( T(n) \) denote the running time of the merge sort algorithm.
    Then \(T(n)\) satisfies
    \begin{equation*}
        \label{eq:merge-sort-full}
        T(n) = 
        \begin{cases} 
            \Theta(1) & \text{if } n = 1, \\
            D(n) + 
            T\left(\left\lfloor \frac{n}{2} \right\rfloor \right) +
            T\left(\left\lceil \frac{n}{2} \right\rceil \right) + 
            C(n) & \text{otherwise}.
        \end{cases}
    \end{equation*}

    \pause

    Usually we can ignore 
    \begin{itemize}
        \item the floor and ceiling functions, 
        \item and the boundary conditions.
    \end{itemize}

    This leads to the simplified equation
    \begin{equation*}
        \label{eq:merge-sort-simple}
        T(n) = 
            2 T\left( \frac{n}{2} \right) +
            \Theta(n)
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Solve an Exact Recursive Equation}
    
    Suppose instead of asymptotic notations in the recursion, we have
    \begin{equation*}
        \label{eq:merge-sort-full}
        T(n) = 
        \begin{cases} 
            c & \text{if } n = 1, \\
            2 T\left(\frac{n}{2}\right) +
            cn & \text{otherwise}.
        \end{cases}
    \end{equation*}
    Moreover, assume that $n = 2^{k}$ for some integer $k \ge 0$.

    Solving this recursive equation, we get
    \begin{equation*}
        T(n) = cn \lg n + cn.
    \end{equation*}
    The argument uses a \tree{}.
\end{frame}

\begin{frame}
    \frametitle{Solve an Exact Recursive Equation by Tree}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.9\textwidth]{merge-sort-tree-01.png}
        \includegraphics<2>[width=0.7\textwidth]{merge-sort-tree-02.png}
    \end{figure}

    \only<2>{\cake{} How many levels are there in this \tree{}?}
\end{frame}

\begin{frame}
    \frametitle{Solving a Recursive Equation}
    
    Given a recursion of the form
    \begin{equation*}
        T(n) = a T(n/b) + f(n)
    \end{equation*}
    there are three ways to solve it:
    \begin{itemize}
        \item Substitution Method: Section~4.3.
        \item Recursion-Tree Method: Section~4.4.
        \item Master Method: Section~4.5.
    \end{itemize}
    These will be covered in the next lecture.
\end{frame}

\section{\acs{ita} 4.1 The maximum-subarray problem}

\begin{frame}[c]
    \frametitle{A Motivational Example}

    \vspace{1em}
    
    \begin{columns}[c]

        \begin{column}{0.5\textwidth}
            The prices of a stock are given in the
            following plot.

            \medskip{}

            \cake{} If you buy once and sell once, when should you do so?

            \medskip{}

            \begin{tikzpicture}[y=0.5cm, x=0.8cm]
                % Define the axis
                \draw[->] (0,5) -- (5,5) node[right] {Day};
                \draw[->] (0,5) -- (0,12) node[above] {Price};

                % Draw the points for the stock prices
                \foreach \x/\y in {0/10,1/11,2/7,3/10,4/6} {
                    \fill (\x,\y) circle (2pt);
                }

                % Connect the points
                \draw[line width=1pt] (0,10) -- (1,11) -- (2,7) -- (3,10) -- (4,6);

                % Label the days
                \foreach \x in {0,1,2,3,4}
                \draw (\x,5.2) -- (\x,4.8) node[below] {$\x$};

                % Label the prices
                \foreach \y in {6,7,8, 9, 10,11}
                \draw (0,\y) -- (-0.5,\y) node[left] {\y};
            \end{tikzpicture}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-in-cash.jpg}
                \caption{What is the algorithm to maximize \emoji{moneybag}!}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{The Maximum Subarray Problem}
    
    Given an array \( A[1, \ldots, n] \), the \alert{maximum subarray problem} aims to find indices \( p \) and \( q \) such that \( 1 \leq p \leq q \leq n \), maximizing the sum
    \begin{equation*}
        \sum_{i=p}^{q} A[i].
    \end{equation*}

    \laughing{} You can think of $A[i]$ as the change of stock from price day $i-1$
    to day $i$.

    \pause{}

    \cake{} Can you think of a brute force algorithm to solve this problem?

    \puzzle{} What is the time complexity of this brute force algorithm?
\end{frame}

\begin{frame}
    \frametitle{The Divide and Conquer Algorithm}

    \begin{enumerate}
        \item \textbf{Divide}: Split the array into two halves.
        \item \textbf{Conquer}: Recursively find the maximum subarray in each
            case:
            \begin{itemize}
                \item Maximum subarray entirely in the left half.
                \item Maximum subarray entirely in the right half.
                \item Maximum subarray crossing the midpoint.
            \end{itemize}
        \item \textbf{Combine}: Pick the maximum subarray from each of the three
            possibilities.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Find Maximum Crossing Midpoint Subarray}
    \begin{algorithmic}[1]
        \only<1>{
            \Procedure{Max-Crossing-Subarr}{$A, \text{low}, \text{mid}, \text{high}$}
            \State $\text{left-sum} \gets -\infty$
            \State $\text{sum} \gets 0$
            \For{$i \gets \text{mid} \text{ downto low}$}
            \State $\text{sum} \gets \text{sum} + A[i]$
            \If{$\text{sum} > \text{left-sum}$}
            \State $\text{left-sum} \gets \text{sum}$
            \State $\text{max-left} \gets i$
            \EndIf
            \EndFor
            \algstore{bkmark}
        }
        \only<2>{
            \algrestore{bkmark}
            \State $\text{right-sum} \gets -\infty$
            \State $\text{sum} \gets 0$
            \For{$j \gets \text{mid} + 1 \text{ to high}$}
            \State $\text{sum} \gets \text{sum} + A[j]$
            \If{$\text{sum} > \text{right-sum}$}
            \State $\text{right-sum} \gets \text{sum}$
            \State $\text{max-right} \gets j$
            \EndIf
            \EndFor
            \State \Return $(\text{max-left}, \text{max-right}, \text{left-sum} + \text{right-sum})$
            \EndProcedure
        }
    \end{algorithmic}

    \only<2>{\cake{} What is the time complexity of this procedure?}
\end{frame}

\begin{frame}{Find Maximum Subarray Pseudo Code}
    \begin{algorithmic}[1]
        \only<1>{
        \Procedure{Max-Subarr}{$A, l, h$}  % changed low to l and high to h
            \If{$h == l$}
                \State \Return $(l, h, A[l])$  % changed low to l and high to h
            \EndIf
            \State $m \gets (l + h) / 2$  % changed mid to m, low to l, and high to h
            \State $(ll, lh, ls) \gets \Call{Max-Subarr}{A, l, m}$  % shortened variable names
            \State $(rl, rh, rs) \gets \Call{Max-Subarr}{A, m + 1, h}$  % shortened variable names
            \State $(cl, ch, cs) \gets \Call{Max-Crossing-Subarr}{A, l, m, h}$  % shortened variable names
            \algstore{bkmark}
        }
        \only<2>{
            \algrestore{bkmark}
            \If{$ls \geq rs$ \textbf{and} $ls \geq cs$}  % shortened variable names
            \State \Return $(ll, lh, ls)$  % shortened variable names
            \ElsIf{$rs \geq ls$ \textbf{and} $rs \geq cs$}  % shortened variable names
            \State \Return $(rl, rh, rs)$  % shortened variable names
            \Else
            \State \Return $(cl, ch, cs)$  % shortened variable names
            \EndIf
        \EndProcedure
        }
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Time Complexity of Maximum Subarray}

    Let \( T(n) \) denote the running time of the previous algorithm.
    Then \(T(n)\) satisfies
    \begin{equation*}
        \label{eq:merge-sort-full}
        T(n) = 
        \begin{cases} 
            \Theta(1) & \text{if } n = 1, \\
            D(n)  +
            2 T\left(\frac{n}{2}\right) +
            \Theta(n) +
            C(n) & \text{otherwise}.
        \end{cases}
    \end{equation*}

    \pause{}

    This simplifies to
    \begin{equation*}
        \label{eq:merge-sort-simple}
        T(n) = 
            2 T\left( \frac{n}{2} \right) +
            \Theta(n)
    \end{equation*}
    As the recursion is the same as merge sort, the solution is also
    \begin{equation*}
        T(n) = \Theta(n \log n).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    \cake{} What is the maximum subarray that crosses the midpoint in the following
    array?
    \begin{equation*}
        A = \langle 1, 2, 3, 3, 1, 2, -1, 0 \rangle.
    \end{equation*}
\end{frame}

\section{\acs{ita} 4.2 Strassen's algorithm for matrix multiplication}

\begin{frame}
    \frametitle{Review: Matrix Multiplication}
    
    \cake{} Do you remember how to compute
    \begin{equation*}
        \begin{bmatrix}
            1 & 2 \\
            3 & 4 \\
        \end{bmatrix}
        \begin{bmatrix}
            5 & 6 \\
            7 & 8 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Square Matrix Multiplication by Brute Force}
    
    \begin{algorithmic}
        \Procedure{SQUARE-MATRIX-MULTIPLY}{$A, B$}
        \State $n \gets A.\text{rows}$
        \State let $C$ be a new $n \times n$ matrix
        \For{$i = 1$ to $n$}
        \For{$j = 1$ to $n$}
        \State $c_{ij} \gets 0$
        \For{$k = 1$ to $n$}
        \State $c_{ij} \gets c_{ij} + a_{ik} \cdot b_{kj}$
        \EndFor
        \EndFor
        \EndFor
        \State \Return $C$
        \EndProcedure
    \end{algorithmic}

    \cake{} What is the time complexity of this algorithm?
\end{frame}

\begin{frame}{Matrix Partitioning}
    Given \( n \times n \) matrices \( A \), \( B \), and \( C = A B \),
    we can partition them
    \begin{equation*}
        A = \begin{pmatrix} A_{11} & A_{12} \\ A_{21} & A_{22} \end{pmatrix}, \quad
        B = \begin{pmatrix} B_{11} & B_{12} \\ B_{21} & B_{22} \end{pmatrix}, \quad
        C = \begin{pmatrix} C_{11} & C_{12} \\ C_{21} & C_{22} \end{pmatrix}
    \end{equation*}
    where the sub-matrices are of size \( n/2 \times n/2 \).

    \pause{}

    % Replace this comment with the example
    For instance, consider
    \[
        A = \begin{pmatrix}
            2 & 3 & 0 & 2 \\
            2 & 3 & 0 & 0 \\
            2 & 1 & 2 & 2 \\
            2 & 2 & 3 & 0
        \end{pmatrix},
        B = \begin{pmatrix}
            3 & 3 & 3 & 2 \\
            1 & 0 & 1 & 3 \\
            3 & 1 & 1 & 1 \\
            3 & 3 & 0 & 0
        \end{pmatrix},
        \quad
        C = \begin{pmatrix}
            15 & 12 & 9 & 13 \\
            9 & 6 & 9 & 13 \\
            19 & 14 & 9 & 9 \\
            17 & 9 & 11 & 13
        \end{pmatrix}
    \]

    \cake{} Can you identify what \( A_{11} \), \dots, \( C_{22} \) are?
\end{frame}

\begin{frame}{Matrix Multiplication by Partitioning}
    Then, $C = A \cdot B$ can be written as:
    \begin{equation*}
        \begin{pmatrix} C_{11} & C_{12} \\ C_{21} & C_{22} \end{pmatrix} =
        \begin{pmatrix} A_{11} & A_{12} \\ A_{21} & A_{22} \end{pmatrix} \cdot
        \begin{pmatrix} B_{11} & B_{12} \\ B_{21} & B_{22} \end{pmatrix}
    \end{equation*}

    This leads to four corresponding equations.
    \begin{align*}
        C_{11} &= A_{11} \cdot B_{11} + A_{12} \cdot B_{21}, \\
        C_{12} &= A_{11} \cdot B_{12} + A_{12} \cdot B_{22}, \\
        C_{21} &= A_{21} \cdot B_{11} + A_{22} \cdot B_{21}, \\
        C_{22} &= A_{21} \cdot B_{12} + A_{22} \cdot B_{22}.
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Recursive Square Matrix Multiplication Algorithm}
    
    \begin{algorithmic}
        \Function{SMMR}{$A, B$} % Square-Matrix-Multiply-Recursive abbreviated as SMMR
        \State $n \gets A.\text{rows}$
        \State $C \gets$ new $n \times n$ matrix
        \If{$n = 1$}
        \State $C_{11} \gets a_{11} \cdot b_{11}$
        \Else
        \State Partition $A, B,$ and $C$ as in equations (4.9)
        \State $C_{11} \gets \Call{SMMR}{A_{11}, B_{11}} + \Call{SMMR}{A_{12}, B_{21}}$
        \State $C_{12} \gets \Call{SMMR}{A_{11}, B_{12}} + \Call{SMMR}{A_{12}, B_{22}}$
        \State $C_{21} \gets \Call{SMMR}{A_{21}, B_{11}} + \Call{SMMR}{A_{22}, B_{21}}$
        \State $C_{22} \gets \Call{SMMR}{A_{21}, B_{12}} + \Call{SMMR}{A_{22}, B_{22}}$
        \EndIf
        \State \Return $C$
        \EndFunction
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Time Complexity of Recursive Square Matrix Multiplication}

    Let \( T(n) \) denote the running time of \textsc{smmr}.
    Then \(T(n)\) satisfies
    \begin{equation*}
        \label{eq:merge-sort-full}
        T(n) = 
        \begin{cases} 
            \Theta(1) & \text{if } n = 1, \\
            8 T\left(\frac{n}{2} \right)
            +
            D(n) + C(n) & \text{otherwise}.
        \end{cases}
    \end{equation*}

    Thus,
    \begin{equation*}
        \label{eq:merge-sort-simple}
        T(n) = 
            8 T\left(\frac{n}{2} \right) + \Theta(n^2).
    \end{equation*}

    In the next lecture, we'll see that this implies $T(n) = \Theta(n^{\lg 8})$.

    \cake{} So is this a ``useful'' algorithm?
\end{frame}

\begin{frame}
    \frametitle{Strassen's Algorithm}
    
    Strassen's method improves \textsc{smmr} by reducing the number of
    multiplication to $7$.

    Thus it's running time satisfies
    \begin{equation*}
        \label{eq:merge-sort-simple}
        T(n) = 
            7 T\left(\frac{n}{2} \right) + \Theta(n^2).
    \end{equation*}

    In the next lecture, we'll see that this implies $T(n) = \Theta(n^{\lg 7})$.
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 2.3
                \item
                    Section 4.1
                \item
                    Section 4.2
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 2.3: 1--3.
                \item
                    Exercises 4.1: 1--2, 4-5.
                \item
                    Exercises 4.2: 1--5.
            \end{itemize}
        \end{column}
    \end{columns}

\end{frame}

\end{document}
