\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Linear Programming (Part 4)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 29.4.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 29.4: 1-5.
            \end{itemize}

            \medskip{}

            \smiling{} Section 29.5 is \emph{not} required.
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 29.4 Duality}

\begin{frame}[c]
    \frametitle{Duality}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}

            \alert{Duality}  --- Given a maximization problem, we can define a
            minimization problem such that the two problems have the same optimal
            solution.

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-mountains.jpg}
                \caption{A mountain and its dual}
            \end{figure}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{Maximum Flow}

    Let $G = (V, E)$ be a directed graph in which each edge $(u,v)$ has a
    capacity $c(u, v)$.

    In the max-flow problem, we want to find the maximum flow in $G$ from $s$ to
    $t$ such that $f_{u,v}$, the flow on $(u,v)$, is at most $c(u,v)$, for every
    edge $(u,v)$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{max-flow-with-capacity.png}
        \caption{$G$ with a flow of $11+12=23$ unit from $s$ to $t$. Is it a max-flow?}
        \label{fig:max-flow-with-capacity}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Minimum Cut}

    Given a cut of $(S, T=V-S)$ of $G$, the capacity of the cut is
    \begin{equation*}
        c(S,T) = \sum_{u \in S} \sum_{v \in T} c(u,v)
    \end{equation*}

    In the min-cut problem, we want to find a cut of $(S, T)$ of $G$ such that
    its capacity is minimized.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{min-cut.png}
        \caption{A cut of capacity $12+14=26$. Is it a min-cut?}
        \label{fig:min-cut}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Duality of Max-Flow and Min-Cut}

    \begin{tcolorbox}[title={Corollary 26.5}]
        The value of any flow $G$ is bounded above by the capacity of any cut of
        $G$.
    \end{tcolorbox}

    \begin{tcolorbox}[title={Theorem 26.6}]
        The maximum flow in $G$ equals the capacity of the minimum cut.
    \end{tcolorbox}

    \smiling{} This implies that either the flow in Figure~\ref{fig:max-flow-with-capacity}
    is \emph{not} maximum, 
    or the cut in Figure~\ref{fig:min-cut} is \emph{not} minimum.
\end{frame}

\begin{frame}
    \frametitle{The Dual \ac{lp} Problem}
    Given an \ac{lp} problem in standard form
    \begin{equation*}
        \begin{aligned}
            \text{maximize}   \qquad & \sum_{j=1}^{n} c_{j} x_{j} \\
            \text{subject to} \qquad & \sum_{j=1}^{n} a_{ij} x_{j} \le b_{i}
                                     & \qquad \text{for all } i \in \{1, \ldots, m\} \\
                                     & x_{j} \ge 0 & \qquad \text{for all } j \in \{1, \ldots, n\}
        \end{aligned}
    \end{equation*}
    we define its \alert{dual}  \ac{lp} problem as
    \begin{equation*}
        \begin{aligned}
            \text{minimize}   \qquad & \sum_{i=1}^{m} b_{i} y_{i} \\
            \text{subject to} \qquad & \sum_{i=1}^{m} a_{ij} y_{i} \ge c_{j}
                                     & \qquad \text{for all } j \in \{1, \ldots, n\} \\
                                     & y_{j} \ge 0 & \qquad \text{for all } i \in \{1, \ldots, m\}
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example of a Dual Problem}
    Consider the following problem
    \begin{align*}
        \text{maximize} \quad & 3x_1 + x_2 \\
        \text{subject to} \quad & x_1 + x_2 \leq 30 \\
                                & 2x_1 + 2x_2 \leq 24 \\
                                & 4x_1 + x_2 \leq 36 \\
                                & x_1, x_2 \geq 0
    \end{align*}
    \cake{} What are $b$, $c$, $n$ and $m$ here? What is the dual problem?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Can you find the dual?
    \begin{equation*}
        \begin{aligned}
            \text{maximize} \quad & x_1 + 10 x_2 \\
            \text{subject to} \quad & x_1 \le 5 \\
                                    & x_2 \le 3 \\
                                    & x_1, x_2 \geq 0.
        \end{aligned}
    \end{equation*}
    What are the optimal objective values of the original and the dual?
\end{frame}

\begin{frame}
    \frametitle{Lemma 29.8: Weak \ac{lp} Duality}

    Let $\bar{x}$ and $\bar{y}$ be feasible solutions to the original and dual
    problems respectively.
    Then 
    \begin{equation*}
        \sum_{j=1}^{n} c_{j} \bar{x}_{j} \le \sum_{i=1}^{m} b_{i} \bar{y}_{i}
    \end{equation*}

\end{frame}

\begin{frame}{Corollary 29.9}
    Both $\bar{x}$ and $\bar{y}$ are optimal if
    \begin{equation*}
        \sum_{j=1}^{n} c_{j} \bar{x}_{j} = \sum_{i=1}^{m} b_{i} \bar{y}_{i}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The Optimal Solutions}

    Let the final slack form returned by \textsc{Pivot-Repeatedly} be
    \begin{equation*}
        \begin{aligned}
            z & = v' + \sum_{j \in N} c_{j}' x_{j} \\
            x_i & = b_i' - \sum_{j \in N} a_{ij}' x_{j}
        \end{aligned}
    \end{equation*}
    Then its basic solution $\bar{x}$ is the optimal solution for the original
    problem.

    We will show that
    \begin{equation*}
        \bar{y}_{i} = 
        \begin{cases}
            - c_{n+i}' & \text{if } n+i \in N \\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}
    is the optimal solution for the dual problem.
\end{frame}

\begin{frame}
    \frametitle{Example of an Optimal Solution}

    \only<1>{
        The following problem
        \begin{equation*}
            \begin{aligned}
                \text{maximize} \quad & x_1 + 10 x_2 \\
                \text{subject to} \quad & x_1 \le 5 \\
                                        & x_2 \le 3 \\
                                        & x_1, x_2 \geq 0.
            \end{aligned}
        \end{equation*}
        has a dual problem:
        \begin{equation*}
            \begin{aligned}
                \text{minimize} \quad & 5 y_1 + 3 y_2 \\
                \text{subject to} \quad & y_1 \ge 1 \\
                                        & y_2 \ge 10 \\
                                        & y_1, y_2 \geq 0.
            \end{aligned}
        \end{equation*}
        \cake{} What is the optimal objective value of the dual?
    }
    \only<2>{
        Solving the original with \textsc{Pivot-Repeatedly}, we get the final
        slack form
        \begin{equation*}
            \begin{aligned}
                z & = 35 - x_3  - 10 x_{4} \\
                x_1 & = 5 - x_1 \\
                x_1 & = 3 - x_4
            \end{aligned}
        \end{equation*}
        How to use the formula
        \begin{equation*}
            \bar{y}_{i} = 
            \begin{cases}
                - c_{n+i}' & \text{if } n+i \in N \\
                0 & \text{otherwise}
            \end{cases}
        \end{equation*}
        to find an optimal solution for the dual?
    }
    \only<3>{
        It is obvious that $\bar{y} = (1, 10)$ is the optimal solution for the
        dual:
        \begin{equation*}
            \begin{aligned}
                \text{minimize} \quad & 5 y_1 + 3 y_2 \\
                \text{subject to} \quad & y_1 \ge 1 \\
                                        & y_2 \ge 10 \\
                                        & y_1, y_2 \geq 0.
            \end{aligned}
        \end{equation*}
    }
\end{frame}

\begin{frame}
    \frametitle{Theorem 29.10: Linear-programming duality}
    Suppose  $\bar{x} = (\bar{x}_1, \dots, \bar{x}_{m+n})$ is the basic solution
    of the final slack form returned by \textsc{Pivot-Repeatedly}.

    Let $\bar{y}$ be the solution for the dual derived by
    \begin{equation*}
        \bar{y}_{i} = 
        \begin{cases}
            - c_{n+i}' & \text{if } n+i \in N \\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}

    Then
    \begin{equation*}
        \sum_{j=1}^{n} c_{j} \bar{x}_{j} = \sum_{i=1}^{m} b_{i} \bar{y}_{i},
    \end{equation*}
    which implies that both $\bar{x}$ and $\bar{y}$ are optimal.

    \cake{} Why does the equation imply optimality?
\end{frame}

\begin{frame}
    \frametitle{Proof Sketch}

    \only<1-3>{%
        Assume that the final slack form reached in \textsc{Simplex} contains
        \begin{equation*}
            z = v' + \sum_{j \in N} c_{j}' x_{j}
        \end{equation*}
    }%
    \only<2-3>{%
        Letting $c_j' = 0$ for all $j \in B$, we have
        \begin{equation*}
            z = v' + \sum_{j \in N} c_{j}' x_{j} + \sum_{j \in B} c_{j}' x_{j} = v' + \sum_{j = 1}^{m+n} c_{j}' x_{j}
        \end{equation*}
    }%
    \only<3-5>{%
        Since the slack form and the original form are equivalent,
        for \emph{any} choices of $x_1, \dots, x_{n}$ (which decides $x_{n+1}
        \dots, x_{m+n}$),
        \begin{equation*}
            \sum_{j=1}^{n} c_{j} x_{j} 
            =
            z
            = 
            v' + \sum_{j=1}^{m+n} c_{j}' x_{j}
            .
        \end{equation*}
    }%
    \only<4>{%
        Taking $x_1 = \bar{x}_1, \dots, x_{m+n} = \bar{x}_{m+n}$, we have
        \begin{equation*}
            \begin{aligned}
                \sum_{j=1}^{n} c_{j} \bar{x}_{j} 
                &
                = 
                v' + \sum_{j=1}^{m+n} c_{j}' \bar{x}_{j}
                \\
                &
                =
                v' + \sum_{j\in N} c_{j}' \bar{x}_{j} + \sum_{j\in B} c_{j}' \bar{x}_{j}
                \\
                &
                =
                v' + \sum_{j\in N} c_{j}' 0 + \sum_{j\in B} 0 x_{j}
                \\
                &
                =
                v'
                .
            \end{aligned}
        \end{equation*}
    }%
    \only<5>{%
        Using the definition of $\bar{y}$, we also have, for \emph{any} $x_{1}, \dots , x_{m+n}$
        \begin{equation*}
            \sum_{j=1}^{n} 
            c_{j} x_{j} 
            =
            \temoji{thinking}
            =
            \left(v' - \sum_{i=1}^{m} b_i \bar{y}_i\right) 
            + \sum_{j=1}^{n} \left( c'_j + \sum_{i=1}^{m} a_{ij} \bar{y}_i \right) x_j
        \end{equation*}
        \cake{} What do we get if we take $x_1 = x_2 = \dots = x_{m+n} = 0$?
    }%
    \only<6>{%
        Thus
        \begin{equation*}
            \sum_{i=1}^{m} b_i \bar{y}_i = v' = \sum_{j=1}^{n} c_j
            \bar{x}_j.
        \end{equation*}
        And we are done!
    }%
\end{frame}

\begin{frame}
    \frametitle{Non-Standard Form}

    Show that the following \ac{lp} problem (not in standard form)
    \begin{equation*}
        \begin{aligned}
            \text{maximize}   \qquad & \sum_{j=1}^{n} c_{j} x_{j} \\
            \text{subject to} \qquad & \sum_{j=1}^{n} a_{ij} x_{j} = b_{i}
                                     & \qquad \text{for all } i \in \{1, \ldots, m\} \\
                                     & x_{j} \ge 0 & \qquad \text{for all } j \in \{1, \ldots, n\}
        \end{aligned}
    \end{equation*}
    has a \alert{dual} \ac{lp} problem
    \begin{equation*}
        \begin{aligned}
            \text{minimize}   \qquad & \sum_{i=1}^{m} b_{i} y_{i} \\
            \text{subject to} \qquad & \sum_{i=1}^{m} a_{ij} y_{i} \ge c_{i}
                                     & \qquad \text{for all } j \in \{1, \ldots, n\} \\
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The Fundamental Duality Properties}

    The rules finding the dual of a general \ac{lp} problem are given in the
    following table.

    \begin{table}[h!]
        \small
        \centering
        \begin{tabular}{|c|c|}
            \hline
            \textbf{Primal (Maximize)} & \textbf{Dual (Minimize)} \\
            \hline
            $i$ constraint $\leq$ & $i$ variable $\geq$ 0 \\
            $i$ constraint $\geq$ & $i$ variable $\leq$ 0 \\
            $i$ constraint $=$ & $i$ variable unrestricted \\
            $j$ variable $\geq$ 0 & $j$ constraint $\geq$ \\
            $j$ variable $\leq$ 0 & $j$ constraint $\leq$ \\
            $j$ variable unrestricted & $j$ constraint $=$ \\
            \hline
        \end{tabular}
        \caption{Primal and Dual Relationships}
        \label{tab:primal_dual}
    \end{table}

    This table is what Exercise 29.4-2 is asking for.

    See a proof \href{https://web.mit.edu/15.053/www/AMP-Chapter-04.pdf}{here}.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    \small{}
    \begin{table}[h!]
        \centering
        \begin{tabular}{|c|c|}
            \hline
            \textbf{Primal (Maximize)} & \textbf{Dual (Minimize)} \\
            \hline
            $i$ constraint $\leq$ & $i$ variable $\geq$ 0 \\
            $i$ constraint $\geq$ & $i$ variable $\leq$ 0 \\
            $i$ constraint $=$ & $i$ variable unrestricted \\
            $j$ variable $\geq$ 0 & $j$ constraint $\geq$ \\
            $j$ variable $\leq$ 0 & $j$ constraint $\leq$ \\
            $j$ variable unrestricted & $j$ constraint $=$ \\
            \hline
        \end{tabular}
    \end{table}

    Use the table above to find the dual problem of
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{equation*}
                \begin{aligned}
                    \text{maximize} \quad & x_1 + 10 x_2 \\
                    \text{subject to} \quad & x_1 \ge 5 \\
                                            & x_2 \ge 3 \\
                                            & x_1 + x_2 = 20 \\
                                            & x_1, x_2 \geq 0.
                \end{aligned}
            \end{equation*}
        \end{column}
        \begin{column}{0.5\textwidth}
            
        \end{column}
    \end{columns}
\end{frame}

\end{document}
