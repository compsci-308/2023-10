\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Linear Programming (Part 1)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 29.1.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 29.1: 1--7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 29 Linear Programming}

\begin{frame}{A Election Problem}
    Imagine you are a campaign manager for an upcoming election.

    The number of voters are: 100k \emoji{cityscape}, 200k
    \emoji{house-with-garden}, 50k \emoji{national-park}.

    At least 50k, 100k, and 25k \emoji{ballot-box} from each
    group are needed.

    Determine the least amount of ads money needed.

    \begin{table}[ht]
        \small
        \centering
        \begin{tabular}{lccc}
        \hline
        \textbf{Policy} & \emoji{cityscape} & \emoji{house-with-garden} & \emoji{national-park} \\ \hline
        Build \emoji{motorway} & -2k \emoji{thumbs-down}             & 5k \emoji{thumbs-up}                & 3k \emoji{thumbs-up}             \\
        Control \emoji{gun} & 8k \emoji{thumbs-up}              & 2k \emoji{thumbs-up}                 & -5k \emoji{thumbs-down}            \\
        Subsidize  \emoji{tractor} & 0k \emoji{neutral-face}              & 0k \emoji{neutral-face}                 & 10k \emoji{thumbs-up}             \\
        Tax \emoji{oil-drum}    & 10k \emoji{thumbs-up}             & 0k \emoji{neutral-face}                 & -2k \emoji{thumbs-down}            \\ \hline
        \end{tabular}
        \caption{Effects on voter per \$1000 spend on ads}
        \label{tab:policy_effects}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{A Mathematical Model}

    Let $x_{1}, x_{2}, \ldots, x_{4}$ be the number of thousands of
    \emoji{dollar} spent on \temoji{motorway}, \temoji{gun}, \temoji{tractor} and
    \temoji{oil-drum}.

    Then you want to minimize the linear \alert{objective function} 
    \begin{equation*}
        x_1 + x_2 + x_3 + x_4
    \end{equation*}
    while satisfying the following linear \alert{constraints} 
    \begin{equation*}
        \begin{aligned}
            -2x_1 + 8x_2 + 0x_3 + 10x_4 & \geq 50 \\
            5x_1 + 2x_2 + 0x_3 + 0x_4  &\geq 100 \\
            3x_1 - 5x_2 + 10x_3 - 2x_4 & \geq 25
        \end{aligned}
    \end{equation*}
    This type of problems are called a \alert{\ac{lp} problem}.
\end{frame}

\begin{frame}
    \frametitle{A Second Example}

    Suppose that in order get at least 8 units of vitamin A and 10 units of
    vitamin C, we need to eat two types of food:
    \begin{itemize}
        \item \emoji{carrot} --- 2 units of vitamin A and 1 unit of vitamin C per kg, \$5/kg.
        \item \emoji{orange} --- 1 unit of vitamin A and 2 units of vitamin C per kg, \$7/kg.
    \end{itemize}

    We want to minimize the cost of the food combination.

    How to formulate this as an \ac{lp} problem?
\end{frame}

\begin{frame}
    \frametitle{\tps{} --- Balancing Studies and Gaming}

    You're a student with 20 hours per week for studying and gaming.

    Playing games is 10 times more enjoyable than studying.

    You want to maximize total enjoyment given the constraints:
    \begin{itemize}
        \item Minimum study time \(5\) hours.
        \item Minimum gaming time \(3\) hours.
    \end{itemize}

    \cake{} How to formulate this as a \ac{lp} problem?
\end{frame}

\begin{frame}
    \frametitle{Linear Programming Problem}
    
    \alert{\acf{lp}} optimizes a linear objective function subject to linear
    constraints.

    Common applications include:
    \begin{itemize}
        \item Resource allocation in businesses for cost minimization.
        \item Diet optimization for nutritional balance at minimal cost.
        \item Optimizing product mix in manufacturing to maximize profit.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Linear Function}

    Given a set of real numbers \( a_1, a_2, \ldots, a_n \) and a set of variables
    \( x_1, x_2, \ldots, x_n \), we define a \alert{linear function} \( f \) on
    those variables by
    \[
        f(x_1, x_2, \ldots, x_n) = a_1x_1 + a_2x_2 + \ldots + a_nx_n = \sum_{j=1}^{n} a_jx_j .
    \]

    \pause{}
    If \( b \) is a real number and \( f \) is a linear function, 
    \[
        f(x_1, x_2, \ldots, x_n) = b
    \]
    is a \alert{linear equality} and 
    \[
        f(x_1, x_2, \ldots, x_n) \leq b
        \qquad
        \text{and}
        \qquad
        f(x_1, x_2, \ldots, x_n) \geq b
    \]
    are \alert{linear inequalities}. 

    We call them \alert{linear constraints} or simply \alert{constraints}.
\end{frame}

\begin{frame}
    \frametitle{Linear Program Example}

    Let us first consider the following \ac{lp} problem in with two
    variables:    
    \begin{align*}
        \text{maximize} \quad & x_1 + x_2 \\
        \text{subject to} \quad & 4x_1 - x_2 \leq 8 \\
                                & 2x_1 + x_2 \leq 10 \\
                                & 5x_1 - 2x_2 \geq -2 \\
                                & x_1, x_2 \geq 0.
    \end{align*}
\end{frame}

\begin{frame}[c]
    \frametitle{The Feasible Area}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
        The intersection of the constraints
        \begin{align*}
            & 4x_1 - x_2 \leq 8 \\
            & 2x_1 + x_2 \leq 10 \\
            & 5x_1 - 2x_2 \geq -2 \\
            & x_1, x_2 \geq 0.
        \end{align*}
        forms the \alert{feasible area}.
        \end{column}
        \begin{column}{0.5\textwidth}
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\textwidth]{lp-feasible.png}
            \caption{The feasible area}
        \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{The Objective Value}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The value of the objective function
            \begin{equation*}
                x_1 + x_2
            \end{equation*}
            is called the \alert{objective value}. 

            \bigskip{}

            We could compute it at every point in the feasible area to find the
            optimal value.

            \bigskip{}

            \think{} How to be more efficient?
        \end{column}
        \begin{column}{0.5\textwidth}
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\textwidth]{lp-objective-function.png}
            \caption{The feasible area}
        \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\tps{} Exercise 29.1-4}
    
    \cake{} What is the feasible area for the following \ac{lp} problem?
    \begin{equation*}
        \begin{aligned}
            \text{maximize} \qquad & 3x_1 - 2x_2 \\
            \text{subject to} \qquad & x_1 + x_2 \leq 2 \\
                                     & -2x_1 - 2x_2 \leq -10 \\
                                     & x_1, x_2 \geq 0.
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Linear Programming Algorithms}

    The three best-known algorithms for solving linear programming problems are:
    \begin{itemize}
        \item The \alert{Simplex algorithm} performs efficiently in practice but
            may take exponential time for contrived inputs.
        \item \alert{Ellipsoid algorithm}: A polynomial-time method, slow in
            practice.
        \item \alert{Interior-point methods}: Polynomial-time, traverse the
            interior of the feasible region, often faster than Simplex for large
            inputs.
    \end{itemize}

    Further details on these algorithms are available in the chapter notes.

\end{frame}

\begin{frame}
    \frametitle{\ac{ilp}}

    \alert{\ac{ilp}} are similar to \ac{lp}, but
    with the added constraint that some or all variables are integers.

    \alert{Complexity}:
    \begin{itemize}
        \item \ac{lp} is typically solved in polynomial time.
        \item \ac{ilp} is NP-hard.
    \end{itemize}

    \alert{Applications}:
    \begin{itemize}
        \item \ac{lp} is used in diverse fields like economics, engineering, and military for continuous optimization.
        \item \ac{ilp} is essential in areas requiring discrete decisions, like scheduling, resource allocation, and logistics.
    \end{itemize}
\end{frame}

\section{\acs{ita} 29.1 Standard and Slack Forms}

\begin{frame}
    \frametitle{Canonical Forms of Linear Programs}
    We will use two canonical forms of linear programming problems:
    \begin{itemize}
        \item \alert{Standard Form}: Involves maximizing a linear function
            subject to linear \emph{inequalities} constraints.
        \item \alert{Slack Form}: Focuses on maximizing a linear function but
            with linear \emph{equalities} constraints.
    \end{itemize}

    Standard form is widely used for representing linear programs.

    Slack form is handy when detailing the simplex algorithm.
\end{frame}

% Frame 1: Introduction to Standard Form
\begin{frame}
\frametitle{Standard Form in Linear Programming}

In \alert{standard form}, we have the following components:
\begin{itemize}
  \item A vector \( c = ( c_1, c_2, \ldots, c_n) \) known as cost coefficients.
  \item A vector \( b = (b_1, b_2, \ldots, b_m) \) representing the resource limits.
  \item A matrix of $A= (a_{ij})_{1 \le i \le m, 1 \le j \le n}$.
\end{itemize}
The \emoji{bullseye} is to find \( (x_1, x_2, \ldots, x_n) \) that
\begin{align*}
\text{maximize}   \quad & \sum_{j=1}^{n} c_j x_j \\
\text{subject to} \quad & \sum_{j=1}^{n} a_{ij} x_j \leq b_i \quad \text{for } i = 1, 2, \ldots, m \\
                        & x_j \geq 0 \quad \text{for } j = 1, 2, \ldots, n.
\end{align*}

We will denote an \ac{ilp} problem by $(A, b, c)$
\end{frame}

% Frame 2: Matrix Form of Standard Form
\begin{frame}
\frametitle{Matrix Form of Standard Form}

In other words, we seek a vector \( x = (x_1, x_2, \ldots, x_n) \) 
\begin{align*}
\text{maximize}   \quad & c^{T} x \\
\text{subject to} \quad & A x \le b \\
                        & x \geq 0.
\end{align*}
We call \begin{itemize}
    \item \( c^T x = c \cdot x \) the \alert{objective Function},
    \item \( A x \leq b \) and
        \( x \geq 0 \) the \alert{constraints},
    \item \ \( x \geq 0 \) the \alert{nonnegativity constraints}.
\end{itemize}

An \ac{lp} problem does not necessarily have the nonnegativity constraints.
But the \emph{standard form} has them.
\end{frame}

\begin{frame}{Terminology of Linear Programming Solutions}
  Linear programs can have various types of solutions:
  \begin{itemize}
    \item A \alert{feasible solution} satisfies all the constraints.
    \item An \alert{infeasible solution} doesn't satisfy at least one constraint.
    \item An \alert{optimal solution} maximizes the objective value over all feasible solutions. The maximum value is called the \alert{optimal objective value}.
    \item A \ac{lp} problem is \alert{infeasible} if it has no feasible solutions.
    \item If there are feasible solutions but the optimal objective value is not finite, the \ac{lp} problem is \alert{unbounded}.
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Standard Form Conversion in \ac{lp}}

\ac{lp} problems can always be standardized. 

Common deviations from standard form are:

\begin{itemize}
    \item Objective function is for minimization.
    \item Variables without nonnegativity constraints.
    \item Constraints with an equal sign.
    \item Constraints with a greater-than-or-equal-to sign.
\end{itemize}

Each point represents a conversion requirement for standardization.

\end{frame}

\begin{frame}
    \frametitle{Equivalent Linear Programs}

    Two \emph{maximization} linear programs \( L \) and \( L' \) are \alert{equivalent} if
    for every feasible solution to \( L \) with objective value \( z \), there
    exists a feasible solution to \( L' \) with the same objective value, and vice
    versa.

    \pause{}

    A \emph{maximization} \ac{lp} problem \( L \) and a \emph{minimization}
    \ac{lp} problem \( L' \) are \alert{equivalent} if for every feasible
    solution to \( L \) with objective value \( z \), there exists a feasible
    solution to \( L' \) with the objective value \(-z\), and vice versa.
\end{frame}

\begin{frame}
    \frametitle{Convert to Standard Form: Minimization to Maximization}
    
    A \ac{lp} problem such as
    \begin{align*}
        \text{minimize} \quad & -2x_1 + 3x_2 \\
        \text{subject to} \quad & x_1 + x_2 = 7, \\
                          & x_1 - 2x_2 \leq 4, \\
                          & x_1 \geq 0,
    \end{align*}
    can be converted to an equivalent maximization problem by negating the
    coefficients
    \begin{align*}
        \text{maximize} \quad & 2x_1 - 3x_2 \\
        \text{subject to} \quad & x_1 + x_2 = 7, \\
                          & x_1 - 2x_2 \leq 4, \\
                          & x_1 \geq 0,
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Convert to Standard Form: Add Nonnegativity Constraints}
    
    A \ac{lp} problem such as
    \begin{align*}
        \text{maximize} \quad & 2x_1 - 3x_2 \\
        \text{subject to} \quad & x_1 + x_2 = 7, \\
                          & x_1 - 2x_2 \leq 4, \\
                          & x_1 \geq 0,
    \end{align*}
    can be converted to an equivalent problem with nonnegativity constraints
    for all variables
    \begin{align*}
        \text{maximize} \quad & 2x_1 - 3x_2' + 3x_2'' \\
        \text{subject to} \quad & x_1 + x_2' - x_2'' = 7 \\
        & x_1 - 2x_2' + 2x_2'' \leq 4 \\
        & x_1, x_2', x_2'' \geq 0.
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Convert to Standard Form: Remove Equalities}
    
    A \ac{lp} problem such as
    \begin{align*}
        \text{maximize} \quad & 2x_1 - 3x_2' + 3x_2'' \\
        \text{subject to} \quad & x_1 + x_2' - x_2'' = 7 \\
        & x_1 - 2x_2' + 2x_2'' \leq 4 \\
        & x_1, x_2', x_2'' \geq 0.
    \end{align*}
    can be converted to an equivalent problem with inequality constraints 
    \begin{align*}
        \text{maximize} \quad & 2x_1 - 3x_2' + 3x_2'' \\
        \text{subject to} \quad & x_1 + x_2' - x_2'' \le 7 \\
        & x_1 + x_2' - x_2'' \ge 7 \\
        & x_1 - 2x_2' + 2x_2'' \leq 4 \\
        & x_1, x_2', x_2'' \geq 0.
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Convert to Standard Form: Final Step}
    
    \cake{} How to convert a \ac{lp} problem such as
    \begin{align*}
        \text{maximize} \quad & 2x_1 - 3x_2' + 3x_2'' \\
        \text{subject to} \quad & x_1 + x_2' - x_2'' \le 7 \\
        & x_1 + x_2' - x_2'' \ge 7 \\
        & x_1 - 2x_2' + 2x_2'' \leq 4 \\
        & x_1, x_2', x_2'' \geq 0.
    \end{align*}
    in to standard form?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Convert the following to the standard form
    \begin{align*}
        \text{minimize} \quad & -2x_1 - 3x_2 \\
        \text{subject to} \quad & -x_1  + 5 x_2 = -5 \\
        & x_1 \geq 0.
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Back to Equality Constraints}
    
    To solve a \ac{lp} problem with \emph{simplex method}, 
    we convert it to an equivalent
    problem in which the nonnegativity constraints are the only inequality
    constraints.

    \pause{}
    Thus, for each constraint in the form of
    \begin{equation*}
        \sum_{j=1}^n a_{ij} x_j \le b_i,
    \end{equation*}
    we introduce a new variable $x_{n+i} \ge 0$ to rewrite it as
    \begin{equation*}
        \begin{aligned}
            x_{n+i} & = b_i- \sum_{j=1}^n a_{ij} x_j, \\
            x_{n+i} & \ge 0
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example of Slack Form}
    
    \only<1>{
        The following \ac{lp} problem in standard form
        \begin{align*}
            \text{maximize} \quad & 2x_1 - 3x_2 + 3x_3 \\
            \text{subject to} \quad & x_1 + x_2 - x_3 \le 7 \\
            & -x_1 - x_2 + x_3 \le -7 \\
            & x_1 - 2x_2 + 2x_3 \leq 4 \\
            & x_1, x_2, x_3 \geq 0.
        \end{align*}
    }
    \only<2>{
        can be converted to the following:
        \begin{align*}
        \text{maximize}\quad & 2x_1 - 3x_2 + 3x_3 \\
        \text{subject to}\quad 
            & x_4 = 7 - x_1 - x_2 + x_3 \\
            & x_5 = -7 + x_1 + x_2 - x_3 \\
            & x_6 = 4 - x_1 + 2x_2 - 2x_3 \\
            & x_1, x_2, x_3, x_4, x_5, x_6 \geq 0 .
        \end{align*}

        We call these variables on the \ac{lhs} \alert{basic variables}, 
        and these on the \ac{rhs} \alert{nonbasic variables}.
    }
\end{frame}

\begin{frame}
    \frametitle{Slack Form}

    To be more concise, we often simply write
    \begin{align*}
        \text{maximize}\quad & 2x_1 - 3x_2 + 3x_3 \\
        \text{subject to}\quad 
                             & x_4 = 7 - x_1 - x_2 + x_3 \\
                             & x_5 = -7 + x_1 + x_2 - x_3 \\
                             & x_6 = 4 - x_1 + 2x_2 - 2x_3 \\
                             & x_1, x_2, x_3, x_4, x_5, x_6 \geq 0 .
    \end{align*}
    simply as
    \begin{align*}
        z &= 2x_1 - 3x_2 + 3x_3 \\
        x_4 &= 7 - x_1 - x_2 + x_3 \\
        x_5 &= -7 + x_1 + x_2 - x_3 \\
        x_6 &= 4 - x_1 + 2x_2 - 2x_3.
    \end{align*}
    This is called the \alert{slack form}.
\end{frame}

\begin{frame}
    \frametitle{Write Slack Form with a Tuple}

    A \ac{lp} problem in slack form
    \begin{equation*}
        \begin{aligned}
            z & = v + \sum_{j \in N} c_{j} x_{j} \\
            x_i & = b_{i} - \sum_{j \in N} a_{ij} x_j, \qq{for} i \in B
        \end{aligned}
    \end{equation*}
    can also be written as a tuple $(N, B, A, b, c, v)$ 
    where
    \begin{itemize}
        \item $N$ is the set of indices nonbasic variables
        \item $B$ is the set of indices basic variables
        \item $A = (a_{ij})$ is the matrix of coefficients
        \item $b = (b_i)$ is a vector
        \item $v$ is a constant
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{} What is $(N, B, A, b, c, v)$ for the following problem?
    \begin{align*}
        z &= 28 - \frac{x_3}{6} - \frac{x_5}{6} - \frac{2x_6}{3} \\
        x_1 &= 8 + \frac{x_3}{6} + \frac{x_5}{6} - \frac{x_6}{3} \\
        x_2 &= 4 - \frac{8x_3}{3} - \frac{2x_5}{3} + \frac{x_6}{3} \\
        x_4 &= 18 - \frac{x_3}{2} + \frac{x_5}{2},
    \end{align*}
\end{frame}

\end{document}
