\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Greedy Algorithms (Part 1)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ita} 16 Greedy Algorithms}

\begin{frame}[c]
    \frametitle{Greedy Algorithms}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            A \alert{greedy algorithm} selects the local optimum at each step.

            \bigskip{}

            It aims for a globally optimal solution through successive local
            choices.

            \bigskip{}

            This method is efficient but not always optimal.

            \bigskip{}

            \cake{} What greedy algorithms do you use in life?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-greedy.jpg}
                \caption{Greed can be healthy!}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 16.1 An Activity-selection Problem}

\subsection{The Problem}

\begin{frame}
    \frametitle{A Film Festival Problem}

    The screening schedule of $6$ \film{} at a film festival are shown below.

    What is the max number of films which you
    can watch in their entirety?

    \begin{figure}[htpb]
        \footnotesize
        \begin{tikzpicture}[
            every node/.style={midway},
            style={graphedge, draw=black},
            xscale=1.3,
            yscale=1.2,
            auto
            ]
            \input{../graphs/interval-4.tex}
        \end{tikzpicture}
        \caption*{The screening schedule}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Activity Selection Problem}

    Consider a sequence of \( n \) intervals
    \begin{equation*}
        [s_1, f_1), [s_2, f_2), \ldots, [s_n, f_n),
    \end{equation*}
    where \( 0 < s_i < f_i < \infty \) for all \( i = 1, \ldots, n \).

    \hint{} Think of the intervals as the starting and finishing time of activities.

    Assume also that:
    \begin{equation*}
        f_1 \leq f_2 \leq \ldots \leq f_{n-1} \leq f_n.
    \end{equation*}

    The objective is to find the maximum set of non-overlapping
    intervals in the sequence.
\end{frame}

\subsection{A Dynamic Programming Solution}

\begin{frame}
    \frametitle{Some Notations}
    
    Let
    \begin{equation*}
        S_{i,j} = \{[s_k, f_k): f_i \le s_k < f_k \le s_j\}.
    \end{equation*}

    \cake{} Which intervals are in $S_{\only<1>{2,2}\only<2>{2,3}\only<3>{1,6}\only<4>{2,6}}$?

    \begin{figure}[htpb]
        \footnotesize
        \begin{tikzpicture}[
            every node/.style={midway},
            style={graphedge, draw=black},
            xscale=1.3,
            yscale=1.2,
            auto
            ]
            \input{../graphs/interval-3.tex}
        \end{tikzpicture}
        \caption*{Six intervals}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A Dynamic Programming Solution}

    Let $A_{i,j}$ be the maximum subset of non-overlapping intervals in
    $S_{i,j}$.

    If $A_{i,j}$ includes $[s_{k}, f_{k})$, then
    \begin{equation*}
        A_{i,j} = A_{i,k} \cup \{[s_{k}, f_{k})\} \cup A_{k,j},
    \end{equation*}
    where the union is over three disjoint sets.

    \pause{}
    
    Let $c[i,j] = \abs{A_{i,j}}$, we have the recursion:
    \begin{equation*}
        c[i,j] = 
        \begin{cases}
            0 & \text{if } i \ge j, \\
            \max_{i < k < j} c[i,k] + 1 + c[k,j] & \text{otherwise}.
        \end{cases}
    \end{equation*}

    \cake{} Why cannot we include $k=i$ or $k=j$ in the above recursion?
\end{frame}

\subsection{A Greedy Algorithm}

\begin{frame}
    \frametitle{A Key Observation}
    
    In the maximum subset of non-overlapping intervals,
    there exists one interval which finishes first.

    Note that we can replace it by $[s_{1}, f_{1})$ while still having
    an optimal solution.

    \begin{figure}[htpb]
        \footnotesize
        \begin{tikzpicture}[
            every node/.style={midway},
            style={graphedge, draw=black},
            xscale=1.3,
            yscale=1.2,
            auto
            ]
            \input{../graphs/interval-2.tex}
        \end{tikzpicture}
        \caption*{Swap the first interval in the optimal solution}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 16.1}
    
    Let $f_{0} = 0$ and
    \begin{equation*}
        S_k = \{[s_i, f_i): s_i \ge f_{k}\}, \qquad \text{for all } k = 0, \ldots, n.
    \end{equation*}

    Let $[s_m, f_{m})$ be the interval which finishes first in $S_{k}$.

    Then one of the maximum subsets of $S_{k}$ containing non-overlapping intervals
    includes $[s_{m}, f_{m})$.

    \cake{} What is $S_{0}$?

    \cake{} Is it true that we always have $[s_{m}, f_{m}) = [s_{k+1},
    f_{k+1})$?

\end{frame}

\begin{frame}
    \frametitle{A Greedy Algorithm}
    \begin{algorithmic}[1]
        \Procedure{Recursive-Selector}{$s, f, k, n$}
        \State $m \gets k + 1$
        \While{$m \leq n \And s[m] < f[k]$}
        \State $m \gets m + 1$
        \EndWhile
        \If{$m \leq n$}
        \State \Return $\{[s_m, f_{m})\} \cup \Call{Recursive-Selector}{s, f, m, n}$
        \Else
        \State \Return $\emptyset$
        \EndIf
        \EndProcedure
    \end{algorithmic}

    \cake{} What is the \emoji{stopwatch} complexity of
    $\textsc{Recursive-Selector}(s, f, 0, n)$?
\end{frame}

\begin{frame}
    \frametitle{An Iterative Implementation}
    
    \begin{algorithmic}
        \Procedure{Greedy-Selector}{$s, f$}
        \State $n \gets \text{length}(s)$
        \State $A \gets \{ [s_1,f_1) \}$
        \State $k \gets 1$
        \For{$m \gets 2$ \textbf{to} $n$}
        \If{$s[m] \geq f[k]$}
        \State $A \gets A \cup \{[s_m, f_m)\}$
        \State $k \gets m$
        \EndIf
        \EndFor
        \State \Return $A$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    \cake{} What is output of the \greedy{} algorithm for the following
    intervals?
    
    \begin{equation*}
        [4, 6),
        [1, 2),
        [0, 3),
        [2, 5),
        [6, 7),
        [3, 5).
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{Time for Toilet}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Suppose that we also require that there is at least 15
            minutes between two screenings which we select.  

            \bigskip{}

            \cake{} How should we modify our greedy algorithm?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-film.jpg}
                \caption{Rushing to \emoji{toilet} between films}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Interval graphs}

    Given a list of intervals $(S_{v})_{v \in V}$, the \emph{interval graph} is
    the graph $G$ with vertex set $V$ and edge set
    \begin{equation*}
        E = \{ uv : S_{u} \cap S_{v} \ne \emptyset\}
    \end{equation*}

    \vspace{-2em}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{figure}[htpb]
                \footnotesize
                \begin{tikzpicture}[
                    every node/.style={midway},
                    style={graphedge, draw=black},
                    yscale=0.6,
                    xscale=0.9,
                    auto
                    ]
                    \input{../graphs/interval-4.tex}
                \end{tikzpicture}
                \caption{Intervals}
            \end{figure}

            \emoji{mega} Take Discrete Mathematics to learn more!
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.6\textwidth]{film-festival-1.pdf}
                \caption{An interval graph $G$}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{Number of Screenings Halls}

    Assume that two \film{} with overlapping screening time must be into
    different cinema halls.

    \cake{} Can you design an algorithm to find the minimum number of
    halls required to accommodate a film festival schedule?

    \vspace{-2em}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{figure}[htpb]
                \footnotesize
                \begin{tikzpicture}[
                    every node/.style={midway},
                    style={graphedge, draw=black},
                    yscale=0.6,
                    xscale=0.9,
                    auto
                    ]
                    \input{../graphs/interval-4.tex}
                \end{tikzpicture}
                \caption{Intervals}
            \end{figure}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.6\textwidth]{film-festival-1.pdf}
                \caption{An interval graph $G$}
            \end{figure}
        \end{column}
    \end{columns}

    \zany{} This is to ask for an algorithm to compute the chromatic number of
    the corresponding interval graph.
\end{frame}

\section{\acs{ita} 16.2 Elements of the greedy strategy}

\begin{frame}{Greedy Algorithms}
    More generally, we design greedy algorithms as follows:
    \begin{enumerate}
        \item Formulate the problem to allow a choice leading to a single subproblem.
        \item Validate optimal substructure, ensuring a combined solution of the greedy choice and the optimal subproblem solution is also optimal.
    \end{enumerate}

    \pause{}

    Problems that can be solved using \greedy{} algorithms have:

    \begin{itemize}
        \item \alert{Greedy-choice Property} ---
            A \alert{locally optimal} (greedy) choice leads towards a \alert{globally optimal} solution.
        \item \alert{Optimal Substructure} ---
            An optimal solution contains optimal solutions to subproblems.
    \end{itemize}

\end{frame}

\subsection{Greedy Versus Dynamic Programming}

\begin{frame}[c]{The 0-1 Knapsack Problem}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            A \bunny{} robbing a store finds $n$ items.

            \bigskip{}

            The $i$-th item is worth $v_i$ \emoji{dollar} and weighs $w_i$ pounds,
            where $v_i$ and $w_i$ are integers. 

            \bigskip{}

            The \bunny{} wants to take as valuable a load as possible, 
            but he can carry at most $W$ pounds in his knapsack.

            \bigskip{}

            Which items should he take?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-thief.jpg}
                \caption{A \bunny{} robbing a store}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]{The Fractional Knapsack Problem}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            In the fractional knapsack problem, we allow the \bunny{} to take a
            fractions of the items.

            \bigskip{}

            \hint{} Think the 0-1 version as robbing a \emoji{gem}
            store,
            and the fractional version as robbing a \emoji{cheese} store.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-cheese.jpg}
                \caption{A \bunny{} robbing a store}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Greedy Algorithm or Dynamic Programming?}

    As the example below shows, the \greedy{} algorithm does not always work for
    the 0-1 knapsack problem.

    But it does work for the fractional knapsack problem.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{knap-sack.png}
        \caption{The 0-1 and fractional knapsack problem}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Solution For 0-1 Knapsack Problem}
    
    \begin{algorithmic}
        \small
    \Procedure{Knapsack}{$v, w, n, W$}
    \For{$i \gets 0$ \textbf{to} $n$}
        \State Initialize a new array $K[0 \ldots n][0 \ldots W]$
        \For{$w \gets 0$ \textbf{to} $W$}
        \If{$i = 0$ \textbf{or} $w = 0$}
            \State $K[i][w] \gets \blankshort{}$
        \ElsIf{$w[i] \leq w$}
            \State $K[i][w] \gets \blankshort{}$
        \Else
            \State $K[i][w] \gets \blankshort{}$
        \EndIf
        \EndFor
    \EndFor
    \State \Return $K[n][W]$
    \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Solution For Fractional Knapsack Problem}
    
    \begin{algorithmic}
    \small
    \Procedure{Knapsack}{$v, w, n, W$}
        \State $V \gets 0$ \Comment{Total value initialized to 0}
        \State \Call{SortByRatio}{$v, w$} \Comment{Sort by
        value-to-weight ratio in decreasing order}
        \For{$i \gets 1$ \textbf{to} $n$}
            \If{$w[i] \leq W$}
                \State $V \gets V + v[i]$ \Comment{Take the whole item}
                \State $W \gets W - w[i]$ \Comment{Decrease available weight}
            \Else
                \State $V \gets V + \left(\blankshort{}\right)$ \Comment{Take fraction of the item}
                \State \textbf{break}
            \EndIf
        \EndFor
        \State \textbf{return} $V$ \Comment{Return the total value}
    \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}{The Change-making Problem}
    We have infinitely many \coin{} with denominations in \(\{d_1, \ldots, d_n\}\)
    where
    \begin{equation*}
        d_1 < d_2 \dots < d_n.
    \end{equation*}

    We want to make a specified amount \(A\) with these coins while
    minimize the number of \coin{}.

    \only<+>{\cake{} Is the problem just another way to describe the 0-1
    knapsack problem?}

    \only<+>{\cake{} What is the greedy algorithm for solving this problem?}

    \only<+>{\cake{} Can you find an example in which the greedy algorithm fails to find
    an optimal solution?}

    \only<+>{\cake{} If not, how can we modify the problem so that the greedy algorithm
    will always find the optimal solution?}
\end{frame}

\begin{frame}
    \frametitle{More Examples}
    
    \cake{} Can you think of some more examples where the greedy algorithm do
    not always return the optimal solution?
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 16.1.
                \item
                    Section 16.2.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 16.1: 1--5.
                    Exercises 16.2: 1--5.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
