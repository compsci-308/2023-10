\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Minimum Spanning Trees (Part 1)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 23.1.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 23.1: 1--10.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 23 Minimum Spanning Trees}

\begin{frame}
    \frametitle{Spanning subgraphs}
    
    A \emph{subgraph} $H = (W,F)$ of $G = (V, E)$ is a \alert{spanning subgraph}
    when $W = V$. 

    \cake{}  Is the green part a spanning subgraph?

    \begin{figure}[htpb]
        \centering
            \includegraphics<1>[width=0.5\linewidth]{subgraph.pdf}
            \includegraphics<2>[width=0.5\linewidth]{subgraph-spanning.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Spanning Trees}
    
    A \emph{subgraph} $H = (W,F)$ of $G = (V, E)$ is a \alert{spanning tree}
    if $H$ is both a \emph{spanning subgraph} of $G$ and a \emph{tree}.
    
    \cake{}  Is the green part a spanning tree?

    \begin{figure}
        \centering
        \includegraphics<1>[width=0.5\linewidth]{subgraph.pdf}
        \includegraphics<2>[width=0.5\linewidth]{subgraph-spanning.pdf}
        \includegraphics<3>[width=0.5\linewidth]{subgraph-spanning-tree.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\ac{mst}}
    
    \only<1>{A \alert{weighted graph} is a graph in which each edge has a
    numerical value, known as its \alert{weight}.}
    
    \only<2>{A \alert{\acf{mst}} is a spanning tree of a weighted graph that
    minimizes the sum of the weights of its edges.}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{Minimum-spanning-tree.png}
        \caption{A \only<2>{\ac{mst} of a }\only<1-2>{weighted graph}. Source: \href{https://en.wikipedia.org/wiki/Minimum_spanning_tree\#/media/File:Minimum_spanning_tree.svg}{Wikipedia}}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{The Importance of \ac{mst}}
    \begin{columns}[c] % align columns
        \begin{column}{.55\textwidth}
            Reasons to study \acp{mst}:
            \begin{itemize}
                \item Network Efficiency: Optical fiber networks.
                \item Cost Savings: Reduces \emoji{moneybag} in node
                    connections.
                \item Optimization: Travelling salesman problem.
                \item Infrastructure: \emoji{train} networks.
                \item Data Clustering: Groups similar points in machine learning.
            \end{itemize}
        \end{column}%
        \begin{column}{.45\textwidth}
            \begin{figure}[htpb]
                \centering
                % Insert your image here. Image description: A comic-style illustration of a bunny dressed as a stockbroker, ecstatic about its faster optical fiber network, aiding in high-frequency trading. The network showcases the application of minimum spanning trees.
                \includegraphics[width=\textwidth]{bunny-stockbroker.jpg}
                \caption{Fast Connections, Faster Trades}
            \end{figure}
        \end{column}%
    \end{columns}
\end{frame}

\section{\acs{ita} 23.1 Growing a Minimum Spanning Tree}

\subsection{Generic Algorithm for \ac{mst}}

\begin{frame}
    \frametitle{Generic Algorithm for \ac{mst}}

    Loop invariant for line 3 --- $A$ is \emph{always} a subset of some minimum spanning tree.

    The edges in $A$ are called \alert{safe edges}.

    \begin{algorithmic}[1]

        \Procedure{Generic-MST}{$G, w$}
        \State $A \gets \emptyset$
        \While {$A$ does not form a spanning tree}
        \State find an edge $(u, v)$ that is safe for $A$
        \State $A \gets A \cup \{(u, v)\}$
        \EndWhile
        \State \Return $A$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
\frametitle{Cuts in an Undirected Graph}

\only<1>{\alert{Cut:} For an undirected graph \( G = (V, E) \), a cut \( (S, V
\setminus S) \) is a partition of \( V \).}

\only<2>{\alert{Crossing the Cut:} An edge \( (u, v) \in E \) crosses the cut
\( (S, V \setminus S) \) if one of its endpoints is in \( S \) and the other is
in \( V \setminus S \).}

\only<3>{\alert{Respecting a Cut:} A cut \textit{respects} a set \( A \) of
edges if no edge in \( A \) crosses the cut.}

\only<4>{\alert{Light Edge:} An edge is a \textit{light edge} crossing a cut if
its weight is the minimum of any edge crossing the cut. There may be multiple
light edges crossing a cut in case of ties.}

\only<5>{\alert{Light Edge with a Property:} An edge is a \textit{light edge}
satisfying a given property if its weight is the minimum of any edge satisfying
that property.}

\begin{figure}[htpb]
    \centering
    \includegraphics<1>[width=0.8\textwidth]{cut-graph.pdf}
    \includegraphics<2>[width=0.8\textwidth]{cut-graph-crossing.pdf}
    \includegraphics<3>[width=0.8\textwidth]{cut-graph-respect.pdf}
    \includegraphics<4->[width=0.8\textwidth]{cut-graph-weight.pdf}
    \caption{%
        \only<1>{A cut of $G$ with $S = \{1,4,8,2\}$}%
        \only<2>{Edges crossing the cut}%
        \only<3>{\cake{} Which set of edges does the cut respect?}%
        \only<4>{\cake{} Which edge is the light edge?}%
        \only<5>{\cake{} Which edge is the light edge with weight at least 3?}%
    }
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Theorem 23.1}

\only<1>{Let \( G = (V, E) \) be a connected, undirected and weighted graph. }

\only<2>{Let \( A \) be a subset of \( E \) that is included in some \ac{mst}
for \( G \). }

\only<3>{Let \( (S, V - S) \) be any cut of \( G \) that respects \( A \). }

\only<4-5>{Let \( (u, v) \) be a light edge crossing \( (S, V - S) \). 
Then, edge \( (u, v) \) is safe for \( A \).}

\begin{figure}[htpb]
    \centering
    \includegraphics<1>[width=0.9\textwidth]{cut-graph-mst.pdf}
    \includegraphics<2>[width=0.9\textwidth]{cut-graph-mst-partial.pdf}
    \includegraphics<3>[width=0.9\textwidth]{cut-graph-mst-respected.pdf}
    \includegraphics<4>[width=0.9\textwidth]{cut-graph-mst-corssing.pdf}
    \includegraphics<5>[width=0.9\textwidth]{cut-graph-mst-safe.pdf}
    \caption{%
        \only<1>{An weighted graph $G$}%
        \only<2>{$A$ is some edges in a \ac{mst}}%
        \only<3>{A cut respecting $A$. \cake{} Can you think of another?}%
        \only<4>{\cake{} Which one is light among the crossing edges?}%
        \only<5>{When there are multiple light edges, pick one arbitrarily to
        be $(u,v)$.}%
    }
\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Proof of Theorem 23.1}

    Proof: 
    \only<1>{If $T$, the \ac{mst} containing $A$ also contains $(u,v)$, then we are
    done.}%
    \only<2->{If not, we build a new \ac{mst} $T'$ that contains $(u,v)$ and $A$.}%

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.9\textwidth]{cut-graph-mst-contain-safe.pdf}
        \includegraphics<3>[width=0.7\textwidth]{cut-graph-mst-do-not-contain.png}
        \includegraphics<2>[width=0.9\textwidth]{cut-graph-mst-not-contain-safe.pdf}
        \caption{%
            \only<1>{$T$ contains $(u,v)$}%
            \only<3>{$T$ does not contain $(u,v)$}%
            \only<2>{Since $T$ does not contain $(u,v)=(2,8)$, we replace
            $(x,y)=(8,5)$ by $(u,v)$ and get a different \ac{mst} $T'$}%
        }
    \end{figure}
\end{frame}

\begin{frame}
\frametitle{Corollary 23.2}

\only<1>{Let \( G = (V, E) \) be a connected, undirected and weighted graph.}

\only<2>{Let \( A \) be a subset of \( E \) that is included in some \ac{mst}
for \( G \).}

\only<3>{Let \( C = (V_C, E_C) \) be a connected component (tree)
in the forest \( G_A = (V, A) \).}

\only<4>{If \( (u, v) \) is a light edge connecting \( C \) to some other component in \(
G_A \), then \( (u, v) \) is safe for \( A \).}

\begin{figure}[htpb]
    \centering
    \includegraphics<1>[width=0.9\textwidth]{cut-graph-mst.pdf}
    \includegraphics<2>[width=0.9\textwidth]{cut-graph-mst-partial.pdf}
    \includegraphics<3>[width=0.9\textwidth]{cut-graph-mst-C.pdf}
    \includegraphics<4>[width=0.9\textwidth]{cut-graph-mst-C-safe.pdf}
    \caption{%
        \only<1>{An weighted graph $G$}%
        \only<2>{Let $A$ be some edges in a \ac{mst}}%
        \only<3>{Let $C = (\{2,5\}, \{(2,5)\})$}%
        \only<4>{Light crossing edges --- Let $(u,v)=(2,8)$}%
    }
\end{figure}
\end{frame}

\subsection{\zany{} The Wolf Hunters of Wall Street}

\begin{frame}[c]
    \frametitle{The Wolves of Wall Street}

    \begin{columns}[c]
        \begin{column}{0.6\textwidth}

            In 2007, Brad Katsuyama, a trader of
            Wall Street, noticed when he sent a stock order to multiple stock
            exchanges, the offer often vanished.

            \bigskip{}

            But when he only sent it to one exchange,
            then it always went through.

            \bigskip{}

            Investigation revealed the culprit -- various predatory
            high-frequency-trading algorithms.
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.7\textwidth]{flash-boys-dealys.jpg}
                \caption{The distance from Katsuyama to the exchanges.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{How to Stop the Wolves?}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The solution was paradoxical: adding a delay to synchronize order
            arrivals at all exchanges. 

            \bigskip{}

            The IEX (Investors Exchange) was later founded, employing a
            350-microsecond delay to neutralize the speed advantage of
            high-frequency traders.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{flash-boys-coils.jpg}
                \caption{32 miles of fiber optic cable for 350 microseconds delay.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Exercise 23.1}

\begin{frame}
    \frametitle{Exercise 23.1-6}
    Show that a graph has a unique minimum spanning tree if, for every cut of the
    graph, there is a unique light edge crossing the cut.

    \pause{} Show that the converse is not true.
\end{frame}

\begin{frame}
    \frametitle{\only<2>{\tps{}} Exercise 23.1-7}
    \only<1>{Argue that if all edge weights of a graph are positive, then any
    subset of edges that connects all vertices and has minimum total weight must
be a tree.}

    \only<2>{Give an example to show that the same conclusion does not follow if we allow
    some weights to be none positive.}
\end{frame}

\begin{frame}
    \frametitle{Exercise 23.1-8}
    
    \only<1>{Let \( T \) be a minimum spanning tree of a graph \( G \), and let \( L \)
    be the sorted list of the edge weights of \( T \). }

    \only<2>{Show that for any other
    minimum spanning tree \( T' \) of \( G \), the list \( L \) is also the
    sorted list of edge weights of \( T' \).}

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.9\textwidth]{cut-graph-mst-1.pdf}
        \includegraphics<2>[width=0.9\textwidth]{cut-graph-mst-2.pdf}
        \caption{%
            \only<1>{An \ac{mst} $T$. What is $L$?}%
            \only<2>{Another \ac{mst} $T'$. What is $L$?}%
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Induced subgraphs}

    An induced subgraph \( G' \) is formed by a subset of vertices \( V' \) of a
    graph \( G \) and all edges in \( G \) whose endpoints are both in \( V' \).

    \begin{figure}[htpb]
        \centering
        \begin{subfigure}{0.49\textwidth}
            \centering
            \includegraphics[width=\linewidth]{subgraph.pdf}
            \caption{A subgraph which is \emph{not} an induced subgraph}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.49\textwidth}
            \centering
            \includegraphics[width=\linewidth]{subgraph-induced.pdf}
            \caption{A subgraph which is also an induced subgraph}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exercise 23.1-9}

    \only<1>{Let \( T \) be a \ac{mst} of a graph \( G = (V, E) \), and let \( V' \) be a
    subset of \( V \). }

    \only<2>{Let \( T' \) be the subgraph of \( T \) induced by \( V'
    \), and let \( G' \) be the subgraph of \( G \) induced by \( V' \).}

    \only<3>{Show that if \( T' \) is connected, then \( T' \) is a minimum spanning tree
    of \( G' \).}

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.9\textwidth]{mst-induced-subgraph-1.pdf}
        \includegraphics<2->[width=0.6\textwidth]{mst-induced-subgraph-2.pdf}
        \caption{%
            \only<1>{Example of $G$, $T$ and $V'$}%
            \only<2-3>{Example of $G'$, $T'$}%
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exercise 23.1-10}
    Given a graph $G$ and a \ac{mst} $T$, 
    suppose that we decrease the weight of one of the edges in $T$. 

    Show that $T$ is still a \ac{mst} for $G$.
\end{frame}

\end{document}
