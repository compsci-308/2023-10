\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Elementary Graph Algorithms (Part 2)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ita} 22.3 Depth-First Search}

\begin{frame}
    \frametitle{\ac{dfs} Overview}

    \acs{dfs} aims to explore as deeply as possible in the graph before
    backtracking:

    \begin{enumerate}[<+->]
        \item Explore from the most recently discovered vertex with unexplored edges.
        \item Explore all edges of the current vertex.
        \item Backtrack to the vertex's discovery point once all edges are explored.
        \item Continue until all vertices reachable from the original source are visited.
        \item \bomb{} Unlike \ac{bfs}, if undiscovered vertices exist, \ac{dfs}
            select a new source and repeat.
    \end{enumerate}

    \only<+->{The process continues until every vertex is explored.}
\end{frame}

\begin{frame}
    \frametitle{A Visual Comparison with \ac{bfs}}
    
    When solving a maze, \only<1>{the \acs{bfs} strategy explores}\only<2>{the
    \acs{dfs} go deep}.

    \cake{} Which one is more advantageous?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.6\textwidth]{maze-bfs.jpg}
        \includegraphics<2>[width=0.6\textwidth]{maze-dfs.jpg}
        \caption{\only<1>{\ac{bfs}}\only<2>{\ac{dfs}} solution}
    \end{figure}

    Try it out at \url{https://brkwok.github.io/Maze-Solver/}.
\end{frame}

\begin{frame}
    \frametitle{Predecessor Subgraph}

    For a graph $G = (V, E)$, we define the
    \alert{predecessor subgraph} of a \ac{dfs} as $G_{\pi} = (V_{\pi}, E_{\pi})$,
    where
    \begin{equation*}
        V_{\pi} = V
    \end{equation*}
    and
    \begin{equation*}
        E_{\pi} = \{(v.\pi, v) : v \in V_{\pi} \ne \text{NIL}\}.
    \end{equation*}

    $G_{\pi}$ for \ac{dfs} forms a \alert{depth-first forest} comprising 
    \alert{depth-first trees}.
\end{frame}

\begin{frame}
    \frametitle{Pseudocode}

    \begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \begin{algorithmic}[1]
                \only<1>{
                    \Procedure{DFS}{$G$}
                    \For{each vertex $u \in G.V$}
                    \State $u.color \gets \text{WHITE}$
                    \State $u.\pi \gets \text{NIL}$
                    \EndFor
                    \State $time \gets 0$
                    \For{each vertex $u \in G.V$}
                    \If{$u.color == \text{WHITE}$}
                    \State \Call{DFS-VISIT}{$G, u$}
                    \EndIf
                    \EndFor
                    \EndProcedure
                }
                \only<2>{
                    \Procedure{DFS-VISIT}{$G, u$}
                    \State $time \gets time + 1$
                    \State $u.d \gets time$
                    \State $u.color \gets \text{GRAY}$
                    \For{each $v \in G.Adj[u]$}
                    \If{$v.color == \text{WHITE}$}
                    \State $v.\pi \gets u$
                    \State \Call{DFS-VISIT}{$G, v$}
                    \EndIf
                    \EndFor
                    \State $u.color \gets \text{BLACK}$
                    \State $time \gets time + 1$
                    \State $u.f \gets time$
                    \EndProcedure
                }
            \end{algorithmic}
        \end{column}
        \begin{column}{0.4\textwidth}
            \cake{} The \emoji{stopwatch} complexity?
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example Running of \ac{dfs}}

    \vspace{2em}
    
    \begin{figure}[htpb]
        \centering
        \begin{overlayarea}{0.6\textwidth}{5cm} % Adjust the height to fit your images
            \foreach \n in {1,...,16}{% start from 1, go up to 16
                \only<\n>{%
                    \pgfmathtruncatemacro{\imagenumber}{\n - 1} % subtract 1 for the actual image number
                    \ifnum\imagenumber<10
                        \includegraphics[width=\textwidth]{dfs-tile-0\imagenumber.png}
                    \else
                        \includegraphics[width=\textwidth]{dfs-tile-\imagenumber.png}
                    \fi
                }
            }
        \end{overlayarea}
        \caption{Running \ac{dfs}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Parenthesis Structure}

    \cake{} Do you see a pattern?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=\textwidth]{dfs-parenthesis-01.jpg}
        \includegraphics<2>[width=\textwidth]{dfs-parenthesis-02.jpg}
        \caption{\only<1>{The result of running \ac{dfs}}\only<2>{The intervals
        of discovering and finishing times}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 22.7}

    In a \ac{dfs}, for vertices \( u \) and \( v \),
    one and only one of these holds:

    \begin{itemize}
        \item The intervals \( [u.d, u.f] \) and \( [v.d, v.f] \) are disjoint, with neither \( u \) nor \( v \) being a descendant of the other.
        \item Interval \( [u.d, u.f] \) lies within \( [v.d, v.f] \), with \( u \) as \( v \)'s descendant.
        \item Interval \( [v.d, v.f] \) lies within \( [u.d, u.f] \), with \( v \) as \( u \)'s descendant.
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Corollary 22.8}

    Vertex $v$ is a \emph{proper} descendant of vertex $u$ in the
    depth-first forest for if and only if $u.d < v.d < v.f < u.f$.
\end{frame}
\begin{frame}
    \frametitle{Theorem 22.9 --- White Path Theorem}

    In a depth-first forest, vertex \( v \) is a descendant of vertex \( u \) if
    and only if at the time \( u.d \), there
    is a path from \( u \) to \( v \) consisting entirely of white vertices.

    \only<1-2>{
        \begin{figure}[htpb]
            \centering
            \includegraphics<1>[width=0.6\textwidth]{dfs-tile-00.png}
            \includegraphics<2>[width=0.6\textwidth]{dfs-tile-15.png}
            \caption{\cake{} Which vertices will be descendants of \( s \)?}
        \end{figure}
    }
    \only<3>{
        Proof of $\Rightarrow$:
    }
    \only<4>{
        Proof of $\Leftarrow$:
    }
\end{frame}

\begin{frame}
    \frametitle{Four Type of Edges}
    We can categorize edges encountered in a \ac{dfs} into four distinct types:

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{dfs-parenthesis-01.jpg}
        \caption{The four types of edges: tree, back, forward and cross}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Distinguish Types of Edges}


    When we first explore an edge $(a,b)$, the colour of
    vertex $b$ tells us something about the edge:
    \begin{itemize}[<+->]
        \item \blankshort{} indicates a tree edge,
        \item \blankshort{} indicates a back edge, and
        \item \blankshort{} indicates a forward \only<+->{or cross} edge.
    \end{itemize}

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.5\textwidth]{dfs-tile-00.png}
        \includegraphics<2>[width=0.5\textwidth]{dfs-tile-04.png}
        \includegraphics<3>[width=0.5\textwidth]{dfs-tile-10.png}
        \includegraphics<4>[width=0.5\textwidth]{dfs-tile-13.png}
        \caption{A \only<1>{tree}\only<2>{back}\only<3>{forward}\only<4>{cross} edge}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 22.10}

    In a \ac{dfs} search of an undirected graph $G$, every edge is either a tree edge or a back edge.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.5\textwidth]{dfs-tile-10.png}
        \includegraphics<2>[width=0.5\textwidth]{dfs-tile-13.png}
        \caption{Not happening in undirected graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    If we start with a \ac{dfs} search on the following graph from vertex 3,
    what are the types of edges.

    Assume that vertices in adjacent-lists are sorted in increasing order of
    their labels.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{directed-graph-with-5-vertices.pdf}
    \end{figure}
\end{frame}

\section{\acs{ita} 22.4 Topological Sort}

\begin{frame}[c]
    \frametitle{Sequencing Tasks: The Right Order Matters}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Imagine a \bunny{} that wakes up late for class.

            \medskip{}

            The \bunny{} must put on its clothes, but some items must be worn before others.

            \medskip{}

            For instance, \emoji{socks} should be on before \emoji{shoe}.

            \medskip{}

            What sequence allows the \bunny{} to dress in a correct order?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-in-a-rush.jpg}
                \caption{A bunny getting ready in haste}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\acp{dag} in Task Scheduling}

    % Overview of DAG
    A \alert{\acf{dag}} is a digraph without cycles.

    It can be used to represent the dependencies between tasks, as the dependencies
    of a task should not be circular.

    % Display the graph
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=0.6, 
                transform shape, 
                ->,
                >=stealth',
                shorten >=1pt,
                auto,
                node distance=2.5cm, thick,
                main node/.style={circle,draw=none,font=\huge,inner sep=0pt}]
                \node[main node] (1) {\emoji{socks}};
                \node[main node] (2) [below of=1] {\emoji{shoe}};
                \node[main node] (4) [right of=1] {\emoji{shirt}};
                \node[main node] (7) [below of=4] {\emoji{briefs}};
                \node[main node] (3) [below of=7] {\emoji{jeans}};
                \node[main node] (5) [below of=3] {belt};
                \node[main node] (6) [right of=4] {\emoji{necktie}};

                \path[every node/.style={font=\sffamily\small}]
                    (1) edge node [left] {} (2)
                    (3) edge node [right] {} (5)
                    (4) edge node [left] {} (7)
                    (4) edge node [left] {} (6)
                    (3) edge node [right] {} (2)
                    (7) edge node [left] {} (3);
            \end{tikzpicture}
        \end{center}
        \caption{Example of a \ac{dag}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Topological Sorting via Depth-First Search}

    \only<1>{
        \alert{Topological sorting} arranges a directed graph's vertices
        linearly, ensuring that for every directed edge \( u \rightarrow v \),
        vertex \( u \) precedes \( v \). 

        This ordering facilitates determining a
        sequence to execute tasks without violating dependencies.
    }

    \only<2>{
        Implementing this involves performing a \ac{dfs} and positioning the
        vertices in descending order of their finish times.
    }

    \begin{figure}[htpb]
        \centering
        \includegraphics<1-2>[width=0.7\textwidth]{topological-sorting-01.png}
        \includegraphics<2>[width=0.9\textwidth]{topological-sorting-02.png}
        \caption{Illustration of Topological Sorting}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lemma 22.11}

    A directed graph $G$ is acyclic if and only if a depth-first search of $G$ yields no back edges.

    \only<1>{
        Proof of $\Rightarrow$:
    }
    \only<2>{
        Proof of $\Leftarrow$:
    }
\end{frame}

\begin{frame}
    \frametitle{Theorem 22.12}
    The following \textsc{Topological-Sort} produces a topological sort of $G$ if it is a \ac{dag}.

    \begin{algorithmic}
        \Procedure{Topological-Sort}{$(G)$}
        \State \Call{DFS}{$G$} to compute finishing times $v.f$ for each vertex $v$
        \State as each vertex is finished, insert it onto the front of a linked list
        \State \Return the linked list of vertices
        \EndProcedure
    \end{algorithmic}
\end{frame}

\section{\acs{ita} 22.5 Strongly Connected Components}

\begin{frame}
    \frametitle{Strongly Connected Components}

    An \alert{\ac{scc}} is a maximum subgraph of a directed graph $G$ in which every
    vertex is reachable from every other vertex.

    Many graph algorithms first decompose a graph into \acp{scc}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{scc-in-directe-graph.pdf}
        \caption{Example of \acp{scc}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Transpose of a Directed Graph}

    Given a directed graph $G$, the transpose of $G$ is the directed graph $G^T$
    in which every edge is reversed.

    \cake{} Why does $G$ and $G^{T}$ have the same \acp{scc}?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.7\textwidth]{scc-in-directe-graph.pdf}
        \includegraphics<2>[width=0.7\textwidth]{scc-in-transposed-graph.pdf}
        \caption{$G\only<2>{^T}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Using \ac{dfs} to Find \ac{scc}}

    \begin{algorithmic}[1]
        \Procedure{Strongly-Connected-Components}{$G$}
        \State \Call{DFS}{$G$} to compute finishing times $u.f$ for each vertex $u$
        \State Compute $G^T$
        \State \Call{DFS}{$G^T$}, but in the main loop of DFS, consider the vertices in order of decreasing $u.f$ (as computed in line 1)
        \State Output the vertices of each tree in the depth-first forest formed in line 3 as a separate strongly connected component
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Example of Finding \ac{scc}}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{scc-algorithm.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Idea of the Algorithm}

    \only<1>{
        If we contract each \ac{scc} into a single vertex, then the
        result is a \ac{dag}, which we call the \alert{component graph} of $G$ and
        denote it by $G^{scc}$.

        \cake{} Why is $G^{scc}$ a \ac{dag}?
    }

    \only<2>{
        \astonished{} The second \ac{dfs} essentially visits the \acp{scc} in topologically sorted
        order of $G^{scc}$.
    }

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.6\textwidth]{scc-original.png}
        \includegraphics<2>[width=0.6\textwidth]{scc-transposed.png}
        \includegraphics[width=0.6\textwidth]{scc-dag.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lemma 22.13}

    Let \( C \) and \( C' \) be distinct strongly connected components in
    directed graph \( G = (V, E) \), let \( u, v \in C \), let \( u', v' \in C'
    \), and suppose that \( G \) contains a path \( u \to u' \).
    Then \( G \) cannot also contain a path \( v' \to v \).
\end{frame}

\begin{frame}
    \frametitle{Lemma 22.14}


    Let \( C \) and \( C' \) be distinct strongly connected components in
    directed graph \( G = (V, E) \). Suppose that there is an edge \( (u, v) \in
    E \), where \( u \in C \) and \( v \in C' \). Then \( f(C) > f(C') \).

\end{frame}

\begin{frame}{Corollary 22.15}
    Let \( C \) and \( C' \) be distinct strongly connected components in
    directed graph \( G = (V, E) \). Suppose that there is an edge \( (u, v) \in
    E^{T} \), where \( u \in C \) and \( v \in C' \). Then \( f(C) < f(C') \).
\end{frame}

\begin{frame}
    \frametitle{Theorem 22.16}
    The procedure \textsc{Strongly-Connected-Components} correctly computes the
    strongly connected components of the directed graph $G$ provided as its input.
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 22.3.
                \item
                    Section 22.4.
                \item
                    Section 22.5.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 22.3: 1--6.
                \item
                    Exercises 22.4: 1--5.
                \item
                    Exercises 22.5: 1--5.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
