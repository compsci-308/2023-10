\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Minimum Spanning Trees (Part 2)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 23.2.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 23.2: 1--5, 8.
                \item
                    Problems 23-1, 23-2.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 23.2 The algorithms of Kruskal and Prim}

\begin{frame}
    \frametitle{Review: Generic Algorithm for \ac{mst}}

    The algorithms of Kruskal and Prim are based one the generic
    algorithm for \ac{mst}.

    But they use different rules to find a safe edge.

    \cake{} What is a safe edge?

    \begin{algorithmic}[1]
        \Procedure{Generic-MST}{$G, w$}
        \State $A \gets \emptyset$
        \While {$A$ does not form a spanning tree}
        \State find an edge $(u, v)$ that is safe for $A$
        \State $A \gets A \cup \{(u, v)\}$
        \EndWhile
        \State \Return $A$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\subsection{Kruskal's algorithm}

\begin{frame}
    \frametitle{Overview of Kruskal's Algorithm}

    Kruskal's algorithm works as follows:
    \begin{enumerate}
        \item Sort all graph edges in non-decreasing order of weight.
        \item Add the cheapest edge to the \ac{mst} if it doesn't create a cycle.
        \item Repeat until \ac{mst} spans all vertices.
    \end{enumerate}

    \think{} How to show this algorithm is correct?
\end{frame}

\begin{frame}
    \frametitle{Review: Corollary 23.2}

    Let $A$ be a subset of the edges of a \ac{mst}.
    Then $A$ is forest.

    Take any component of $A$ as a cut. The light edges for the cut is safe.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{cut-graph-mst-C.pdf}
        \caption{%
            \only<1>{\cake{} What are the light edges for $S = \{5,2\}$?}
            \only<2>{\cake{} What are the light edges for $S = \{3,6\}$?}
            \only<3>{\cake{} What are the light edges for $S = \{7\}$?}
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Correctness of Kruskal's Algorithm}

    The correctness is guaranteed by Corollary 23.2    

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.9\textwidth]{cut-graph-mst-C-not-safe.pdf}
        \includegraphics<2>[width=0.9\textwidth]{cut-graph-mst-C-safe-again.pdf}
        \caption{%
            \only<1>{\cake{} Why is $(2,8)$ \emph{not} safe?}%
            \only<2>{\cake{} Why is $(3,8)$ is safe?}%
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Underlining Algorithm}

    Kruskal's algorithm leverages 
    \begin{itemize}
        \item sorting algorithms 
        \item and disjoint-set data structures which provides the operations 
            like 
            \begin{itemize}
                \item $\textsc{Make-Set}(v)$,
                \item $\textsc{Find-Set}(v)$
                \item $\textsc{Union}(u, v)$,
            \end{itemize}
            each of which takes $O(\log \abs{V})$ time.
    \end{itemize}

    Assuming $G$ is connected, then we have
    \begin{equation*}
        \abs{V} - 1 \le \abs{E} \leq \binom{\abs{V}}{2}.
    \end{equation*}

    Thus the \emoji{stopwatch} complexity is $O(\abs{E} \log \abs{V})$ as shown next.
\end{frame}

\begin{frame}{Pseudo Code for Kruskal's Algorithm}
    \begin{algorithmic}[1]
        \small
        \Procedure{Kruskal}{$G, w$}
        \State $A \gets \varnothing$
        \For{each vertex $v \in G.V$}
        \State $\textsc{Make-Set}(v)$
        \EndFor
        \State Sort the edges of $G.E$ into nondecreasing order by $w$
        \For{each edge $(u, v) \in G.E$}
        \If{$\textsc{Find-Set}(u) \neq \textsc{Find-Set}(v)$}
        \State $A \gets A \cup \{(u, v)\}$
        \State $\textsc{Union}(u, v)$
        \EndIf
        \EndFor
        \State \textbf{return} $A$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Example Run of Kruskal's Algorithm}

    \vspace{3em}

    \begin{figure}[htpb]
        \centering
        \begin{overlayarea}{0.8\textwidth}{4cm} % Adjust the height to fit your images
            \foreach \n in {1,...,8}{% start from 1, go up to 7
                \only<\n>{%
                    \pgfmathtruncatemacro{\imagenumber}{\n - 1} % subtract 1 for the actual image number
                    \ifnum\imagenumber<10
                        \includegraphics[width=\textwidth]{kruskal-tile-0\imagenumber.jpg}
                    \else
                        \includegraphics[width=\textwidth]{kruskal-tile-\imagenumber.jpg}
                    \fi
                }
            }
        \end{overlayarea}
    \end{figure}

    \only<7>{\cake{} Can you complete the run?}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{} Run Kruskal's algorithm on the following graph.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{mst-kruskal.pdf}
    \end{figure}
\end{frame}

\subsection{Prim's algorithm}

\begin{frame}
    \frametitle{Overview of Prim's Algorithm}

    \only<1>{
        Prim's algorithm constructs the \ac{mst} as follows:
        \begin{enumerate}
            \item Initialize the \ac{mst} with a single vertex, chosen arbitrarily from the graph.
            \item Grow the \ac{mst} by adding the cheapest edge from the tree to a vertex not yet in the tree.
            \item Repeat step 2 until all vertices are included in the \ac{mst}.
        \end{enumerate}

        \hint{} The correctness is guaranteed by Corollary 23.2    
    }

    \only<2>{
        Prim's algorithm relies on:
        \begin{itemize}
            \item A min-priority queue $Q$.
            \item An attribute $\texttt{key}$ for each vertex, representing the minimum weight
                of any edge connecting the vertex to the partial \ac{mst}.
            \item A boolean array to track vertices already included in the \ac{mst}.
        \end{itemize}
    }
\end{frame}

\begin{frame}
    \frametitle{Min-Priority Queue}

    A \alert{min-priority}  queue is a data structure that
    provides the following operations: 
    \begin{itemize}
        \item $\textsc{Build-Queue}(V)$: Build a queue. Complexity: \( O(\abs{V}) \).
        \item $\textsc{Extract-Min}(Q)$: Removes and returns the element with
            the lowest \texttt{key}. 
            Complexity: \( O(\log \abs{V}) \).
        \item $\textsc{Decrease-Key}(Q, u, \texttt{newKey})$: Reduce the
            \texttt{key} of $u$ to \texttt{newKey}.  
            Complexity: \( O(\log \abs{V}) \).
    \end{itemize}
\end{frame}

\begin{frame}{Pseudo Code for Kruskal's Algorithm}
    \begin{algorithmic}[1]
        \only<1>{
        \Procedure{Prim}{$G, w, r$}
            \State let $B[1..\abs{V}]$ be a new bit array
            \For{each $u \in G.V$}
                \State $u.\texttt{key} = \infty$
                \State $u.\pi = \texttt{NIL}$
                \State $B[u] = \texttt{True}$ \Comment{$u \in Q$}
            \EndFor
            \State $r.\texttt{key} = 0$
            \State $Q = \textsc{Build-Queue}(G.V)$
            \algstore{bkmark}
        }
        \only<2>{
            \algrestore{bkmark}
            \While{$Q \neq \emptyset$}
                \State $u = \textsc{Extract-Min}(Q)$
                \State let $B[u] = \texttt{False}$
                \For{each $v \in G.Adj[u]$}
                    \If{$B[v]==\texttt{True}$ and $w(u, v) < v.\texttt{key}$}
                        \State $v.\pi = u$
                        \State \textsc{Decrease-Key}(Q, $v$, $w(u, v)$)
                        \Comment{$v.\texttt{key}=w(u,v)$}
                    \EndIf
                \EndFor
            \EndWhile
        \EndProcedure
        }
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Correctness of Prim's Algorithm}

    Prior to each iteration of the \texttt{while} loop of lines 8--16,
    \begin{enumerate}
        \item \( A = \{(v, v.\pi) : v \in V - \{r\} - Q\} \).
        \item The vertices already placed into the \ac{mst} are those in \( V
            - Q \).
        \item For all vertices \( v \) in \( Q \), if \( v.\pi \neq \texttt{NIL} \),
            then 
            \begin{itemize}
                \item \( v.key < \infty \) 
                \item and \( v.key \) is the weight of a light edge
                    \( (v, v.\pi) \) connecting \( v \) to some vertex already placed into
                    the \ac{mst}.
            \end{itemize}
    \end{enumerate}

    \hint{} So each time a vertex $u$ is dequeued, 
    it is guaranteed that $(u.\pi, u)$ is a safe edge.
\end{frame}

\begin{frame}
    \frametitle{Time Complexity of Prim's Algorithm}

    With a \alert{binary min-heap} to implement the \alert{min-priority queue} ,
    the time complexity is \( O(( \abs{V} + \abs{E}) \log \abs{V}) \),
    considering:

    \begin{itemize}
        \item \textsc{Extract-Min} costs \( O(\log \abs{V}) \),
            accumulated over all calls.
        \item The \texttt{while} loop processes \( O(\abs{E}) \) times, once for each edge.
        \item Checking $v \in Q$ costs constant time.
        \item Setting $v.\texttt{key} = w(u,v)$ costs in \( O(\log \abs{V}) \) time.
    \end{itemize}

    Thus, total time with a binary min-heap matches \( O(\abs{E} \log
    \abs{V}) \), akin to Kruskal's algorithm. 

    Using a \alert{Fibonacci heap} for the queue, the time complexity is
    improved to \( O(\abs{E} + \abs{V} \log \abs{V}) \).
\end{frame}

\begin{frame}
    \frametitle{Example Run of Prime's Algorithm}

    \vspace{3em}

    \begin{figure}[htpb]
        \centering
        \begin{overlayarea}{0.8\textwidth}{4cm} % Adjust the height to fit your images
            \foreach \n in {1,...,9}{% start from 1, go up to 9
                \only<\n>{%
                    \pgfmathtruncatemacro{\imagenumber}{\n - 1} % subtract 1 for the actual image number
                    \ifnum\imagenumber<10
                        \includegraphics[width=\textwidth]{prim-tile-0\imagenumber.jpg}
                    \else
                        \includegraphics[width=\textwidth]{prim-tile-\imagenumber.jpg}
                    \fi
                }
            }
        \end{overlayarea}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{} Run Prim's algorithm on the following graph with $r=7$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{mst-prim.pdf}
    \end{figure}
\end{frame}

\subsection{Exercise 23.2}

\begin{frame}{Exercise 23.2-8}

    Consider a divide-and-conquer algorithm for \ac{mst}

    \begin{enumerate}
        \item Partition vertex set \( V \) of graph \( G = (V, E) \) into \( V_1 \) and \( V_2 \) such that \( |V_1| \) and \( |V_2| \) differ by at most 1.
        \item Define \( E_1 \) as edges incident only on \( V_1 \), and \( E_2 \) as edges incident only on \( V_2 \).
        \item Recursively compute \ac{mst} for subgraphs \( G_1 = (V_1, E_1) \) and \( G_2 = (V_2, E_2) \).
        \item Select the lightest edge in \( E \) that connects \( V_1 \) and \( V_2 \) to merge two MSTs.
    \end{enumerate}

    \cake{} Do you think this algorithm is correct?
\end{frame}


\section{\acs{ita} 23 Problems}

\begin{frame}
    \frametitle{Problem 23-1 --- Uniqueness of \ac{mst}}

    Let \( G = (V, E) \) be a connected graph with $\abs{E} \ge \abs{V}$
    with distinct edge weights. 
    Show that $T$, the \ac{mst} of $G$, is unique.

    \begin{block}{Exercise 23.1-8}
        Let $T$ and $T'$ be the \ac{mst} of some graph $G$.
        Let $L$ and $L'$ be the weights of the edges of $T$ and $T'$, respectively.
        Then $L = L'$.
    \end{block}
\end{frame}


\begin{frame}
    \frametitle{Problem 23-1: Non-uniqueness of second-best \ac{mst}}

    \only<1>{Let $\mathcal{T}$ be the set spanning trees of $G$.

        The second-best \ac{mst} of $G$ is the spanning tree $T'$ with
        \begin{equation*}
            w(T') = \min_{T'' \in \scT-\{T\}} w(T'')
        \end{equation*}
        where $T$ is the unique \ac{mst} of $G$.}

    \only<2->{
        Show that the second-best \ac{mst} of $G$ is not necessarily unique.

        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.5\textwidth]{mst-second-best.pdf}
            \caption{%
                \cake{}
                \only<2>{What is the \ac{mst} of $G$?}%
                \only<3>{What is one second-best \ac{mst} of $G$?}%
                \only<4>{What is another second-best \ac{mst} of $G$?}%
            }
        \end{figure}
    }
\end{frame}


\begin{frame}{Problem 23-1: Existence of Second-best \ac{mst}}
    Show that we can replace an edge in \(T\), the unique \ac{mst} of $G$, to get
    a second-best \ac{mst}.

    Proof: 

    \only<1>{
        Let $T'$ be the second-best \ac{mst} of $G$.

        Since $T' \ne T$, then there exists an edge $(u,v) \in T'$ such that
        $(u,v) \notin T$.

        Let $C$ be the cycle containing $(u,v)$ and the path from $u$ to $v$ in
        $T'$. 
    }
    \only<2>{
        Then the heaviest edge $(x,y) \in C$ must satisfy $w(x,y) > w(u,v)$ and
        $(x,y) \notin T$.

        \begin{block}{Exercise 23.1-5}
            Let $e$ be the heaviest edge on some cycle of $G$.
            Let $G' = (V, E - \{e\})$. Then $G'$ has a \ac{mst} which is also an
            \ac{mst} of $G$.
        \end{block}
    }
    \only<3>{
        As a result, there cannot be another edge $(u',v')$ like $(u,v)$.
        Otherwise, we can find $T''$ with $w(T) < w(T'') < w(T')$.
    }
    \only<4>{
        Thus, $T' = T - (u,v) + (x,y)$.

        \astonished{} Moreover, $(u, v)$ must be the second heaviest edge in
        $C$.
    }
\end{frame}



\begin{frame}{Problem 23-1: Maximum Weight Edge on a Path}
    Describe an \( O(\abs{V}^2) \)-time algorithm to compute \(\max[u, v]
    \) for all vertex pairs.

    \begin{algorithmic}[1]
        \footnotesize
        \Procedure{Heaviest-Edge}{$T$}
            \State let $\text{maxEdge}[1\dots\abs{V}, 1\dots\abs{V}]$ be a new array
            \For{each $u, v \in T$}
                \If{$(u,v) \in T.E$}
                    \State \Return $w(u, v)$
                \Else
                    \State $x \gets$ the next vertex on the path from $u$ to $v$
                    \Comment{Problem 21-3}
                    \State $\text{maxEdge}[u,v] \gets \max(w(u,x), \textsc{Heaviest-Edge}(x, v))$
                \EndIf
            \EndFor
            \State \Return $\text{maxEdge}$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}{Problem 23-1: Algorithm for Second-best \ac{mst}}
    Find an algorithm to compute the second-best \ac{mst} of \( G \).

    \only<1>{
        \begin{algorithmic}[1]
        \footnotesize
        \Procedure{ModifiedMST}{$G$}
            \State $T \gets$ \Call{MST}{$G$} \Comment{Using a \(O(E + V\log V)\) time}
            \State $\text{minDiff} \gets \infty$
            \State $\text{minEdge} \gets$ \texttt{NIL}
            \State $\text{maxEdge} \gets$ \Call{Heaviest-Edge}{$T$}
            \ForAll{$(u, v) \in V \times V$}
                \State $w(u,v) \gets w - \text{maxEdge}[u, v]$ \Comment{$w(u,v) = \infty$ iff $(u, v) \notin G.E$}
                \If{$d < \text{minDiff}$}
                    \State $\text{minDiff} \gets d$
                    \State $\text{minEdge} \gets (u, v)$
                \EndIf
            \EndFor
            \If{$\text{minEdge} \neq$ \texttt{NIL}}
                \State $T \gets T + \{\text{minEdge}\} - \{\text{max weight edge in path from} \ u \ \text{to} \ v \ \text{in} \ T\}$
            \EndIf
            \State \Return $T$
        \EndProcedure
        \end{algorithmic}
    }
    \only<2>{%
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.5\textwidth]{mst-second-best.pdf}
            \caption{\cake{} Try to find the second-best \ac{mst} with the
            algorithm}
        \end{figure}
    }
\end{frame}

\end{document}
