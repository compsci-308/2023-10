\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Divide and Conquer (Part 2)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ita} 4.3  The substitution method for solving recurrences}

\begin{frame}[c]
    \frametitle{Experimental Mathematics}
    
    % Use columns environment for split layout
    \begin{columns}[c]
    
        % Left column for definition
        \begin{column}{0.5\textwidth}
            \alert{Experimental Mathematics} is a branch of mathematics which
            employs computations and simulations to test conjectures and
            identify patterns.
        \end{column}
        
        % Right column for comic figure
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{bunny-experimental-math.jpg}
                \caption{Math is 1\% inspiration and 99\% not eating the paper.}
            \end{figure}
        \end{column}
        
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Let's do Some Experiments}
    
    Consider the recursion:
    \begin{equation*}
        T(n) = 
        \begin{cases} 
            1 & \text{if } n = 1 \\
            2 T(n-1) + 1 & \text{otherwise}
        \end{cases}
    \end{equation*}
    
    Let's compute some values:
    \begin{equation*}
        \langle T(1) \dots T(12) \rangle
        =
        \langle 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095 \rangle
    \end{equation*}
    
    \cake{} Can you see a pattern?

    \hint{} You can also search the sequence on \href{https://oeis.org/}{OEIS}.
\end{frame}

\begin{frame}
    \frametitle{Proof by Induction}
    
    To prove a proposition $S(n)$ is true for all $n \ge n'$ by induction:
    
    \begin{itemize}
        \item \textbf{Base Case:} Prove $S(n')$ is true.
        \item \textbf{Inductive Step:} Assuming $S(n)$ is true if $n=k$ for some
            $k \ge n'$, prove $S(n)$ is true for $n=k+1$.
    \end{itemize}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{bunny-domino.jpg}
        \caption{The domino metaphor for induction}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example of Proof by Induction}
    
    We'll use induction to prove our guess
    \begin{equation}
        \label{eq:guess}
        T(n) = 2^{n} - 1, \qquad \text{for all } n \ge 1
    \end{equation}

    \pause{}
    
    \emph{Base Case:}
    For \( n = 1 \), we have
    \begin{equation*}
        T(1) = 2^{1} - 1 = 1
    \end{equation*}
    
    \pause{}
    
    \emph{Inductive Step:}
    Assume that \eqref{eq:guess} holds for \( n = k \ge 0 \).
    Then
    \begin{align*}
        T(k+1) &= 2T(k) + 1 \\
               &= 2(2^{k} - 1) + 1 \\
               &= 2^{k+1} - 2 + 1 \\
               &= 2^{k+1} - 1
    \end{align*}
\end{frame}

%\begin{frame}
%    \frametitle{A Useful Result}
%    
%    Let \( g(n) \) and \( f(n) \) be two functions with $g(n) > 0$. 
%
%    Then we have
%    \begin{equation*}
%        g(n) = O(f(n))
%    \end{equation*}
%    if and only if there exists a constant \( C \) such that:
%    \begin{equation*}
%        0 \le f(n) \le C g(n) \quad \text{for all } n \ge 1.
%    \end{equation*}
%
%    \cake{} Why is this true?
%\end{frame}

\begin{frame}
    \frametitle{Guessing An Upper Bound}
    
    Consider
    \begin{equation*}
        T(n) = 
        \begin{cases} 
            1 & \text{if } n = 1, \\
            2 T\left(\floor*{\frac{n}{2}}\right) +
            n & \text{otherwise}.
        \end{cases}
    \end{equation*}

    \cake{} Why it is easy to guess that
    \begin{equation*}
        \label{eq:guess}
        T(n) = O(n \log n)
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Proof by Strong Induction}
    
    To prove a proposition $S(n)$ is true for all $n \ge n'$ by induction:
    
    \begin{itemize}
        \item \textbf{Base Case:} Prove $S(n)$ is true for $n \in \{n', \dots,
            n''\}$.
        \item \textbf{Inductive Step:} Assuming $S(n)$ is true if $n \in \{n',
            \dots, k\}$ for some $k \ge n''$, prove $S(n)$ is true for $n=k+1$.
    \end{itemize}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{bunny-domino.jpg}
        \caption{The domino metaphor for induction}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Proving the Upper Bound}
    To prove $T(n) = O(n \log n)$, we show that
    there exists $c$ and $n_0$ such that
    \begin{equation}
        \label{eq:upper-bound}
        T(n) \le c n \log n \qquad \text{for all } n \ge n_0.
    \end{equation}

    \only<1-3>{
        \emph{Inductive Step:} 
        Assume that \eqref{eq:upper-bound} holds if 
        $n_0 \le n \le k-1$ 
        for some integer $k \ge 2 n_0$.
    }

    \only<2>{
        \emph{Base Case:}
        Let's take $n_0 = 1$.

        \cake{} Can we prove that \eqref{eq:upper-bound} holds if 
        $n_0 \le n \le k-1$ for $k = 2 n_0 = 2$.
    }
    \only<3>{
        \emph{Base Case:}
        Let's take $n_0 = 2$.

        \cake{} Can we prove that \eqref{eq:upper-bound} holds if 
        $n_0 \le n \le k-1$ for $k = 2 n_0 = 4$.
    }
    \only<4>{\hint{} We will ignore the base case in the future since we can
    choose $n_0$.}
\end{frame}

\begin{frame}
    \frametitle{How to Make a Good Guess}
    
    \only<1->{
        Consider the recurrence relation
        \[
            T(n) = 2 T\left(\left\lfloor\frac{n}{2}\right\rfloor + 17\right) +
            n.
        \]
    }
    
    \only<2>{
        Since \(\left\lfloor\frac{n}{2}\right\rfloor + 17\) is close to
        \(\frac{n}{2}\) when \( n \) is large. A good guess would be
        \[
            T(n) = O(n \log n).
        \]
    }
    
    \only<3>{
        % Overlay 2: We can also use a computer to do some numeric computation.
        Numeric computation can further confirm our guess.

        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.7\textwidth]{guess-upper-bound.pdf}
            \caption{The upper bound of \eqref{eq:upper-bound}.}
        \end{figure}
    }
\end{frame}

\begin{frame}
    \frametitle{Narrowing the Gap}
    
    Consider again that
    \begin{equation*}
        T(n) = 
        \begin{cases} 
            1 & \text{if } n = 1, \\
            2 T\left(\floor*{\frac{n}{2}}\right) +
            n & \text{otherwise}.
        \end{cases}
    \end{equation*}

    \cake{} Why it is obvious that $T(n) = \Omega(n)$.

    It is also not difficult to prove that $T(n) = O(n^2)$.

    We can then try to narrow the gap until we get $T(n) = \Theta(n \log n)$.
\end{frame}

\begin{frame}
    \frametitle{Why a Good Guess is Important}

    Consider
    \begin{equation*}
        T(n) = T(\lceil n/2 \rceil) + T(\lfloor n/2 \rfloor) + 1
    \end{equation*}
    We want to show that $T(n) = O(n)$.

    \only<1>{
        Why trying to prove that there exist $c, n_0$ such that
        \[
            T(n) \le c n \qquad \text{for all } n \ge n_0
        \]
        fails?
    }

    \only<2>{
        Why trying to prove the stronger statement
        that there exist $c, d, n_0$ such that
        \[
            T(n) \le c n -d \qquad \text{for all } n \ge n_0
        \]
        succeeds?
    }
\end{frame}

\begin{frame}
    \frametitle{Why Is This Wrong?}
    
    Consider again that
    \begin{equation*}
        T(n) = 
        \begin{cases} 
            1 & \text{if } n = 1, \\
            2 T\left(\floor*{\frac{n}{2}}\right) +
            n & \text{otherwise}.
        \end{cases}
    \end{equation*}

    % Pitfall
    We may mistakenly try to prove $T(n) = O(n)$ 
    by guessing $T(n) = cn$ and claim
    \begin{equation*}
        T(n) \leq 2c \ln(n/2) + n \leq cn + n = O(n).
    \end{equation*}
    
    \cake{} What is wrong?
\end{frame}

\begin{frame}
    \frametitle{Changing Variables}
    
    How can we solve
    \begin{equation*}
        T(n) = 2 T(\floor{\sqrt{n}}) + \lg n.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    What is the best asymptotic upper bound for $T(n)$ can you guess for
    \begin{equation*}
        T(n) = T(n-1) + \Theta(n)
    \end{equation*}
    and
    \begin{equation*}
        T(n) = T(\floor{n/2}) + \Theta(n)
    \end{equation*}
\end{frame}

\section{\acs{ita} 4.4 The recursion-tree method for solving recurrences}

\begin{frame}
    \frametitle{The Recursion Tree Method}
    
    Consider
    \begin{equation*}
        T(n) = 3 T(\floor{n/4}) + \Theta(n^2).
    \end{equation*}
    We will use a recursion tree to show that $T(n) = O(n^2)$.

    \only<1>{\cake{} Why do we have $T(n) = \Omega(n^2)$?}

    \only<2>{
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=\textwidth]{recursion-tree-01.png}
            \caption{Recursion \tree{} for $T(n) = 3 T(\floor{n/4}) + \Theta(n^2)$}
        \end{figure}
    }
\end{frame}

\begin{frame}
    \frametitle{The Complete Recursion Tree}

    Expanding the tree recursively, we eventually get the following tree:
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{recursion-tree-02.png}
        \caption{The complete recursion \tree{} for $T(n) = 3 T(\floor{n/4}) + \Theta(n^2)$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Proving the Upper Bound}
    
    By the recursion tree, we have
    \begin{equation*}
    T(n) = \sum_{i=0}^{ \log_4 n-1 } \left( \frac{3}{16} \right)^i cn^2 +
    \Theta(n^{\log_4 3})
    \end{equation*}

    How to show this implies that
    \begin{equation*}
        T(n) = O(n^2)?
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{A More Complicated Tree}
    
    Consider
    \begin{equation*}
        T(n) = T\left(\frac{n}{3} \right) + T\left(\frac{2n}{3} \right) + O(n)
    \end{equation*}

    What does its recursion tree look like?
\end{frame}

\section{\acs{ita} Master's Theorem}

\begin{frame}
    \frametitle{Theorem 4.1 --- Master's Theorem}

    Let \( a \geq 1 \) and \( b > 1 \) be constants. 
    Let
    \begin{equation*}
        T(n) = aT\left(\frac{n}{b}\right) + f(n), \text{for all } n \in \dsN.
    \end{equation*}
    \vspace{-2em}
    \begin{enumerate}[<+->]
        \item If \( f(n) = O(n^{\log_b a - \epsilon}) \) for some constant \( \epsilon > 0 \), then \( T(n) = \Theta(n^{\log_b a}) \).
        \item If \( f(n) = \Theta(n^{\log_b a}) \), then \( T(n) = \Theta(n^{\log_b a} \log n) \).
        \item If \( f(n) = \Omega(n^{\log_b a + \epsilon}) \) for some constant \( \epsilon > 0 \), and if \( af\left(\frac{n}{b}\right) \leq cf(n) \) for some constant \( c < 1 \) and all sufficiently large \( n \), then \( T(n) = \Theta(f(n)) \).
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Example of Case 2}
    
    \begin{block}{Master's Theorem}
    Let \( a \geq 1 \) and \( b > 1 \) be constants. 
    Let
    \begin{equation*}
        T(n) = aT\left(\frac{n}{b}\right) + f(n), \text{for all } n \in \dsN.
    \end{equation*}
    2.~If \( f(n) = \Theta(n^{\log_b a}) \), then \( T(n) = \Theta(n^{\log_b a} \log n) \).
    \end{block}

    \cake{} To apply the theorem to $T(n) = 2 T(n/2) + \Theta(n)$, what should
    $a, b, f(n)$ be?
\end{frame}

\begin{frame}
    \frametitle{Example of Case 1}
    
    \begin{block}{Master's Theorem}
    Let \( a \geq 1 \) and \( b > 1 \) be constants. 
    Let
    \begin{equation*}
        T(n) = aT\left(\frac{n}{b}\right) + f(n), \text{for all } n \in \dsN.
    \end{equation*}
    1.~If \( f(n) = O(n^{\log_b a - \epsilon}) \) for some constant \( \epsilon > 0 \), then \( T(n) = \Theta(n^{\log_b a}) \).
    \end{block}

    \cake{} To apply the theorem to $T(n) = 9 T(n/3) + n$, what should $a, b, f(n)$ and
    $\epsilon$ be?
\end{frame}

\begin{frame}
    \frametitle{Example of Case 3}
    
    \begin{block}{Master's Theorem}
    Let \( a \geq 1 \) and \( b > 1 \) be constants. 
    Let
    \begin{equation*}
        T(n) = aT\left(\frac{n}{b}\right) + f(n), \text{for all } n \in \dsN.
    \end{equation*}
    3.~If \( f(n) = \Omega(n^{\log_b a + \epsilon}) \) for some constant \( \epsilon > 0 \), and if \( af\left(\frac{n}{b}\right) \leq cf(n) \) for some constant \( c < 1 \) and all sufficiently large \( n \), then \( T(n) = \Theta(f(n)) \).
    \end{block}

    \cake{} To apply the theorem to $T(n) = 3 T(n/4) + n \log n$, what should $a, b, f(n)$ and
    $\epsilon$ be?
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 4.3
                \item
                    Section 4.4
                \item
                    Section 4.5
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 4.3: 1--3, 5, 6, 9.
                \item
                    Exercises 4.4: 1--5, 7.
                \item
                    Exercises 4.5: 1--4.
            \end{itemize}
        \end{column}
    \end{columns}

\end{frame}

\end{document}
