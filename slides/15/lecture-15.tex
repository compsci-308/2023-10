\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Shortest Paths (Part 1)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 24.1.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 24.1: 1--5.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 23 Single-Source Shortest Paths}

\begin{frame}
    \frametitle{Pathfinding in Games}

    \think{} How do AI find path in complicated environments such as Starcraft 2?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{starcraft-2.jpg}
        \caption{Screenshot of Starcraft 2}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Shortest Path from Kunshan to Chengdu}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            A \bunny{} drives from Kunshan to Chengdu. 

            \bigskip{}

            It has a road \emoji{world-map} on which the
            distance between each pair of adjacent cities are marked. 

            \bigskip{}

            How can the \bunny{} find the shortest route?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-drive-car.jpg}
                \caption{Driving on the shortest path}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Shortest Path Problem}
    Given a weighted, directed graph \( G = (V, E) \) with a weight function \(
    w: E \rightarrow \mathbb{R} \), the \alert{shortest path problem}  involves finding
    the minimum weight path between two vertices.

    The \alert{weight of a path}  \( p = \langle v_0, v_1, \ldots, v_k \rangle \) is
    defined as:
    \[ w(p) = \sum_{i=1}^{k} w(v_{i-1}, v_i) \]

    \pause{}

    The \alert{shortest-path weight}  from \( u \) to \( v \), denoted \(\delta(u, v) \), is:
    \[ \delta(u, v) = \left\{
            \begin{array}{ll}
                \min \{ w(p): u \leadsto^{p} v \} & \text{if there is a path from } u \text{ to } v, \\
                \infty & \text{otherwise}.
            \end{array}
    \right. \]

    A path \( p \) is a \alert{shortest path}  from \( u \) to \( v \) if \( w(p) = \delta(u, v) \).
\end{frame}

\begin{frame}[c]
    \frametitle{Applications of the Shortest-Path Problem}

    \alert{Route Planning} \emoji{world-map}
    \begin{itemize}
        \item GPS systems finding the quickest route from point A to B.
        \item Algorithms like Dijkstra's or $A^{\ast{}}$ used in Google Maps.
    \end{itemize}

    \alert{Network Routing} \emoji{computer}
    \begin{itemize}
        \item Determining the most efficient data path in internet traffic.
        \item Routing protocols like OSPF or BGP.
    \end{itemize}
    \alert{Game Development} \emoji{joystick}
    \begin{itemize}
        \item Pathfinding for characters or AI in virtual environments.
        \item AI algorithms in games for efficient movement of NPCs.
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Variants of Shortest-Paths Problems}
    This chapter focuses on the \alert{single-source shortest-paths problem}: find a
    shortest path from a specified \alert{source vertex} \( s \) to every other
    vertex \( v \in V \). 

    Variations include:

    \begin{itemize}
        \item Single-destination shortest-paths problem 
        \item Single-pair shortest-path problem 
        \item All-pairs shortest-paths problem
    \end{itemize}
\end{frame}

\begin{frame}{Lemma 24.1}
    Let \( p = \langle v_0, v_1, \ldots, v_k \rangle \) be a shortest path from
    \( v_0 \) to \( v_k \). 

    For any \( i \) and \( j \) with \( 0 \leq i \leq j
    \leq k \), the subpath \( p_{ij} = \langle v_i, v_{i+1}, \ldots, v_j \rangle
    \) of \( p \) from \( v_i \) to \( v_j \) is also a shortest path.
\end{frame}

\begin{frame}
    \frametitle{Negative Cycles}

    If there is a negative cycle in \( G \), then $\delta(u,v)$ may not be
    well-defined.

    Some algorithms assume there is no negative-weight edge.

    Others allow such edges, as long as the source cannot reach a
    negative-weight cycle.

    \begin{figure}[htpb]
        \begin{tikzpicture}[>=Stealth, 
            node distance=1.5cm, 
            auto, 
            every node/.style={graphnode, minimum size=0.6cm, inner sep=0pt, font=\footnotesize},
            ]
            % Nodes
            \node (s) {0};
            \node (c) [right of=s] {5};
            \node (a) [above of=c] {3};
            \node (e) [below of=c] {$-\infty$};
            \node (b) [right of=a] {-1};
            \node (d) [right of=c] {11};
            \node (f) [right of=e] {$-\infty$};
            \node (g) [right of=d] {$-\infty$};

            \begin{scope}[
                every node/.style={draw=none},
                ]
                % Edges
                \path[->, thick]
                    (s) edge node {3} (a)
                    (a) edge node {-4} (b)
                    (b) edge node {4} (g)
                    (s) edge node {5} (c)
                    (c) edge[bend left] node {6} (d)
                    (d) edge node {8} (g)
                    (d) edge[bend left] node {-3} (c)
                    (e) edge[bend right] node[below] {-6} (f)
                    (f) edge[bend right] node[below] {7} (g)
                    (s) edge node {2} (e)
                    (f) edge[bend right] node {3} (e)
                    ;
            \end{scope}
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cycles}

    \cake{} Can a shortest path contain
    \begin{itemize}[<+->]
        \item a negative-weight cycle,
        \item or a positive-weight cycle,
        \item or a zero-weight cycle?
    \end{itemize}

    \only<+->{
        The answer is \emph{no} in all cases.

        \bomb{} When we talk about shortest paths, we mean the shortest \alert{simple
        path} , i.e., path without cycles.

        Thus a shortest path contains at most $\abs{V}-1$ edges.
    }
\end{frame}

\begin{frame}
    \frametitle{Predecessor}

    The algorithms (in this chapter) will maintain an
    attribute $v.\pi$ which is the \alert{predecessor} of $v$ in the shortest path from $s$ to $v$.

    By checking $v.\pi$, we can find the shortest path from
    $s$ to $v$.

    We define the \alert{predecessor subgraph}  $G_{\pi}$ as the subgraph with vertex set
    \begin{equation*}
        V_{\pi} = \{ v \in V : v.\pi \neq \texttt{NIL} \} \cup \{s\}
    \end{equation*}
    and edge set
    \begin{equation*}
        E_{\pi} = \{ (v.\pi, v) : v \in V_{\pi} - \{s\}\}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Shortest-paths trees}

    \only<1>{
        The algorithms will produce $G_{\pi}$ which properties:
        \begin{itemize}
            \item it is a tree with root $s$,
            \item it has a vertex set $V'$ containing all vertices reachable from $s$,
            \item for all vertex $v \in V'$, the path from $s$ to $v$ in $G_{\pi}$ is a
                shortest-path from $v$ to $s$ in $G$.
        \end{itemize}
        We call such a subgraph of $G$ a \alert{shortest-paths tree}.
    }

    \begin{figure}[htpb]
        \centering
        \includegraphics<2>[width=0.6\textwidth]{shortest-path-tree-tile-00.png}
        \includegraphics<3>[width=0.6\textwidth]{shortest-path-tree-tile-01.png}
        \includegraphics<4>[width=0.6\textwidth]{shortest-path-tree-tile-02.png}
        \caption{%
            \only<2>{A weighted directed graph}%
            \only<3>{A shortest-paths tree}%
            \only<4>{Another shortest-paths tree}%
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Shortest-path Estimate}

    The algorithms maintain an attribute $v.d$ which is the upper bound of the
    weight of the shortest path from $s$ to $v$.

    We call $v.d$ the \alert{shortest-path estimate} of $v$.


    \begin{algorithmic}[1]
        \Procedure{Initialize-Single-Source}{$G, s$}
        \For{each vertex $v \in G.V$}
            \State $v.d = \infty$
            \State $v.\pi = \texttt{NIL}$
        \EndFor
        \State $s.d = 0$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Relax}

    \alert{Relaxing} an edge means
    \begin{itemize}
        \item testing whether we can improve the shortest path to $v$ by going
            through $u$ 
    \item and, if so, updating $v.d$ and $v.\pi$.
    \end{itemize}

    \only<1>{
        \begin{algorithmic}[1]
            \Procedure{Relax}{$u, v, w$}
            \If{$v.d > u.d + w(u,v)$}
            \State $v.d = u.d + w(u,v)$
            \State $v.\pi = u$
            \EndIf
            \EndProcedure
        \end{algorithmic}
    }
    \only<2->{
        \begin{figure}[htpb]
            \centering
            \includegraphics<2>[width=0.45\textwidth]{relax-01.png}
            \includegraphics<3>[width=0.45\textwidth]{relax-01.png}
            \caption{A ``\only<2>{successful}\only<3>{failed}'' relaxation of an edge $(u,v)$}
        \end{figure}
    }
\end{frame}

\begin{frame}{Properties of Shortest Paths}
  \only<1>{
     \alert{Triangle Inequality:} For any edge \((u, v)\), the path from \(s\)
     to \(v\) through \(u\) is at most the sum of the shortest path from \(s\)
     to \(u\) and the edge weight from \(u\) to \(v\).
  }
  \only<2>{
     \alert{Upper-bound Property:} The distance estimate $v.d$ for any vertex
     \(v\) is an upper bound on the shortest path length from \(s\) to \(v\),
     which never increases.
  }
  \only<3>{
     \alert{No-path Property:} If no path exists from \(s\) to \(v\), the
     shortest path estimate remains infinite.
  }
  \only<4>{
     \alert{Convergence Property:} Once a vertex \(u\) is processed, the
     shortest path estimate to \(u\) does not change.
  }
  \only<5>{
     \alert{Path-relaxation Property:} If $p = \langle v_{0}, v_{1}, \dots v_{k} \rangle$
     is the shortest path from \(v_0=s\) to \(v_k\), 
     then
     relaxing the edges in the order \((v_0, v_1), \dots, (v_{k-1}, v_k)\) will
     make $v_{k}.d = \delta(s, v_k)$.

     This hold \emph{regardless} of any other relaxation steps that occur.
  }
  \only<6>{
     \alert{Predecessor-subgraph Property:} After all vertices are processed,
     the predecessor subgraph forms a shortest-paths tree rooted at the
     source \(s\).
  }
\end{frame}

\section{\acs{ita} 24.1 The Bellman-Ford algorithm}

\begin{frame}
    \frametitle{Overview of Bellman-Ford Algorithm}

    The \alert{Bellman-Ford algorithm} allows for some of the edges
    to have negative weights. 

    \begin{itemize}
        \item The algorithm returns \texttt{False} if a
            \alert{negative-weight cycle} is reachable from the source $s$. 
        \item In the absence of such cycles, it successfully computes the shortest
            paths from $s$ and their corresponding weights.
    \end{itemize}

    The process involves \emph{relaxation} of every edge.
\end{frame}

\begin{frame}
    \frametitle{Lemma 24.2}

    Assume that there is no negative-weight cycle reachable from the source $s$.

    Then after the $\abs{V} - 1$ iterations of the for loop, $s.d=\delta(s, v)$ for
    all vertices reachable from $s$.

    \pause{}

    \begin{block}{Corollary 24.3}
        Assume the condition of Lemma 24.2.

        Then there is a path from $s$ to every vertex $v$ if and only if
        after \textsc{Bellman-Ford} terminates $s.d < \infty$ for every vertex $v$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Theorem 24.4}

    If there is no negative-weight cycle reachable from the source $s$, then
    after \textsc{Bellman-Ford} terminates,
    \begin{itemize}
        \item $v.d = \delta(s,v)$ for all vertices from $s$,
        \item $G_{\pi}$ as a shortest-paths tree rooted at $s$,
        \item and the algorithm returns \texttt{True}.
    \end{itemize}

    Otherwise, \textsc{Bellman-Ford} returns \texttt{False}.
\end{frame}

\begin{frame}
    \frametitle{Pseudocode}

    \cake{} What is the \emoji{stopwatch} complexity?

    \begin{algorithmic}[1]
        \small
        \Procedure{Bellman-Ford}{$G, w, s$}
        \State \Call{Initialize-Single-Source}{$G, s$}
        \For{$i = 1$ \texttt{to} $|G.V| - 1$}
            \For{each edge $(u, v) \in G.E$}
                \State \Call{Relax}{$u, v, w$}
            \EndFor
        \EndFor
        \For{each edge $(u, v) \in G.E$}
            \If{$v.d > u.d + w(u, v)$}
                \State \Return \texttt{False}
            \EndIf
        \EndFor
        \State \Return \texttt{True}
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Example Run}
    In the example, if $(u,v)$ is shaded, then $v.\pi = u$.

    The edges are relaxed in the order
    \begin{equation*}
    (t, x), (t, y), (t, z), (x, t), (y, x), (y, z), (z, x), (z, s), (s, t), (s, y)
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \begin{overlayarea}{0.5\textwidth}{4cm} % Adjust the height to fit your images
            \foreach \n in {1,...,5}{%
                \only<\n>{%
                    \pgfmathtruncatemacro{\imagenumber}{\n - 1} % subtract 1 for the actual image number
                    \includegraphics[width=\textwidth]{bellman-ford-tile-0\imagenumber.png}
                }
            }
        \end{overlayarea}
    \end{figure}
\end{frame}

\subsection{Exercises}

\begin{frame}
    \frametitle{Exercise 24.1-4}
    Modify the Bellman-Ford algorithm so that it sets $v.d$ to
    $-\infty$ for all vertices $v$ for which there is a negative-weight cycle on
    some path from the source to $v$.

    \begin{algorithmic}[1]
        \small
        \only<1>{
            \Procedure{Bellman-Ford-Modified}{$G, w, s$}
            \State {\bf Initialize-Single-Source}(G, s)  
            \For{$i \gets 1$ {\bf to} $|G.V| - 1$}
                \For{{\bf each} edge $(u, v) \in G.E$}  
                \State \Call{Relax}{$u, v, w$}
                \EndFor
            \EndFor
            \algstore{bkmark}
        }
        \only<2>{
            \algrestore{bkmark}
            \For{{\bf each} edge $(u, v) \in G.E$}
                \If{$v.d > u.d + w(u, v)$}
                    \State {\bf mark} $v$ 
                \EndIf
            \EndFor
            \For{{\bf each} vertex $u \in$ marked vertices}
                \If{$u$ is marked}
                    \State \Call{DFS-Mark}{$u$}
                \EndIf
            \EndFor  
            \EndProcedure
        }
        \only<3>{
            \Procedure{DFS-Mark}{$u$}
            \If{$u \neq \texttt{NIL}$ and $u.d \neq -\infty$}
                \State $u.d \gets -\infty$
                \For{{\bf each} $v$ {\bf in} $G.Adj[u]$}
                    \State \Call{DFS-Mark}{$v$}
                \EndFor
            \EndIf
            \EndProcedure
        }
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Exercise 24.1-5}

    Let \( G = (V, E) \) be a weighted, directed graph with weight function \(w: E \rightarrow \mathbb{R} \). 

    Give an \( O(|V||E|) \)-time algorithm to find, 
    for each vertex \( v \in V \), 
    the value
    \begin{equation*}
        \delta^{\ast{}}(v) = \min_{u \in V}\{ \delta(u, v) \}
    \end{equation*}

    \pause{}

    \laughing{} You cannot find a good solution via Google.

    Solution: 
    \begin{itemize}
        \item Add an extra vertex $s$ and edges from $s$ to all vertices with
        weight $0$.
        \item Run \textsc{Bellman-Ford-Modified} from Exercise 24.1-4.
        \item Then $v.d$ is the answer.
    \end{itemize}
\end{frame}

\end{document}
