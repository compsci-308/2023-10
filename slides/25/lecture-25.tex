\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../language.tex}

\title{Lecture \lecturenum{} --- NP-Completeness (Part 1)}

\begin{document}

\maketitle

\begin{frame}[standout,c]
    \vspace{1em}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-evalution.jpg}
        \caption*{\emoji{pray}\textcolor{white}{Please use the next 15 minutes
        to write course evaluation.}}
    \end{figure}
\end{frame}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    Chapter 34 introduction part.
                \item
                    Section 34.1
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 34.1: 1-6.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ita} 34 NP-Completeness}

\subsection{Classification of Problems}

\begin{frame}[c]
    \frametitle{Problems Unsolvable with Algorithms}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The \alert{Halting Problem}  asks whether 
            there exists an algorithm that can determine, 
            for any other algorithm and its input, 
            whether the algorithm halts or \emoji{running} indefinitely.

            \bigskip{}

            Alan Turing (1936) showed that the answer is no.

            \bigskip{}

            \emoji{cry} There are problems algorithms \alert{cannot} solve.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Alan-Truing.jpg}
                \caption{Alan Turing (1912-1954). \emoji{uk} mathematician,
                computer scientist, logician, cryptanalyst.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Turing's Proof}

    Assuming such an algorithm \textsc{Halts} exists,
    Turing asks will the following algorithm terminate?

    \begin{algorithmic}[1]
        \Procedure{G}{}
        \If{\textsc{Halts(G)}}
        \While{True}
        \EndWhile
        \EndIf
        \EndProcedure
    \end{algorithmic}

    \cake{} What happens if \textsc{Halts(G)} is true?
\end{frame}

\begin{frame}[c]
    \frametitle{Problems Solvable with Algorithms}

    We categorize algorithms based on their running time in relation to input size $n$:
    \begin{itemize}
        \item \alert{Polynomial-time} algorithms have a worst-case running time of $O(n^k)$ for some constant $k$.
        \item \alert{Superpolynomial-time} algorithms exceed $O(n^k)$ for any constant $k$.
    \end{itemize}

    \pause{}

    Thus problems solvable by algorithms can be categorized as:
    \begin{itemize}
        \item \alert{Tractable} --- solvable by polynomial-time algorithms.
        \item \alert{Intractable} --- require superpolynomial-time algorithms.
    \end{itemize}

    \cake{} Examples of each?
\end{frame}

\begin{frame}{NP-Complete Problems}
    \alert{NP-Complete problems} are a set of problems in computational theory
    that are \emph{notoriously difficult}:
    \begin{itemize}
        \item No \alert{polynomial-time algorithm} has been found for these
            problems.
        \item It is unproven whether a polynomial-time algorithm \alert{can or
            cannot} exist for them.
    \end{itemize}

    \pause{}

    This called the \alert{$\text{P} \ne \text{NP}$} problem.

    \astonished{} It is one of the most intriguing and unsolved
    questions in computer science since 1971. 

    \emoji{star-struck} You will get quite a lot of \emoji{trophy} and \emoji{money-bag} if
    you can solve it.
\end{frame}

\begin{frame}
    \frametitle{Examples of NP-Complete Problems}

    The distinction between NP-complete problems and similar problems
    solvable in polynomial time (P) can be quite subtle.

    Sometimes changing one word or number in a problem changes it into
    an NP-complete problem!

    \begin{table}
        \centering
        \begin{tabular}{|c|c|}
            \hline
            \alert{\weary{} NP-Complete Problems} & \alert{\smiling{} Similar Problems in P} \\
            \hline
            Longest Simple Path & Shortest Simple Path \\
            Hamiltonian Cycle & Eulerian Cycle \\
            3-SAT & 2-SAT \\
            \hline
        \end{tabular}
        \caption{Comparing NP-Complete problems with similar P problems}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{Longest Versus Shortest Simple Path}

    Using Mathematica, it takes $5\cdot{}10^{-5}$ seconds 
    to find the shortest path from $a$ to $d$ with Dijkstra's algorithm,
    and $3\cdot{}10^{-2}$ seconds to find the longest path with brute force.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{long-and-short-path.pdf}
        \caption{Longest and shortest simple path.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Eulerian Graphs Versus Hamiltonian Graphs}
    
    A graph is \alert{eulerian} if it contains a \emph{circuit} which visits
    every edge exactly once.

    A graph is \alert{hamiltonian} if it contains a \emph{cycle} which visits
    every edge exactly once.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{hamiltonian.png}
        \caption{\cake{} Which is Eulerian and which is Hamiltonian?}
    \end{figure}
\end{frame}

\subsection{NP-completeness and the classes P and NP}

\begin{frame}
    \frametitle{Classes of Problems}

    In this chapter, we will consider three fundamental classes of problems in computational complexity:
    \begin{itemize}[<+->]
        \item[\smiling{}] \alert{P (Polynomial Time)} -- Problems that
            can be solved by a deterministic algorithm in polynomial time.
        \item[\emoji{monocle-face}] \alert{NP (Nondeterministic Polynomial
            Time)} --
            Problems that, although we may not know how to solve them
            efficiently, if given a proposed solution, we can verify its
            correctness in polynomial time.
        \item[\weary{}] \alert{NPC (NP-Complete)} --- The hardest problems in
            NP.    
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example of NP Problems}
    
    The problem of deciding if $G = (V,E)$ is hamiltonian is in NP.

    This is because given a list of vertices $\langle v_1, \dots, v_{\abs{V}}
    \rangle$ (a \alert{certificate}),
    we can check if they form cycle in linear time, i.e., O($\abs{V}$).

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{non-hamiltonian-graph.pdf}
        \caption{%
            \cake{} Is this hamiltonian? % 
            Is $\langle a, b, i, j, c, e, f, h, d, g\rangle$ a hamiltonian cycle?}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Relationships among P, NP, and NPC}
    
    Any problem in P is also in NP, i.e., $\text{P} \subseteq \text{NP}$ ---
    if it can be solved in polynomial time, it can be verified in polynomial time.

    \only<1>{%
        \emoji{wink}
        If you know how to solve a problem,
        then you should know if an answer is correct.
    }%

    \pause{}

    NPC contains the \emph{hardest} problem in NP, because if you can solve any
    of them in polynomial time, you can solve all of them in polynomial time.

    Most \emoji{scientist} believe that $\text{P}=\text{NP}$ is too good to be
    possible.

    \cake{} Why would finding an algorithm solving a problem in NPC in
    polynomial time imply $\text{P} = \text{NP}$?
\end{frame}

\begin{frame}[c]
    \frametitle{Piratical Implications}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}

            \weary{} In practice, this means if a problem is NP-complete, you'd better forget
            about finding an efficiently algorithm to solve it \emph{all} the
            time.

            \bigskip{}

            \smiling{} But you can try to find 
            an efficient algorithm to solve it \emph{most} of the time, 
            or solve it approximately \emph{all} the time.

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-programmer.jpg}
                \caption{NPC is not an excuse!}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\acf{tsp}}

    The \acs{tsp} is a classic NP-complete problem.

    But Vašek Chvátal and others managed to solve pretty large \ac{tsp}
    instances.

    \vspace{-1em}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.55\textwidth]{TSP.jpg}
                \caption{In Pursuit of the Travelling Salesman}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.6\textwidth]{Vasek-Chvatal.jpg}
                \caption{Václav (Vašek) Chvátal}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Decision Versus Optimization}

    Many problems are \alert{optimization problems},
    i.e., finding the solution which maximizes or minimizes some property.

    Example includes the SHORTEST-PATH problem, the Knapsack problem, etc.

    NP Problems are \alert{decision problems}, i.e., a Yes/No question.

    Example includes the if a graph is eulerian or hamiltonian.

    But solving a decision problem often means solving an optimization problem,
    and vice versa.
\end{frame}

\begin{frame}
    \frametitle{SHORTEST-PATH and PATH}

    Consider the optimization problem SHORTEST-PATH and the decision problem
    PATH, which decides if in a graph $G$ there is a path of length at most 
    $k$ from vertex $u$ to vertex $v$.

    \cake{} How can we use one to solve the other?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Assume that you have an algorithm $\textsc{HasCycle}(G, k)$ which
    returns $1$ if $G$ has a cycle of length $k$ and $0$ otherwise.

    \cake{} How do you use it to find the length of the longest cycle in $G$?
    Complete the following algorithm.

    \begin{tcolorbox}
        \begin{algorithmic}
            \Procedure{FindLongestCycle}{$G$}
            \State
            \State
            \State
            \State
            \EndProcedure
        \end{algorithmic}
    \end{tcolorbox}
\end{frame}

%\begin{frame}
%    \frametitle{Reduction}
%    To show a decision problem B is NP-Complete, we 
%    show that another decision problem A can be reduced to it as follows:
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\textwidth]{reduction.png}
%        \caption{Reduction to NP-Complete problems}
%    \end{figure}
%
%    Thus, if A is NP-Complete, then B is NP-Complete.
%
%    \think{} For this to work, we need first to find at least one NP-Complete
%    problem.
%\end{frame}

\section{\acs{ita} 34.1 P --- Polynomial Time}

\subsection{Abstract Problem}

\begin{frame}
    \frametitle{Abstract Problem}

    We must first define what a problem is in a formal context. 

    An \alert{abstract problem} \(Q\) is characterised as a binary relation on a set
    \(I\) of \alert{problem instances} and 
    a set \(S\) of \alert{problem solutions}.

    In the SHORTEST-PATH problem:
    \begin{itemize}
        \item an \emph{instance} is a graph $G$ along with two vertices \(s\) and \(t\),
        \item a \emph{solution} is a sequence of vertices in the graph, representing
            a path from \(s\) to \(t\).
    \end{itemize}

    \pause{}

    A \alert{decision} problem is an abstract problem,
    but its solution set is simply $S = \{0, 1\}$.
\end{frame}

\subsection{Encodings}

\begin{frame}
    \frametitle{Encoding}

    To solve a problem with \emoji{computer},
    it must be encoded.

    An encoding of a set of objects $\scS$ 
    is a function $e$ from $\scS$ to $\{0,1\}^{\ast}$, i.e., the set of binary strings.

    For example,
    \begin{itemize}
        \item $\dsN = \{0, 1, 2, 3, \dots\}$ 
            can be encoded as $\{0, 1, 10, 11, \dots\}$.
        \item Letter A can be encoded as \texttt{1000001}.
        \item Whatever you are looking at on your \emoji{iphone},
            it is encoded in some way.
    \end{itemize}

    \laughing{} If we are living in simulation in some super computer of
    \emoji{alien}, we are also encoded.
\end{frame}

\begin{frame}
    \frametitle{Polynomial-Time Solvability}

    A \alert{concrete problem} is a problem whose instance set contains only binary strings
    strings.

    An algorithm \alert{solves} a concrete problem if it can provide a solution
    in time \( O(T(n)) \), where \( n \) is the length of the problem instance
    \(i \in \{0,1\}^{\ast}\), denoted as \( n = |i| \).

    A concrete problem is \alert{polynomial-time solvable} if there exists an algorithm
    that solves it in \( O(n^k) \) for some constant \( k \).
\end{frame}

\begin{frame}
    \frametitle{Abstract to Concrete}

    To extend solvability to abstract problems,
    we need an encoding \( e: I \rightarrow \{0,1\}^* \) which transforms 
    \( I \) to $\{0,1\}^\ast{}$.

    \weary{} Unfortunately, which encoding we use affects the efficiency of
    solving the problem.

    For example, we can encode a number $k$ 
    \begin{itemize}
        \item $k$ 1s (\alert{unary encoding}),
        \item or the binary representation of $k$ (\alert{binary encoding}).
    \end{itemize}
    The former is much less efficient than the latter.

    \smiling{} In practice, if we rule out the slow ones, which encoding we are
    using does not really matter.
\end{frame}

\begin{frame}{Related Encodings}
    A function \( f: \{0,1\}^* \rightarrow \{0,1\}^* \) is \alert{polynomial-time computable} if there exists an algorithm \( A \) that, given any input \( x \in \{0,1\}^* \), computes \( f(x) \) in polynomial time.

    \pause{}

    For a set \( I \) of problem instances, two encodings \( e_1 \) and \( e_2 \) are \alert{polynomially related} if there exist functions \( f_{12} \) and \( f_{21} \) such that:
    \begin{itemize}
        \item For any instance \( i \in I \), \( f_{12}(e_1(i)) = e_2(i) \) and \( f_{21}(e_2(i)) = e_1(i) \).
        \item Both \( f_{12} \) and \( f_{21} \) are computable in polynomial time.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Lemma 34.1}
    Let \( Q \) be an abstract decision problem on an instance set \( I \), and
    let \( e_1 \) and \( e_2 \) be polynomially related encodings on \( I \).
    Then, \( e_1(Q) \in P \) if and only if \( e_2(Q) \in P \).

    \hint{} Basically, using binary or base 3 encoding does not matter.
\end{frame}

\begin{frame}
    \frametitle{Assumptions Encoding}

    We will assume problem instances are encoded in a
    standard fashion using binary representations. 

    We will also ignore the difference between abstract and concrete problems.

    This is \emoji{ok} since our analysis results will be true for any encoding
    polynomially related to the standard one.

    Notation wise, we will use \(\langle G \rangle\) denote a standard
    presentation \( G \).
\end{frame}

\subsection{A Formal-language Framework}

\begin{frame}
    \frametitle{Formal Language Theory}

    An alphabet $\Sigma$ is a finite set of symbols.

    A \alert{language} $L$ over $\Sigma$ is a set of strings of symbols from
    $\Sigma$.

    Let $\epsilon$ be the \alert{empty string} 
    and $\emptyset$ be the set of \alert{empty strings}.

    Let $\Sigma^{\ast{}}$ be the set of all strings over $\Sigma$.

    \only<1>{%
        \cake{} What is $\Sigma^{\ast{}}$ if $\Sigma = \{\temoji{apple}\}$.
    }%

    \pause{}

    The \alert{complement} of a language $L$ is 
    $\overline{L} = \Sigma^{\ast{}} - L$.

    Then \alert{conctatenation} of two languages $L_1$ and $L_2$ is
    \begin{equation*}
        L_{1} L_{2} = \{x_1 x_2 : x_1 \in L_{1}, x_2 \in L_{2}\}.
    \end{equation*}

    \only<2>{%
        \cake{} What is $L \Sigma$ if $L=\{蔡\}$ and $\Sigma$ is all Chinese
        characters.
    }%

%    \pause{}
%
%    The \alert{closure} or \alert{Kleene star} of a language \( L \) is the language
%    \begin{equation*} 
%        L^\ast{} = \{\varepsilon\} \cup L \cup L^2 \cup L^3 \cup \ldots , 
%    \end{equation*}
%    where \( L^k \) is the language obtained by concatenating \( L \) to itself \( k \) times.
\end{frame}

\begin{frame}{Decision Problems and Languages}
    In the realm of \alert{language theory}, any decision problem \( Q \) is
    viewed as a subset of \( \Sigma^* \), where \( \Sigma = \{0, 1\} \).

    Instances of \( Q \) yielding a Yes answer are elements of 
    a language \( L \) over \( \Sigma \), such that:
    \begin{equation*} 
        L = \{ x \in \Sigma^* : Q(x) = 1 \} 
    \end{equation*}

    \pause{}

    For example, \alert{PATH} is represented by the language:
    \begin{equation*} 
        \begin{aligned}
            \text{PATH} & = \{ \\
                & \langle G, u, v, k \rangle : G = (V, E) \text{ is an undirected graph},  \\
                & u, v \in V, k \geq 0, \\
                & \text{ and a path from } u \text{ to } v \text{ in } G \text{ with at most } k \text{ edges exists} \}. 
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Accept and Reject a Language}

    The \alert{formal language framework} provides a way to
    connect \alert{decision problems} and the \alert{algorithms} that
    resolve them.

    An algorithm \( A \) \alert{accepts} a string \( x \) from the set \( \{0,
    1\}^* \) if, for input \( x \), \( A(x) = 1 \).

    Conversely, \( A \) \alert{rejects} a string \( x \) if \( A(x) = 0 \).

    The \alert{language accepted} by an algorithm \( A \) is the collection \( L
    = \{x \in \{0, 1\}^* : A(x) = 1\} \), which encompasses all strings that \(
    A \) accepts.
\end{frame}

\begin{frame}
    \frametitle{Decide Versus Accept}

    Even if $L$ is accepted by $A$, $A$ may not be able to
    reject $x \notin L$. For example, it may loop forever.

    A language \( L \) is \alert{decided} 
    by an algorithm \( A \) 
    if \( A \) accepts every string \( x \in L \), 
    and $A$ rejects every string \(x \notin L \).

    \bomb{} Note that \( A \) \alert{decides} \( L \) implies \( A \) accepts \( L \),
    but the converse is not necessarily true.
\end{frame}

\begin{frame}
    \frametitle{Example}
    Consider the algorithm that take $\langle G, u, v, k \rangle$ as input which
    \begin{itemize}
        \item runs BFS algorithm on $G$ with $u$ as the root;
        \item
            returns 1 if there is a path of length at most $k$ from $u$ to $v$
            in $G$,
        \item  otherwise eats imaginary electronic \emoji{icecream} until you unplug the
            computer in frustration.
    \end{itemize}

    \cake{} Does this algorithm accept the language PATH? Does it decide PATH?
\end{frame}

\begin{frame}
    \frametitle{Decide/Accept in Polynomial Time}

    \( L \) is \alert{accepted in polynomial time} by \( A \) 
    if for any string $x \in L$ of length \( n \), 
    \( A \) accepts $x$ in \( O(n^k) \) time.

    \( L \) is \alert{decided in polynomial time} by \( A \) 
    if for any string $x \in \{0,1\}^{\ast{}}$ of length \( n \), 
    \( A \) \emph{correctly} accepts $x$ or rejects $x$ in \( O(n^k) \) time.

    \bomb{} Note that if $L$ can be decided in polynomial time, 
    then $L$ can be accepted in polynomial time.
\end{frame}

\begin{frame}
    \frametitle{Alternative Definition of P}

    Using the framework of language theory,
    we can define
    \begin{equation*}
        \begin{aligned}
            \text{P} 
            &
            = 
            \{
                L \subseteq \{0,1\}^* : \\
            &
                \text{There exists an algorithm $A$ 
                    which decides $L$ in polynomial time.}
            \}
        \end{aligned}
    \end{equation*}

    \pause{}

    \begin{tcolorbox}[title={Theorem 34.2}]
        \astonished{} In fact, we also have:
        \begin{equation*}
            \begin{aligned}
                \text{P} 
                &
                = 
                \{
                    L: \\
                &
                \text{There exists an algorithm $A$ 
                which accepts $L$} \\
                &
                \text{in polynomial time.}
                \}
            \end{aligned}
        \end{equation*}
    \end{tcolorbox}

    \hint{} The proof is nonconstructive.
\end{frame}

\end{document}
