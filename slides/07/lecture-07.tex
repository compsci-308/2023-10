\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Dynamic Programming (Part 1)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ita} 15 Dynamic Programming}

\begin{frame}[c]{Fibonacci's Bunny Puzzle}

    \begin{columns}[c]
    \begin{column}{0.5\textwidth}
      Imagine a pair of bunnies that reproduce every month. 

      \medskip{}

      Starting with an \emph{immature} single pair, each pair becomes
      \emph{mature} and gives birth to a new \emph{immature} pair beginning
      their second month of life. 

      \medskip{}

      How many pairs of bunnies will there be after \( n \) months?
    \end{column}

    \begin{column}{0.5\textwidth}
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=\textwidth]{bunny-multiply.jpg}
            \caption{Fibonacci and his bunnies}
        \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Fibonacci's Bunny Puzzle in Picture}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{Fibonacci-Rabbits.jpg}
        \caption{Counting Fibonacci's bunnies}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Fibonacci Sequence}

    The Fibonacci sequence is defined recursively as:
    \begin{equation*}
        F_n = 
        \begin{cases}
            0 & \text{if } n = 0 \\
            1 & \text{if } n = 1 \\
            F_{n-1} + F_{n-2} & \text{if } n > 1
        \end{cases}
    \end{equation*}

    \puzzle{} Can you prove the following solution?
    \begin{equation*}
        F_n = 
        \frac{1}{\sqrt{5}} \left( \frac{1 + \sqrt{5}}{2} \right)^n
        -
        \frac{1}{\sqrt{5}} \left( \frac{1 - \sqrt{5}}{2} \right)^n,
        \text{for all } n \in \mathbb{N}.
    \end{equation*}


    \weary{} It's not
    the most efficient for large values of $n$ due to the irrational number
    $\sqrt{5}$.
\end{frame}

\begin{frame}
    \frametitle{Fibonacci Numbers in Nature}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{FibonacciChamomile.jpg}
        \caption{Yellow chamomile head showing the arrangement in 21 (blue) and 13 (cyan) spirals}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Naive Way}
    
    \begin{algorithmic}[1]
        \Procedure{FibonacciNaive}{$n$}
        \If{$n = 0$}
        \State \Return 0
        \ElsIf{$n = 1$}
        \State \Return 1
        \Else
        \State \Return \Call{FibonacciNaive}{$n-1$} + \Call{FibonacciNaive}{$n-2$}
        \EndIf
        \EndProcedure
    \end{algorithmic}

    \cake{} Let's try to compute $F_5$ with the naive algorithm.

    \puzzle{} What is the time complexity of this algorithm?
\end{frame}

\begin{frame}
    \frametitle{A Better Way}
    
    Let's try to compute $F_5$ with the following improved algorithm:

    \begin{algorithmic}[1]
    \Procedure{Fibonacci}{$n$}
        \State $\text{Initialize array } dp[0 \ldots n] \text{ with zeros}$
        \State $dp[0] \gets 0$
        \State $dp[1] \gets 1$
        \For{$i \gets 2 \text{ to } n$}
            \State $dp[i] \gets dp[i-1] + dp[i-2]$
        \EndFor
        \State \Return $dp[n]$
    \EndProcedure
    \end{algorithmic}

    An algorithm in this style is called \alert{dynamic programming}.

    \cake{} What is the time complexity of this algorithm?
\end{frame}


\begin{frame}{Dynamic Programming}
    \alert{Dynamic programming} is usually applied to optimization problems.

    It has for elements:
    \begin{enumerate}
        \item Characterize structure of an optimal solution.
        \item Define the value of an optimal solution recursively.
        \item Compute the value in a bottom-up fashion.
        \item Construct the optimal solution from computed info.
    \end{enumerate}
\end{frame}


\begin{frame}{Dynamic Programming vs Divide-and-Conquer}
    \emph{Commonality:} Both solve problems by combining solutions to subproblems.

    \emph{Divide-and-Conquer:}
    \begin{itemize}
      \item Disjoint subproblems
      \item Recursive solution
    \end{itemize}

    \emph{Dynamic Programming:}
    \begin{itemize}
      \item Overlapping subproblems
      \item Solve each subproblem once, store in table
    \end{itemize}
\end{frame}

\begin{frame}[c]{Richard E. Bellman}
    \begin{columns}[c]
    % Left column for the story
    \begin{column}{0.5\textwidth}
      In 1950, Richard E.~Bellman introduced \alert{dynamic programming} to mask the
      mathematical essence of his work. 

      \bigskip{}

      During an era when military sponsors
      were skeptical of mathematics, 
      he opted for a term that would be both uncriticizable and appealing.
    \end{column}
    
    % Right column for the picture and caption
    \begin{column}{0.5\textwidth}
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\columnwidth]{Richard-Ernest-Bellman.jpg}
            \caption{Richard E. Bellman (1920--1984)}
        \end{figure}
    \end{column}
  \end{columns}
\end{frame}
\section{\acs{ita} 15.1 Rod Cutting}

\begin{frame}[c]
    \frametitle{The Rod Cutting Problem}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Given:
            \begin{itemize}
                \item A rod of length \( n \).
                \item A table containing \(p_i\)'s for the
                    price of a piece of length \(i\).
            \end{itemize}

            Determine: 

            \begin{itemize}
                \item The maximum revenue \( r_n \) achievable by possibly cutting the rod
                into integral lengths and selling them.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-cut-rod.jpg}
                \caption{How to cut a rod to maximize revenue?}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example of Rod Cutting}
    
    Given the following price table:
    \begin{table}[h]
        \centering
        \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
            \hline
            $p_1$ & $p_2$ & $p_3$ & $p_4$ & $p_5$ & $p_6$ & $p_7$ & $p_8$ & $p_9$ & $p_{10}$ \\
            \hline
            1 & 5 & 8 & 9 & 10 & 17 & 17 & 20 & 24 & 30 \\
            \hline
        \end{tabular}
    \end{table}

    Let $r_n$ be the maximum profit achievable by cutting the rod of length $n$.

    \cake{} What are $r_1, \dots, r_4$?
\end{frame}

\begin{frame}
    \frametitle{A Recursion For the Maximum Profit}
    
    Can you see why we have
    \begin{equation*}
        r_n = \max_{1 \le i \le n} (p_i + r_{n-i}).
    \end{equation*}

    \vspace{4cm}

    \cake{} What is $r_5$ in the previous example given this formula?
\end{frame}

\begin{frame}
    \frametitle{A Naive Algorithm}
    
    A naive algorithm for computing $r_n$ is the following:
    \begin{algorithmic}[1]
        \Procedure{CUT-ROD}{$p, n$}
        \If{$n == 0$}
        \State \Return $0$
        \EndIf
        \State $q \gets -\infty$
        \For{$i = 1$ \textbf{to} $n$}
        \State $q \gets \max(q, p[i] + \textsc{CUT-ROD}(p, n - i))$
        \EndFor
        \State \Return $q$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{The Inefficiency}
    
    The naive algorithm is inefficient as many $r_n$'s must be computed more than once.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{rod-cut-r-4.png}
        \caption{The inefficiency in computing $r_4$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Time Complexity}
    
    Let $T(n)$ be the time complexity of the naive algorithm.
    Then
    \begin{equation*}
        T(n) = 
        \begin{cases}
            1 & \text{if } n = 0, \\
            1 + \sum_{i = 0}^{n-1} T(i) & \text{if } n > 0.
        \end{cases}
    \end{equation*}

    \pause{}

    \cake{} What are the values of $T(0), T(1), \dots, T(3)$?

    \vspace{1.5cm}

    \cake{} Can you guess a closed-form expression for $T(n)$?

    \vspace{1.5cm}

    \puzzle{} Prove your guess is correct.
\end{frame}

\begin{frame}
    \frametitle{Dynamic Programming}
    
    There are two ways in dynamic programming for improving a naive recursive
    algorithm:

    \only<1>{\emph{Top-down with memoization:} This method uses a recursive
        approach with a modification to save results of subproblems, typically
        in an array or hash table.}
    \only<2>{\emph{Bottom-up:} This approach sorts subproblems by their natural
        size. It ensures that smaller subproblems are solved first, such that
        each solution relies only on previously solved smaller subproblems.}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{rod-cut-r-4.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Rod Cutting with Memoization}
    
    \only<1>{
        \begin{algorithmic}[1]
            \Procedure{MEM-CUT-ROD}{$p, n$}
            \State \textbf{let} $r[0..n]$ \textbf{be} a new array
            \For{$i = 0$ \textbf{to} $n$}
            \State $r[i] = -\infty$
            \EndFor
            \State \Return \Call{MEM-CUT-ROD-AUX}{$p, n, r$}
            \EndProcedure
        \end{algorithmic}
    }
    \only<2>{
        \begin{algorithmic}[1]
            \Procedure{MEM-CUT-ROD-AUX}{$p, n, r$}
            \If{$r[n] \geq 0$}
            \State \Return $r[n]$
            \EndIf
            \If{$n == 0$}
            \State $q = 0$
            \Else
            \State $q = -\infty$
            \For{$i = 1$ \textbf{to} $n$}
            \State $q = \max(q, p[i] + \Call{MEM-CUT-ROD-AUX}{$p, n - i, r$})$
            \EndFor
            \EndIf
            \State $r[n] = q$
            \State \Return $q$
            \EndProcedure
        \end{algorithmic}
    }
\end{frame}

\begin{frame}
    \frametitle{Time Complexity}
    
    What is the time complexity of the memoization algorithm?
\end{frame}

\begin{frame}
    \frametitle{Rod Cutting with Bottom-Up}

    \begin{algorithmic}[1]
        \Procedure{BOTTOM-UP-CUT-ROD}{$p, n$}
        \State \textbf{let} $r[0..n]$ \textbf{be} a new array
        \State $r[0] \gets 0$
        \For{$j = 1$ \textbf{to} $n$}
        \State $q \gets -\infty$
        \For{$i = 1$ \textbf{to} $j$}
        \State $q \gets \max(q, p[i] + r[j - i])$
        \EndFor
        \State $r[j] \gets q$
        \EndFor
        \State \Return $r[n]$
        \EndProcedure
    \end{algorithmic}

    \cake{} What is the time complexity of this algorithm?
\end{frame}

\begin{frame}
    \frametitle{Time Complexity}
    
    What is the time complexity of the bottom-up algorithm?
\end{frame}

\begin{frame}
    \frametitle{Subproblem Graphs}
    
    When thinking about dynamic programming, we often use graphs to represent
    the dependency of subproblems.

    Typically, the time complexity of solving a subproblem is proportional to
    its out-degree.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            % Nodes
            \foreach \n in {0,...,4} {
                \node[circle, draw, minimum size=0.4cm] (node\n) at (0,\n) {\n};
            }

            % Arrows
            \foreach \source/\dest in {0/1, 1/2, 2/3, 3/4} {
                \draw[<-] (node\source) -- (node\dest);
            }

            % Curved Arrows
            \draw[->, bend right=60] (node4) to (node0);
            \draw[->, bend right=-45] (node4) to (node1);
            \draw[->, bend right=40] (node4) to (node2);
            \draw[->, bend right=-45] (node3) to (node0);
            \draw[->, bend right=40] (node3) to (node1);
            \draw[->, bend right=40] (node2) to (node0);
        \end{tikzpicture}
        \caption{Example: $r_4$ depends on $r_1, r_2, r_3$, etc.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Reconstructing a Solution}
    
    The solution of rod-cutting can be reconstructed as follows:

    \only<1>{
        \begin{algorithmic}[1]
            \Procedure{EXTENDED-BOTTOM-UP-CUT-ROD}{$p, n$}
            \State \textbf{let} $r[0..n]$ and $s[0..n]$ \textbf{be} new arrays
            \State $r[0] \gets 0$
            \For{$j = 1$ \textbf{to} $n$}
            \State $q \gets -\infty$
            \For{$i = 1$ \textbf{to} $j$}
            \State $q \gets \max(q, p[i] + r[j - i])$
            \State $s[j] \gets i$
            \EndFor
            \State $r[j] \gets q$
            \EndFor
            \State \Return $(r, s)$
            \EndProcedure
        \end{algorithmic}
    }
    \only<2>{
        \begin{algorithmic}[1]
            \Procedure{Print-Cut-Rod-Solution}{$p, n$}
            \State $(r, s) \gets \text{Extended-Bottom-Up-Cut-Rod}(p, n)$
            \While{$n > 0$}
            \State \text{print} $s[n]$
            \State $n \gets n - s[n]$
            \EndWhile
            \EndProcedure
        \end{algorithmic}
    }
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Given the following price table:
    \begin{table}[h]
        \centering
        \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
            \hline
            $p_1$ & $p_2$ & $p_3$ & $p_4$ & $p_5$ & $p_6$ & $p_7$ & $p_8$ & $p_9$ & $p_{10}$ \\
            \hline
            5 & 0 & 4 & 0 & -1 & 3 & 2 & 0 & 4 & 3 \\
            \hline
        \end{tabular}
    \end{table}

    \cake{} What are $r_1, \dots, r_4$ and $s_1, \dots, s_4$? 

    \cake{} What is the optimal way to cut a rod of length $4$?
\end{frame}

\begin{frame}{Variations of the Rod-Cutting Problem}
    How should we modify the algorithm,
    \only<1>{if each piece must have a minimum length \(l\)?}
    \only<2>{if each cut has a cost $c$?}
    \only<3>{if the price depends on the length of the rod?}
    \only<4>{if the number of pieces limited to at most $m$?}
    \begin{algorithmic}[1]
        \Procedure{BOTTOM-UP-CUT-ROD}{$p, n$}
        \State \textbf{let} $r[0..n]$ \textbf{be} a new array
        \State $r[0] \gets 0$
        \For{$j = 1$ \textbf{to} $n$}
        \State $q \gets -\infty$
        \For{$i = 1$ \textbf{to} $j$}
        \State $q \gets \max(q, p[i] + r[j - i])$
        \EndFor
        \State $r[j] \gets q$
        \EndFor
        \State \Return $r[n]$
        \EndProcedure
    \end{algorithmic}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \acs{ita}

            \acf{ita}
            \coursereading{}
            \begin{itemize}
                \item
                    Section 15.1.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    Exercises 15.1: 1--5.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
