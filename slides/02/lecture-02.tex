\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Examples of Algorithms Analysis}

\begin{document}

\maketitle

\lectureoutline{}

\section{Running Time --- Sorting Algorithms}

\subsection{Sorting Problems}

\begin{frame}
    \frametitle{Sorting problems}

    How to design an algorithm with

    \begin{itemize}
        \item Input: A array of $n$ numbers $\langle a_1, a_2, \ldots, a_n
            \rangle$.
        \item Output: A permutation (of the ordinal array) $\langle a_1', a_2',
            \ldots, a_n' \rangle$ such that
            \begin{equation*}
                a_1' \le a_2' \le \ldots \le a_n'.
            \end{equation*}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Instances}
    
    Such an input is called an \alert{instance} of the \alert{sorting problem}.

    In general, an \alert{instance}  of a problem consists of the input
    needed to compute a solution to the problem.

    The running time of an algorithm often depends on the instance.

    ``Good'' instances have short running times.

    ``Bad'' instances have long running times.
\end{frame}

\begin{frame}
    \frametitle{Average and Worst-Case analysis}

    \alert{Average analysis} focuses on the average running time.

    \alert{Worst-case analysis} focuses on the worst-case running time.

    \cake{} Which of these analysis is more ``practical''?
\end{frame}

\begin{frame}
    \frametitle{Keys and Records}

    The numbers to be sorted are called \alert{keys}, often paired with \alert{satellite data}.

    Together, they form a \alert{record}.

    For example, a spreadsheet containing student record with ID, name, age,
    GPA, etc., can be sorted using any column as \emph{key}.

    Each row in the spreadsheet is a \emph{record}.
\end{frame}

\subsection{Insertion Sort}

\begin{frame}
    \frametitle{Insertion sort}

    An efficient algorithm for sorting a small number of elements
    works the way you might sort a hand of playing cards.

    \only<1>{
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.6\textwidth]{sort-cards.jpg}
            \caption{Insertion sort}
        \end{figure}
    }
    \only<2>{
        \begin{enumerate}
            \item Start with an empty left hand and the cards face down.
            \item Remove one card at a time from the table and insert it
                into the correct position in the left hand.
            \item To find the correct position for a card, compare it with each of
                the cards already in the hand, from right to left.
        \end{enumerate}
    }
\end{frame}

\begin{frame}
    \frametitle{Pseudo code of insertion sort}

    Input: an array $A$ containing the numbers to be sorted.

    Output: the array $A$ with the numbers sorted.

    \begin{algorithmic}[1]
        \State Input: an array $A$ containing the numbers to be sorted.
        \State Output the array $A$ with the numbers sorted.
        \For{$i = 2$ to $\text{length}(A)$}
        \State $\text{key} \gets A[i]$
        \State $j \gets i - 1$
        \While{$j > 0$ and $A[j] > \text{key}$}
        \State $A[j + 1] \gets A[j]$
        \State $j \gets j - 1$
        \EndWhile
        \State $A[j + 1] \gets \text{key}$
        \EndFor
    \end{algorithmic}
\end{frame}

\begin{frame}
    \frametitle{Example of insertion sort}
    
    Let try out the insertion sort on $A = \langle 5, 2, 4, 6, 1, 3 \rangle$.
\end{frame}

\begin{frame}
    \frametitle{Why do we care?}

    Instead of banging our heads on the \emoji{keyboard} and trying to find better
    algorithm, why do not we just buy a better \emoji{desktop-computer}?

    \cake{} What do you think?
\end{frame}

\begin{frame}
    \frametitle{The efficiency of sorting algorithms}

    Different algorithms devised to solve the same problem often
    differ dramatically in their efficiency. For example, to sort $n$ numbers,

    \begin{itemize}
        \item \alert{Insertion Sort} takes time roughly $c_1 n^2$,
        \item \alert{Merge Sort} takes time roughly $c_2 n \log n$,
    \end{itemize}
    where $c_1$ and $c_2$ are constant that do not depend on $n$.

    \hint{} Insertion sort typically has a smaller constant factor than merge
    sort, so that $c_1 < c_2$.

    \cake{} Which algorithm is faster?
\end{frame}

\begin{frame}
    \frametitle{When $n$ is small}

    According to this
    \href{https://w3.cs.jmu.edu/spragunr/CS240/pas/sorting/sorting.shtml}{experiment},
    insertion sort is faster when $n$ is small.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{compare-sorting-algorithm.jpg}
        \caption{Comparison of sorting algorithms}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{When $n$ is large}
    Note that
    \begin{equation}
        \label{eq:insertion-sort}
        \lim_{n \to \infty} \frac{c_{1} n^{2}}{c_{2} n \log n} = \infty
    \end{equation}

    Thus, no matter what $c_1, c_2$ are, for any $M > 0$, there exists an
    $n_{0}$, such that
    \begin{equation*}
        \frac{c_{1} n^{2}}{c_{2} n \log n} > M
    \end{equation*}
    for all $n \ge n_0$.

    \puzzle{} Can you prove \eqref{eq:insertion-sort}?
\end{frame}

\begin{frame}
    \frametitle{A Thought Experiment}

    Let's dive deeper with a practical example:
    \begin{itemize}
        \item Consider \( n = 10^7 \).
        \item Insertion sort: \( 50 n \log(n) \) instructions.
        \item Merge sort: \( 2 n^2 \) instructions.
    \end{itemize}

    Execution environment:
    \begin{itemize}
        \item Insertion sort on a \emoji{laptop}: \( 10^7 \) instructions/sec.
        \item Merge sort on a \emoji{desktop-computer}: \( 10^{10} \) instructions/sec.
    \end{itemize}

    \cake{} Which algorithm will be faster?
\end{frame}

\begin{frame}
    \frametitle{Beyond performance}

    Besides running time, 
    there are many aspect of an algorithm that can be analyzed:

    \begin{itemize}
        \item \alert{Optimality}: Produces the optimal results.
        \item \alert{Correctness}: Produces expected results.
        \item \alert{Modularity}: Independent, manageable code units.
        \item \alert{Maintainability}: Easily modifiable code.
        \item \alert{Functionality}: Set of capabilities.
        \item \alert{Robustness}: Handles unexpected inputs gracefully.
        \item \alert{User-friendliness}: Intuitive for users.
        \item \alert{Programmer time}: Time needed for coding tasks.
        \item \alert{Simplicity}: Least complex solution.
        \item \alert{Extensibility}: Adaptable for new features.
        \item \alert{Reliability}: Consistent performance.
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Which Algorithm to choose}

    Among many sorting algorithms, deciding the best algorithm for a given application depends on:
    \begin{itemize}
        \item The number of items to be sorted.
        \item The extent of pre-sorting among items.
        \item Restrictions on item values.
        \item The computer's architecture.
        \item Storage devices: main memory, disks, or tapes.
        \item \dots (other factors as applicable).
    \end{itemize}
\end{frame}

\subsection{Radix sort}
    
\begin{frame}
    \frametitle{Example: Exams in Sweden}

    In \emoji{sweden}, exams are anonymously graded using a random $5$-digit ID
    for each student. 

    Fifty papers are sorted by IDs for efficient grade entry.

    \cake{} Which algorithm should you use? Merge sort or insertion
    sort?
\end{frame}

\begin{frame}
    \frametitle{Radix sort}

    \only<1>{
        The problem with both algorithm is that they both needs comparing $5$-digit
        numbers.

        It is easy for \emoji{computer} but hard for \teacher{}.
    }
    \only<2>{
        The best way to do this is thus to use \alert{radius sort}:
        \begin{itemize}
            \item A non-comparative sorting algorithm
            \item Sorts numbers by processing each digit at a time
            \item Efficient for sorting fixed-length numbers, like \(5\)-digit IDs.
        \end{itemize}
    }
    \only<3>{
        Let's see how it works by considering sorting the following $3$-digit
        IDs
        \begin{equation*}
            [170, 045, 075, 090, 002, 802, 002, 066]
        \end{equation*}
    }
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Try to sort the following $3$-digit IDs with radius sort.
    \begin{equation*}
        [530, 976, 297, 155, 266]
    \end{equation*}
\end{frame}

\section{Optimality --- Robot Tour Optimization}

\begin{frame}[c]
    \frametitle{Robot tour problem}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Robot arm with soldering iron needs to
            \begin{itemize}
                \item Order contact points for sequential soldering.
                \item Minimize travel distance for assembly.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \begin{center}
                    \includegraphics[width=\textwidth]{robot-arm-tour.jpg}
                \end{center}
                \caption{Robot arm tour}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Robot Tour Optimization Algorithm}

    How can we design an algorithm with
    \begin{itemize}
        \item Input: A set $S$ of $n$ points in the plane.
        \item Output: The shortest cycle that visits each point in $S$.
    \end{itemize}

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.3\textwidth]{robot-arm-tour-example-01.pdf}
        \includegraphics<2>[width=0.3\textwidth]{robot-arm-tour-example-02.pdf}
        \caption{Optimal robot arm tour}
    \end{figure}
    \only<2>{\cake{} Why are all the lines straight in the optimal solution?}
\end{frame}

\begin{frame}
    \frametitle{The Nearest Neighbour Algorithm}
    
    The nearest neighbour algorithm works as follows:
    \begin{enumerate}
        \item Start a some point $p_{0}$.
        \item Go to the nearest point $p_{1}$ which has not been visited.
        \item Repeat until finish.
    \end{enumerate}

    This works \only<1>{well}\only<2>{badly} for the following instance.
    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.8\textwidth]{robot-arm-tour-good.jpg}
        \includegraphics<2>[width=0.8\textwidth]{robot-arm-tour-bad.jpg}
        \caption{A \only<1>{good}\only<2>{bad} instance for the nearest
        neighbour algorithm}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Closest Pair Algorithm}
    
    This algorithm connects the closest pair of points, whose connection will
    not cause a cycle or a 3-way branch, until all points are in one tour.

    This gives the optimal solution to the previous problem.
\end{frame}

\begin{frame}
    \frametitle{When the Closest Pair Algorithm Fails}

    The closest pair algorithm fails in the following instance.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{robot-arm-tour-cp-bad.jpg}
        \caption{A bad instance for the closes pair algorithm}
    \end{figure}

    \cake{} How do you prove that the left tour is longer than the right one?
\end{frame}

\begin{frame}
    \frametitle{The brute force/exhaustive search algorithm}
    
    The brute force algorithm simply tries every possible tour
    and find the shortest one.

    This guarantees the optimal solution to the previous problem.

    But it is too slow for practical use.

    \cake{} If there are $n$ points, how many tour does the brute force
    algorithm need to check?
\end{frame}

\begin{frame}[c]
    \frametitle{Travelling Salesman Problem}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Imagine you need to visit multiple cities. 

            \bigskip{}

            The times it takes to travel from one city to another are given

            \bigskip{}

            How to find the quickest route to visit each city
            exactly once and then return to the starting point. 
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-travelling-salesman.jpg}
                \caption{Travelling salesman problem}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example of Travelling Salesman Problem}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{travelling-salesman-01.pdf}
        \caption{The shortest travelling salesman tour}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Travelling salesman problem --- formal description}
    
    This problem asks for the shortest possible route that visits each vertex
    exactly once and returns to the original vertex.
    
    \begin{itemize}
        \item Input: A graph \( G = (V, E) \) where \( V \) is the set of
            vertices (cities) and \( E \) is the set of edges with associated
            weights (distances).
        \item Output: The shortest possible cycle that visits each vertex in \(
            V \) exactly once and returns to the starting vertex.
    \end{itemize}
    
    \emoji{cry} The only known algorithm which guarantees the optimal solution is
    brute force.
\end{frame}

\section{Correctness --- Prime Test}

\begin{frame}
    \frametitle{The correctness of an algorithm}
    An algorithm is said to be \alert{correct}  if, for every input instance, it halts with
    the correct output.

    An \alert{incorrect}  algorithm might not halt on some input
    instances, or it might halt with an incorrect answer

    Incorrect algorithms can still be useful.
\end{frame}

\begin{frame}
    \frametitle{What Constitutes a ``Correct'' Output?}
    The definition of a ``correct'' output is context-dependent and may vary
    based on specific requirements or objectives.

    For instance, in the context of a map application, ``correctness'' could
    signify various outcomes:
    \begin{itemize}
        \item A route that minimizes travel time.
        \item A route that is fastest while also avoiding tolls.
        \item A route that covers the shortest physical distance.
        \item And so on \dots
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Incorrect Algorithms Can be Useful}

    The commonly used public key cryptography system RSA relies on determining
    whether a large number $n$ is prime.

    \cake{} How can we do this by brute force?

    \pause{}

    This is difficult to do efficiently.

    Instead RSA relies on a algorithm which may return incorrect results:
    \begin{itemize}
        \item If it declares $n$ is composite, then it is definitely true.
        \item If it declares $n$ is prime, then it is true most of the time.
    \end{itemize}

    \cool{} Works pretty well in practice.
\end{frame}

\begin{frame}
    \frametitle{Prime test in RSA}
    
    \only<1>{
        The algorithm in RSA which tests whether $n$ is prime is based on the following:

        \begin{block}{Fermat's little theorem}
            If $p$ is prime, then for every $1 ≤ a < p$,
            \begin{equation*}
                a^{p−1} ≡ 1 \pmod p.
            \end{equation*}
        \end{block}
    }
    \only<2>{
        The algorithm is as follows:

        \begin{algorithmic}[1]
            \State Input: Positive integer n
            \State Output: yes/no
            \State Pick a random positive integer $a < n$ at random
            \If{$a^{n−1} ≡ 1 \pmod n$}
                \State \Return yes
            \Else
                \State \Return no
            \EndIf
        \end{algorithmic}
    }
\end{frame}

\section{Granularity --- Integer Multiplication}

\begin{frame}
    \frametitle{Integer multiplication}
    \cake{} Do you remember how to compute $5678 \times 1234$ with
    \temoji{pencil}?

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{school-multiplication.png}
        \caption{Multiplication as in elementary school}
    \end{figure}

    \cake{}
    How many multiplications do we need to 
    compute $x \cdot y$ given two $n$-digit number $x$ and $y$?
\end{frame}

\begin{frame}
    \frametitle{Another way to computer $x \cdot y$}
    We can write
    \begin{equation*}
        x = x_{1} 10^{n/2} + x_{0},
        \qquad
        y = y_{1} 10^{n/2} + y_{0},
    \end{equation*}
    where $x_1, x_0, y_1, y_0$ are $n/2$-digit numbers.

    Then
    \begin{equation*}
        \begin{aligned}
        x y 
        &
        = 
        (x_{1} 10^{n/2} + x_{0})
        (y_{1} 10^{n/2} + y_{0})
        \\
        &
        = 
        x_1 y_1 10^{n}
        +
        (x_1 y_0 + x_0 y_1) 10^{n/2}
        +
        x_0 y_0
        \end{aligned}
    \end{equation*}

    \cake{} How many multiplications do we need to compute $x
    \cdot y$?
\end{frame}

\begin{frame}
    \frametitle{Karatsuba's observation}

    Karatsuba noticed that, to compute
    \begin{equation*}
        \begin{aligned}
        x y 
        &
        = 
        (x_{1} 10^{n/2} + x_{0})
        (y_{1} 10^{n/2} + y_{0})
        \\
        &
        = 
        x_1 y_1 10^{n}
        +
        (x_1 y_0 + x_0 y_1) 10^{n/2}
        +
        x_0 y_0
        \end{aligned}
    \end{equation*}
    we can use
    \begin{equation*}
        (x_1 y_0 + x_0 y_1)
        =
        (x_1 + x_0)(y_1 + y_0)
        -
        x_1 y_1
        -
        x_0 y_0
        .
    \end{equation*}

    \cake{} How many multiplications do we need to compute $x
    \cdot y$?
\end{frame}

\begin{frame}
    \frametitle{Karatsuba's algorithm}

    To compute $x \cdot y$, we use
    \begin{equation*}
        x y 
        = 
        x_1 y_1 10^{n}
        +
        (x_1 y_0 + x_0 y_1) 10^{n/2}
        +
        x_0 y_0
    \end{equation*}
    and
    \begin{equation*}
        (x_1 y_0 + x_0 y_1)
        =
        (x_1 + x_0)(y_1 + y_0)
        -
        x_1 y_1
        -
        x_0 y_0
        .
    \end{equation*}
    This involves three multiplications of two $n/2$-digits numbers:
    \begin{itemize}
        \item $x_1 y_1$
        \item $x_0 y_0$
        \item $(x_1 + x_0)(y_1 + y_0)$
    \end{itemize}
    We use the same method to compute them.

    Later we will see, the total number multiplications needed is
    \begin{equation*}
        \Theta(n^{\log_2 3})
    \end{equation*}
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{\courseassignment{}}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \acf{ita}
            \begin{itemize}
                \item[\emoji{pencil}] 
                    Exercises 2.1: 1--4.
                \item[\emoji{pencil}] 
                    Exercises 2.2: 1--2, 4.
                \item[\emoji{pencil}] 
                    Exercises 2.3: 1--6.
                \item[\emoji{computer}] 
                    Problem 2-1.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}
\end{document}
