
import sys

def matrix_chain_order(p):
    n = len(p) - 1
    m = [[0 for _ in range(n)] for _ in range(n)]
    s = [[0 for _ in range(n)] for _ in range(n)]

    for l in range(2, n + 1):
        for i in range(n - l + 1):
            j = i + l - 1
            m[i][j] = float('inf')
            for k in range(i, j):
                q = m[i][k] + m[k + 1][j] + p[i] * p[k + 1] * p[j + 1]
                if q < m[i][j]:
                    m[i][j] = q
                    s[i][j] = k
    return m, s

def print_optimal_parens(s, i, j):
    if i == j:
        print(f"A{i+1}", end='')
    else:
        print("(", end='')
        print_optimal_parens(s, i, s[i][j])
        print_optimal_parens(s, s[i][j] + 1, j)
        print(")", end='')

def main():
    if len(sys.argv) != 2:
        print("Usage: python optimal_chain.py '5, 10, 3, 12, 5, 50, 6'")
        sys.exit(1)

    p = list(map(int, sys.argv[1].split(',')))
    n = len(p) - 1  # Define n within the scope of the main function
    m, s = matrix_chain_order(p)
    print("The minimum number of multiplications is:", m[0][n-1])
    print("The optimal parenthesization is: ", end='')
    print_optimal_parens(s, 0, n-1)
    print("\n")

if __name__ == "__main__":
    main()
