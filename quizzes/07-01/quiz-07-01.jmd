---
title: COMPSCI 308 Design and Analysis of Algorithms
subtitle: Quiz 07 (01)
author: "Instructor Xing Shi Cai"
date: 2023-12-07
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using StatsBase
using SymPy
using WeaveHelper
using Random

```

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{eye}}$ Don't Cheat}]
During the quiz/exam do \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult anything other than one A4-sized double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} \Large{} $\text{\emoji{zap}}$ Follow Rules}]
\begin{itemize}
    \item[\emoji{student}] Enter your \textbf{Exam ID} (do not write your name) in the designated space below.
    \item[\emoji{ab}] Support your proofs with both equations and descriptive reasoning.
    \item[\emoji{pencil}] Use only pen for writing as pencil marks are not clearly visible.
    \item[\emoji{question}] Restrict your inquiries to clarifications on typos or English vocabulary only.
\end{itemize}
\end{tcolorbox}

\begin{center}
    Exam ID: $`j print_empty_box(5.7, 1.5)`$
\end{center}

![Good Luck](./bunny.jpg){width=240px}

\pagebreak{}

# (1pt)

A \textbf{Hamiltonian path} in a graph is a simple path that visits every vertex
exactly once. Show that the language 
\begin{equation*}
\text{Ham-Path} = \{\langle G, u, v \rangle : \text{there is a Hamiltonian path from $u$ 
to $v$ in graph $G$\}}
\end{equation*}
belongs to \textsc{NP}.

:bulb: Explain what is a certificate in this problem
and why verifying the certificate takes only polynomial time.

```julia

answer = L"""
A certificate in this case is a list of vertices $P = (v_1, v_2, \ldots, v_k)$.

To show that the language $\text{Ham-Path}$ belongs to NP,
we need to check if $P$ forms a Hamiltonian path from $u$ to $v$ in polynomial time.

Assume that we use adjacent lists to represent the graph.
Let $n$ be the number of vertices in $G$
and $m$ be the number of edges in $G$.
Given $P$, the verification algorithm needs to check:

1. If $n = k$, i.e., if $P$ contains $n$ vertices, which takes $O(1)$ time;
2. If $u = v_1$ and $v_k = v$, which takes $O(1)$ time;
3. If for any $i \in \{1, \dots, k-1\}$, if $(v_i, v_{i+1})$ is an edge,
which takes at most $O(mn$) time, since we are using adjacent lists;
4. If there are no duplicate vertices in $P$, which can be done by a double loop
   that take $O(n^2)$ time.
Thus the verification algorithm takes $O(n^2)$ time in the worst case.
"""

long_answer(answer, vfill=true)
```
`j quiz_pagebreak()`


# (1pt)

Show the following problem is NP-Complete.

MAX-SAT : Given a CNF formula and an integer $g$, 
find a truth assignment that satisfies at least $g$ clauses.

:bulb: First argue that why it is in NP. 
Then show that $\text{3-CNF-SAT} \leq_p \text{MAX-SAT}$,
i.e., we can reduce in polynomial time a problem of 3-CNF-SAT to 
a problem in MAX-SAT.

```julia

answer = L"""
\textbf{MAX-SAT in NP}

Given a CNF formula and an integer \( g \), 
a certificate for the MAX-SAT problem
is a truth assignment to the variables.
Assume that there are $n$ varaibles and $m$ clause.
It take $O(n)$ time to evaluate a clause under the truth assignment.
Thus, it takes $O(n m)$ time to evaluate all the clauses.
If we know the output of each clause given the truth assignment,
then we know if there are at least $g$ clauses satisfied.

\textbf{MAX-SAT is NP-Hard}

Given 3-CNF-SAT instance with CNF formula \( \phi \) and \( n \) clauses, 
consider the pair \( (\phi,n) \).
If a truth assignment 
satisfises all clauses of $\phi$,
i.e., $\phi \in \text{3-CNF-SAT}$,
then $(\phi,n) \in \text{MAX-SAT}$.
If a truth assignment 
satisfises at least $n$ clauses in $\phi$,
(which means it satisfises all the $n$ clauses in $\phi$),
i.e., $(\phi,n) \in \text{MAX-SAT}$.
then $\phi \in \text{3-CNF-SAT}$.
In other words, 
we have
$\phi \in \text{3-CNF-SAT}$ if and only if $(\phi, n) \in \text{MAX-SAT}$.
Since it takes only $O(1)$ time to compute $(\phi, n)$ from $\phi$ (extracting
the number of clauses), we have polynomial time reduction from 3-CNF-SAT to MAX-SAT.
"""

long_answer(answer, vfill=true)
```
`j quiz_pagebreak()`

# (1pt)

Convert $\phi = x \leftrightarrow (y \lor z)$ to 3-CNF.

:bulb: Steps:

1. Write down the truth table for $\phi$.
2. Find a formula equivalent to $\neg \phi$ using the truth table.
3. Convert $\phi = \neg \neg \phi$ to CNF DeMorgan's laws.

```julia
short_answer(
    L"x \leftrightarrow (y \lor z)", 
    L"(\neg x\lor y\lor z)\land (x\lor \neg y\lor \neg z)\land (x\lor \neg y\lor z)\land (x\lor y\lor \neg z)", 
    width=12,
    height=3)
```
