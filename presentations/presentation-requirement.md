---
title: Presentation Requirements for COMPSCI 308
author: Xing Shi Cai
date: 2023-11-02
geometry:
- margin=1.5in
...

# The Presentation

In this course, you'll deliver a 15-minute presentation on an algorithm topic to enhance your scientific communication skills.

## Schedule

Presentations will occur on Fridays of weeks 5-7.

Sign up for a time slot on Canvas using this [guide](https://community.canvaslms.com/t5/Student-Guide/How-do-I-sign-up-for-a-Scheduler-appointment-in-the-Calendar/ta-p/536) before 11:59 PM on Sunday of week 2 to avoid automatic assignment.

## Attendance

Attendance is mandatory only during the week of your presentation. Attending other presentations is encouraged for broader learning.

## Format

Opt for either a slide-show or whiteboard presentation.

# The Rehearsal

Rehearsal is key. 
Participate as both a speaker and an audience member *before* 12:00 am of the day your scheduled to present.
The audience for each rehearsal will be given by the instructor.

## Grading

The rehearsal will constitute 20% of your presentation grade.

## Proof

Submit a Zoom video recording of your rehearsal and a photo of the completed feedback form.

# Topic

Your chosen topic should:

1. Be within the scope of algorithm design and analysis.
2. Not be covered by our course textbook.
3. Be suitably complex for your peers.

## Finding Topics:

Explore the following for topic ideas:

* [Quanta Magazine](https://www.quantamagazine.org/)
* [Hacker News](https://news.ycombinator.com/)
* Reference texts such as "Algorithm Design" by Kleinberg & Tardos, or "Algorithms" by Dasgupta, Papadimitriou, & Vazirani, or Sedgewick & Wayne.

If uncertain, seek guidance from the instructor.

## Announcing Topics:
After selecting a topic, comment below to inform fellow students and prevent duplication.

# Exemplary Talks

Inspire your presentation by viewing [past student presentations](https://duke.box.com/s/lousc9o14adugoin2yu1jgjc2kye8wk6).

# Presentation Tips

- Engage your audience with a compelling topic.
- Prioritize clarity over technical complexity.
- Emphasize the significance of your topic.
- Ensure there's a takeaway for all attendees.
- Rehearse with a peer at least once.
- Be prepared for and open to interruptions.
- Have additional content ready but adhere to your time limit.
- Be prepared to concisely wrap up if time runs out.

\pagebreak{}

# Rubrics

## Explanation (20%)
- **Excellent**: Precise, easily understood, and well-motivated.
- **Good**: Mostly clear with good motivation.
- **Acceptable**: Clear but may lack depth or motivation.
- **Needs Improvement**: Difficult to follow and weakly motivated.

## Difficulty (10%)
- **Excellent**: Advanced topics are comprehensible.
- **Good**: Topics are appropriate but could be slightly more/less complex.
- **Acceptable**: Moderately too simple or advanced.
- **Needs Improvement**: Topics are too simple or too complex for the audience.

## Slides (10%)
- **Excellent**: Well-organized, visually appealing, error-free.
- **Good**: Mostly well-organized, minor errors.
- **Acceptable**: Some organizational issues, some errors present.
- **Needs Improvement**: Disorganized and contains numerous errors.

## Creativity (10%)
- **Excellent**: Highly engaging, creative, includes questions and humour.
- **Good**: Shows some creativity and humour.
- **Acceptable**: Lacks creativity or humour.
- **Needs Improvement**: Unengaging, lacking in creativity and humour.

## Response to Feedback (10%)
- **Excellent**: Handles feedback with confidence and shows deep understanding.
- **Good**: Responds well to most questions.
- **Acceptable**: Addresses some questions but may struggle.
- **Needs Improvement**: Has difficulty responding to questions.

## Time Control (10%)
- **Excellent**: Adheres perfectly to the 15-minute limit.
- **Good**: Slightly exceeds or is under the time limit.
- **Acceptable**: Noticeably shorter than the allotted time.
- **Needs Improvement**: Significantly deviates from the time limit.

## English Fluency (10%)
- **Excellent**: Articulate and fluent.
- **Good**: Effective articulation and fluency.
- **Acceptable**: Basic proficiency shown.
- **Needs Improvement**: Struggles with fluency and articulation. In particular
  reading from a script.

## Feedback Rehearsal (10%)
- **Excellent**: Provides a detailed feedback form.
- **Good**: Provides feedback form with most sections complete.
- **Acceptable**: Omits some sections on the feedback form.
- **Needs Improvement**: Provides a very simple feedback form.

## Rehearsal Proof (10%)
- **Excellent (5 points)**: Provides a complete rehearsal video that demonstrates thorough preparation.
- **Good (4 points)**: Provides a rehearsal video with minor areas for improvement.
- **Acceptable (3 points)**: Provides a rehearsal video, but with noticeable omissions or lack of practice.
- **Needs Improvement (2 points)**: Video provided, but rehearsal is clearly inadequate or poorly demonstrated.
