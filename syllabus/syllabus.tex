%! TEX program = lualatex
\documentclass[11pt]{article}

% Misc packages
\usepackage{ifthen}
\usepackage{xspace}
\usepackage{ifpdf}
\usepackage{framed}
\usepackage{amsmath}
\usepackage{amssymb}

% Colours
\PassOptionsToPackage{dvipsnames,svgnames,x11names,table}{xcolor}
\usepackage{xcolor}
\definecolor{dku-blue}{HTML}{003A81}
\definecolor{dku-green}{HTML}{006F3F}

% Fonts
\usepackage[no-math]{fontspec}
%\usepackage{xunicode}
\usepackage{xltxtra}
\defaultfontfeatures{Mapping=tex-text,Numbers=Lining}
% \newfontfamily\cnfont{Hiragino Sans GB}
\AtBeginDocument{%
    \setmainfont{Carlito}%
    \setsansfont[
        BoldFont={Carlito}]{Carlito}%
    \raggedbottom%
}
% \usepackage[sf]{noto}

% Multi-language support
\usepackage{polyglossia}
\setmainlanguage{english}

% Links
\usepackage{url}
\renewcommand{\UrlBreaks}{\do\.\do\@\do\\\do\/\do\!\do\_\do\|\do\;\do\>\do\]%
    \do\)\do\,\do\?\do\'\do+\do\=\do\#\do\-}
\renewcommand{\UrlFont}{\normalfont}
\usepackage{hyperref} % Required for adding links	and customizing them
\hypersetup{colorlinks, breaklinks, urlcolor=Maroon, linkcolor=Maroon,
    citecolor=Green} % Set link colors
\hypersetup{%
    pdfauthor=Xing Shi Cai,%
    pdftitle=COMPSCI 308 Design and Analysis of Algorithms}

% Geometry
\usepackage[a4paper,
    includehead,tmargin=2cm,nohead,
    hmargin=2cm,
    includefoot,foot=1.5cm,bmargin=2cm]{geometry}

% Tabular
\usepackage{tabularx}
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{array}
\newcommand\gvrule{\color{lightgray}\vrule width 0.5pt}

% From pandoc
\renewcommand{\tabularxcolumn}[1]{m{#1}}
\usepackage{calc} % for calculating minipage widths
% Correct order of tables after \paragraph or \subparagraph
\usepackage{etoolbox}
\makeatletter
\patchcmd\longtable{\par}{\if@noskipsec\mbox{}\fi\par}{}{}
\makeatother
% Allow footnotes in longtable head/foot
\IfFileExists{footnotehyper.sty}{\usepackage{footnotehyper}}{\usepackage{footnote}}
\makesavenoteenv{longtable}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
    \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
    \usepackage{selnolig}  % disable illegal ligatures
\fi

% For including pictures from current directory
\graphicspath{.}

% Misc
\usepackage{enumitem}
\usepackage{parskip}
\usepackage{hanging}

\newcommand{\push}{\hangpara{2em}{1}}

% Section titles
\usepackage{titlesec}
\titleformat{\section}{\raggedright\large\bfseries\color{dku-blue}}{}{0em}{}[{\titlerule[\heavyrulewidth]}]
\titleformat{\subsection}{\raggedright\itshape\color{dku-blue}}{}{0em}{}

% Emoji
\usepackage{emoji}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{tabularx}{\textwidth}{@{}Xm{6cm}}
    \arrayrulecolor{dku-blue}\toprule\vspace{6pt}
    {\bfseries\color{dku-blue}
        {\Large COMPSCI 308} \newline\newline
        {\Large Design and Analysis of Algorithms} \newline\newline
        {\Large Fall 2023, Session 2}}
     & \includegraphics[width=6cm]{dku-logo.pdf} \\
    \bottomrule
\end{tabularx}

\vspace{1em}

\textcolor{dku-blue}{\rule{\textwidth}{1pt}}

{
    \setlength{\parskip}{0.2em}
    \textbf{Class meeting time:}
    See DKU Hub.

    \textbf{Academic credit:} 4 DKU credits.

    \textbf{Course format:}
    Lectures.

    \textbf{Asynchronous study:}
    For students who cannot attend the lectures in person or remotely, slides and video
    recordings will be provided.

    \textbf{Office hour:}
    Two one-hour in-person sessions each week.
    Exact time to be announced via ED.

    \textbf{Last update:} \today{}
}

\vspace{-1em}\textcolor{dku-blue}{\rule{\textwidth}{1pt}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Instructor's information}

Dr.\ \href{https://newptcai.gitlab.io/}{Xing Shi Cai}
Assistant Professor of Mathematics, Duke Kunshan University

Dr.\ Cai received his PhD in computer science at McGill University in Canada.
After that he worked as postdoc researcher at Uppsala University in Sweden.
His main research interest is in applying probability theory in the analysis of
algorithms and complex networks.

Website: \url{https://newptcai.gitlab.io/teaching/}

Email: \href{mailto:xingshi.cai@dukekunshan.edu.cn}{xingshi.cai@dukekunshan.edu.cn}

\section*{What is this course about?}

An \textbf{algorithm} is any well-defined computational procedure that takes some value,
or set of values, as input and produces some value, or set of values, as output.
An algorithm is thus a sequence of computational steps that transform the input
into the output. An algorithm can also be viewed as a tool for solving a
well-specified computational problem. The statement of the problem specifies in
general terms the desired input/output relationship. The algorithm describes a
specific computational procedure for achieving that input/output relationship.

After having a target problem, the next step is to design an algorithm that can
address the target problem. \textbf{Algorithm design} is a coherent discipline-one needs
a specific set of concepts to define, a computational problem and a specific set
of tools to design an optimal algorithm to solve it. Designing the right
algorithm for a given application is a major creative act—that of taking a
problem and pulling a solution out of the ether. The space of choices you can
make in algorithm design is enormous, leaving you plenty of freedom.

Solely designing algorithms may not be a proper approach to solve a specific
problem. Understanding their behavioural characteristics together with their
strengths and weaknesses is critical. \textbf{Algorithm analysis} is investigated for
this purpose, in particular, for determining how much of a resource, such as
time or memory, an algorithm uses as a function of some characteristic of the
input to the algorithm, usually the size of the input.

This course will touch all the major algorithm design and analysis steps while
taking data structures into account. All the relevant concepts will be
exemplified through existing algorithms and problems besides implementing
algorithms in Python. To be specific, the design and analysis of efficient
algorithms including sorting, searching, dynamic programming, graph algorithms,
nondeterministic algorithms and computationally hard problems and other related
topics will be studied. 

This course will be carried out in line with the DKU's animating principles. In
particular, Collaborative Problem Solving, Research and Practice and Lucid
Communication will be directly involved with this course while touching the
Independence and Creativity aspect. The course will be primarily executed
through in-class quick quizzes, in-class individual / group discussions and
weekly assignments.

\section*{What background knowledge do I need before taking this course?}

COMPSCI 308 has two prerequisites:
\begin{itemize}
    \item COMPSCI 201: Introduction to Programming and Data Structures
    \item COMPSCI 203: Discrete Math for Computer Science or MATH 205/206:
        Probability and Statistics 
\end{itemize}
one anti-requisite:
\begin{itemize}
    \item COMPSCI 301: Algorithms and Databases
\end{itemize}
\section*{What will I learn in this course?}

By the end of this course, you will be able to:

\begin{enumerate}
    \item design efficient and effective algorithms of varying types
    \item implement algorithms in consideration of the problem requirements and computational resources
    \item utilize appropriate data structures while developing algorithms
    \item introduce new algorithmic solutions for new problems
    \item evaluate the theoretical boundaries of given algorithms
    \item analyze the behaviour and performance of given algorithms, referring to their strengths and weaknesses
    \item identify the resemblance between different problems, leading to problem hardness analysis
\end{enumerate}


\section*{What will I do in this course?}

\subsection*{Lectures}

Lectures will feature in-depth discussions on the day's topic. Prior to each
lecture, videos covering essential information will be made
available. You are expected to review these materials beforehand.

\subsection*{Readiness Assurance Quizzes}

At the start of each class, a brief quiz will assess your comprehension of the
day's topic, based on pre-recorded videos.

\subsection*{Recommended Exercises}

Though no assignments are required, each lecture will conclude with a set of
recommended exercises for further practice.

\subsection*{Weekly Quizzes}

Each week will culminate in a quiz, featuring questions aligned with the
recommended exercises.

\subsection*{Presentation}

You will be assigned a topic in algorithm design for a 15-minute presentation.

\subsection*{Final Exam}

A final exam will be administered in Week 8.

\subsection*{Weekly Essay}

Each week, write a one-page essay focusing on key knowledge, challenges, and
suggestions for course improvement.

\subsection*{In-Class and Online Discussions}

Engaging in in-class and online discussions will contribute to a small portion
of your final grade.

\section*{How can I prepare for the class sessions to be successful?}

Before each lecture / synchronous session, read or overview the assigned reading
in the text besides watching the recorded video explaining the basics of the
corresponding topic.

During each class, think and participate actively. Ask questions whenever you
have any. NOTE: your participation is highly important for the success of the
class and you should attend the class whenever possible.

After each class, review the notes, assigned reading, and recorded zoom videos
if necessary. Make sure you understand both the intuition and the detailed
techniques. Ask questions whenever you have any. Finish your homework and weekly
journal in time.

Before each quiz, review the materials and the homework. Ask questions whenever
you have any. 

Benefit from the office hours. You are always welcome whether you have a
question, need some help, or want to share an interesting idea with me regarding
the course content.

\section*{What required texts, materials, and equipment will I need?}

Introduction to Algorithms (3rd Ed.) by Thomas H.~Cormen, Charles E.~Leiserson,
Ronald L.~Rivest and Clifford Stein,  MIT Press, 2009
(\url{https://mitpress.mit.edu/books/introduction-algorithms-third-edition})

\section*{What optional texts or resources might be helpful?}

There are many useful textbooks or resources to benefit from.
\begin{itemize}
\item Algorithm Design (1st Ed.) by John Kleinberg and Eva Tardos,  Pearson -
    Addison Wesley, 2005
    (\url{https://www.pearson.com/us/higher-education/program/Kleinberg-Algorithm-Design/PGM319216.html})
\item Algorithms (1st Ed) by Sanjoy Dasgupta, Christos Papadimitriou and Umesh
    Vazirani, McGraw-Hill, 2006
    (\url{https://www.mheducation.com/highered/product/algorithms-dasgupta-papadimitriou/M9780073523408.html})
\item Algorithms (4th Ed) by Robert Sedgewick, Kevin Wayne, Addison-Wesley, 2011
    (\url{https://algs4.cs.princeton.edu/})
\item Algorithms Illuminated (Part 1): The Basics by Tim Roughgarden,
    Soundlikeyourself Publ, 2017 (\url{http://www.algorithmsilluminated.org/})
\item Algorithms Illuminated (Part 2): Graph Algorithms and Data Structures by
    Tim Roughgarden, Soundlikeyourself Publ, 2018
    (\url{http://www.algorithmsilluminated.org/})
\item Algorithms Illuminated (Part 3): Greedy Algorithms and Dynamic Programming
    by Tim Roughgarden, Soundlikeyourself Publ, 2019
    (\url{http://www.algorithmsilluminated.org/})
\item Algorithms Illuminated (Part 4): Algorithms for NP-Hard Problems by Tim
    Roughgarden, Soundlikeyourself Publ, 2020
    (\url{http://www.algorithmsilluminated.org/})
\item Data Structures and Algorithms in Python (1st Ed) by Michael T. Goodrich,
    Roberto Tamassia, Michael H. Goldwasser, Wiley, 2013
    (\url{https://www.wiley.com/en-us/Data+Structures+and+Algorithms+in+Python-p-9781118290279})
\item Problem Solving with Algorithms and Data Structures using Python (2nd Ed)
    by Brad Miller and David Ranum, Franklin, Beedle \& Associates, 2011
    (\emph{Free Book}:
    \url{https://runestone.academy/runestone/books/published/pythonds/index.html})
\end{itemize}

Additional resources will be provided on Ed (\url{https://edstem.org}) as the
course progresses.

\section*{How will my grade be determined?}

Course grades will be assigned according to a standard 10-pt scale:


\begin{longtable}[h]{@{}llll@{}}
\toprule
Grades & Percentage & C+ & [77\%, 80\%) \\
\midrule
\endhead
A+ & [98\%, 100\%] & C & [73\%, 77\%) \\
A & [93\%, 98\%) & C- & [70\%, 73\%) \\
A- & [90\%, 93\%) & D+ & [67\%, 70\%) \\
B+ & [87\%, 90\%) & D & [63\%, 67\%) \\
B & [83\%, 87\%) & D- & [60\%, 63\%) \\
B- & [80\%, 83\%) & F & [0\%, 60\%) \\
\bottomrule
\end{longtable}

\emoji{bomb} Points are rounded downwards, e.g., 92.99 will become to 92 (A-).
Petitions for increasing grades will \emph{not} get replies.
Regrade requests will be \emph{rejected} unless the grader misread an answer.

The course grade will be based on:

\begin{itemize}
    \item Weekly Quizzes: 50\%
    \item Final Exam: 25\%
    \item Presentation: 10\%
    \item Readiness Assurance Quizzes: 5\%
    \item Weekly Essays: 5\%
    \item In-class Discussion: 2.5\%
    \item Online (ED) Discussion: 2.5\%
\end{itemize}

You will be given the chance to retake any quiz in the subsequent week.

\section*{What are the course policies?}

\subsection*{Communications}

Questions should be posted on
\href{https://go.canvas.duke.edu/}{Ed}.
Emails will most likely \emph{not} get replied.

\subsection*{Discussion Guidelines}

Civility is an essential ingredient for academic discourse. All communications
for this course should be conducted constructively, civilly, and respectfully.
Differences in beliefs, opinions, and approaches are to be expected. Please
bring any communications you believe to be in violation of this class policy to
the attention of your instructor. Active interaction with peers and your
instructor is essential to success in this course, paying particular attention
to the following: 

\begin{itemize}
\item Be respectful of others and their opinions, valuing diversity in
    backgrounds, abilities, and experiences.  
\item Challenging the ideas held by others is an integral aspect of critical
    thinking and the academic process. Please word your responses carefully, and
    recognize that others are expected to challenge your ideas. A positive
    atmosphere of healthy debate is encouraged.  
\item Read your online discussion posts carefully before submitting them. 
\end{itemize}

\subsection*{Remote learning}

If you are not able to come to classes or exams in-person, you will need
to send the instructor
\begin{itemize}
    \item the permission from the Dean of Undergraduate Studies,
    \item or proof of other special circumstances, such as illness.
\end{itemize}
Failing to do so will result in falling the class.

\subsection*{Exams and Quizzes}

All quizzes and the exams will be closed book.
But you are allowed to bring one A4-sized double-sided cheat sheet.
Books, collaborations, calculators, the Internet,
and other aids are \emph{not} allowed during exams.
Cheating for the first time will result in getting zero points.
Repeated infractions will cause failing the course.

The final exam must be taken during the assigned time.
Make-up final exam (if approved) will take place at a certain time in the following
semester/session arranged by Dean's office and the instructor.

\subsection*{Academic Integrity}

As a student, you should abide by the academic honesty standard of the
Duke Kunshan University. Its Community Standard states: ``Duke Kunshan
University is a community comprised of individuals from diverse cultures
and backgrounds. We are dedicated to scholarship, leadership, and
service and to the principles of honesty, fairness, respect, and
accountability. Members of this community commit to reflecting upon and
upholding these principles in all academic and non-academic endeavors,
and to protecting and promoting a culture of integrity and trust.'' For
all graded work, students should pledge that they have neither given nor
received any unacknowledged aid.

\subsection*{Academic Policy \& Procedures}
You are responsible for knowing and adhering to academic policy and
procedures as published in University Bulletin and Student Handbook.
Please note, an incident of behavioral infraction or academic dishonesty
(cheating on a test, plagiarizing, etc.) will result in immediate action
from me, in consultation with university administration (e.g., Dean of
Undergraduate Studies, Student Conduct, Academic Advising). Please visit
the Undergraduate Studies website for additional guidance related to
academic policy and procedures. Academic integrity is everyone's
responsibility.

Please refer to \href{https://undergrad.dukekunshan.edu.cn/en/undergraduate-bulletin}{Undergraduate Bulletin} and visit the
\href{https://dukekunshan.edu.cn/en/academics/advising}{Office of Undergraduate Advising website} for DKU course
policies and guidelines.


\subsection*{Academic Disruptive Behavior and Community Standard}
Please avoid all forms of disruptive behavior, including but not limited
to: verbal or physical threats, repeated obscenities, unreasonable
interference with class discussion, making/receiving personal phone
calls, text messages or pages during class, excessive tardiness, leaving
and entering class frequently without notice of illness or other
extenuating circumstances, and persisting in disruptive personal
conversations with other class members. Please turn off phones, pagers,
etc.\ during class unless instructed otherwise. Laptop computers may be
used for class activities allowed by the instructor during synchronous
sessions. If you choose not to adhere to these standards, I will take
action in consultation with university administration (e.g., Dean of
Undergraduate Studies, Student Conduct, Academic Advising).

\subsection*{Academic Accommodations}
If you need to request accommodation for a disability, you need a signed
accommodation plan from Campus Health Services, and you need to provide
a copy of that plan to me. Visit the Office of Student Affairs website
for additional information and instruction related to accommodations.

\section*{What campus resources can help me during this course?}

\subsection*{Academic Advising and Student Support}
Please consult with me about appropriate course preparation and
readiness strategies, as needed. Consult your academic advisors on
course performance (i.e., poor grades) and academic decisions (e.g.,
course changes, incompletes, withdrawals) to ensure you stay on track
with degree and graduation requirements. In addition to advisors, staff
in the Academic Resource Center can provide recommendations on academic
success strategies (e.g., tutoring, coaching, student learning
preferences). All ARC services will continue to be provided online.
Please visit the
\href{https://dukekunshan.edu.cn/en/academics/advising}{Office
of Undergraduate Advising website} for additional information related
to academic advising and student support services.

\subsection*{Writing and Language Studio}
For additional help with academic writing---and more generally with
language learning---you are welcome to make an appointment with the
Writing and Language Studio (WLS). To accommodate students who are
learning remotely as well as those who are on campus, writing and
language coaching appointments are available in person and online. You
can register for an account, make an appointment, and learn more about
WLS services, policies, and events on the
\href{https://dukekunshan.edu.cn/en/academics/language-and-culture-center/writing-and-language-studio}{WLS
website}. You can also find writing and language learning resources on
the \href{https://sakai.duke.edu/x/mQ6xqG}{Writing \&
Language Studio Sakai site}.

\subsection*{Online resources}
The authors of the textbook Applied Combinatorics have made some lecture
videos and slides available online
\href{https://sites.gatech.edu/math3012openresources/}{here}.

You may find the following websites helpful not only for this course:

\begin{itemize}
\item
  \href{https://math.stackexchange.com/}{Math Stack
  Exchange} --- Ask questions about mathematics and get answers from
  other users.
\item
  \href{https://mathworld.wolfram.com/}{Wolfram Mathworld}
  --- A good place to look up mathematical definitions.
\end{itemize}

\subsection*{What is the expected course schedule?}

\emoji{bomb} Exact topics covered in each week may subject to change.

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.20}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.80}}@{}}
\toprule
\endhead
Week 1 & 
- Introduction to Algorithms

- Asymptotic Notations
\\
\midrule
Week 2 & - Divide and Conquer

- Dynamic Programming
\\
\midrule
Week 3 &
- Greedy Algorithms

- Elementary Graph Algorithms
Recitation (+Lab): TBA
\\
\midrule
Week 4 & 
- Minimum Spanning Trees

- Shortest Path Algorithms
\\
\midrule
Week 5 & 
- Linear Programming

- Duality

- Maximum Flow
\\
\midrule
Week 6 & 
- Randomized Algorithms

- Hash Tables
\\
\midrule
Week 7 &
- P and NP

- NP-Completeness

- Approximation Algorithms
\\
\bottomrule
\end{longtable}

\end{document}
